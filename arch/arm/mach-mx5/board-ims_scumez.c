/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/slab.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/i2c.h>
#include <linux/i2c/qt602240_ts.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/pwm_backlight.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/keypad.h>
#include <asm/mach/flash.h>
#include <mach/gpio.h>
#include <mach/mmc.h>
#include <mach/mxc_dvfs.h>
#include <mach/mxc_edid.h>
#include <mach/iomux-SCUMEZ.h>
#include <mach/i2c.h>
#include <mach/mx51.h>
#include <mach/mxc_iim.h>
#include <sound/tpa6130a2-plat.h>
#include <linux/proc_fs.h>
#ifdef CONFIG_HOLT_ARINC
#include <linux/spi/holt_arinc_platform.h>
#endif

#include "devices.h"
#include "crm_regs.h"
#include "usb.h"

/*!
 * @file mach-mx51/mx51_babbage.c
 *
 * @brief This file contains the board specific initialization routines.
 *
 * @ingroup MSL_MX51
 */

// enable various log messages in this module
//#define IMS_LOG
//#define IMS_LOG_PINS
#define SCUMEZ_AUDIO
#define SCUMEZ_RS485GPIO

#define SCUMEZ_PMIC_INT			(0*32 + 8) /* GPIO_1_8  */

#define SCUMEZ_DISCRETE_INPUT1		(0*32 + 11) /* GPIO_1_11 */
#define SCUMEZ_DISCRETE_INPUT2		(0*32 + 12) /* GPIO_1_12 */
#define SCUMEZ_DISCRETE_INPUT3		(0*32 + 13) /* GPIO_1_13 */
#define SCUMEZ_DISCRETE_INPUT4		(0*32 + 14) /* GPIO_1_14 */
#define SCUMEZ_DISCRETE_INPUT5		(0*32 + 15) /* GPIO_1_15 */
#define SCUMEZ_DISCRETE_INPUT6		(0*32 + 16) /* GPIO_1_16 */
#define SCUMEZ_DISABLEC			(0*32 + 17) /* GPIO_1_17 */
#define SCUMEZ_ACTIVE			(0*32 + 18) /* GPIO_1_18 */

#define SCUMEZ_PRIMARY_SCU_NOT		(0*32 + 25) /* GPIO_1_25 */
#define SCUMEZ_DISCRETE_OUT1		(0*32 + 26) /* GPIO_1_26 */
#define SCUMEZ_DISCRETE_OUT2		(0*32 + 27) /* GPIO_1_27 */
#define SCUMEZ_DISCRETE_OUT3		(0*32 + 28) /* GPIO_1_28 */

#define SCUMEZ_FEC_PHY_RESET		(1*32 + 14) /* GPIO_2_14 */
#define SCUMEZ_AUDAMP_STBY		(1*32 + 17) /* GPIO_2_17 */
#define SCUMEZ_RS485_2_TX_ENABLE	(1*32 + 24) /* GPIO_2_24 UART2 RTS */
#define SCUMEZ_RS485_3_TX_ENABLE	(1*32 + 31) /* GPIO_2_31 UART3 RTS */

#define SCUMEZ_SPI2_SCLK		(2*32 + 10) /* GPIO_3_10 */
#define SCUMEZ_SPI2_MISO		(2*32 + 11) /* GPIO_3_11 */
#define SCUMEZ_SPI2_MOSI		(2*32 + 25) /* GPIO_3_25 */
#define SCUMEZ_ADV7180_INTR		(3*32 + 13) /* GPIO_4_14 */
#define SCUMEZ_CSPI2_SS3		(2*32 + 26) /*  GPIO_3_26 */
#define SCUMEZ_CSPI2_SS2		(2*32 + 27) /*  GPIO_3_27 */
#define SCUMEZ_CSPI2_SS1		(2*32 + 28) /*  GPIO_3_28 */

#define SCUMEZ_ARINC429_FLAG1		(2*32 + 29) /*  GPIO_3_29*/
#define SCUMEZ_ARINC429_FLAG2		(2*32 + 30) /*  GPIO_3_30*/
#define SCUMEZ_ARINC429_FLAG3		(2*32 + 31) /*  GPIO_3_31*/

#define SCUMEZ_ARINC429_MR		(3*32 + 0) /* GPIO_4_0  */
#define SCUMEZ_SYSTEM_TYPE_0		(3*32 + 3) /* GPIO_4_3  */
#define SCUMEZ_SYSTEM_TYPE_1		(3*32 + 4) /* GPIO_4_4  */
#define SCUMEZ_SYSTEM_TYPE_2		(3*32 + 5) /* GPIO_4_5  */
#define SCUMEZ_SYSTEM_TYPE_3		(3*32 + 6) /* GPIO_4_6  */

#define SCUMEZ_ADV7180_RESET		(3*32 + 9) /* GPIO_4_9 */
#define SCUMEZ_ADV7180_INTR		(3*32 + 13) /* GPIO_4_14 */
#define SCUMEZ_ADV7180_PWRDWN		(3*32 + 14) /* GPIO_4_14 */

#define SCUMEZ_AUDIO_CLK_EN		(3*32 + 26) /* GPIO_4_26 */


extern int __init mx51_babbage_init_mc13892(void);
extern struct cpu_wp *(*get_cpu_wp)(int *wp);
extern void (*set_num_cpu_wp)(int num);
extern struct dvfs_wp *(*get_dvfs_core_wp)(int *wp);

static int num_cpu_wp;

static struct pad_desc mx51babbage_gpio_pads[] =
{
	// Discrete gpio pins
	MX51_PAD_USBH1_CLK__GPIO_1_25,      // ok SCU Primary_SCU#_c gpio1_25 (in)  25
	MX51_PAD_USBH1_DIR__GPIO_1_26,      // ok SCU Discrete Out 1 gpio1_26 (out)
	MX51_PAD_USBH1_STP__GPIO_1_27,      // ok SCU discrete out 2 gpio1_27 (out)
	MX51_PAD_USBH1_NXT__GPIO_1_28,      // ok SCU discrete out 3 gpio1_28 (out)
	MX51_PAD_USBH1_DATA0__GPIO_1_11,    // ok SCU discrete in 1  gpio1_11 (in)
	MX51_PAD_USBH1_DATA1__GPIO_1_12,    // ok SCU discrete in 2  gpio1_12 (in)
	MX51_PAD_USBH1_DATA2__GPIO_1_13,    // ok SCU discrete in 3  gpio1_13 (in)
	MX51_PAD_USBH1_DATA3__GPIO_1_14,    // ok SCU discrete in 4  gpio1_14 (in)
	MX51_PAD_USBH1_DATA4__GPIO_1_15,    // ok SCU discrete in 5  gpio1_15 (in)
	MX51_PAD_USBH1_DATA5__GPIO_1_16,    // ok SCU discrete in 6  gpio1_16 (in)
	MX51_PAD_USBH1_DATA6__GPIO_1_17,    // ok SCU SCU_Discable#_c  gpio1_11 (in)
	MX51_PAD_USBH1_DATA7__GPIO_1_18,    // ok SCU SCU_Active      gpio1_18  (out)
    
};

static struct pad_desc mx51babbage_pads[] = {

	/* from schematic p 5 */
	MX51_PAD_EIM_D16__UART2_CTS,
	MX51_PAD_EIM_D17__UART2_RXD,
	MX51_PAD_EIM_D18__UART2_TXD,
#ifdef SCUMEZ_RS485GPIO
	MX51_PAD_EIM_D19__GPIO_2_3,
#else
	MX51_PAD_EIM_D19__UART2_RTS,
#endif

	MX51_PAD_GPIO_1_0__GPIO_1_0,        // ok EVK/SCU
	MX51_PAD_GPIO_1_1__GPIO_1_1,        // ok EVK/SCU
	MX51_PAD_GPIO_1_4__GPIO_1_4,        // ok EVK/SCU
	MX51_PAD_GPIO_1_5__GPIO_1_5,        // ok EVK/SCU
	MX51_PAD_GPIO_1_6__GPIO_1_6,        // ok EVK/SCU
	MX51_PAD_GPIO_1_7__GPIO_1_7,
	MX51_PAD_GPIO_1_8__GPIO_1_8,        // ok EVK/SCU

	MX51_PAD_EIM_D20__GPIO_2_4,		// FEQ0#
	MX51_PAD_EIM_D21__GPIO_2_5,		// FEQ1#
	MX51_PAD_EIM_D22__GPIO_2_6,		// NO_BURST
	MX51_PAD_EIM_D23__GPIO_2_7,		// NO_SYNC

	MX51_PAD_EIM_D24__UART3_CTS,
	MX51_PAD_EIM_D25__UART3_RXD,
	MX51_PAD_EIM_D26__UART3_TXD,
#ifdef SCUMEZ_RS485GPIO
	MX51_PAD_EIM_D27__GPIO_2_9,
#else
	MX51_PAD_EIM_D27__UART3_RTS,
#endif

	MX51_PAD_EIM_A16__GPIO_2_10,		// CKIH_FREQ_SEL[0]
	MX51_PAD_EIM_A17__GPIO_2_11,		// CKIH_FREQ_SEL[1]/DIAG_LED_GPIO
	MX51_PAD_EIM_A18__GPIO_2_12,		// BT_LPB[0]/SPARE1
	MX51_PAD_EIM_A19__GPIO_2_13,		// BT_LPB[1]
	MX51_PAD_EIM_A20__GPIO_2_14,		// BT_UART_SRC[0]/RST_ENET_B
	MX51_PAD_EIM_A21__GPIO_2_15,		// BT_UART_SRC[1]/ENET_IRQ_B
	MX51_PAD_EIM_A22__GPIO_2_16,		// TBD3
	MX51_PAD_EIM_A23__GPIO_2_17,        	// BT_HPN_EN
	MX51_PAD_EIM_A27__GPIO_2_21,

	MX51_PAD_EIM_EB2__FEC_MDIO,
	MX51_PAD_EIM_EB3__FEC_RDAT1,

	MX51_PAD_EIM_CS0__GPIO_2_25,		// SPARE2
	MX51_PAD_EIM_CS1__GPIO_2_26,		// SPARE3

	MX51_PAD_EIM_CS2__FEC_RDAT2,
	MX51_PAD_EIM_CS3__FEC_RDAT3,
	MX51_PAD_EIM_CS4__FEC_RX_ER,
	MX51_PAD_EIM_CS5__FEC_CRS,

	MX51_PAD_EIM_OE__GPIO_2_24,		// SPARE4
	MX51_PAD_EIM_DTACK__GPIO_2_31,		// SPARE5

	MX51_PAD_NANDF_RB1__SD4_CMD,
#if 0 // use gpio for CSPI2
	MX51_PAD_NANDF_RB2__GPIO_3_10,		// CSPI2_SCLK
	MX51_PAD_NANDF_RB3__GPIO_3_11,		// CSPI2_MISO
	MX51_PAD_NANDF_D15__GPIO_3_25,		// CSPI2_MOSI
#else
	MX51_PAD_NANDF_RB2__CSPI2_SCLK,
	MX51_PAD_NANDF_RB3__CSPI2_MISO,
	MX51_PAD_NANDF_D15__CSPI2_MOSI,		// CSPI2_MOSI
#endif

	MX51_PAD_NANDF_CS0__GPIO_3_16,		// SPARE6

	MX51_PAD_NANDF_CS2__SD4_CLK,
	MX51_PAD_NANDF_CS3__SD4_DATA0,
	MX51_PAD_NANDF_CS4__SD4_DATA1,
	MX51_PAD_NANDF_CS5__SD4_DATA2,
	MX51_PAD_NANDF_CS6__SD4_DATA3,

	MX51_PAD_NANDF_D0__GPIO_4_8,		// SD4_CD

	MX51_PAD_NANDF_D2__GPIO_4_6,		// system type 0
	MX51_PAD_NANDF_D3__GPIO_4_5,		// system type 1
	MX51_PAD_NANDF_D4__GPIO_4_4,		// system type 2
	MX51_PAD_NANDF_D5__GPIO_4_3,		// system type 3

	MX51_PAD_NANDF_D8__GPIO_4_0,		// arinc MR         96
	MX51_PAD_NANDF_D9__GPIO_3_31,		// flag1 gpio 3_31  95
	MX51_PAD_NANDF_D10__GPIO_3_30,		// flag2 gpio 3_30  94
	MX51_PAD_NANDF_D11__GPIO_3_29,		// flag3 gpio 3_29  93

#if 1 // use gpio for CSPI2 SS
	MX51_PAD_NANDF_D12__GPIO_3_28,		// CSPI2_SS1
	MX51_PAD_NANDF_D13__GPIO_3_27,		// CSPI2_SS2
	MX51_PAD_NANDF_D14__GPIO_3_26,		// CSPI2_SS3
#else
	MX51_PAD_NANDF_D12__CSPI2_SS1,
	MX51_PAD_NANDF_D13__CSPI2_SS2,
	MX51_PAD_NANDF_D14__CSPI2_SS3,
#endif

	/* from schematic p 6 */
	MX51_PAD_USBH1_CLK__GPIO_1_25,		// Primary_SCU#_c gpio1_25 (in)  25
	MX51_PAD_USBH1_DIR__GPIO_1_26,		// Discrete Out 1 gpio1_26 (out)
	MX51_PAD_USBH1_STP__GPIO_1_27,		// discrete out 2 gpio1_27 (out)
	MX51_PAD_USBH1_NXT__GPIO_1_28,		// discrete out 3 gpio1_28 (out)
	MX51_PAD_USBH1_DATA0__GPIO_1_11,	// discrete in 1  gpio1_11 (in)
	MX51_PAD_USBH1_DATA1__GPIO_1_12,	// discrete in 2  gpio1_12 (in)
	MX51_PAD_USBH1_DATA2__GPIO_1_13,	// discrete in 3  gpio1_13 (in)
	MX51_PAD_USBH1_DATA3__GPIO_1_14,	// discrete in 4  gpio1_14 (in)
	MX51_PAD_USBH1_DATA4__GPIO_1_15,	// discrete in 5  gpio1_15 (in)
	MX51_PAD_USBH1_DATA5__GPIO_1_16,	// discrete in 6  gpio1_16 (in)
	MX51_PAD_USBH1_DATA6__GPIO_1_17,	// SCUMEZ_Discable#_c  gpio1_11 (in)
	MX51_PAD_USBH1_DATA7__GPIO_1_18,	// SCUMEZ_Active      gpio1_18  (out)
    
	/* SD1 fixed part on SCU */
	MX51_PAD_SD1_CMD__SD1_CMD,
	MX51_PAD_SD1_CLK__SD1_CLK,
	MX51_PAD_SD1_DATA0__SD1_DATA0,
	MX51_PAD_SD1_DATA1__SD1_DATA1,
	MX51_PAD_SD1_DATA2__SD1_DATA2,
	MX51_PAD_SD1_DATA3__SD1_DATA3,

//	MX51_PAD_SD2_DATA0__SD2_DATA0,
//	MX51_PAD_SD2_DATA1__SD2_DATA1,
//	MX51_PAD_SD2_DATA2__SD2_DATA2,
//	MX51_PAD_SD2_DATA3__SD2_DATA3,

	MX51_PAD_AUD3_BB_TXD__AUD3_BB_TXD,
	MX51_PAD_AUD3_BB_RXD__AUD3_BB_RXD,
	MX51_PAD_AUD3_BB_CK__AUD3_BB_CK,
	MX51_PAD_AUD3_BB_FS__AUD3_BB_FS,

	MX51_PAD_CSPI1_MOSI__CSPI1_MOSI,
	MX51_PAD_CSPI1_MISO__CSPI1_MISO,
	MX51_PAD_CSPI1_SS0__CSPI1_SS0,
	MX51_PAD_CSPI1_SS1__CSPI1_SS1,		// PMIC
	MX51_PAD_CSPI1_RDY__GPIO_4_26,		// AUDIO_CLK_EN_B
	MX51_PAD_CSPI1_SCLK__CSPI1_SCLK,

	/* UART1 */
	MX51_PAD_UART1_RXD__UART1_RXD,
	MX51_PAD_UART1_TXD__UART1_TXD,
	MX51_PAD_UART1_RTS__UART1_RTS,
	MX51_PAD_UART1_CTS__UART1_CTS,

//	MX51_PAD_UART3_RXD__GPIO_1_22,
//	MX51_PAD_UART3_TXD__GPIO_1_23.
//	MX51_PAD_OWIRE_LINE__GPIO_1_24,
//	MX51_PAD_OWIRE_LINE__SPDIF_OUT1,	// SYS_ON_OFF_REQ

	MX51_PAD_KEY_COL4__I2C2_SCL,
	MX51_PAD_KEY_COL5__I2C2_SDA,

	/* from schematic p 7 */
//	MX51_PAD_GPIO_1_0__GPIO_1_0,
//	MX51_PAD_GPIO_1_1__GPIO_1_1,
	MX51_PAD_GPIO_1_2__PWM_PWMO,		// PWM_OUT
	MX51_PAD_GPIO_1_4__GPIO_1_4,		// WDOG_B
//	MX51_PAD_GPIO_1_5__GPIO_1_5,
//	MX51_PAD_GPIO_1_6__GPIO_1_6,
//	MX51_PAD_GPIO_1_7__GPIO_1_7,
	MX51_PAD_GPIO_1_8__GPIO_1_8,		// INT_FROM_PMIC
//	MX51_PAD_GPIO_1_9__GPIO_1_9,

	/* from schematic p 8 */
	MX51_PAD_CSI1_D8__GPIO_3_12,		// SPARE8
	MX51_PAD_CSI1_D9__GPIO_3_13,		// SPARE7
//	MX51_PAD_CSI1_D10__CSI1_D10,
//	MX51_PAD_CSI1_D11__CSI1_D11,
	MX51_PAD_CSI1_D12__CSI1_D12,
	MX51_PAD_CSI1_D13__CSI1_D13,
	MX51_PAD_CSI1_D14__CSI1_D14,
	MX51_PAD_CSI1_D15__CSI1_D15,
	MX51_PAD_CSI1_D16__CSI1_D16,
	MX51_PAD_CSI1_D17__CSI1_D17,
	MX51_PAD_CSI1_D18__CSI1_D18,
	MX51_PAD_CSI1_D19__CSI1_D19,
	MX51_PAD_CSI1_VSYNC__CSI1_VSYNC,
	MX51_PAD_CSI1_HSYNC__CSI1_HSYNC,
	MX51_PAD_CSI1_PIXCLK__CSI1_PIXCLK,


	MX51_PAD_CSI2_D12__GPIO_4_9,		// RST_CAPTURE_B
	MX51_PAD_CSI2_VSYNC__GPIO_4_13,		// CAPTURE_IRQ_B
	MX51_PAD_CSI2_HSYNC__GPIO_4_14,		// #7180_PWDN

//	MX51_PAD_DISP1_DAT6__DISP1_DAT6,
//	MX51_PAD_DISP1_DAT7__DISP1_DAT7,
//	MX51_PAD_DISP1_DAT8__DISP1_DAT8,
//	MX51_PAD_DISP1_DAT9__DISP1_DAT9,
//	MX51_PAD_DISP1_DAT10__DISP1_DAT10,
//	MX51_PAD_DISP1_DAT11__DISP1_DAT11,
//	MX51_PAD_DISP1_DAT12__DISP1_DAT12,
//	MX51_PAD_DISP1_DAT13__DISP1_DAT13,
//	MX51_PAD_DISP1_DAT14__DISP1_DAT14,
//	MX51_PAD_DISP1_DAT15__DISP1_DAT15,
//	MX51_PAD_DISP1_DAT16__DISP1_DAT16,
//	MX51_PAD_DISP1_DAT17__DISP1_DAT17,
//	MX51_PAD_DISP1_DAT18__DISP1_DAT18,
//	MX51_PAD_DISP1_DAT19__DISP1_DAT19,
//	MX51_PAD_DISP1_DAT20__DISP1_DAT20,
//	MX51_PAD_DISP1_DAT21__DISP1_DAT21,
//	MX51_PAD_DISP1_DAT22__DISP1_DAT22,
//	MX51_PAD_DISP1_DAT23__DISP1_DAT23,

	MX51_PAD_DI_GP3__FEC_TX_ER,         // pullup set to fix net problem
	MX51_PAD_DI2_PIN2__FEC_MDC,
	MX51_PAD_DISP2_DAT6__FEC_TDAT1,
	MX51_PAD_DISP2_DAT7__FEC_TDAT2,
	MX51_PAD_DISP2_DAT8__FEC_TDAT3,
	MX51_PAD_DISP2_DAT9__FEC_TX_EN,
	MX51_PAD_DISP2_DAT10__FEC_COL,
	MX51_PAD_DISP2_DAT11__FEC_RXCLK,
	MX51_PAD_DISP2_DAT12__FEC_RX_DV,    // missing from defs, added from kernel 31
	MX51_PAD_DISP2_DAT13__FEC_TX_CLK,
	MX51_PAD_DISP2_DAT14__FEC_RDAT0,
	MX51_PAD_DISP2_DAT15__FEC_TDAT0,

};

static struct dvfs_wp dvfs_core_setpoint[] = {
	{33, 8, 33, 10, 10, 0x08},
	{26, 0, 33, 20, 10, 0x08},
	{28, 8, 33, 20, 30, 0x08},
	{29, 0, 33, 20, 10, 0x08},
};

/* working point(wp): 0 - 800MHz; 1 - 166.25MHz; */
static struct cpu_wp cpu_wp_auto[] = {
	{
	 .pll_rate = 1000000000,
	 .cpu_rate = 1000000000,
	 .pdf = 0,
	 .mfi = 10,
	 .mfd = 11,
	 .mfn = 5,
	 .cpu_podf = 0,
	 .cpu_voltage = 1175000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 800000000,
	 .pdf = 0,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 0,
	 .cpu_voltage = 1100000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 166250000,
	 .cpu_podf = 4,
	 .cpu_voltage = 850000,},
};

static struct fb_videomode video_modes[] = {
		{
	 /* NTSC TV output */
	 "TV-NTSC", 60, 720, 480, 74074,
	 122, 15,
	 18, 26,
	 1, 1,
	 FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
	 FB_VMODE_INTERLACED,
			0,}, 
		{
	 /* PAL TV output */
	 "TV-PAL", 50, 720, 576, 74074,
	 132, 11,
	 22, 26,
	 1, 1,
	 FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
	 FB_VMODE_INTERLACED | FB_VMODE_ODD_FLD_FIRST,
			0,}, 
};

struct cpu_wp *mx51_babbage_get_cpu_wp(int *wp)
{
	*wp = num_cpu_wp;
	return cpu_wp_auto;
}

void mx51_babbage_set_num_cpu_wp(int num)
{
	num_cpu_wp = num;
	return;
}

//static struct mxc_w1_config mxc_w1_data = {
//	.search_rom_accelerator = 1,
//};

static struct dvfs_wp *mx51_babbage_get_dvfs_core_table(int *wp)
{
	*wp = ARRAY_SIZE(dvfs_core_setpoint);
	return dvfs_core_setpoint;
}

extern void mx5_ipu_reset(void);
static struct mxc_ipu_config mxc_ipu_data = {
	.rev = 2,
	.reset = mx5_ipu_reset,
};

extern void mx5_vpu_reset(void);
static struct mxc_vpu_platform_data mxc_vpu_data = {
	.iram_enable = false,
	.iram_size = 0x14000,
	.reset = mx5_vpu_reset,
};

#ifndef CONFIG_HOLT_ARINC
// gpio handling of SCU chip select lines
//%%jws%%
void mx51_babbage_gpio_csdown(int chipselect, int change, int busnum)
{

    // %%jws%% inactive is high for arinc429 ss
    u32 gpio;

    //%%jws%% (cs not used in either case 
    //printk(KERN_INFO "ss%d.%d down chng %d", busnum, chipselect, change);

    switch ( busnum ) {
    // only on EVK would this be needed
#if 0
    case 1:
    switch (chipselect) {
        case 2:
            // if change == 0 then do nothing, full duplex
            if (change == 0) {
                gpio= IOMUX_TO_GPIO(MX51_PIN_CSPI1_SS1);
                gpio_request(gpio, "cspi1_ss1");
                gpio_direction_output(gpio, 0);
                gpio_set_value(gpio, 1 );
            }
            break;
        case 3:
            if (change == 0) {
                gpio= IOMUX_TO_GPIO(MX51_PIN_DI1_PIN11);
                gpio_request(gpio, "di1_pin11");
                gpio_direction_output(gpio, 0);
                gpio_set_value(gpio, 1 );
            }
            break;
        case 4:
            break;
        default:
            break;
        }
    break;
#endif
    // SCU cspi 2
    case 2:
        // ecspi chip 2
        switch (chipselect) {
            // SS1
            case 2:
                // if change == 0 then do nothing, full duplex
                if (change == 0) {
                    // MX51_PAD_NANDF_D12__GPIO_3_28
                    gpio= SCUMEZ_CSPI2_SS1;
                    gpio_request(gpio, "CSPI2_SS1");
                    gpio_direction_output(gpio, 1);
                }
                break;
            // SS2
            case 3:
                if (change == 0) {
                    // MX51_PAD_NANDF_D13__GPIO_3_27
                    gpio= SCUMEZ_CSPI2_SS2;
                    gpio_request(gpio, "CSPI2_SS2");
                    gpio_direction_output(gpio, 1);
                }
                break;
            // SS3
            case 4:
                if (change == 0) {
                    // MX51_PAD_NANDF_D14__GPIO_3_26
                    gpio= SCUMEZ_CSPI2_SS3;
                    gpio_request(gpio, "CSPI2_SS3");
                    gpio_direction_output(gpio, 1);
                }
                break;
        }
        break;
    default:
        break;
    }
}
EXPORT_SYMBOL(mx51_babbage_gpio_csdown);

void mx51_babbage_gpio_csup(int chipselect, int change, int busnum)
{
    // %%jws%% active is low for arinc429 ss
    u32 gpio;

    //%%jws%% chipselect not used in either case, noisy on bus 1
    //printk(KERN_ERR "ss%d.%d up", busnum, chipselect);

    switch ( busnum ) {
// not supported on SCU anymore
#if 0  
    case 1:
        switch(chipselect) {
        case 2:
            gpio= IOMUX_TO_GPIO(MX51_PIN_CSPI1_SS1);
            mxc_request_iomux(MX51_PIN_CSPI1_SS1, IOMUX_CONFIG_GPIO);
            mxc_free_iomux(MX51_PIN_CSPI1_SS1, IOMUX_CONFIG_GPIO);
            gpio_request(gpio, "cspi1_ss1");
            gpio_direction_output(gpio, 0);
            gpio_set_value(gpio, 0 );
            break;
        case 3:
            gpio= IOMUX_TO_GPIO(MX51_PIN_DI1_PIN11);
            mxc_request_iomux(MX51_PIN_DI1_PIN11, IOMUX_CONFIG_GPIO);
            mxc_free_iomux(MX51_PIN_DI1_PIN11, IOMUX_CONFIG_GPIO);
            gpio_request(gpio, "di1_pin11");
            gpio_direction_output(gpio, 0);
            gpio_set_value(gpio, 0 );
            break;
        case 4:
            break;
        default:
            break;

        }
        break;
#endif
    // SCU cspi 2
    case 2:
        // controller ecspi 2 
        switch(chipselect) {
        case 2:
            // MX51_PAD_NANDF_D12__GPIO_3_28
            gpio= SCUMEZ_CSPI2_SS1;
            //mxc_request_iomux(MX51_PIN_NANDF_D12, IOMUX_CONFIG_GPIO);
            //mxc_free_iomux(MX51_PIN_NANDF_D12, IOMUX_CONFIG_GPIO);
            gpio_request(gpio, "CSPI2_SS1");
            gpio_direction_output(gpio, 0);
            break;
        case 3:
            // MX51_PAD_NANDF_D13__GPIO_3_27
            gpio= SCUMEZ_CSPI2_SS2;
            //mxc_request_iomux(MX51_PIN_NANDF_D13, IOMUX_CONFIG_GPIO);
            //mxc_free_iomux(MX51_PIN_NANDF_D13, IOMUX_CONFIG_GPIO);
            gpio_request(gpio, "CSPI2_SS2");
            gpio_direction_output(gpio, 0);
            break;
        case 4:
            // MX51_PAD_NANDF_D14__GPIO_3_26
            gpio= SCUMEZ_CSPI2_SS3;
            //mxc_request_iomux(MX51_PIN_NANDF_D14, IOMUX_CONFIG_GPIO);
            //mxc_free_iomux(MX51_PIN_NANDF_D14, IOMUX_CONFIG_GPIO);
            gpio_request(gpio, "CSPI2_SS3");
            gpio_direction_output(gpio, 0);
            break;
        default:
            break;

        }
        break;
    default:
        break;
    }
    //%%jws%%
    //printk(KERN_DEBUG "ss%d up chng %d", chipselect, change);
}
//%%jws%%
EXPORT_SYMBOL(mx51_babbage_gpio_csup);
#endif

/* workaround for ecspi chipselect pin may not keep correct level when idle */
static void mx51_babbage_gpio_spi_chipselect_active(int cspi_mode, int status,
					     int chipselect)
{
	switch (cspi_mode) {
	case 1:
		switch (chipselect) {
		case 0x1:
			{
			struct pad_desc cspi1_ss0 = MX51_PAD_CSPI1_SS0__CSPI1_SS0;

			mxc_iomux_v3_setup_pad(&cspi1_ss0);
			break;
			}
		case 0x2:
			{
			struct pad_desc cspi1_ss0_gpio = MX51_PAD_CSPI1_SS0__GPIO_4_24;

			mxc_iomux_v3_setup_pad(&cspi1_ss0_gpio);
/*
			gpio_request(BABBAGE_CSP1_SS0_GPIO, "cspi1-gpio");
			gpio_direction_output(BABBAGE_CSP1_SS0_GPIO, 0);
			gpio_set_value(BABBAGE_CSP1_SS0_GPIO, 1 & (~status));
*/
			break;
			}
		default:
			break;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

static void mx51_babbage_gpio_spi_chipselect_inactive(int cspi_mode, int status,
					       int chipselect)
{
	switch (cspi_mode) {
	case 1:
		switch (chipselect) {
		case 0x1:
			break;
		case 0x2:
//			gpio_free(BABBAGE_CSP1_SS0_GPIO);
			break;

		default:
			break;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

static struct mxc_spi_master mxcspi1_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active = mx51_babbage_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_babbage_gpio_spi_chipselect_inactive,
#ifndef CONFIG_HOLT_ARINC
	.chipselect_up = mx51_babbage_gpio_csup,
	.chipselect_down = mx51_babbage_gpio_csdown,
#endif
};

#ifndef CONFIG_HOLT_ARINC
static struct mxc_spi_master mxcspi2_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active = mx51_babbage_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_babbage_gpio_spi_chipselect_inactive,
	.chipselect_up = mx51_babbage_gpio_csup,
	.chipselect_down = mx51_babbage_gpio_csdown,
};
#endif
#if 0 // use gpio for CSPI2
#include <linux/spi/spi_gpio.h>

static struct spi_gpio_platform_data spi_bitbang_data = {
        .sck =  SCUMEZ_SPI2_SCLK,
        .mosi = SCUMEZ_SPI2_MOSI,
	.miso = SCUMEZ_SPI2_MISO,
	.num_chipselect = 4,	// only assigned is CSI2_HSYNC
};

static struct platform_device spi_gpio_device = {
        .name = "spi_gpio",
        .id = 2,
        .dev = {
                .platform_data = &spi_bitbang_data,
        },
};
#endif


static struct imxi2c_platform_data mxci2c_data = {
	.bitrate = 50000,
};

static struct tve_platform_data tve_data = {
	.dac_reg = "VVIDEO",
};

static struct mxc_dvfs_platform_data dvfs_core_data = {
	.reg_id = "SW1",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.gpc_vcr_offset = MXC_GPC_VCR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 30,
};

static struct mxc_bus_freq_platform_data bus_freq_data = {
	.gp_reg_id = "SW1",
	.lp_reg_id = "SW2",
};
static struct mxc_dvfsper_data dvfs_per_data = {
	.reg_id = "SW2",
	.clk_id = "gpc_dvfs_clk",
	.gpc_cntr_reg_addr = MXC_GPC_CNTR,
	.gpc_vcr_reg_addr = MXC_GPC_VCR,
	.gpc_adu = 0x0,
	.vai_mask = MXC_DVFSPMCR0_FSVAI_MASK,
	.vai_offset = MXC_DVFSPMCR0_FSVAI_OFFSET,
	.dvfs_enable_bit = MXC_DVFSPMCR0_DVFEN,
	.irq_mask = MXC_DVFSPMCR0_FSVAIM,
	.div3_offset = 0,
	.div3_mask = 0x7,
	.div3_div = 2,
	.lp_high = 1250000,
	.lp_low = 1250000,
};

static struct resource mxcfb_resources[] = {
	[0] = {
	       .flags = IORESOURCE_MEM,
	       },
};

static struct mxc_fb_platform_data fb_data[] = {
	{
		.interface_pix_fmt = IPU_PIX_FMT_YUV444,
		.mode_str = "TV-NTSC", 
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
	{
		.interface_pix_fmt = IPU_PIX_FMT_YUV444,
		.mode_str = "TV-PAL", 
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
};

static void mxc_iim_enable_fuse(void)
{
	u32 reg;

	if (!ccm_base)
		return;
	/* Enable fuse blown */
	reg = readl(ccm_base + 0x64);
	reg |= 0x10;
	writel(reg, ccm_base + 0x64);
}

static void mxc_iim_disable_fuse(void)
{
	u32 reg;

	/* Disable fuse blown */
	if (!ccm_base)
		return;

	reg = readl(ccm_base + 0x64);
	reg &= ~0x10;
	writel(reg, ccm_base + 0x64);
}

static struct mxc_iim_data iim_data = {
	.bank_start = MXC_IIM_MX51_BANK_START_ADDR,
	.bank_end   = MXC_IIM_MX51_BANK_END_ADDR,
	.enable_fuse = mxc_iim_enable_fuse,
	.disable_fuse = mxc_iim_disable_fuse,
};

extern int primary_di;
static int __init mxc_init_fb(void)
{
	// SCU has display only to do video composite out, and
	// to support video capture.  LVDS reset GPIO is not
	// configured

	// mxc_fb_devices defined three frame buffer assets (and uses dma for each)

	if (primary_di) {
		printk(KERN_INFO "DI1 is primary\n");

		/* DI1 -> DP-BG channel: */
		mxc_fb_devices[1].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[1].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);

		/* DI0 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);
	} else {
		printk(KERN_INFO "DI0 is primary\n");

		/* DI0 -> DP-BG channel: */
		mxc_fb_devices[0].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[0].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);

		/* DI1 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);
	}

	/*
	 * DI0/1 DP-FG channel:
	 */
	mxc_register_device(&mxc_fb_devices[2], NULL);

	return 0;
}
device_initcall(mxc_init_fb);

// pwdn: 0, power up device 
// pwdn: 1, power down device
// device power down is low-true
static void adv7180_pwdn(int pwdn)
{
	int power = gpio_get_value(SCUMEZ_ADV7180_PWRDWN);
	pr_debug( "%s@%d power %d->%d\n", __func__, __LINE__, power, pwdn ^ 1 );
	gpio_set_value(SCUMEZ_ADV7180_PWRDWN, (pwdn==0)?1:0 );
	if ( 0 == power ) // need delay if turning the power on
		mdelay(5);
}

static void adv7180_reset( void )
{
	pr_debug( "%s@%d\n", __func__, __LINE__ );
	gpio_set_value(SCUMEZ_ADV7180_RESET, 0 );
	mdelay(5); // spec'd minimum reset of 5ms
	gpio_set_value(SCUMEZ_ADV7180_RESET, 1 );
	mdelay(5); // spec'd minimum reset recovery of 5ms
}

static void adv7180_platform_init( void )
{
	printk( KERN_DEBUG "%s@%d\n", __func__, __LINE__ );
	gpio_request(SCUMEZ_ADV7180_RESET, "adv7180-reset");
	gpio_request(SCUMEZ_ADV7180_PWRDWN, "adv7180-pwrdwn");

	gpio_direction_output(SCUMEZ_ADV7180_RESET, 0);

	gpio_direction_output(SCUMEZ_ADV7180_PWRDWN, 0);
}

static void adv7180_platform_release( void )
{
	printk( KERN_DEBUG "%s@%d\n", __func__, __LINE__ );
	gpio_set_value(SCUMEZ_ADV7180_RESET, 0 );
	gpio_set_value(SCUMEZ_ADV7180_PWRDWN, 0 );
	gpio_free(SCUMEZ_ADV7180_RESET);
	gpio_free(SCUMEZ_ADV7180_PWRDWN);
}

static struct mxc_tvin_platform_data adv7180_data = {
	.dvddio_reg = NULL,
	.dvdd_reg = NULL,
	.avdd_reg = NULL,
	.pvdd_reg = NULL,
	.pwdn = &adv7180_pwdn,
	.reset = &adv7180_reset,
	.platform_init = &adv7180_platform_init,
	.platform_exit = &adv7180_platform_release,
	.cvbs = true,
	.mode = TVIN_ADV7180_COMPOSITE_NTSCM,
};

static struct i2c_board_info mxc_i2c1_board_info[] __initdata = {
	{
		.type = "sgtl5000-i2c",
		.addr = 0x0a,
	},
	{
		.type = "adv7180",
		.addr = 0x21,
		.platform_data = &adv7180_data,
	},
};

#ifndef CONFIG_HOLT_ARINC
/* Structure for configuing three arinc controllers */
static struct spi_board_info board_arinc1_board_info[] = {
    {
        .modalias = "spidev",   /* Expose spi to userspace */
        .max_speed_hz = 500000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 1,       /* ChipSelect for McSPI */
        .mode = 4,              /* SPI Mode */
    },
};

static struct spi_board_info board_arinc2_board_info[] = {
    {
        .modalias = "spidev",   /* Expose spi to userspace */
        .max_speed_hz = 500000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 2,       /* ChipSelect for McSPI */
        .mode = 4,              /* SPI Mode */
    },
};

static struct spi_board_info board_arinc3_board_info[] = {
    {
        .modalias = "spidev",   /* Expose spi to userspace */
        .max_speed_hz = 500000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 3,       /* ChipSelect for McSPI */
        .mode = 4,              /* SPI Mode */
    },
};
#else
/* Structure for configuing three arinc controllers */
static struct spi_board_info arinc_board_info[] = {
	{
		.modalias = "hi3597-arinc",
		.max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
		.bus_num = 2,           /* McSPI Bus Number */
		.chip_select = 1,       /* ChipSelect for McSPI */
		.mode = 0, // SPI_CS_HIGH,	/* SPI Mode */
		.platform_data = (void *)SCUMEZ_ARINC429_FLAG1,
		.controller_data = (void *)SCUMEZ_CSPI2_SS1
	},
	{
		.modalias = "hi3587-arinc",
		.max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
		.bus_num = 2,           /* McSPI Bus Number */
		.chip_select = 2,       /* ChipSelect for McSPI */
		.mode = 0, // SPI_CS_HIGH,	/* SPI Mode */
		.platform_data = (void *)SCUMEZ_ARINC429_FLAG2,
		.controller_data = (void *)SCUMEZ_CSPI2_SS2
	},
	{
		.modalias = "hi3587-arinc",
		.max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
		.bus_num = 2,           /* McSPI Bus Number */
		.chip_select = 3,       /* ChipSelect for McSPI */
		.mode = 0, // SPI_CS_HIGH,	/* SPI Mode */
		.platform_data = (void *)SCUMEZ_ARINC429_FLAG3,
		.controller_data = (void *)SCUMEZ_CSPI2_SS3
	},
	{
	},
};

struct arinc_spi_platform_info arinc_platform_info = {
	.cspi_info = &mxcspi2_device,
	.chip_info = arinc_board_info,
};

struct platform_device arinc_spi_device = {
	.name = "holt_arinc",
	.dev = {
		.platform_data = &arinc_platform_info,
	},
};

#endif

static int sdhc_write_protect(struct device *dev)
{
	//%%jws%%
	printk(KERN_INFO "%s", __FUNCTION__ );
	return 0;
}

static unsigned int sdhc_get_card_det_status(struct device *dev)
{
	printk(KERN_INFO "%s card status dev %d", __FUNCTION__, to_platform_device(dev)->id );
	// neither device on an scu supports card detection, but leave spot to put it in
	//%%jws%%
	return 0;
}

static struct mxc_mmc_platform_data mmc1_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	    MMC_VDD_31_32,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 52000000,
	.card_inserted_state = 1,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
	.power_mmc = NULL,
};

// SCU uses microd SD so card inserted state is always 1, inserted state
static struct mxc_mmc_platform_data mmc4_data = {
	// %%jws%% use same as mmc1 IMS_DEBUG: try 3.3 
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
		MMC_VDD_31_32,
//	.ocr_mask = MMC_VDD_32_33,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 50000000,
	.card_inserted_state = 1, 
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
};

#ifdef SCUMEZ_AUDIO

static int mxc_sgtl5000_amp_enable(int enable)
{
//	gpio_set_value(SCUMEZ_AUDAMP_STBY, enable ? 1 : 0);
	return 0;
}

// clock enable is high true
static int mxc_sgtl5000_clock_enable(int enable)
{
	printk( KERN_DEBUG "%s@%d: enable %d\n", __func__, __LINE__, enable );
	gpio_set_value(SCUMEZ_AUDIO_CLK_EN, enable);
	mdelay(10); // oscillator takes up to 10mS to start-up 
	return 0;
}

static int headphone_det_status(void)
{
	return 1; // Always present
}

static struct mxc_audio_platform_data sgtl5000_data = {
	.ssi_num = 1,
	.src_port = 2,
	.ext_port = 3,
	.hp_status = headphone_det_status,
	.amp_enable = mxc_sgtl5000_amp_enable,
	.clock_enable = mxc_sgtl5000_clock_enable,
	.sysclk = 25000000,

};

static struct platform_device mxc_sgtl5000_device = {
	.name = "imx-3stack-sgtl5000",
};
#endif

static int __initdata enable_w1 = { 0 };
static int __init w1_setup(char *__unused)
{
	enable_w1 = 1;
	return cpu_is_mx51();
}

__setup("w1", w1_setup);


// ---------------------------
// System Type
// ---------------------------
struct gpio systemType_gpio[] = {
	{ .gpio = SCUMEZ_SYSTEM_TYPE_3, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_3", },
	{ .gpio = SCUMEZ_SYSTEM_TYPE_2, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_2", },
	{ .gpio = SCUMEZ_SYSTEM_TYPE_1, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_1", },
	{ .gpio = SCUMEZ_SYSTEM_TYPE_0, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_0", },
};

// these definitions should be moved somewhere board specific that makes them
// available to anyone who might call IMSSystemType
#define SYSTEM_TYPE_MASK	0xF
#define	 RDU_REV_B	(0b0000 & SYSTEM_TYPE_MASK)
#define	 RDU_REV_C	(0b1101 & SYSTEM_TYPE_MASK)
#define SCU_MEZZ	(0b0010 & SYSTEM_TYPE_MASK)
#define NIU_REV  	(0b0011 & SYSTEM_TYPE_MASK)
#define SCU2_ESB   (0b0001 & SYSTEM_TYPE_MASK)
#define SCU2_MEZZ  (0b0100 & SYSTEM_TYPE_MASK)

static char *systemType_toStr( int type )
{
	switch ( type ) {
	case RDU_REV_B:     return "RDU Rev B";
	case RDU_REV_C:     return "RDU Rev C";
	case SCU_MEZZ:      return "SCU Mezz";
	case NIU_REV:       return "NIU";
	case SCU2_ESB:      return  "SCU2 ESB";
    case SCU2_MEZZ:     return  "SCU2 MEZZ";
    default:           return "Unknown";
	}
}

int IMSSystemType( void )
{
	static int type = -1;
	if ( type == -1 ) {
		type =	(gpio_get_value( SCUMEZ_SYSTEM_TYPE_3 ) << 3 ) |
			(gpio_get_value( SCUMEZ_SYSTEM_TYPE_2 ) << 2 ) |
			(gpio_get_value( SCUMEZ_SYSTEM_TYPE_1 ) << 1 ) |
			(gpio_get_value( SCUMEZ_SYSTEM_TYPE_0 ) << 0 );

		// NOTE: masking off the top bits because of varying configurations in populated resistors.
		type &= SYSTEM_TYPE_MASK;

		printk(KERN_INFO"%s: detected board type %s(%02x)\n", __func__, systemType_toStr( type ), type);
	}
	return type;
}
EXPORT_SYMBOL(IMSSystemType);

static int boardType_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	int type = IMSSystemType();
	len += sprintf(buf+len,"%s(%02x)\n", systemType_toStr( type ), type);
	*eof = 1;
	return len;
}


/*!
 * Board specific fixup function. It is called by \b setup_arch() in
 * setup.c file very early on during kernel starts. It allows the user to
 * statically fill in the proper values for the passed-in parameters. None of
 * the parameters is used currently.
 *
 * @param  desc         pointer to \b struct \b machine_desc
 * @param  tags         pointer to \b struct \b tag
 * @param  cmdline      pointer to the command line
 * @param  mi           pointer to \b struct \b meminfo
 */
static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
	char *str;
	struct tag *t;
	struct tag *mem_tag = 0;
	int total_mem = SZ_512M;
	int left_mem = 0;
	int gpu_mem = SZ_64M;
	int fb_mem = SZ_32M;

	mxc_set_cpu_type(MXC_CPU_MX51);

	get_cpu_wp = mx51_babbage_get_cpu_wp;
	set_num_cpu_wp = mx51_babbage_set_num_cpu_wp;
	get_dvfs_core_wp = mx51_babbage_get_dvfs_core_table;
	num_cpu_wp = ARRAY_SIZE(cpu_wp_auto);

	for_each_tag(mem_tag, tags) {
		if (mem_tag->hdr.tag == ATAG_MEM) {
			total_mem = mem_tag->u.mem.size;
			left_mem = total_mem - gpu_mem - fb_mem;
			break;
		}
	}

	for_each_tag(t, tags) {
		if (t->hdr.tag == ATAG_CMDLINE) {
			str = t->u.cmdline.cmdline;
			str = strstr(str, "mem=");
			if (str != NULL) {
				str += 4;
				left_mem = memparse(str, &str);
				if (left_mem == 0 || left_mem > total_mem)
					left_mem = total_mem - gpu_mem - fb_mem;
			}

			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_memory=");
			if (str != NULL) {
				str += 11;
				gpu_mem = memparse(str, &str);
			}

			break;
		}
	}

	if (mem_tag) {
		fb_mem = total_mem - left_mem - gpu_mem;
		if (fb_mem < 0) {
			gpu_mem = total_mem - left_mem;
			fb_mem = 0;
		}
		mem_tag->u.mem.size = left_mem;

		/*reserve memory for gpu*/
		gpu_device.resource[5].start =
				mem_tag->u.mem.start + left_mem;
		gpu_device.resource[5].end =
				gpu_device.resource[5].start + gpu_mem - 1;
#if defined(CONFIG_FB_MXC_SYNC_PANEL) || \
	defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)
		if (fb_mem) {
			mxcfb_resources[0].start =
				gpu_device.resource[5].end + 1;
			mxcfb_resources[0].end =
				mxcfb_resources[0].start + fb_mem - 1;
		} else {
			mxcfb_resources[0].start = 0;
			mxcfb_resources[0].end = 0;
		}
#endif
	}
}

#define PWGT1SPIEN (1<<15)
#define PWGT2SPIEN (1<<16)
#define USEROFFSPI (1<<3)

static void mxc_power_off(void)
{
	/* We can do power down one of two ways:
	   Set the power gating
	   Set USEROFFSPI */

	/* Set the power gate bits to power down */
	pmic_write_reg(REG_POWER_MISC, (PWGT1SPIEN|PWGT2SPIEN),
		(PWGT1SPIEN|PWGT2SPIEN));
}

#if 0
void mxc_verify_gpio(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_gpio_pads, ARRAY_SIZE(mx51babbage_gpio_pads));
}

void mxc_verify_pins(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
}
#endif

static void __init mx51_babbage_io_init(void)
{
	// %%jws%% verify the pad values before setting them
    #ifdef IMS_LOG_PINS
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
    #endif

	mxc_iomux_v3_setup_multiple_pads(mx51babbage_pads,
					ARRAY_SIZE(mx51babbage_pads));

	/* PMIC interrupt */
	gpio_request(SCUMEZ_PMIC_INT, "pmic-int");
	gpio_direction_input(SCUMEZ_PMIC_INT);

	// PHY reset moved to here
	printk(KERN_INFO "%s reset FEC PHY", __FUNCTION__ );
	/* reset FEC PHY */
	gpio_request( SCUMEZ_FEC_PHY_RESET, "fec-phy-reset");
	gpio_direction_output( SCUMEZ_FEC_PHY_RESET, 0);
	msleep(10);
	gpio_set_value( SCUMEZ_FEC_PHY_RESET, 1);
	gpio_free( SCUMEZ_FEC_PHY_RESET );

	// free pins we are going to work on
	gpio_free(SCUMEZ_PRIMARY_SCU_NOT);
	gpio_free(SCUMEZ_DISCRETE_INPUT1);
	gpio_free(SCUMEZ_DISCRETE_INPUT2);
	gpio_free(SCUMEZ_DISCRETE_INPUT3);
	gpio_free(SCUMEZ_DISCRETE_INPUT4);
	gpio_free(SCUMEZ_DISCRETE_INPUT5);
	gpio_free(SCUMEZ_DISCRETE_INPUT6);
	gpio_free(SCUMEZ_DISABLEC);
	gpio_free(SCUMEZ_ACTIVE);
	gpio_free(SCUMEZ_DISCRETE_OUT1);
	gpio_free(SCUMEZ_DISCRETE_OUT2);
	gpio_free(SCUMEZ_DISCRETE_OUT3);

	// Primary SCU#    input MX51_PIN_USBH1_CLK
	gpio_request(SCUMEZ_PRIMARY_SCU_NOT, "gpio1_25"); /* mezz_gpio:primary_scu */
	gpio_direction_input(SCUMEZ_PRIMARY_SCU_NOT);
	gpio_export(SCUMEZ_PRIMARY_SCU_NOT, 0);	/* Export, direction locked down */

	// Discrete In 1  input MX51_PIN_USBH1_DATA0
	gpio_request(SCUMEZ_DISCRETE_INPUT1, "gpio1_11"); /* mezz_gpio:discree_in_1 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT1);
	gpio_export(SCUMEZ_DISCRETE_INPUT1, 0);	/* Export, direction locked down */

	// Discrete In 2  input MX51_PIN_USBH1_DATA1
	gpio_request(SCUMEZ_DISCRETE_INPUT2, "gpio1_12"); /* mezz_gpio:discrete_in_2 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT2);
	gpio_export(SCUMEZ_DISCRETE_INPUT2, 0);	/* Export, direction locked down */

	// Discrete In 3  input MX51_PIN_USBH1_DATA2
	gpio_request(SCUMEZ_DISCRETE_INPUT3, "gpio1_13"); /* mezz_gpio:discrete_in_3 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT3);
	gpio_export(SCUMEZ_DISCRETE_INPUT3, 0);	/* Export, direction locked down */

	// Discrete In 4  input MX51_PIN_USBH1_DATA3
	gpio_request(SCUMEZ_DISCRETE_INPUT4, "gpio1_14"); /* mezz_gpio:discrete_in_4 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT4);
	gpio_export(SCUMEZ_DISCRETE_INPUT4, 0);	/* Export, direction locked down */

	// Discrete In 5  input MX51_PIN_USBH1_DATA4
	gpio_request(SCUMEZ_DISCRETE_INPUT5, "gpio1_15"); /* mezz_gpio:discrete_in_5 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT5);
	gpio_export(SCUMEZ_DISCRETE_INPUT5, 0);	/* Export, direction locked down */

	// Discrete In 6  input MX51_PIN_USBH1_DATA5
	gpio_request(SCUMEZ_DISCRETE_INPUT6, "gpio1_16"); /* mezz_gpio:discrete_in_6 */
	gpio_direction_input(SCUMEZ_DISCRETE_INPUT6);
	gpio_export(SCUMEZ_DISCRETE_INPUT6, 0);	/* Export, direction locked down */

	// Discrete Out 1  output MX51_PIN_USBH1_DIR
	gpio_request(SCUMEZ_DISCRETE_OUT1, "gpio1_26"); /* mezz_gpio:discrete_out_1 */
	gpio_direction_output(SCUMEZ_DISCRETE_OUT1,0);
	gpio_export(SCUMEZ_DISCRETE_OUT1, 0);	/* Export, direction locked down */

	// Discrete Out 2  output MX51_PIN_USBH1_STP
	gpio_request(SCUMEZ_DISCRETE_OUT2, "gpio1_27"); /* mezz_gpio:discrete_out_2 */
	gpio_direction_output(SCUMEZ_DISCRETE_OUT2,0);
	gpio_export(SCUMEZ_DISCRETE_OUT2, 0);	/* Export, direction locked down */

	// Discrete Out 3  output MX51_PIN_USBH1_NXT
	gpio_request(SCUMEZ_DISCRETE_OUT3, "gpio1_28"); /* mezz_gpio:discrete_out_3 */
	gpio_direction_output(SCUMEZ_DISCRETE_OUT3,0);
	gpio_export(SCUMEZ_DISCRETE_OUT3, 0);	/* Export, direction locked down */

	// SCUMEZ_Disble#_c  input MX51_PIN_USBH1_DATA6
	gpio_request(SCUMEZ_DISABLEC, "gpio1_17"); /* mezz_gpio:scu_disable */
	gpio_direction_input(SCUMEZ_DISABLEC);
	gpio_export(SCUMEZ_DISABLEC, 0);	/* Export, direction locked down */

	// SCUMEZ_Active     output X51_PIN_USBH1_DATA7
	gpio_request(SCUMEZ_ACTIVE, "gpio1_18"); /* mezz_gpio:scu_active */
	gpio_direction_output(SCUMEZ_ACTIVE,0);
	gpio_export(SCUMEZ_ACTIVE, 0);	/* Export, direction locked down */

	/* Enable RS485_2_TX_ENABLE and set in RX state as default */
	gpio_request(SCUMEZ_RS485_2_TX_ENABLE, "RS485_2_TX_ENABLE");
	gpio_direction_output(SCUMEZ_RS485_2_TX_ENABLE, 0);

	/* Enable RS485_2_TX_ENABLE and set in RX state as default */
	gpio_request(SCUMEZ_RS485_3_TX_ENABLE, "RS485_3_TX_ENABLE");
	gpio_direction_output(SCUMEZ_RS485_3_TX_ENABLE, 0);


	/*
	Define 3 SS lines for ecspi2, and 3 flag inputs for interrupts.  also output for Arinc MR
	*/

#if 0
	// Arinc429 MR Output NANDF_D8 GPIO4_0 (96)
	mxc_free_iomux(MX51_PIN_NANDF_D8, IOMUX_CONFIG_GPIO);   // free pin since we are setting value
	gpio = gpio_request(IOMUX_TO_GPIO(MX51_PIN_NANDF_D8 ), "arincmr");
	gpio_direction_output(IOMUX_TO_GPIO(MX51_PIN_NANDF_D8 ),0);
	gpio_set_value(gpio, 1);   
	gpio_free(IOMUX_TO_GPIO(MX51_PIN_NANDF_D8 )); 
#endif

	gpio_request(SCUMEZ_ARINC429_MR, "SCUMEZ_ARINC429_MR");   // nandf_d8  gpio 4_0
	gpio_direction_output(SCUMEZ_ARINC429_MR, 0);
	gpio_free(SCUMEZ_ARINC429_MR);

	// Arinc429 SPI2_SS1 NANDF_D12 GPIO 3_28 gpio 92   (U1200)  HI-3597PSIF-40
	gpio_request(SCUMEZ_CSPI2_SS1, "SCUMEZ_CSPI2_SS1"); // nandf_d12  gpio 3_28
	gpio_direction_output(SCUMEZ_CSPI2_SS1, 1);
	gpio_free(SCUMEZ_CSPI2_SS1);

	// Arinc429 SPI2_SS2 NANDF_D13 GPIO 3_27 gpio 91   (U1203) HI-3587PQ
	gpio_request(SCUMEZ_CSPI2_SS2, "SCUMEZ_CSPI2_SS2");  // nandf_d13  gpio 3_27
	gpio_direction_output(SCUMEZ_CSPI2_SS2, 1);
	gpio_free(SCUMEZ_CSPI2_SS2);

	// Arinc429 SPI2_SS3 NANDF_D14 GPIO 3_26 gpio 90   (U1204) HI-3587PQ
	gpio_request(SCUMEZ_CSPI2_SS3, "SCUMEZ_CSPI2_SS3");  // nandf_d14  gpio 3_26
	gpio_direction_output(SCUMEZ_CSPI2_SS3, 1);
	gpio_free(SCUMEZ_CSPI2_SS3);

	// note rev1 schematic has 29-30-31 in wrong order.  This code is correct
	// 
	// Arinc429 SPI2_FLAG1 NANDF_D11 GPIO 3_29 gpio 93   (U1200)  HI-3597PSIF-40
	gpio_request(SCUMEZ_ARINC429_FLAG1, "SCUMEZ_ARINC429_FLAG1");   // nandf_d11 gpio_3_29
	gpio_direction_input(SCUMEZ_ARINC429_FLAG1);
	gpio_free(SCUMEZ_ARINC429_FLAG1);

	// Arinc429 SPI2_FLAG2 NANDF_D10 GPIO 3_30 gpio 94   (U1203)  HI-3587PQ
	gpio_request(SCUMEZ_ARINC429_FLAG2, "SCUMEZ_ARINC429_FLAG2");   //  nandf_d10  gpio_3_30
	gpio_direction_input(SCUMEZ_ARINC429_FLAG2);
	gpio_free(SCUMEZ_ARINC429_FLAG2);

	// Arinc429 SPI2_FLAG1 NANDF_D9 GPIO 3_31 gpio 95   (U1204)  HI-3587PQ
	gpio_request(SCUMEZ_ARINC429_FLAG3, "SCUMEZ_ARINC429_FLAG3"); //nandf_d9
	gpio_direction_input(SCUMEZ_ARINC429_FLAG3);
	gpio_free(SCUMEZ_ARINC429_FLAG3);




	////////////////////////////////////////////////////////////////////////////
	/// SCU inputs (4)  system type for all platforms

	// free pins we are going to work on
	gpio_free(SCUMEZ_SYSTEM_TYPE_0);
	gpio_free(SCUMEZ_SYSTEM_TYPE_1);
	gpio_free(SCUMEZ_SYSTEM_TYPE_2);
	gpio_free(SCUMEZ_SYSTEM_TYPE_3);

	// System type 0
	gpio_request(SCUMEZ_SYSTEM_TYPE_0, "gpio4_3");
	gpio_direction_input(SCUMEZ_SYSTEM_TYPE_0);
	gpio_free(SCUMEZ_SYSTEM_TYPE_0);

	// System type 1
	gpio_request(SCUMEZ_SYSTEM_TYPE_1, "gpio4_4");
	gpio_direction_input(SCUMEZ_SYSTEM_TYPE_1);
	gpio_free(SCUMEZ_SYSTEM_TYPE_1);

	// System type 2
	gpio_request(SCUMEZ_SYSTEM_TYPE_2, "gpio4_5");
	gpio_direction_input(SCUMEZ_SYSTEM_TYPE_2);
	gpio_free(SCUMEZ_SYSTEM_TYPE_2);

	// System type 3
	gpio_request(SCUMEZ_SYSTEM_TYPE_3, "gpio4_6");
	gpio_direction_input(SCUMEZ_SYSTEM_TYPE_3);
	gpio_free(SCUMEZ_SYSTEM_TYPE_3);

	/* audio_clk_en_b */
	gpio_request(SCUMEZ_AUDIO_CLK_EN, "audio-clk-en");
	gpio_direction_output(SCUMEZ_AUDIO_CLK_EN, 1);

#ifdef IMS_LOG
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
#endif
}


// Proc Entries
struct proc_dir_entry *rave_proc_dir;

static int _reset_reason;
static int resetReason_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	switch (_reset_reason) {
	case 0x09:
		len += sprintf(buf+len, "Reset reason: User\n");
		break;
	case 0x01:
		len += sprintf(buf+len, "Reset reason: Power-on\n");
		break;
	case 0x10:
	case 0x11:
		len += sprintf(buf+len, "Reset reason: WDOG\n");
		break;
	default:
		len += sprintf(buf+len, "Reset reason unknown: 0x%x\n", _reset_reason);
		break;
	}

	*eof = 1;
	return len;
}


/*!
 * Board specific initialization.
 */
static void __init mxc_board_init(void)
{
	void    *memptr;

	mxc_ipu_data.di_clk[0] = clk_get(NULL, "ipu_di0_clk");
	mxc_ipu_data.di_clk[1] = clk_get(NULL, "ipu_di1_clk");
	mxc_ipu_data.csi_clk[0] = clk_get(NULL, "csi_mclk1");
	mxc_ipu_data.csi_clk[1] = clk_get(NULL, "csi_mclk2");

	/* SD card detect irqs */
 
	// SCU has no SD card detect GPIOs.  SD1 is a fixed part.  SD4 is a
	// micro footprint SD card.  Card present is assumed, there is no hardware sense
	// for card present.
	// write protect is not implemented for micro sd cards in hardware either so
	// no gpio for that either.

	mxc_cpu_common_init();
	mx51_babbage_io_init();

	mxc_register_device(&mxc_dma_device, NULL);
	mxc_register_device(&mxc_wdt_device, NULL);
	mxc_register_device(&mxcspi1_device, &mxcspi1_data);
#ifndef CONFIG_HOLT_ARINC
	mxc_register_device(&mxcspi2_device, &mxcspi2_data);
//#else
//        platform_device_register(&spi_gpio_device);
#endif
	mxc_register_device(&mxci2c_devices[1], &mxci2c_data);
	mxc_register_device(&mxc_rtc_device, NULL);
//	mxc_register_device(&mxc_w1_master_device, &mxc_w1_data);
	mxc_register_device(&mxc_ipu_device, &mxc_ipu_data);
	mxc_register_device(&mxc_tve_device, &tve_data);
	mxc_register_device(&mxcvpu_device, &mxc_vpu_data);
	mxc_register_device(&gpu_device, NULL);
	mxc_register_device(&mxcscc_device, NULL);
	mxc_register_device(&mx51_lpmode_device, NULL);
	mxc_register_device(&busfreq_device, &bus_freq_data);
	mxc_register_device(&sdram_autogating_device, NULL);
	mxc_register_device(&mxc_dvfs_core_device, &dvfs_core_data);
	mxc_register_device(&mxc_dvfs_per_device, &dvfs_per_data);
	mxc_register_device(&mxc_iim_device, &iim_data);
	mxc_register_device(&mxc_pwm1_device, NULL);

	mxc_register_device(&mxcsdhc1_device, &mmc1_data);
	mxc_register_device(&mxcsdhc4_device, &mmc4_data);

	mxc_register_device(&mxc_ssi1_device, NULL);
	mxc_register_device(&mxc_ssi2_device, NULL);
	mxc_register_device(&mxc_ssi3_device, NULL);
	mxc_register_device(&mxc_fec_device, NULL);
	mxc_register_device(&mxc_v4l2_device, NULL);
	mxc_register_device(&mxc_v4l2out_device, NULL);

	mx51_babbage_init_mc13892();

	i2c_register_board_info(1, mxc_i2c1_board_info,
		ARRAY_SIZE(mxc_i2c1_board_info));

	pm_power_off = mxc_power_off;

	{
		// check returns from some calls on SCU and print diag messages
#ifndef CONFIG_HOLT_ARINC
		int discard;
		discard = spi_register_board_info(board_arinc1_board_info, ARRAY_SIZE(board_arinc1_board_info) );
		printk(KERN_INFO "spi_register_board_info %8.8X", discard);
		discard = spi_register_board_info(board_arinc2_board_info, ARRAY_SIZE(board_arinc2_board_info) );
		printk(KERN_INFO "spi_register_board_info %8.8X", discard);
		discard = spi_register_board_info(board_arinc3_board_info, ARRAY_SIZE(board_arinc3_board_info) );
		printk(KERN_INFO "spi_register_board_info %8.8X", discard);
#else
//		spi_register_board_info(arinc_board_info, ARRAY_SIZE(arinc_board_info) );
		platform_device_register( &arinc_spi_device );
#endif
	}
#ifdef SCUMEZ_AUDIO
//	gpio_request(SCUMEZ_AUDAMP_STBY, "audioamp-stdby");
//	gpio_direction_output(SCUMEZ_AUDAMP_STBY, 0);
	mxc_register_device(&mxc_sgtl5000_device, &sgtl5000_data);
#endif

	/* SCU has no USB init for now */

	memptr = ioremap( MX51_SRC_BASE_ADDR, 0x1000 );
	_reset_reason = readl(memptr + 0x8);
	iounmap( memptr );

	{
		rave_proc_dir = proc_mkdir( "rave", NULL );
		create_proc_read_entry( "reset_reason",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				resetReason_proc,
				NULL );		/* client data */

		create_proc_read_entry( "board_type",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				boardType_proc,
				NULL );		/* client data */
	}
}

static void __init mx51_babbage_timer_init(void)
{
	struct clk *uart_clk;

	/* Change the CPU voltages for TO2*/
	if (mx51_revision() == IMX_CHIP_REVISION_2_0) {
		cpu_wp_auto[0].cpu_voltage = 1175000;
		cpu_wp_auto[1].cpu_voltage = 1100000;
		cpu_wp_auto[2].cpu_voltage = 1000000;
	}

	mx51_clocks_init(32768, 24000000, 22579200, 24576000);

	uart_clk = clk_get_sys("mxcintuart.0", NULL);
	early_console_setup(UART1_BASE_ADDR, uart_clk);
}

static struct sys_timer mxc_timer = {
	.init	= mx51_babbage_timer_init,
};

/*
 * The following uses standard kernel macros define in arch.h in order to
 * initialize __mach_desc_MX51_BABBAGE data structure.
 */
/* *INDENT-OFF* */
MACHINE_START(MX51_BABBAGE, "Freescale MX51 Babbage Board")
	/* Maintainer: Freescale Semiconductor, Inc. */
	.phys_io	= AIPS1_BASE_ADDR,
	.io_pg_offst	= ((AIPS1_BASE_ADDR_VIRT) >> 18) & 0xfffc,
	.fixup = fixup_mxc_board,
	.map_io = mx5_map_io,
	.init_irq = mx5_init_irq,
	.init_machine = mxc_board_init,
	.timer = &mxc_timer,
MACHINE_END
