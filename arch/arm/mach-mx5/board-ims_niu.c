/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/slab.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/i2c.h>
#include <linux/i2c/qt602240_ts.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/i2c/at24.h>
#include <linux/hwmon.h>
#include <linux/pwm_backlight.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/keypad.h>
#include <asm/mach/flash.h>
#include <mach/gpio.h>
#include <mach/mmc.h>
#include <mach/mxc_dvfs.h>
#include <mach/mxc_edid.h>
#include <mach/iomux-NIU.h>
#include <mach/i2c.h>
#include <mach/mx51.h>
#include <mach/mxc_iim.h>
#include <linux/proc_fs.h>
#include <linux/phy.h>
#include <linux/fec.h>
#include <linux/spi/liufpga.h>

#include "devices.h"
#include "crm_regs.h"
#include "usb.h"

#ifdef CONFIG_MARVELL_M88E6161
#include <net/marvellswitch_platform.h>
#endif

/*!
 * @file mach-mx51/mx51_babbage.c
 *
 * @brief This file contains the board specific initialization routines.
 *
 * The NIU board file is to support the NI Platform.
 *  The NI platform is a baseboard that can/will have many different
 *  interface boards attached. Each new interface board will
 *  make the system into a new LRU.
 *
 * @ingroup MSL_MX51
 */

// enable various log messages in this module
//#define IMS_LOG
//#define IMS_LOG_PINS
//#define SCUMEZ_AUDIO
//#define SCUMEZ_RS485GPIO

// GPIO 1
#define NI_HOST_INTERRUPT_B        (0*32 + 2)  // GPIO_1_2
#define NI_PWM_OUT                 (0*32 + 3)  // GPIO_1_3
#define NI_WDOG_B_GPIO1_4          (0*32 + 4)  // GPIO_1_4
#define NI_INT_FROM_PMIC_GPIO1_4   (0*32 + 8)  // GPIO_1_8
#define NI_MCU_I2C_SCL             (0*32 + 11) // GPIO_1_11
#define NI_MCU_I2C_SDA             (0*32 + 12) // GPIO_1_12
#define NI_PINSTRAP1               (0*32 + 13) // GPIO_1_13
#define NI_PINSTRAP2               (0*32 + 14) // GPIO_1_14
#define NI_PINSTRAP3               (0*32 + 15) // GPIO_1_15
#define NI_PINSTRAP4               (0*32 + 16) // GPIO_1_16
#define NI_PINSTRAP5               (0*32 + 17) // GPIO_1_17
#define NI_PINSTRAP6               (0*32 + 24) // GPIO_1_24
#define NI_PINSTRAP7               (0*32 + 25) // GPIO_1_25


// GPIO 2
#define NI_GPIO_2_27               (1*32 + 27) // GPIO_2_27
#define NI_GPIO_2_28               (1*32 + 28) // GPIO_2_28
#define NI_GPIO_2_29               (1*32 + 29) // GPIO_2_29
#define NI_GPIO_2_30               (1*32 + 30) // GPIO_2_30


// GPIO 3
#define NI_ADC1                    (2*32 + 5)  // GPIO_3_5
#define NI_ADC2                    (2*32 + 6)  // GPIO_3_6
#define NI_ADC3                    (2*32 + 7)  // GPIO_3_7
#define NI_ADC4                    (2*32 + 8)  // GPIO_3_8
#define NI_CTRL0                   (2*32 + 12) // GPIO_3_12
#define NI_CTRL1                   (2*32 + 13) // GPIO_3_13
#define NI_CTRL2                   (2*32 + 14) // GPIO_3_14
#define NI_CTRL3                   (2*32 + 15) // GPIO_3_15


// GPIO 4
#define NI_SYSTEM_TYPE_0           (3*32 + 3)  // GPIO_4_3
#define NI_SYSTEM_TYPE_1           (3*32 + 4)  // GPIO_4_4
#define NI_SYSTEM_TYPE_2           (3*32 + 5)  // GPIO_4_5
#define NI_SYSTEM_TYPE_3           (3*32 + 6)  // GPIO_4_6
#define NI_SD4_CD                  (3*32 + 8)  // GPIO_4_8
#define NI_CTRL4                   (3*32 + 9)  // GPIO_4_9
#define NI_INT0                    (3*32 + 10) // GPIO_4_10
#define NI_INT1                    (3*32 + 11) // GPIO_4_11
#define NI_INT2                    (3*32 + 12) // GPIO_4_12
#define NI_INT3                    (3*32 + 13) // GPIO_4_13
#define NI_INT4                    (3*32 + 14) // GPIO_4_14
#define NI_INT5                    (3*32 + 15) // GPIO_4_15
#define NI_SW_INT                  (3*32 + 20) // GPIO_4_20
#define NI_SW_RESET                (3*32 + 26) // GPIO_4_26


#define NI_CSP1_SS0_GPIO           (3*32 + 24) // GPIO_4_24
#define NI_CSP1_SS1_GPIO           (3*32 + 24) // GPIO_4_24
#define NI_CSP1_SS2_GPIO           (3*32 + 24) // GPIO_4_24
#define NI_CSP1_SS3_GPIO           (3*32 + 24) // GPIO_4_24


extern int __init mx51_babbage_init_mc13892(void);
extern struct cpu_wp *(*get_cpu_wp)(int *wp);
extern void (*set_num_cpu_wp)(int num);
extern struct dvfs_wp *(*get_dvfs_core_wp)(int *wp);

static int num_cpu_wp;

void populateMainNpInfo(struct memory_accessor *mem_accessor, void *context);
void populateInterfaceNpInfo(struct memory_accessor *mem_accessor, void *context);

/*static struct pad_desc mx51babbage_gpio_pads[] =
{
	// Discrete gpio pins
	MX51_PAD_USBH1_CLK__GPIO_1_25,      // ok SCU Primary_SCU#_c gpio1_25 (in)  25
	MX51_PAD_USBH1_DIR__GPIO_1_26,      // ok SCU Discrete Out 1 gpio1_26 (out)
	MX51_PAD_USBH1_STP__GPIO_1_27,      // ok SCU discrete out 2 gpio1_27 (out)
	MX51_PAD_USBH1_NXT__GPIO_1_28,      // ok SCU discrete out 3 gpio1_28 (out)
	MX51_PAD_USBH1_DATA0__GPIO_1_11,    // ok SCU discrete in 1  gpio1_11 (in)
	MX51_PAD_USBH1_DATA1__GPIO_1_12,    // ok SCU discrete in 2  gpio1_12 (in)
	MX51_PAD_USBH1_DATA2__GPIO_1_13,    // ok SCU discrete in 3  gpio1_13 (in)
	MX51_PAD_USBH1_DATA3__GPIO_1_14,    // ok SCU discrete in 4  gpio1_14 (in)
	MX51_PAD_USBH1_DATA4__GPIO_1_15,    // ok SCU discrete in 5  gpio1_15 (in)
	MX51_PAD_USBH1_DATA5__GPIO_1_16,    // ok SCU discrete in 6  gpio1_16 (in)
	MX51_PAD_USBH1_DATA6__GPIO_1_17,    // ok SCU SCU_Discable#_c  gpio1_11 (in)
	MX51_PAD_USBH1_DATA7__GPIO_1_18,    // ok SCU SCU_Active      gpio1_18  (out)
    
};*/

static struct pad_desc mx51babbage_pads[] = {

        // EIM Bus
        MX51_PAD_EIM_DA0__EIM_DA0,
        MX51_PAD_EIM_DA1__EIM_DA1,
        MX51_PAD_EIM_DA2__EIM_DA2,
        MX51_PAD_EIM_DA3__EIM_DA3,
        MX51_PAD_EIM_DA4__EIM_DA4,
        MX51_PAD_EIM_DA5__EIM_DA5,
        MX51_PAD_EIM_DA6__EIM_DA6,
        MX51_PAD_EIM_DA7__EIM_DA7,
        MX51_PAD_EIM_DA8__EIM_DA8,
        MX51_PAD_EIM_DA9__EIM_DA9,
        MX51_PAD_EIM_DA10__EIM_DA10,
        MX51_PAD_EIM_DA11__EIM_DA11,
        MX51_PAD_EIM_DA12__EIM_DA12,
        MX51_PAD_EIM_DA13__EIM_DA13,
        MX51_PAD_EIM_DA14__EIM_DA14,
        MX51_PAD_EIM_DA15__EIM_DA15,
        MX51_PAD_EIM_D16__EIM_D16,
        MX51_PAD_EIM_D17__EIM_D17,
        MX51_PAD_EIM_D18__EIM_D18,
        MX51_PAD_EIM_D19__EIM_D19,
        MX51_PAD_EIM_D20__EIM_D20,
        MX51_PAD_EIM_D21__EIM_D21,
        MX51_PAD_EIM_D22__EIM_D22,
        MX51_PAD_EIM_D23__EIM_D23,
        MX51_PAD_EIM_D24__EIM_D24,
        MX51_PAD_EIM_D25__EIM_D25,
        MX51_PAD_EIM_D26__EIM_D26,
        MX51_PAD_EIM_D27__EIM_D27,
        MX51_PAD_EIM_D28__EIM_D28,
        MX51_PAD_EIM_D29__EIM_D29,
        MX51_PAD_EIM_D30__EIM_D30,
        MX51_PAD_EIM_D31__EIM_D31,
        MX51_PAD_EIM_A16__EIM_A16,
        MX51_PAD_EIM_A17__EIM_A17,
        MX51_PAD_EIM_A18__EIM_A18,
        MX51_PAD_EIM_A19__EIM_A19,
        MX51_PAD_EIM_A20__EIM_A20,
        MX51_PAD_EIM_A21__EIM_A21,
        MX51_PAD_EIM_A22__EIM_A22,
        MX51_PAD_EIM_A23__EIM_A23,
        MX51_PAD_EIM_A25__EIM_A24,
        MX51_PAD_EIM_A24__EIM_A25,
        MX51_PAD_EIM_A26__EIM_A26,
        MX51_PAD_EIM_EB0__EIM_EB0,
        MX51_PAD_EIM_EB1__EIM_EB1,
        MX51_PAD_EIM_OE__EIM_OE,
        MX51_PAD_EIM_CS0__EIM_CS0,
        MX51_PAD_EIM_CS1__EIM_CS1,

        // EIM as Testpoint
        MX51_PAD_EIM_A27__GPIO_2_21,

        // EIM as GPIO
        MX51_PAD_EIM_CS2__GPIO_2_27,
        MX51_PAD_EIM_CS3__GPIO_2_28,
        MX51_PAD_EIM_CS4__GPIO_2_29,
        MX51_PAD_EIM_CS5__GPIO_2_30,
        MX51_PAD_EIM_DTACK__GPIO_2_31,

        // NANDF (NVCC_NDNANDF_C) as GPIO
        MX51_PAD_NANDF_D0__GPIO_4_8,
        MX51_PAD_NANDF_D2__GPIO_4_6,
        MX51_PAD_NANDF_D3__GPIO_4_5,
        MX51_PAD_NANDF_D4__GPIO_4_4,
        MX51_PAD_NANDF_D5__GPIO_4_3,


        // CSI as GPIO
        MX51_PAD_CSI1_D8__GPIO_3_12,
        MX51_PAD_CSI1_D9__GPIO_3_13,
        MX51_PAD_CSI1_VSYNC__GPIO_3_14,
        MX51_PAD_CSI1_HSYNC__GPIO_3_15,
        MX51_PAD_CSI2_D12__GPIO_4_9,
        MX51_PAD_CSI2_D13__GPIO_4_10,
        MX51_PAD_CSI2_D18__GPIO_4_11,
        MX51_PAD_CSI2_D19__GPIO_4_12,
        MX51_PAD_CSI2_VSYNC__GPIO_4_13,
        MX51_PAD_CSI2_HSYNC__GPIO_4_14,
        MX51_PAD_CSI2_PIXCLK__GPIO_4_15,

        // Audio as GPIO
        MX51_PAD_AUD3_BB_CK__GPIO4_20,
        MX51_PAD_AUD3_BB_FS__GPIO4_21,

        // CSPI1
        MX51_PAD_CSPI1_MOSI__CSPI1_MOSI,
        MX51_PAD_CSPI1_MISO__CSPI1_MISO,
        MX51_PAD_CSPI1_SS0__CSPI1_SS0,
        MX51_PAD_CSPI1_SS1__CSPI1_SS1,
        MX51_PAD_DI1_PIN11__CSPI1_SS2,
        MX51_PAD_USBH1_DATA7__CSPI1_SS3,
        MX51_PAD_CSPI1_SCLK__CSPI1_SCLK,
        MX51_PAD_CSPI1_RDY__GPIO_4_26,

        // CSPI2
        MX51_PAD_NANDF_RB2__CSPI2_SCLK,
        MX51_PAD_NANDF_RB3__CSPI2_MISO,
        MX51_PAD_NANDF_D15__CSPI2_MOSI,
	    MX51_PAD_NANDF_RDY_INT__CSPI2_SS0,
        MX51_PAD_NANDF_D12__CSPI2_SS1,
        MX51_PAD_NANDF_D13__CSPI2_SS2,
        MX51_PAD_NANDF_D14__CSPI2_SS3,

        // UART1
        MX51_PAD_UART1_RXD__UART1_RXD,
        MX51_PAD_UART1_TXD__UART1_TXD,

        // UART2
        MX51_PAD_UART2_RXD__UART2_RXD,
        MX51_PAD_UART2_TXD__UART2_TXD,

        // UART3
        MX51_PAD_UART3_RXD__UART3_RXD,
        MX51_PAD_UART3_TXD__UART3_TXD,

        // OWIRE as GPIO
        MX51_PAD_OWIRE_LINE__GPIO_1_24,


        // I2C2
        MX51_PAD_KEY_COL4__I2C2_SCL,
        MX51_PAD_KEY_COL5__I2C2_SDA,

        // USBH1 as GPIO
        MX51_PAD_USBH1_DATA0__GPIO_1_11,
        MX51_PAD_USBH1_DATA1__GPIO_1_12,
        MX51_PAD_USBH1_CLK__GPIO_1_25,

        // USBH2 as GPIO
        MX51_PAD_USBH1_DATA2__GPIO_1_13,
        MX51_PAD_USBH1_DATA3__GPIO_1_14,
        MX51_PAD_USBH1_DATA4__GPIO_1_15,
        MX51_PAD_USBH1_DATA5__GPIO_1_16,
        MX51_PAD_USBH1_DATA6__GPIO_1_17,


        // DISPB as GPIO
        MX51_PAD_DISPB2_SER_DIN__GPIO_3_5,
        MX51_PAD_DISPB2_SER_DIO__GPIO_3_6,
        MX51_PAD_DISPB2_SER_CLK__GPIO_3_7,
        MX51_PAD_DISPB2_SER_RS__GPIO_3_8,

        // FEC
        MX51_PAD_DISP2_DAT15__FEC_TDAT0,
        MX51_PAD_DISP2_DAT6__FEC_TDAT1,
        MX51_PAD_DISP2_DAT7__FEC_TDAT2,
        MX51_PAD_DISP2_DAT8__FEC_TDAT3,
        MX51_PAD_DISP2_DAT9__FEC_TX_EN,
        MX51_PAD_DISP2_DAT10__FEC_COL,
        MX51_PAD_DISP2_DAT11__FEC_RXCLK,
        MX51_PAD_DISP2_DAT12__FEC_RX_DV,
        MX51_PAD_DISP2_DAT13__FEC_TX_CLK,
        MX51_PAD_DISP2_DAT14__FEC_RDAT0,
        MX51_PAD_DI2_DISP_CLK__FEC_RDAT1,
        MX51_PAD_DI_GP4__FEC_RDAT2,
        MX51_PAD_DISP2_DAT0__FEC_RDAT3,
        MX51_PAD_DI2_PIN2__FEC_MDC,
        MX51_PAD_DI2_PIN3__FEC_MDIO,
        MX51_PAD_DI2_PIN4__FEC_CRS,

        // SD1
        MX51_PAD_SD1_CMD__SD1_CMD,
        MX51_PAD_SD1_CLK__SD1_CLK,
        MX51_PAD_SD1_DATA0__SD1_DATA0,
        MX51_PAD_SD1_DATA1__SD1_DATA1,
        MX51_PAD_SD1_DATA2__SD1_DATA2,
        MX51_PAD_SD1_DATA3__SD1_DATA3,
        MX51_PAD_SD2_DATA0__SD2_DATA0,
        MX51_PAD_SD2_DATA1__SD2_DATA1,
        MX51_PAD_SD2_DATA2__SD2_DATA2,
        MX51_PAD_SD2_DATA3__SD2_DATA3,

        // SD4
        MX51_PAD_NANDF_CS2__SD4_CLK,
        MX51_PAD_NANDF_RB1__SD4_CMD,
        MX51_PAD_NANDF_CS3__SD4_DATA0,
        MX51_PAD_NANDF_CS4__SD4_DATA1,
        MX51_PAD_NANDF_CS5__SD4_DATA2,
        MX51_PAD_NANDF_CS6__SD4_DATA3,

        // PMIC
        MX51_PAD_PMIC_INT_REQ__PMIC_INT_REQ,

        // GPIO1
        MX51_PAD_GPIO1_2__GPIO1_2,
        MX51_PAD_GPIO1_3__GPIO1_3,
        MX51_PAD_GPIO1_4__GPIO1_4,
        MX51_PAD_GPIO1_8__GPIO1_8
};

static struct dvfs_wp dvfs_core_setpoint[] = {
	{33, 8, 33, 10, 10, 0x08},
	{26, 0, 33, 20, 10, 0x08},
	{28, 8, 33, 20, 30, 0x08},
	{29, 0, 33, 20, 10, 0x08},
};

/* working point(wp): 0 - 800MHz; 1 - 166.25MHz; */
static struct cpu_wp cpu_wp_auto[] = {
	{
	 .pll_rate = 1000000000,
	 .cpu_rate = 1000000000,
	 .pdf = 0,
	 .mfi = 10,
	 .mfd = 11,
	 .mfn = 5,
	 .cpu_podf = 0,
	 .cpu_voltage = 1175000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 800000000,
	 .pdf = 0,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 0,
	 .cpu_voltage = 1100000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 166250000,
	 .cpu_podf = 4,
	 .cpu_voltage = 850000,},
};


struct cpu_wp *mx51_babbage_get_cpu_wp(int *wp)
{
	*wp = num_cpu_wp;
	return cpu_wp_auto;
}

void mx51_babbage_set_num_cpu_wp(int num)
{
	num_cpu_wp = num;
	return;
}

//static struct mxc_w1_config mxc_w1_data = {
//	.search_rom_accelerator = 1,
//};

static struct dvfs_wp *mx51_babbage_get_dvfs_core_table(int *wp)
{
	*wp = ARRAY_SIZE(dvfs_core_setpoint);
	return dvfs_core_setpoint;
}


/* workaround for ecspi chipselect pin may not keep correct level when idle */
static void mx51_babbage_gpio_spi_chipselect_active(int cspi_mode, int status,
                         int chipselect)
{
    switch (cspi_mode) {
    case 1:
        switch (chipselect) {
        case 0x1:
        {
            struct pad_desc cspi1_ss0 = MX51_PAD_CSPI1_SS0__CSPI1_SS0;

            mxc_iomux_v3_setup_pad(&cspi1_ss0);
            break;
        }
        case 0x2:
        {
            struct pad_desc cspi1_ss0_gpio = MX51_PAD_CSPI1_SS0__GPIO_4_24;

            mxc_iomux_v3_setup_pad(&cspi1_ss0_gpio);
            gpio_request(NI_CSP1_SS0_GPIO, "cspi1-gpio");
            gpio_direction_output(NI_CSP1_SS0_GPIO, 0);
            gpio_set_value(NI_CSP1_SS0_GPIO, 1 & (~status));
            break;
        }
        default:
            break;
        }
        break;
    case 2:
        break;
    case 3:
        break;
    default:
        break;
    }
}

static void mx51_babbage_gpio_spi_chipselect_inactive(int cspi_mode, int status,
                           int chipselect)
{
    switch (cspi_mode) {
    case 1:
        switch (chipselect) {
        case 0x1:
            break;
        case 0x2:
            gpio_free(NI_CSP1_SS0_GPIO);
            break;

        default:
            break;
        }
        break;
    case 2:
        break;
    case 3:
        break;
    default:
        break;
    }
}


// eCSPI1
static struct mxc_spi_master mxcspi1_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active = mx51_babbage_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_babbage_gpio_spi_chipselect_inactive
};


// CSPI2
static struct mxc_spi_master mxcspi2_data = {
	.maxchipselect = 4,
	.spi_version = 23,
};



#if 0 // use gpio for CSPI2
#include <linux/spi/spi_gpio.h>

static struct spi_gpio_platform_data spi_bitbang_data = {
        .sck =  SCUMEZ_SPI2_SCLK,
        .mosi = SCUMEZ_SPI2_MOSI,
	.miso = SCUMEZ_SPI2_MISO,
	.num_chipselect = 4,	// only assigned is CSI2_HSYNC
};

static struct platform_device spi_gpio_device = {
        .name = "spi_gpio",
        .id = 2,
        .dev = {
                .platform_data = &spi_bitbang_data,
        },
};
#endif


// Set the I2C bit rate
static struct imxi2c_platform_data mxci2c_data = {
	.bitrate = 50000,
};


static struct mxc_dvfs_platform_data dvfs_core_data = {
	.reg_id = "SW1",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.gpc_vcr_offset = MXC_GPC_VCR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 30,
};

static struct mxc_bus_freq_platform_data bus_freq_data = {
	.gp_reg_id = "SW1",
	.lp_reg_id = "SW2",
};


static struct mxc_dvfsper_data dvfs_per_data = {
	.reg_id = "SW2",
	.clk_id = "gpc_dvfs_clk",
	.gpc_cntr_reg_addr = MXC_GPC_CNTR,
	.gpc_vcr_reg_addr = MXC_GPC_VCR,
	.gpc_adu = 0x0,
	.vai_mask = MXC_DVFSPMCR0_FSVAI_MASK,
	.vai_offset = MXC_DVFSPMCR0_FSVAI_OFFSET,
	.dvfs_enable_bit = MXC_DVFSPMCR0_DVFEN,
	.irq_mask = MXC_DVFSPMCR0_FSVAIM,
	.div3_offset = 0,
	.div3_mask = 0x7,
	.div3_div = 2,
	.lp_high = 1250000,
	.lp_low = 1250000,
};

static void mxc_iim_enable_fuse(void)
{
	u32 reg;

	if (!ccm_base)
		return;
	/* Enable fuse blown */
	reg = readl(ccm_base + 0x64);
	reg |= 0x10;
	writel(reg, ccm_base + 0x64);
}

static void mxc_iim_disable_fuse(void)
{
	u32 reg;

	/* Disable fuse blown */
	if (!ccm_base)
		return;

	reg = readl(ccm_base + 0x64);
	reg &= ~0x10;
	writel(reg, ccm_base + 0x64);
}

static struct mxc_iim_data iim_data = {
	.bank_start = MXC_IIM_MX51_BANK_START_ADDR,
	.bank_end   = MXC_IIM_MX51_BANK_END_ADDR,
	.enable_fuse = mxc_iim_enable_fuse,
	.disable_fuse = mxc_iim_disable_fuse,
};

//-------------------------------
// I2C Device Setup
// ------------------------------

// I2C Bitbang

#include <linux/i2c-gpio.h>

// 3 I2C2 Devices On Main Board
// EEPROM: 		AT24C04
// RTC:    		DS1341U+
// Temp Sensor:	LM75

// I2C2 Devices on the Interface Board
// EEPROM:			CAT24C04
// Temp & Voltage Sensor:	ADT7411ARQZ


typedef enum {
	I2C2_BOARDINFOIDX_LM75_MAIN = 0,
	I2C2_BOARDINFOIDX_ADT7411_INTERFACE,	
	I2C2_BOARDINFOIDX_AT24C04_MAIN,
	I2C2_BOARDINFOIDX_CAT24C04_INTERFACE,
	I2C2_BOARDINFOIDX_DS1341_MAIN,
} I2C2_devices_E;

// AT24C04 Data structure
struct at24_platform_data at24c04_mainBoard_data = {
	.byte_len  	= 512,
    .page_size 	= 16,
    .flags		= 0,
    .setup		= populateMainNpInfo,
};

struct at24_platform_data at24c04_interfaceBoard_data = {
	.byte_len  	= 512,
    .page_size 	= 16,
    .flags		= 0,
    .setup      = populateInterfaceNpInfo,
};

//ADT7411ARQZ

static struct i2c_board_info mxc_i2c2_board_info[] __initdata = {
	{
	    .type = "lm75",
	    .addr = 0x48,
	},	
	{
		.type = "adt7411",
		.addr = 0x4A,
	},
	{
		.type = "24c04",
		.addr = 0x50,
		.platform_data = &at24c04_mainBoard_data,
	},
	{
		.type = "24c04",
		.addr = 0x54,
		.platform_data = &at24c04_interfaceBoard_data,
	},
	{
		.type = "ds1341",
		.addr = 0x68,
	},
};


// ----------------------
// SPI Device Setup
// ----------------------

static struct mtd_partition mxc_spi_nor_partitions[] = {
    {
     .name = "NOR-config",
     .offset = 0,
     .size = 0x000000400,},
    {
     .name = "NOR-bootloader",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x0000FFC00,},
    {
     .name = "NOR-kernel",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x00300000,},
    {
     .name = "NOR-rootfs",
     .offset = MTDPART_OFS_APPEND,
     .size = MTDPART_SIZ_FULL,},
};

static struct mtd_partition mxc_dataflash_partitions[] = {
    {
     .name = "NOR-config",
     .offset = 0,
     .size = 0x000000400,},
    {
     .name = "NOR-bootloader",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x0000FFC00,},
    {
     .name = "NOR-kernel",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x00300000,},
    {
     .name = "NOR-rootfs",
     .offset = MTDPART_OFS_APPEND,
     .size = MTDPART_SIZ_FULL,},
};

static struct mtd_partition mxc_epcs16_partitions[] = {
    {
     .name = "NOR-fpga",
     .offset = 0,
     .size = MTDPART_SIZ_FULL,}
};

static struct flash_platform_data mxc_spi_flash_data[] = {
    {
     .name = "mxc_spi_nor",
     .parts = mxc_spi_nor_partitions,
     .nr_parts = ARRAY_SIZE(mxc_spi_nor_partitions),
     .type = "sst25vf016b",},
    {
     .name = "mxc_dataflash",
     .parts = mxc_dataflash_partitions,
     .nr_parts = ARRAY_SIZE(mxc_dataflash_partitions),
     .type = "at45db642d",},
    {
     .name = "fpga_epcs_flash",
     .parts = mxc_epcs16_partitions,
     .nr_parts = ARRAY_SIZE(mxc_epcs16_partitions),
     .type = "m25p16",}
};


static struct FpgaLiuPlatformData liu_fpga_platform_data = {
     .fpgaSpiGpio = NI_GPIO_2_27,
};


static struct spi_board_info mxc_dataflash_device[] __initdata = {
    {
     .modalias = "mxc_dataflash",
     .max_speed_hz = 25000000,  /* max spi clock (SCK) speed in HZ */
     .bus_num = 1,
     .chip_select = 1,
     .platform_data = &mxc_spi_flash_data[1],},
    {
     .modalias = "m25p80",
     .max_speed_hz = 15000000,  /* max spi clock (SCK) speed in HZ */
     .bus_num = 2,
     .chip_select = 0,
     .platform_data = &mxc_spi_flash_data[2],},
};


static struct spi_board_info liu_fpga_device[] __initdata = {
    {
     .modalias = "liufpga",
     .max_speed_hz = 3000000,  /* max spi clock (SCK) speed in HZ */
     .bus_num = 2,
     .chip_select = 1,
     .mode = SPI_MODE_0,
     .platform_data = &liu_fpga_platform_data,},
};


// --------------------------
// SDHC Device Setup
// --------------------------

static int sdhc_write_protect(struct device *dev)
{
	unsigned short rc = 0;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is never write protected.)
		rc = 0;
		break;

	case 1:
		// SD-2 (UNUSED)
		rc = 0;
		break;

	case 2:
		// SD-3 (UNUSED)
		rc = 0;
		break;
	case 3:
		// SD-4 (uSD Card Slot)
		// No Write Protect Pin
		rc = 0;
		break;
	}
	return rc;
}

static unsigned int sdhc_get_card_det_status(struct device *dev)
{
	int ret;

	ret = 1;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is always present.)
		ret = 0;
		break;

	case 1:
		// SD-2 (UNUSED)
		break;

	case 2:
		// SD-3 (UNUSED)
		break;
	case 3:
		// SD-4 (uSD Card Slot)
		ret = gpio_get_value(NI_SD4_CD);
		break;
	}
	return ret;
}

static struct mxc_mmc_platform_data mmc1_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	    MMC_VDD_31_32,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 52000000,
	.card_inserted_state = 1,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
	.power_mmc = NULL,
};

// SCU uses microd SD so card inserted state is always 1, inserted state
static struct mxc_mmc_platform_data mmc4_data = {
	// %%jws%% use same as mmc1 IMS_DEBUG: try 3.3 
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
		MMC_VDD_31_32,
//	.ocr_mask = MMC_VDD_32_33,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 50000000,
	.card_inserted_state = 1, 
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
};



// -----------------------------
// SWITCH
// -----------------------------
static struct fec_platform_data fec_data = {
	.phyless	= 1,
	.phy		= PHY_INTERFACE_MODE_MII,
};

#include <net/dsa.h>

static struct dsa_chip_data marvell_switch_chip_data = {
        .mii_bus = &mxc_fec_device.dev,
        .port_names[0]	= "netleft",
        .port_names[1]	= "netright",
        .port_names[2]	= "port1",
        .port_names[3]	= "port2",
        .port_names[4]	= "port3",
        .port_names[5]	= "port4",
        .port_names[6]  = "cpu"
};

static struct dsa_platform_data switch_data = {
        .netdev = &mxc_fec_device.dev,
        .nr_chips = 1,
        .chip = &marvell_switch_chip_data,
        .hw_reset_gpio = NI_SW_RESET,
        .hw_reset_polarity = RESET_ACTIVE_LOW,
};

static struct resource dsa_irq = {
	.start = gpio_to_irq(NI_SW_INT),
	.end = gpio_to_irq(NI_SW_INT),
	.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_LOWLEVEL,
};

static struct platform_device dsa_device = {
        .name = "dsa",
        .dev = {
                .platform_data = &switch_data,
        },
	.num_resources = 1,
	.resource = &dsa_irq,
};

static void setup_ethernet_switch(void)
{
	platform_device_register(&dsa_device);
}



// ---------------------------
// System Type
// ---------------------------
struct gpio systemType_gpio[] = {
	{ .gpio = NI_SYSTEM_TYPE_3, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_3", },
	{ .gpio = NI_SYSTEM_TYPE_2, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_2", },
	{ .gpio = NI_SYSTEM_TYPE_1, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_1", },
	{ .gpio = NI_SYSTEM_TYPE_0, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_0", },
};

// these definitions should be moved somewhere board specific that makes them
// available to anyone who might call IMSSystemType
#define SYSTEM_TYPE_MASK	0x3
#define	RDU_REV_B	(0b0000 & SYSTEM_TYPE_MASK)
#define	RDU_REV_C	(0b1101 & SYSTEM_TYPE_MASK)
#define SCU_MEZZ	(0b0010 & SYSTEM_TYPE_MASK)
#define NI_REV  	(0b0011 & SYSTEM_TYPE_MASK)

static char *systemType_toStr( int type )
{
	switch ( type ) {
	case RDU_REV_B: return "RDU Rev B";
	case RDU_REV_C: return "RDU Rev C";
	case SCU_MEZZ: 	return "SCU Mezz";
	case NI_REV:	return "NI";
	default: 	return "Unknown";
	}
}

int IMSSystemType( void )
{
	static int type = -1;
	if ( type == -1 ) {
		type =	(gpio_get_value( NI_SYSTEM_TYPE_3 ) << 3 ) |
			    (gpio_get_value( NI_SYSTEM_TYPE_2 ) << 2 ) |
			    (gpio_get_value( NI_SYSTEM_TYPE_1 ) << 1 ) |
			    (gpio_get_value( NI_SYSTEM_TYPE_0 ) << 0 );

		// NOTE: masking off the top bits because of varying configurations in populated resistors.
		type &= SYSTEM_TYPE_MASK;

		printk(KERN_INFO"%s: detected board type %s(%02x)\n", __func__, systemType_toStr( type ), type);
	}
	return type;
}
EXPORT_SYMBOL(IMSSystemType);

static int boardType_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	int type = IMSSystemType();
	len += sprintf(buf+len,"%s(%02x)\n", systemType_toStr( type ), type);
	*eof = 1;
	return len;
}



/*!
 * Board specific fixup function. It is called by \b setup_arch() in
 * setup.c file very early on during kernel starts. It allows the user to
 * statically fill in the proper values for the passed-in parameters. None of
 * the parameters is used currently.
 *
 * @param  desc         pointer to \b struct \b machine_desc
 * @param  tags         pointer to \b struct \b tag
 * @param  cmdline      pointer to the command line
 * @param  mi           pointer to \b struct \b meminfo
 */
static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
	char *str;
	struct tag *t;
	struct tag *mem_tag = 0;
	int total_mem = SZ_512M;
	int left_mem = 0;
	int gpu_mem = SZ_64M;
	int fb_mem = SZ_32M;

	mxc_set_cpu_type(MXC_CPU_MX51);

	get_cpu_wp = mx51_babbage_get_cpu_wp;
	set_num_cpu_wp = mx51_babbage_set_num_cpu_wp;
	get_dvfs_core_wp = mx51_babbage_get_dvfs_core_table;
	num_cpu_wp = ARRAY_SIZE(cpu_wp_auto);

	for_each_tag(mem_tag, tags) {
		if (mem_tag->hdr.tag == ATAG_MEM) {
			total_mem = mem_tag->u.mem.size;
			left_mem = total_mem - gpu_mem - fb_mem;
			break;
		}
	}

	for_each_tag(t, tags) {
		if (t->hdr.tag == ATAG_CMDLINE) {
			str = t->u.cmdline.cmdline;
			str = strstr(str, "mem=");
			if (str != NULL) {
				str += 4;
				left_mem = memparse(str, &str);
				if (left_mem == 0 || left_mem > total_mem)
					left_mem = total_mem - gpu_mem - fb_mem;
			}

			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_memory=");
			if (str != NULL) {
				str += 11;
				gpu_mem = memparse(str, &str);
			}

			break;
		}
	}

	if (mem_tag) {
		fb_mem = total_mem - left_mem - gpu_mem;
		if (fb_mem < 0) {
			gpu_mem = total_mem - left_mem;
			fb_mem = 0;
		}
		mem_tag->u.mem.size = left_mem;

		/*reserve memory for gpu*/
		gpu_device.resource[5].start =
				mem_tag->u.mem.start + left_mem;
		gpu_device.resource[5].end =
				gpu_device.resource[5].start + gpu_mem - 1;
#if defined(CONFIG_FB_MXC_SYNC_PANEL) || \
	defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)
		if (fb_mem) {
			mxcfb_resources[0].start =
				gpu_device.resource[5].end + 1;
			mxcfb_resources[0].end =
				mxcfb_resources[0].start + fb_mem - 1;
		} else {
			mxcfb_resources[0].start = 0;
			mxcfb_resources[0].end = 0;
		}
#endif
	}
}

#define PWGT1SPIEN (1<<15)
#define PWGT2SPIEN (1<<16)
#define USEROFFSPI (1<<3)

static void mxc_power_off(void)
{
	/* We can do power down one of two ways:
	   Set the power gating
	   Set USEROFFSPI */

	/* Set the power gate bits to power down */
	pmic_write_reg(REG_POWER_MISC, (PWGT1SPIEN|PWGT2SPIEN),
		(PWGT1SPIEN|PWGT2SPIEN));
}

#if 0
void mxc_verify_gpio(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_gpio_pads, ARRAY_SIZE(mx51babbage_gpio_pads));
}

void mxc_verify_pins(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
}
#endif

static void __init mx51_babbage_io_init(void)
{
	// %%jws%% verify the pad values before setting them
    #ifdef IMS_LOG_PINS
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
    #endif

	mxc_iomux_v3_setup_multiple_pads(mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));

	// -----------------
	// GPIO 1
	// -----------------

	// Free pins we are going to work on
	gpio_free(NI_HOST_INTERRUPT_B);
	gpio_free(NI_PWM_OUT);
	gpio_free(NI_WDOG_B_GPIO1_4);
	gpio_free(NI_INT_FROM_PMIC_GPIO1_4);
	gpio_free(NI_MCU_I2C_SCL);
	gpio_free(NI_MCU_I2C_SDA);
    gpio_free(NI_PINSTRAP1);
    gpio_free(NI_PINSTRAP2);
    gpio_free(NI_PINSTRAP3);
    gpio_free(NI_PINSTRAP4);
    gpio_free(NI_PINSTRAP5);
	gpio_free(NI_PINSTRAP6);
    gpio_free(NI_PINSTRAP7);


	// Schmatic Name:   HOST_INTERRUPT_B
	// Direction:       input
	// MX51 Pin Name:   MX51_PAD_GPIO1_2__GPIO1_2
	gpio_request(NI_HOST_INTERRUPT_B, "gpio1_2");
	gpio_direction_input(NI_HOST_INTERRUPT_B);
	mxc_gpio_set_name(NI_HOST_INTERRUPT_B, "host_interrupt_n");
    gpio_export(NI_HOST_INTERRUPT_B, 0);

	// Schmatic Name:   PWM_Out
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_GPIO1_3__GPIO1_3
	gpio_request(NI_PWM_OUT, "gpio1_3");
	gpio_direction_output(NI_PWM_OUT, 0);
	gpio_free(NI_PWM_OUT);

	// Schmatic Name:   WDOG_B(GPIO1_4)
    // Direction:       output
	// Initial state:   1 (pin is active low)
    // MX51 Pin Name:   MX51_PAD_GPIO1_4__GPIO1_4
	gpio_request(NI_WDOG_B_GPIO1_4, "gpio1_4");
	gpio_direction_output(NI_WDOG_B_GPIO1_4, 1);
	gpio_free(NI_WDOG_B_GPIO1_4);

	// Schmatic Name:   INT_FROM_PMIC(GPIO1_8)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_GPIO1_8__GPIO1_8
	gpio_request(NI_INT_FROM_PMIC_GPIO1_4, "gpio1_8");
	gpio_direction_input(NI_INT_FROM_PMIC_GPIO1_4);
	gpio_free(NI_INT_FROM_PMIC_GPIO1_4);

    // Schmatic Name:   Pinstrap1(GPIO1_13)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA2__GPIO_1_13
	gpio_request(NI_PINSTRAP1, "gpio1_13");
	gpio_direction_input(NI_PINSTRAP1);
	mxc_gpio_set_name(NI_PINSTRAP1, "pinstrap1");
    gpio_export(NI_PINSTRAP1, 0);

    // Schmatic Name:   Pinstrap2(GPIO1_14)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA3__GPIO_1_14
	gpio_request(NI_PINSTRAP2, "gpio1_14");
	gpio_direction_input(NI_PINSTRAP2);
	mxc_gpio_set_name(NI_PINSTRAP2, "pinstrap2");
    gpio_export(NI_PINSTRAP2, 0);

    // Schmatic Name:   Pinstrap3(GPIO1_15)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA4__GPIO_1_15
	gpio_request(NI_PINSTRAP3, "gpio1_15");
	gpio_direction_input(NI_PINSTRAP3);
	mxc_gpio_set_name(NI_PINSTRAP3, "pinstrap3");
    gpio_export(NI_PINSTRAP3, 0);

    // Schmatic Name:   Pinstrap4(GPIO1_16)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA5__GPIO_1_16
	gpio_request(NI_PINSTRAP4, "gpio1_16");
	gpio_direction_input(NI_PINSTRAP4);
	mxc_gpio_set_name(NI_PINSTRAP4, "pinstrap4");
    gpio_export(NI_PINSTRAP4, 0);

    // Schmatic Name:   Pinstrap5(GPIO1_17)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA6__GPIO_1_17
	gpio_request(NI_PINSTRAP5, "gpio1_17");
	gpio_direction_input(NI_PINSTRAP5);
	mxc_gpio_set_name(NI_PINSTRAP5, "pinstrap5");
    gpio_export(NI_PINSTRAP5, 0);

    // Schmatic Name:   Pinstrap6(GPIO1_24)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_OWIRE_LINE__GPIO_1_24
	gpio_request(NI_PINSTRAP6, "gpio1_24");
	gpio_direction_input(NI_PINSTRAP6);
	mxc_gpio_set_name(NI_PINSTRAP6, "pinstrap6");
    gpio_export(NI_PINSTRAP6, 0);

    // Schmatic Name:   Pinstrap7(GPIO1_25)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_CLK__GPIO_1_25
	gpio_request(NI_PINSTRAP7, "gpio1_25");
	gpio_direction_input(NI_PINSTRAP7);
	mxc_gpio_set_name(NI_PINSTRAP7, "pinstrap7");
    gpio_export(NI_PINSTRAP7, 0);

    

	// -----------------
    // GPIO 2
    // -----------------

	// Free pins we are going to work on
    gpio_free(NI_GPIO_2_27);
    gpio_free(NI_GPIO_2_28);
    gpio_free(NI_GPIO_2_29);
    gpio_free(NI_GPIO_2_30);
    gpio_free(NI_INT5);


    // Schmatic Name:   GPIO_2_27
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_CS2__GPIO_2_27
    gpio_request(NI_GPIO_2_27, "gpio2_27");
    gpio_direction_input(NI_GPIO_2_27);
    gpio_free(NI_GPIO_2_27);

    // Schmatic Name:   GPIO_2_28
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_CS3__GPIO_2_28
    gpio_request(NI_GPIO_2_28, "gpio2_28");
    gpio_direction_input(NI_GPIO_2_28);
    gpio_free(NI_GPIO_2_28);

    // Schmatic Name:   GPIO_2_29 (FPGA_RESETn_1V8 on FPGA Interface Board)
    // Direction:       output
    // Initial State:   1 (reset is active low)
    // MX51 Pin Name:   MX51_PAD_EIM_CS4__GPIO_2_29
    gpio_request(NI_GPIO_2_29, "gpio2_29");
    // Hold the FPGA in reset
    gpio_direction_output(NI_GPIO_2_29, 0);
    msleep(20);
    // Pull the FPGA out of reset
    gpio_direction_output(NI_GPIO_2_29, 1);
    mxc_gpio_set_name(NI_GPIO_2_29, "fpga_reset_n");
    gpio_export(NI_GPIO_2_29, 0);

    // Schmatic Name:   GPIO_2_30 (RESET_USB_AND_UARTn_1V8 on FPGA Interface Board)
    // Direction:       output
    // Initial State:   1 (reset is active low)
    // MX51 Pin Name:   MX51_PAD_EIM_CS5__GPIO_2_30
    gpio_request(NI_GPIO_2_30, "gpio2_30");
    gpio_direction_output(NI_GPIO_2_30, 1);
    mxc_gpio_set_name(NI_GPIO_2_30, "usb_hub_reset_n");
    gpio_export(NI_GPIO_2_30, 0);

    // -----------------
    // GPIO 3
    // -----------------

    // Free pins we are going to work on
    gpio_free(NI_ADC1);
    gpio_free(NI_ADC2);
    gpio_free(NI_ADC3);
    gpio_free(NI_ADC4);

    // Schmatic Name:   ADC_1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_DISPB2_SER_DIN__GPIO_3_5
    gpio_request(NI_ADC1, "gpio3_5");
    gpio_direction_input(NI_ADC1);
    gpio_free(NI_ADC1);

    // Schmatic Name:   ADC_2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_DISPB2_SER_DIO__GPIO_3_6
    gpio_request(NI_ADC2, "gpio3_6");
    gpio_direction_input(NI_ADC2);
    gpio_free(NI_ADC2);

    // Schmatic Name:   ADC_3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_DISPB2_SER_CLK__GPIO_3_7
    gpio_request(NI_ADC3, "gpio3_7");
    gpio_direction_input(NI_ADC3);
    gpio_free(NI_ADC3);

    // Schmatic Name:   ADC_4
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_DISPB2_SER_RS__GPIO_3_8
    gpio_request(NI_ADC4, "gpio3_8");
    gpio_direction_input(NI_ADC4);
    gpio_free(NI_ADC4);

    // Schmatic Name:   CTRL0
    // Direction:       output
    // Initial State:   1
    // MX51 Pin Name:   MX51_PAD_CSI1_D8__GPIO_3_12
    gpio_request(NI_CTRL0, "gpio3_12");
    gpio_direction_output(NI_CTRL0, 1);
    mxc_gpio_set_name(NI_CTRL0, "ctrl0");
    gpio_export(NI_CTRL0, 0);

    // Schmatic Name:   CTRL1
    // Direction:       output
    // Initial State:   1
    // MX51 Pin Name:   MX51_PAD_CSI1_D9__GPIO_3_13	
    gpio_request(NI_CTRL1, "gpio3_13");
    gpio_direction_output(NI_CTRL1, 1);
    mxc_gpio_set_name(NI_CTRL1, "ctrl1");
    gpio_export(NI_CTRL1, 0);

    // Schmatic Name:   CTRL2
    // Direction:       output
    // Initial State:   1
    // MX51 Pin Name:   MX51_PAD_CSI1_VSYNC__GPIO_3_14
    gpio_request(NI_CTRL2, "gpio3_14");
    gpio_direction_output(NI_CTRL2, 1);
    mxc_gpio_set_name(NI_CTRL2, "ctrl2");
    gpio_export(NI_CTRL2, 0);

    // Schmatic Name:   CTRL3
    // Direction:       output
    // Initial State:   0 (line driver is active low)
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_FS__GPIO4_21
    gpio_request(NI_CTRL3, "gpio3_15");
    gpio_direction_output(NI_CTRL3, 1);
    mxc_gpio_set_name(NI_CTRL3, "ctrl3");
    gpio_export(NI_CTRL3, 0);


    // -----------------
    // GPIO 4
    // -----------------

    // free pins we are going to work on
    gpio_free(NI_SYSTEM_TYPE_0);
    gpio_free(NI_SYSTEM_TYPE_1);
    gpio_free(NI_SYSTEM_TYPE_2);
    gpio_free(NI_SYSTEM_TYPE_3);
    gpio_free(NI_SD4_CD);
    gpio_free(NI_INT0);
    gpio_free(NI_INT1);
    gpio_free(NI_INT2);
    gpio_free(NI_INT3);
    gpio_free(NI_INT4);
    gpio_free(NI_SW_INT);
    gpio_free(NI_SW_RESET);


    // Schmatic Name:   SYSTEM_TYPE_0
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D5__GPIO_4_3
    gpio_request(NI_SYSTEM_TYPE_0, "gpio4_3");
    gpio_direction_input(NI_SYSTEM_TYPE_0);
    gpio_free(NI_SYSTEM_TYPE_0);

    // Schmatic Name:   SYSTEM_TYPE_1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D4__GPIO_4_4
    gpio_request(NI_SYSTEM_TYPE_1, "gpio4_4");
    gpio_direction_input(NI_SYSTEM_TYPE_1);
    gpio_free(NI_SYSTEM_TYPE_1);

    // Schmatic Name:   SYSTEM_TYPE_2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D3__GPIO_4_5
    gpio_request(NI_SYSTEM_TYPE_2, "gpio4_5");
    gpio_direction_input(NI_SYSTEM_TYPE_2);
    gpio_free(NI_SYSTEM_TYPE_2);

    // Schmatic Name:   SYSTEM_TYPE_3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D2__GPIO_4_6
    gpio_request(NI_SYSTEM_TYPE_3, "gpio4_6");
    gpio_direction_input(NI_SYSTEM_TYPE_3);
    gpio_free(NI_SYSTEM_TYPE_3);


    // Schmatic Name:   SD4_CD
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D0__GPIO_4_8
    gpio_request(NI_SD4_CD, "gpio4_8");
    gpio_direction_input(NI_SD4_CD);
    gpio_free(NI_SD4_CD);

    // Schmatic Name:   CTRL4
    // Direction:       output
    // Initial State:   1
    // MX51 Pin Name:   MX51_PAD_CSI2_D12__GPIO_4_9
    gpio_request(NI_CTRL4, "gpio4_9");
    gpio_direction_output(NI_CTRL4, 1);
    mxc_gpio_set_name(NI_CTRL4, "ctrl4");
    gpio_export(NI_CTRL4, 0);


    // Schmatic Name:   INT0
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_D13__GPIO_4_10
    gpio_request(NI_INT0, "gpio4_10");
    gpio_direction_input(NI_INT0);
    mxc_gpio_set_name(NI_INT0, "int0");
    gpio_export(NI_INT0, 0);

    // Schmatic Name:   INT1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_D18__GPIO_4_11
    gpio_request(NI_INT1, "gpio4_11");
    gpio_direction_input(NI_INT1);
    mxc_gpio_set_name(NI_INT1, "int1");
    gpio_export(NI_INT1, 0);

    // Schmatic Name:   INT2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_D19__GPIO_4_12
    gpio_request(NI_INT2, "gpio4_12");
    gpio_direction_input(NI_INT2);
    mxc_gpio_set_name(NI_INT2, "int2");
    gpio_export(NI_INT2, 0);

    // Schmatic Name:   INT3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_VSYNC__GPIO_4_13
    gpio_request(NI_INT3, "gpio4_13");
    gpio_direction_input(NI_INT3);
    mxc_gpio_set_name(NI_INT3, "int3");
    gpio_export(NI_INT3, 0);

    // Schmatic Name:   INT4
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_HSYNC__GPIO_4_14
    gpio_request(NI_INT4, "gpio4_14");
    gpio_direction_input(NI_INT4);   
    mxc_gpio_set_name(NI_INT4, "int4");
    gpio_export(NI_INT4, 0);

    // Schmatic Name:   INT5
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI2_PIXCLK__GPIO_4_15
    gpio_request(NI_INT5, "gpio4_15");
    gpio_direction_input(NI_INT5);
    mxc_gpio_set_name(NI_INT5, "int5");
    gpio_export(NI_INT5, 0);

    // Schmatic Name:   SW_INT
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_CK__GPIO4_20
    gpio_request(NI_SW_INT, "gpio4_20");
    gpio_direction_input(NI_SW_INT);
    gpio_free(NI_SW_INT);

    // Schmatic Name:   SW_RESET
    // Direction:       output
    // Initial State:   1 (switch reset is active low)
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_FS__GPIO4_21
    gpio_request(NI_SW_RESET, "gpio4_21");
    gpio_direction_output(NI_SW_RESET, 1);
    gpio_free(NI_SW_RESET);


#ifdef IMS_LOG
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
#endif
}

// Proc Entries
struct proc_dir_entry *rave_proc_dir;

static int _reset_reason;
static int resetReason_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	switch (_reset_reason) {
	case 0x09:
		len += sprintf(buf+len, "Reset reason: User\n");
		break;
	case 0x01:
		len += sprintf(buf+len, "Reset reason: Power-on\n");
		break;
	case 0x10:
	case 0x11:
		len += sprintf(buf+len, "Reset reason: WDOG\n");
		break;
	default:
		len += sprintf(buf+len, "Reset reason unknown: 0x%x\n", _reset_reason);
		break;
	}

	*eof = 1;
	return len;
}

#define MAX_PART_NUM_LEN   (19 + 1)
#define MAK_SERIAL_NUM_LEN (9 + 1)
#define MAX_LRU_DOM_LEN    (6 + 1)
#define MAX_BOARD_DOM_LEN  (4 + 1)
#define MAX_REV_LEN        (4 + 1)


typedef struct __attribute__ ((packed)) {
    char            m_lruPartNumber[MAX_PART_NUM_LEN];
    char            m_lruSerialNumber[MAK_SERIAL_NUM_LEN];
    char            m_lruDateOfManufacture[MAX_LRU_DOM_LEN];
    unsigned int    m_mod;
    char            m_boardPartNumber[MAX_PART_NUM_LEN];
    char            m_boardSerialNumber[MAK_SERIAL_NUM_LEN];
    char            m_boardInitialDom[MAX_BOARD_DOM_LEN];
    char            m_boardInitialRev[MAX_REV_LEN];
    char            m_boardUpdatedDom[MAX_BOARD_DOM_LEN];
    char            m_boardUpdatedRev[MAX_REV_LEN];
} MainNameplateEeprom_S;


typedef struct __attribute__ ((packed)) {
    char        m_boardPartNumber[MAX_PART_NUM_LEN];
    char        m_boardSerialNumber[MAK_SERIAL_NUM_LEN];
    char        m_boardInitialDom[MAX_BOARD_DOM_LEN];
    char        m_boardInitialRev[MAX_REV_LEN];
    char        m_boardUpdatedDom[MAX_BOARD_DOM_LEN];
    char        m_boardUpdatedRev[MAX_REV_LEN];
    uint32_t    m_mod;
    uint8_t     m_lruIdentifier;
}  InterfaceNameplateEeprom_S;


MainNameplateEeprom_S main_np_eeprom;
InterfaceNameplateEeprom_S interface_np_eeprom;

static int populate_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
    int len = 0;
    len += sprintf(buf+len,"%s\n", (char *)data_param);
    *eof = 1;
    return len;
}


// This is the callback function when a a specifc at24 eeprom is found.
// Its reads out the eeprom contents via the read function passed back in via
// struct memory_accessor. It then calls part_number_proc, serial_number_proc,
// and dom_proc to populate the procfs entries for each specific field.
void populateMainNpInfo(struct memory_accessor *mem_accessor, void *context) {
    char modString[7] = {0};

	memset(&main_np_eeprom, 0, sizeof(MainNameplateEeprom_S));

	/* Read the Data structure from the EEPROM */
	if(mem_accessor->read( mem_accessor, (char *)&main_np_eeprom, 0, sizeof(MainNameplateEeprom_S)) <=0) {
		/* The read failed. Make sure that the displayed data is just NULL characters */
		memset(&main_np_eeprom, 0, sizeof(MainNameplateEeprom_S));
	}

	/* LRU */
	create_proc_read_entry( "part_number",
                            0,                                /* default mode */
                            rave_proc_dir,                    /* parent dir */
                            populate_proc,
                            main_np_eeprom.m_lruPartNumber ); /* client data */

    create_proc_read_entry( "serial_number",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_lruSerialNumber );

    create_proc_read_entry( "date_of_manufacture",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_lruDateOfManufacture );

    snprintf( modString, 11, "0x%08X", main_np_eeprom.m_mod );
    create_proc_read_entry( "mod",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            modString );


    /* Mian Board */
    create_proc_read_entry( "main_board_part_num",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardPartNumber );

    create_proc_read_entry( "main_board_serial_num",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardSerialNumber );

    create_proc_read_entry( "main_board_date_of_manufacture",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardInitialDom );

    create_proc_read_entry( "main_board_revision",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardInitialRev );

    create_proc_read_entry( "main_board_updated_date_of_manufacture",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardUpdatedDom );

    create_proc_read_entry( "main_board_updated_revision",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardUpdatedRev );
}


typedef enum {
    LRU_RDU_GEN_1 = 0,
    LRU_DDS_GEN_1,
    LRU_DDS_GEN_2,
    LRU_SCU_GEN_1,
    LRU_SCU_GEN_2,
    LRU_NIU,
    LRU_LIU,
    LRU_PCU_B_GEN_1,
    LRU_PCU_B_GEN_2,
    LRU_PCU_T_GEN_1,
    LRU_PCU_T_DLH,
    LRU_RPU,
    LRU_RJU_GEN_1,
    LRU_RJU_GEN_2,

    LRU_TOTAL
} RAVE_LRU_E;


// This is the callback function when a a specifc at24 eeprom is found.
// Its reads out the eeprom contents via the read function passed back in via
// struct memory_accessor. It then calls part_number_proc, serial_number_proc,
// and dom_proc to populate the procfs entries for each specific field.
void populateInterfaceNpInfo(struct memory_accessor *mem_accessor, void *context) {
    char modString[7] = {0};
    char *lruString   = NULL;

    memset(&interface_np_eeprom, 0, sizeof(InterfaceNameplateEeprom_S));

    /* Read the Data structure from the EEPROM */
    if(mem_accessor->read( mem_accessor, (char *)&interface_np_eeprom, 0, sizeof(InterfaceNameplateEeprom_S)) <=0) {
        /* The read failed. Make sure that the displayed data is just NULL characters */
        memset(&interface_np_eeprom, 0, sizeof(InterfaceNameplateEeprom_S));
    }

    /* Mian Board */
    create_proc_read_entry( "if_board_part_num",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            interface_np_eeprom.m_boardPartNumber );

    create_proc_read_entry( "if_board_serial_num",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            interface_np_eeprom.m_boardSerialNumber );

    create_proc_read_entry( "if_board_date_of_manufacture",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            interface_np_eeprom.m_boardInitialDom );

    create_proc_read_entry( "if_board_revision",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            interface_np_eeprom.m_boardInitialRev );

    create_proc_read_entry( "if_board_updated_date_of_manufacture",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            interface_np_eeprom.m_boardUpdatedDom );

    create_proc_read_entry( "if_board_updated_revision",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            main_np_eeprom.m_boardUpdatedRev );

    snprintf( modString, 11, "0x%08X", interface_np_eeprom.m_mod );
    create_proc_read_entry( "if_board_mod",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            modString );

    // Create the LRU String
    if(interface_np_eeprom.m_lruIdentifier == LRU_NIU ||
       interface_np_eeprom.m_lruIdentifier == 0xFF) {
        lruString = "NIU\0";
    }
    else if(interface_np_eeprom.m_lruIdentifier == LRU_LIU) {
        lruString = "LIU\0";
    }
    else {
        lruString = "UNK\0";
    }
    create_proc_read_entry( "lru_type",
                            0,
                            rave_proc_dir,
                            populate_proc,
                            lruString );
}



/*!
 * Board specific initialization.
 */
static void __init mxc_board_init(void)
{
	void    *memptr;
	void    __iomem *weim_base;
	u32     reg;

	/* SD card detect irqs */
 
	// NI has no SD card detect GPIOs.  SD1 is a fixed part.  SD4 is a
	// micro footprint SD card.  Card present is assumed, there is no hardware sense
	// for card present.
	// write protect is not implemented for micro sd cards in hardware either so
	// no gpio for that either.

	mxc_cpu_common_init();
	mx51_babbage_io_init();

	mxc_register_device(&mxc_dma_device, NULL);
	mxc_register_device(&mxc_wdt_device, NULL);

	// eCSPI and CSPI Ports
	mxc_register_device(&mxcspi1_device, &mxcspi1_data);
	mxc_register_device(&mxcspi2_device, &mxcspi2_data);

	mxc_register_device(&mxci2c_devices[1], &mxci2c_data);
	mxc_register_device(&mxc_rtc_device, NULL);
	mxc_register_device(&mxcscc_device, NULL);
	mxc_register_device(&mx51_lpmode_device, NULL);
	mxc_register_device(&busfreq_device, &bus_freq_data);
	mxc_register_device(&sdram_autogating_device, NULL);
	mxc_register_device(&mxc_dvfs_core_device, &dvfs_core_data);
	mxc_register_device(&mxc_dvfs_per_device, &dvfs_per_data);
	mxc_register_device(&mxc_iim_device, &iim_data);
	mxc_register_device(&mxc_pwm1_device, NULL);

	// SDHC Devices
	mxc_register_device(&mxcsdhc1_device, &mmc1_data);
	mxc_register_device(&mxcsdhc4_device, &mmc4_data);

	mxc_register_device(&mxc_ssi1_device, NULL);
	mxc_register_device(&mxc_ssi2_device, NULL);
	mxc_register_device(&mxc_ssi3_device, NULL);

	// FEC
	mxc_register_device(&mxc_fec_device, &fec_data);

	// PMIC
	mx51_babbage_init_mc13892();

	// I2C Devices
	//platform_device_register(&i2c_mcu_bitbang_device);

	i2c_register_board_info(1, mxc_i2c2_board_info,
		ARRAY_SIZE(mxc_i2c2_board_info));

	// SPI Devices
	spi_register_board_info(mxc_dataflash_device,
				ARRAY_SIZE(mxc_dataflash_device));

	spi_register_board_info(liu_fpga_device,
				ARRAY_SIZE(liu_fpga_device));

	pm_power_off = mxc_power_off;

	// USB
	mx5_usb_dr_init();	// OTG Port

	// Ethernet Drivers
	setup_ethernet_switch();

	memptr = ioremap( MX51_SRC_BASE_ADDR, 0x1000 );
	_reset_reason = readl(memptr + 0x8);
	iounmap( memptr );

	{
		rave_proc_dir = proc_mkdir( "rave", NULL );
		create_proc_read_entry( "reset_reason",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				resetReason_proc,
				NULL );		/* client data */

		create_proc_read_entry( "board_type",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				boardType_proc,
				NULL );		/* client data */
	}

        
	//--------------------------------------------------
	// WEIM Bus init
	//--------------------------------------------------
	weim_base = ioremap(MX51_WEIM_BASE_ADDR, SZ_4K);

	//--------------------------------------------------
	// CS0 timings (NIU Interface board FPGA register access) 
	//--------------------------------------------------
	writel(0x00823031, (weim_base + 0x00));	// Chip Select 0 General Configuration Register 1 (CS0GCR1)
						// PSZ   = 0b000 (Page size = 8 words per page) [ignored]
						// WP    = 0b0   (Write protect disabled)
						// GBC   = 0b000 (Gap between Chip Selects = 0)
						// AUS   = 0b1   (Address unshifted)
						// CSREC = 0b000 (0 WEIM clock min CS, OE, WE)
						// SP    = 0b0   (User mode access allowed)
						// DSZ   = 0b010 (Data Port Size = 16 bits on data[31:16]
						// BCS   = 0b00  (Burst Clock Start = 0 clock cycles additional delay)
						// BCD   = 0b011 (Burst Clock Division = Divide WEIM clock by 4)
						// WC    = 0b0   (Write Continuous = use BL value for burst length)
						// BL    = 0b000 (Burst Length = 4 words)
						// CREP  = 0b0   (Configuration Register Enable Polatiry = Active Low)
						// CRE   = 0b0   (Configuration Register Enable = Disabled)
						// RFL   = 0b1   (Read Fix Latency = Fixed)
						// WFL   = 0b1   (Write Fix Latency = Fixed)
						// MUM   = 0b0   (Mulitplex Mode = Disabled)
						// SRD   = 0b0   (Synchronous Read Data = Asynch)   
						// SWR   = 0b0   (Sychronous Write Data = Asynch)
						// CSEN  = 0b1   (Chip Select Enable = Enabled)

	writel(0x00000000, (weim_base + 0x04));	// Chip Select 0 General Configuration Register 2 (CS0GCR2)
						// DAP   = 0b0    (DTACK = Active High)
						// DAE   = 0b0    (Data Acknowledge Enable = Disabled)
						// DAPS  = 0b0000 (Data Acknowledge Poling Start = 3 clocks)
						// ADH   = 0b00   (Address hold time = 0 cycles)

	//writel(0x02081101, (weim_base + 0x08));	// Chip Select 0 Read Configuration Register 1 (CS0RCR1)
    //writel(0x10081101, (weim_base + 0x08));	// ***DEBUG*** Use 16 wait states for debug *******************************************
    writel(0x1f081101, (weim_base + 0x08));	    // ***DEBUG*** Use 16 wait states for debug *******************************************
						// RWSC  = 0b000010 (Read Wait State Control = 2 wait states)
						// RADVA = 0b00     (ADV Assertion = 0 clock cycles between begin and ADV)
						// RAL   = 0b1      (Read ADV Low = Stay asserted until end of access)
						// RADVN = 0b000    (ADV Negation time)
						// OEA   = 0b001    (OE Assertion = 1 clk between begin and OE assert)
						// OEN   = 0b010    (OE Negation  = 1 clk between end   and OE negate)
						// RCSA  = 0b000    (CS Assertion = 0 clk between begin and CS assert) 
						// RCSN  = 0b010    (CS Negation  = 1 clk between end   and CS negate)

	writel(0x00000002, (weim_base + 0x0C));	// Chip Select 0 Read Configuration Register 2 (CS0RCR2)
						// APR   = 0b0    (Async Page Read = single word)
						// PAT   = 0b000  (Page Access Time = 2 clk)
						// RL    = 0b00   (Read Latencyt = 1 cycle BCD=0, 1.5 BCD!=0)
						// RBEA  = 0b000  (Read BE Assertion = 0 clk between betin and BE)
						// RBE   = 0b0    (Read BE Enable = Disabled)
						// RBEN  = 0b010  (Read BE Negation = 2 clk between end of read and BE negate)

	//writel(0x02002482, (weim_base + 0x10));	// Chip Select 0 Write Configuration Register 1 (CS0WCR1)
    //writel(0x10002482, (weim_base + 0x10));	// ***DEBUG*** Use 16 wait states for debug *******************************************
    writel(0x1F002482, (weim_base + 0x10));	    // ***DEBUG*** Use 31 wait states for debug *******************************************
						// WAL   = 0b0      (Write ADV low=According to WADVN)
						// WBED  = 0b0      (Write Byte Enable Disable=Writes enabled)
						// WWSC  = 0b000010 (Write Wait State Control = 2 clk)
						// WADVA = 0b000    (ADV Assertion = 0 clk between begin and ADV assert)
						// WADVN = 0b000    (ADV Negation  = 0 clk between begin and ADV negate)
						// WBEA  = 0b000    (BE Assertion  = 0 clk between begin and BE  assert)
						// WBEN  = 0b010    (BE Negation   = 0 clk between begin and BE  negate)
						// WEA   = 0b010    (WE Assertion  = 2 clk between begin and WE  assert)
						// WEN   = 0b010    (WE Negation   = 2 clk between begin and WE  negate)
						// WCSA  = 0b000    (CS Assertion  = 0 clk between begin and CS  assert)
						// WCSN  = 0b000    (CS Negation   = 0 clk between begin and CS  negate)

	writel(0x00000000, (weim_base + 0x14));	// Chip Select 0 Write Configuration Register 2 (CS0WCR2)
						// WBCDD = 0b0      (Write Burst Clock Divisor Decrement = Disabled)

	writel(0x00000006, (weim_base + 0x90));	// WEIM Configuration Register (WCR)
    writel(0x00000007, (weim_base + 0x90)); // ***DEBUG*** set BCM=1 *******************************************
						// WDOG_LIMIT = 0b00 (Watchdog Limit = 128 BCLK)
						// WDOG_EN    = 0b0  (Memory Watchdog Enabke = Disabled)
						// INTPOL     = 0b0  (Interrupt Polarity = External int polarity is active low)
						// INTEN      = 0b0  (Interrupt Enable = External interrupt Disable)
						// GBCD       = 0b11 (General Burst Clock Divisor = Divide WEIM clk by 4)
						// BCM        = 0b0  (Burst Clock Mode = Only during WEIM access)


    // The following register is set here, but probably should be set is a better place.
	// It is set here as the best spot has not been determined.

	// The CBCDR Register is used to set the  clock source and dividers for many of the
	// MX51's internal clocks. One of these clocks is used as the base clock for the WEIM
	// bus used by the FPGA interface board. Bits 22-24 (emi_slow_podf) determines the
	// clock divider based on the input clock:
	//  Divider for EMI slow podf.
	//  000 divide by 1
	//  001 divide by 2
	//  010 divide by 3
	//  011 divide by 4
	//  100 divide by 5(default)
	//  101 divide by 6
	//  110 divide by 7
	//  111 divide by 8
	// The reference clock, determined by bit 26 (emi_clk_sel), ends of being a 133 MHz reference.
	// For the FPGA the clock needed is 16 MHz, so a divider of 8 is needed, thus bits 22-24 are
	// set to 0x7.
	// See the MX51 Reference manual, section 7.3.3.6 for more information on this register.
	reg = __raw_readl(MXC_CCM_CBCDR);
	reg |= (7 << MXC_CCM_CBCDR_EMI_PODF_OFFSET);
	__raw_writel(reg, MXC_CCM_CBCDR);
                                          
}

static void __init mx51_babbage_timer_init(void)
{
	struct clk *uart_clk;

	/* Change the CPU voltages for TO2*/
	if (mx51_revision() == IMX_CHIP_REVISION_2_0) {
		cpu_wp_auto[0].cpu_voltage = 1175000;
		cpu_wp_auto[1].cpu_voltage = 1100000;
		cpu_wp_auto[2].cpu_voltage = 1000000;
	}

	mx51_clocks_init(32768, 24000000, 22579200, 24576000);

	uart_clk = clk_get_sys("mxcintuart.0", NULL);
	early_console_setup(UART1_BASE_ADDR, uart_clk);
}

static struct sys_timer mxc_timer = {
	.init	= mx51_babbage_timer_init,
};

/*
 * The following uses standard kernel macros define in arch.h in order to
 * initialize __mach_desc_MX51_BABBAGE data structure.
 */
/* *INDENT-OFF* */
MACHINE_START(MX51_BABBAGE, "Freescale MX51 Babbage Board")
	/* Maintainer: Freescale Semiconductor, Inc. */
	.phys_io	= AIPS1_BASE_ADDR,
	.io_pg_offst	= ((AIPS1_BASE_ADDR_VIRT) >> 18) & 0xfffc,
	.fixup = fixup_mxc_board,
	.map_io = mx5_map_io,
	.init_irq = mx5_init_irq,
	.init_machine = mxc_board_init,
	.timer = &mxc_timer,
MACHINE_END
