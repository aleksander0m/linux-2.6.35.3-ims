/*
 * Copyright 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/slab.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/i2c.h>
#include <linux/i2c/qt602240_ts.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/i2c/at24.h>
#include <linux/hwmon.h>
#include <linux/pwm_backlight.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <asm/mach/keypad.h>
#include <asm/mach/flash.h>
#include <mach/gpio.h>
#include <mach/mmc.h>
#include <mach/mxc_dvfs.h>
#include <mach/mxc_edid.h>
#include <mach/iomux-SCU2_MEZZ.h>
#include <mach/i2c.h>
#include <mach/mx51.h>
#include <mach/mxc_iim.h>
#include <linux/proc_fs.h>
#include <linux/phy.h>
#include <linux/fec.h>

#include "devices.h"
#include "crm_regs.h"
#include "usb.h"

#ifdef CONFIG_MARVELL_M88E6161
#include <net/marvellswitch_platform.h>
#endif

#ifdef CONFIG_HOLT_ARINC
#include <linux/spi/holt_arinc_platform.h>
#endif

/*!
 * @file mach-mx51/mx51_babbage.c
 *
 * @brief This file contains the board specific initialization routines.
 *
 * @ingroup MSL_MX51
 */

// enable various log messages in this module
//#define IMS_LOG
//#define IMS_LOG_PINS
//#define SCUMEZ_AUDIO
//#define SCUMEZ_RS485GPIO

// GPIO 1
#define SCU2_MEZZ_PWM_Out               (0*32 + 2)  // GPIO_1_2
#define SCU2_MEZZ_HOST_INTERRUPT_B      (0*32 + 3)  // GPIO_1_3
#define SCU2_MEZZ_WDOG_B_GPIO1_4     	(0*32 + 4)  // GPIO_1_4
#define SCU2_MEZZ_MEZZ_SW_MDIO_ORIG_REVA (0*32 + 5)  // GPIO_1_5
#define SCU2_MEZZ_MEZZ_SW_MDC_ORIG_REVA (0*32 + 6)  // GPIO_1_6
#define SCU2_MEZZ_MEZZ_SW_INT_B         (0*32 + 7)  // GPIO_1_7
#define SCU2_MEZZ_INT_FROM_PMIC         (0*32 + 8)  // GPIO_1_8
#define SCU2_MEZZ_MEZZ_SWITCH_RESET     (0*32 + 9)  // GPIO_1_9
#define SCU2_MEZZ_DISCRETE_IN1_B        (0*32 + 11) // GPIO_1_11
#define SCU2_MEZZ_DISCRETE_IN2_B        (0*32 + 12) // GPIO_1_12
#define SCU2_MEZZ_DISCRETE_IN3_B        (0*32 + 13) // GPIO_1_13
#define SCU2_MEZZ_DISCRETE_IN4_B        (0*32 + 14) // GPIO_1_14
#define SCU2_MEZZ_DISCRETE_IN5_B        (0*32 + 15) // GPIO_1_15
#define SCU2_MEZZ_DISCRETE_IN6_B        (0*32 + 16) // GPIO_1_16
#define SCU2_MEZZ_SCU_DISABLE_B         (0*32 + 17) // GPIO_1_17
#define SCU2_MEZZ_SCU_ACTIVE            (0*32 + 18) // GPIO_1_18
#define SCU2_MEZZ_PRIMARY_SCU_B         (0*32 + 25) // GPIO_1_25
#define SCU2_MEZZ_DISCRETE_OUT1_B       (0*32 + 26) // GPIO_1_26
#define SCU2_MEZZ_DISCRETE_OUT2_B       (0*32 + 27) // GPIO_1_27
#define SCU2_MEZZ_DISCRETE_OUT3_B       (0*32 + 28) // GPIO_1_28

// GPIO 2
#define SCU2_MEZZ_A429_1_MB2            (1*32 + 0)  // GPIO_2_0
#define SCU2_MEZZ_A429_1_MB3            (1*32 + 1)  // GPIO_2_1
#define SCU2_MEZZ_A429_2_MB2            (1*32 + 2)  // GPIO_2_2
#define SCU2_MEZZ_A429_2_MB3            (1*32 + 3)  // GPIO_2_3
#define SCU2_MEZZ_A429_3_MB2            (1*32 + 4)  // GPIO_2_4
#define SCU2_MEZZ_A429_3_MB3            (1*32 + 5)  // GPIO_2_5
#define SCU2_MEZZ_SW_MDIO               (1*32 + 6)  // GPIO_2_6
#define SCU2_MEZZ_SW_MDC                (1*32 + 7)  // GPIO_2_7
#define SCU2_MEZZ_DIAG_LED_GPIO         (1*32 + 11) // GPIO_2_11
#define SCU2_MEZZ_RST_ENET_B            (1*32 + 14)  // GPIO_2_14
#define SCU2_MEZZ_IRQ_B                 (1*32 + 15)  // GPIO_2_15
#define SCU2_1V8_DIG1                   (1*32 + 21)  // GPIO_2_21


// GPIO 3
#define SCU2_MEZZ_MCU_I2C_SCL           (2*32 + 1)  // GPIO_3_1
#define SCU2_MEZZ_MCU_I2C_SDA           (2*32 + 2)  // GPIO_3_2
#define SCU2_MEZ_A429_1_RINT            (2*32 + 3)  // GPIO_3_3
#define SCU2_MEZZ_A429_2_RINT           (2*32 + 4)  // GPIO_3_4
#define SCU2_MEZZ_A429_3_RINT           (2*32 + 5)  // GPIO_3_5
#define SCU2_MEZZ_A429_4_RINT           (2*32 + 6)  // GPIO_3_6
#define SCU2_MEZZ_AC429_1_FLAG          (2*32 + 7)  // GPIO_3_7
#define SCU2_MEZZ_AC429_2_FLAG          (2*32 + 8)  // GPIO_3_8
#define SCU2_MEZZ_A429_MR1              (2*32 + 12) // GPIO_3_12
#define SCU2_MEZZ_EMMC2_RESET           (2*32 + 13) // GPIO_3_13
#define SCU2_MEZZ_A429_3_FLAG           (2*32 + 16) // GPIO_3_16
#define SCU2_MEZZ_A429_4_FLAG           (2*32 + 17) // GPIO_3_17
#define SCU2_MEZZ_A429_1_MB1            (2*32 + 23) // GPIO_2_23
#define SCU2_MEZZ_A429_SS0              (2*32 + 24) // GPIO_2_24
#define SCU2_MEZZ_A429_M2               (2*32 + 27) // GPIO_3_27
#define SCU2_MEZZ_A429_SS1              (2*32 + 28) // GPIO_3_28
#define SCU2_MEZZ_A429_4_MB1            (2*32 + 29) // GPIO_3_29
#define SCU2_MEZZ_A429_TEMPTY2          (2*32 + 30) // GPIO_3_30
#define SCU2_MEZZ_A429_TFULL2           (2*32 + 31) // GPIO_3_31

// GPIO 4
#define SCU2_MEZZ_A429_TEMPTY1          (3*32 + 0)  // GPIO_4_0
#define SCU2_MEZZ_A429_TFULL1           (3*32 + 1)  // GPIO_4_1
#define SCU2_MEZZ_A429_3_MB1            (3*32 + 2)  // GPIO_4_2
#define SCU2_MEZZ_SYSTEM_TYPE_0         (3*32 + 3)  // GPIO_4_3
#define SCU2_MEZZ_SYSTEM_TYPE_1         (3*32 + 4)  // GPIO_4_4
#define SCU2_MEZZ_SYSTEM_TYPE_2         (3*32 + 5)  // GPIO_4_5
#define SCU2_MEZZ_SYSTEM_TYPE_3         (3*32 + 6)  // GPIO_4_6
#define SCU2_MEZZ_A429_2_MB1            (3*32 + 7)  // GPIO_4_7
#define SCU2_ESB_SD4_CD                 (3*32 + 8)  // GPIO_4_8
#define SCU2_MEZZ_DISCRETE_OUT4_B       (3*32 + 18) // GPIO_4_18
#define SCU2_MEZZ_DISCRETE_OUT5_B       (3*32 + 19) // GPIO_4_19
#define SCU2_MEZZ_DISCRETE_OUT6_B       (3*32 + 20) // GPIO_4_20
#define SCU2_MEZZ_DISCRETE_OUT7_B       (3*32 + 21) // GPIO_4_21
#define SCU2_MEZZ_SW_RESET_REQUEST      (3*32 + 26) // GPIO_4_26

#define SCU2_ESB_CSP1_SS0_GPIO          (3*32 + 24) // GPIO_4_24
#define SCU2_ESB_CSP1_SS1_GPIO          (3*32 + 24) // GPIO_4_24

extern int __init mx51_babbage_init_mc13892(void);
extern struct cpu_wp *(*get_cpu_wp)(int *wp);
extern void (*set_num_cpu_wp)(int num);
extern struct dvfs_wp *(*get_dvfs_core_wp)(int *wp);

static int num_cpu_wp;

extern void scu2_esb_mezz_namepate(struct memory_accessor *mem_accessor, struct proc_dir_entry *rave_proc_dir);
void populateUnitInfo(struct memory_accessor *mem_accessor, void *context);

/*static struct pad_desc mx51babbage_gpio_pads[] =
{
	// Discrete gpio pins
	MX51_PAD_USBH1_CLK__GPIO_1_25,      // ok SCU Primary_SCU#_c gpio1_25 (in)  25
	MX51_PAD_USBH1_DIR__GPIO_1_26,      // ok SCU Discrete Out 1 gpio1_26 (out)
	MX51_PAD_USBH1_STP__GPIO_1_27,      // ok SCU discrete out 2 gpio1_27 (out)
	MX51_PAD_USBH1_NXT__GPIO_1_28,      // ok SCU discrete out 3 gpio1_28 (out)
	MX51_PAD_USBH1_DATA0__GPIO_1_11,    // ok SCU discrete in 1  gpio1_11 (in)
	MX51_PAD_USBH1_DATA1__GPIO_1_12,    // ok SCU discrete in 2  gpio1_12 (in)
	MX51_PAD_USBH1_DATA2__GPIO_1_13,    // ok SCU discrete in 3  gpio1_13 (in)
	MX51_PAD_USBH1_DATA3__GPIO_1_14,    // ok SCU discrete in 4  gpio1_14 (in)
	MX51_PAD_USBH1_DATA4__GPIO_1_15,    // ok SCU discrete in 5  gpio1_15 (in)
	MX51_PAD_USBH1_DATA5__GPIO_1_16,    // ok SCU discrete in 6  gpio1_16 (in)
	MX51_PAD_USBH1_DATA6__GPIO_1_17,    // ok SCU SCU_Discable#_c  gpio1_11 (in)
	MX51_PAD_USBH1_DATA7__GPIO_1_18,    // ok SCU SCU_Active      gpio1_18  (out)
    
};*/

static struct pad_desc mx51babbage_pads[] = {

        // EIM as GPIO
        MX51_PAD_EIM_D16__GPIO_2_0,
        MX51_PAD_EIM_D17__GPIO_2_1,
        MX51_PAD_EIM_D18__GPIO_2_2,
        MX51_PAD_EIM_D19__GPIO_2_3,
        MX51_PAD_EIM_D20__GPIO_2_4,
        MX51_PAD_EIM_D21__GPIO_2_5,
        MX51_PAD_EIM_D22__GPIO_2_6,
        MX51_PAD_EIM_D23__GPIO_2_7,
        MX51_PAD_EIM_A17__GPIO_2_11,
        MX51_PAD_EIM_A20__GPIO_2_14,
        MX51_PAD_EIM_A21__GPIO_2_15,

        // NANDF Pins Powered By NVCC_NANDF_A
        MX51_PAD_NANDF_WE_B__GPIO_3_3,
        MX51_PAD_NANDF_RE_B__GPIO_3_4,
        MX51_PAD_NANDF_ALE__GPIO_3_5,
        MX51_PAD_NANDF_CLE__GPIO_3_6,
        MX51_PAD_NANDF_WP_B__GPIO_3_7,
        MX51_PAD_NANDF_RB0__GPIO_3_8,
        MX51_PAD_GPIO_NAND__GPIO_3_12,
        MX51_PAD_NANDF_CS0__GPIO_3_16,
        MX51_PAD_NANDF_CS1__GPIO_3_17,

        // NANDF Pins Powered By NVCC_NDNANDF_B
        MX51_PAD_NANDF_CS7__GPIO_3_23,
        MX51_PAD_NANDF_D7__GPIO_4_1,
        MX51_PAD_NANDF_D8__GPIO_4_0,
        MX51_PAD_NANDF_D9__GPIO_3_31,
        MX51_PAD_NANDF_D10__GPIO_3_30,
        MX51_PAD_NANDF_D11__GPIO_3_29,
        MX51_PAD_NANDF_D13__GPIO_3_27,

        // NANDF (NVCC_NDNANDF_C) as GPIO
        MX51_PAD_NANDF_D1__GPIO_4_7,
        MX51_PAD_NANDF_D2__GPIO_4_6,
        MX51_PAD_NANDF_D3__GPIO_4_5,
        MX51_PAD_NANDF_D4__GPIO_4_4,
        MX51_PAD_NANDF_D5__GPIO_4_3,
        MX51_PAD_NANDF_D6__GPIO_4_2,

        // Audio as GPIO
        MX51_PAD_AUD3_BB_TXD__GPIO_4_18,
        MX51_PAD_AUD3_BB_RXD__GPIO_4_19,
        MX51_PAD_AUD3_BB_CK__GPIO4_20,
        MX51_PAD_AUD3_BB_FS__GPIO4_21,

        // CSPI1
        MX51_PAD_CSPI1_MOSI__CSPI1_MOSI,
        MX51_PAD_CSPI1_MISO__CSPI1_MISO,
        MX51_PAD_CSPI1_SS0__CSPI1_SS0,
        MX51_PAD_CSPI1_SS1__CSPI1_SS1,
        MX51_PAD_CSPI1_SCLK__CSPI1_SCLK,
        MX51_PAD_CSPI1_RDY__GPIO_4_26,

        // CSPI2
        MX51_PAD_NANDF_RB2__CSPI2_SCLK,
        MX51_PAD_NANDF_RB3__CSPI2_MISO,
        MX51_PAD_NANDF_D15__CSPI2_MOSI,
	    MX51_PAD_NANDF_RDY_INT__GPIO_3_24,
        MX51_PAD_NANDF_D12__GPIO_3_28,

        // UART1
        MX51_PAD_UART1_RXD__UART1_RXD,
        MX51_PAD_UART1_TXD__UART1_TXD,

        // UART3
        MX51_PAD_UART3_RXD__UART3_RXD,
        MX51_PAD_UART3_TXD__UART3_TXD,

        // I2C Bitbang
        MX51_PAD_DI1_PIN12__GPIO_3_1,
        MX51_PAD_DI1_PIN13__GPIO_3_2,

        // I2C2
        MX51_PAD_KEY_COL4__I2C2_SCL,
        MX51_PAD_KEY_COL5__I2C2_SDA,

        // CSI1
        MX51_PAD_CSI1_D9__GPIO_3_13,

        // USBH1 as GPIO
        MX51_PAD_USBH1_DATA0__GPIO_1_11,
        MX51_PAD_USBH1_DATA1__GPIO_1_12,
        MX51_PAD_USBH1_DATA2__GPIO_1_13,
        MX51_PAD_USBH1_DATA3__GPIO_1_14,
        MX51_PAD_USBH1_DATA4__GPIO_1_15,
        MX51_PAD_USBH1_DATA5__GPIO_1_16,
        MX51_PAD_USBH1_DATA6__GPIO_1_17,
        MX51_PAD_USBH1_DATA7__GPIO_1_18,
        MX51_PAD_USBH1_CLK__GPIO_1_25,
        MX51_PAD_USBH1_DIR__GPIO_1_26,
        MX51_PAD_USBH1_STP__GPIO_1_27,
        MX51_PAD_USBH1_NXT__GPIO_1_28,

        // FEC
        MX51_PAD_DISP2_DAT15__FEC_TDAT0,
        MX51_PAD_DISP2_DAT1__FEC_RX_ER,
        MX51_PAD_DISP2_DAT6__FEC_TDAT1,
        MX51_PAD_DISP2_DAT7__FEC_TDAT2,
        MX51_PAD_DISP2_DAT8__FEC_TDAT3,
        MX51_PAD_DISP2_DAT9__FEC_TX_EN,
        MX51_PAD_DISP2_DAT10__FEC_COL,
        MX51_PAD_DISP2_DAT11__FEC_RXCLK,
        MX51_PAD_DISP2_DAT12__FEC_RX_DV,
        MX51_PAD_DISP2_DAT13__FEC_TX_CLK,
        MX51_PAD_DISP2_DAT14__FEC_RDAT0,
        MX51_PAD_DI2_DISP_CLK__FEC_RDAT1,
        MX51_PAD_DI_GP3__FEC_TX_ER,
        MX51_PAD_DI_GP4__FEC_RDAT2,
        MX51_PAD_DISP2_DAT0__FEC_RDAT3,
        MX51_PAD_DI2_PIN2__FEC_MDC,
        MX51_PAD_DI2_PIN3__FEC_MDIO,
        MX51_PAD_DI2_PIN4__FEC_CRS,

        // SD1
        MX51_PAD_SD1_CMD__SD1_CMD,
        MX51_PAD_SD1_CLK__SD1_CLK,
        MX51_PAD_SD1_DATA0__SD1_DATA0,
        MX51_PAD_SD1_DATA1__SD1_DATA1,
        MX51_PAD_SD1_DATA2__SD1_DATA2,
        MX51_PAD_SD1_DATA3__SD1_DATA3,
        MX51_PAD_SD2_DATA0__SD2_DATA0,
        MX51_PAD_SD2_DATA1__SD2_DATA1,
        MX51_PAD_SD2_DATA2__SD2_DATA2,
        MX51_PAD_SD2_DATA3__SD2_DATA3,

        // SD4
        MX51_PAD_NANDF_CS2__SD4_CLK,
        MX51_PAD_NANDF_RB1__SD4_CMD,
        MX51_PAD_NANDF_CS3__SD4_DATA0,
        MX51_PAD_NANDF_CS4__SD4_DATA1,
        MX51_PAD_NANDF_CS5__SD4_DATA2,
        MX51_PAD_NANDF_CS6__SD4_DATA3,

        // PMIC
        MX51_PAD_PMIC_INT_REQ__PMIC_INT_REQ,

        // GPIO1
        MX51_PAD_GPIO_1_2__GPIO1_2,
        MX51_PAD_GPIO_1_3__GPIO1_3,
        MX51_PAD_GPIO_1_4__GPIO1_4,
        MX51_PAD_GPIO_1_5__GPIO_1_5,
        MX51_PAD_GPIO_1_6__GPIO_1_6,
        MX51_PAD_GPIO_1_7__GPIO_1_7,
        MX51_PAD_GPIO_1_8__GPIO1_8,
        MX51_PAD_GPIO_1_9__GPIO_1_9,
};

static struct dvfs_wp dvfs_core_setpoint[] = {
	{33, 8, 33, 10, 10, 0x08},
	{26, 0, 33, 20, 10, 0x08},
	{28, 8, 33, 20, 30, 0x08},
	{29, 0, 33, 20, 10, 0x08},
};

/* working point(wp): 0 - 800MHz; 1 - 166.25MHz; */
static struct cpu_wp cpu_wp_auto[] = {
	{
	 .pll_rate = 1000000000,
	 .cpu_rate = 1000000000,
	 .pdf = 0,
	 .mfi = 10,
	 .mfd = 11,
	 .mfn = 5,
	 .cpu_podf = 0,
	 .cpu_voltage = 1175000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 800000000,
	 .pdf = 0,
	 .mfi = 8,
	 .mfd = 2,
	 .mfn = 1,
	 .cpu_podf = 0,
	 .cpu_voltage = 1100000,},
	{
	 .pll_rate = 800000000,
	 .cpu_rate = 166250000,
	 .cpu_podf = 4,
	 .cpu_voltage = 850000,},
};

static struct fb_videomode video_modes[] = {
		{
	 /* NTSC TV output */
	 "TV-NTSC", 60, 720, 480, 74074,
	 122, 15,
	 18, 26,
	 1, 1,
	 FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
	 FB_VMODE_INTERLACED,
			0,}, 
		{
	 /* PAL TV output */
	 "TV-PAL", 50, 720, 576, 74074,
	 132, 11,
	 22, 26,
	 1, 1,
	 FB_SYNC_HOR_HIGH_ACT | FB_SYNC_VERT_HIGH_ACT,
	 FB_VMODE_INTERLACED | FB_VMODE_ODD_FLD_FIRST,
			0,}, 
};

struct cpu_wp *mx51_babbage_get_cpu_wp(int *wp)
{
	*wp = num_cpu_wp;
	return cpu_wp_auto;
}

void mx51_babbage_set_num_cpu_wp(int num)
{
	num_cpu_wp = num;
	return;
}

//static struct mxc_w1_config mxc_w1_data = {
//	.search_rom_accelerator = 1,
//};

static struct dvfs_wp *mx51_babbage_get_dvfs_core_table(int *wp)
{
	*wp = ARRAY_SIZE(dvfs_core_setpoint);
	return dvfs_core_setpoint;
}

extern void mx5_ipu_reset(void);
static struct mxc_ipu_config mxc_ipu_data = {
	.rev = 2,
	.reset = mx5_ipu_reset,
};

extern void mx5_vpu_reset(void);
static struct mxc_vpu_platform_data mxc_vpu_data = {
	.iram_enable = false,
	.iram_size = 0x14000,
	.reset = mx5_vpu_reset,
};


/* workaround for ecspi chipselect pin may not keep correct level when idle */
static void mx51_babbage_gpio_spi_chipselect_active(int cspi_mode, int status,
                         int chipselect)
{
    switch (cspi_mode) {
    case 1:
        switch (chipselect) {
        case 0x1:
        {
            struct pad_desc cspi1_ss0 = MX51_PAD_CSPI1_SS0__CSPI1_SS0;

            mxc_iomux_v3_setup_pad(&cspi1_ss0);
            break;
        }
        case 0x2:
        {
            struct pad_desc cspi1_ss0_gpio = MX51_PAD_CSPI1_SS0__GPIO_4_24;

            mxc_iomux_v3_setup_pad(&cspi1_ss0_gpio);
            gpio_request(SCU2_ESB_CSP1_SS0_GPIO, "cspi1-gpio");
            gpio_direction_output(SCU2_ESB_CSP1_SS0_GPIO, 0);
            gpio_set_value(SCU2_ESB_CSP1_SS0_GPIO, 1 & (~status));
            break;
        }
        default:
            break;
        }
        break;
    case 2:
        break;
    case 3:
        break;
    default:
        break;
    }
}

static void mx51_babbage_gpio_spi_chipselect_inactive(int cspi_mode, int status,
                           int chipselect)
{
    switch (cspi_mode) {
    case 1:
        switch (chipselect) {
        case 0x1:
            break;
        case 0x2:
            gpio_free(SCU2_ESB_CSP1_SS0_GPIO);
            break;

        default:
            break;
        }
        break;
    case 2:
        break;
    case 3:
        break;
    default:
        break;
    }
}


// eCSPI1
static struct mxc_spi_master mxcspi1_data = {
	.maxchipselect = 4,
	.spi_version = 23,
	.chipselect_active = mx51_babbage_gpio_spi_chipselect_active,
	.chipselect_inactive = mx51_babbage_gpio_spi_chipselect_inactive
};


// CSPI2
static struct mxc_spi_master mxcspi2_data = {
	.maxchipselect = 4,
	.spi_version = 23,
};



#if 0 // use gpio for CSPI2
#include <linux/spi/spi_gpio.h>

static struct spi_gpio_platform_data spi_bitbang_data = {
        .sck =  SCUMEZ_SPI2_SCLK,
        .mosi = SCUMEZ_SPI2_MOSI,
	.miso = SCUMEZ_SPI2_MISO,
	.num_chipselect = 4,	// only assigned is CSI2_HSYNC
};

static struct platform_device spi_gpio_device = {
        .name = "spi_gpio",
        .id = 2,
        .dev = {
                .platform_data = &spi_bitbang_data,
        },
};
#endif


// Set the I2C bit rate
static struct imxi2c_platform_data mxci2c_data = {
	.bitrate = 50000,
};

static struct tve_platform_data tve_data = {
	.dac_reg = "VVIDEO",
};

static struct mxc_dvfs_platform_data dvfs_core_data = {
	.reg_id = "SW1",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.gpc_vcr_offset = MXC_GPC_VCR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 30,
};

static struct mxc_bus_freq_platform_data bus_freq_data = {
	.gp_reg_id = "SW1",
	.lp_reg_id = "SW2",
};


static struct mxc_dvfsper_data dvfs_per_data = {
	.reg_id = "SW2",
	.clk_id = "gpc_dvfs_clk",
	.gpc_cntr_reg_addr = MXC_GPC_CNTR,
	.gpc_vcr_reg_addr = MXC_GPC_VCR,
	.gpc_adu = 0x0,
	.vai_mask = MXC_DVFSPMCR0_FSVAI_MASK,
	.vai_offset = MXC_DVFSPMCR0_FSVAI_OFFSET,
	.dvfs_enable_bit = MXC_DVFSPMCR0_DVFEN,
	.irq_mask = MXC_DVFSPMCR0_FSVAIM,
	.div3_offset = 0,
	.div3_mask = 0x7,
	.div3_div = 2,
	.lp_high = 1250000,
	.lp_low = 1250000,
};

static struct resource mxcfb_resources[] = {
	[0] = {
	       .flags = IORESOURCE_MEM,
	       },
};

static struct mxc_fb_platform_data fb_data[] = {
	{
		.interface_pix_fmt = IPU_PIX_FMT_YUV444,
		.mode_str = "TV-NTSC", 
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
	{
		.interface_pix_fmt = IPU_PIX_FMT_YUV444,
		.mode_str = "TV-PAL", 
		.mode = video_modes,
		.num_modes = ARRAY_SIZE(video_modes),
	},
};

static void mxc_iim_enable_fuse(void)
{
	u32 reg;

	if (!ccm_base)
		return;
	/* Enable fuse blown */
	reg = readl(ccm_base + 0x64);
	reg |= 0x10;
	writel(reg, ccm_base + 0x64);
}

static void mxc_iim_disable_fuse(void)
{
	u32 reg;

	/* Disable fuse blown */
	if (!ccm_base)
		return;

	reg = readl(ccm_base + 0x64);
	reg &= ~0x10;
	writel(reg, ccm_base + 0x64);
}

static struct mxc_iim_data iim_data = {
	.bank_start = MXC_IIM_MX51_BANK_START_ADDR,
	.bank_end   = MXC_IIM_MX51_BANK_END_ADDR,
	.enable_fuse = mxc_iim_enable_fuse,
	.disable_fuse = mxc_iim_disable_fuse,
};

extern int primary_di;
static int __init mxc_init_fb(void)
{
	// SCU has display only to do video composite out, and
	// to support video capture.  LVDS reset GPIO is not
	// configured

	// mxc_fb_devices defined three frame buffer assets (and uses dma for each)

	if (primary_di) {
		printk(KERN_INFO "DI1 is primary\n");

		/* DI1 -> DP-BG channel: */
		mxc_fb_devices[1].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[1].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);

		/* DI0 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);
	} else {
		printk(KERN_INFO "DI0 is primary\n");

		/* DI0 -> DP-BG channel: */
		mxc_fb_devices[0].num_resources = ARRAY_SIZE(mxcfb_resources);
		mxc_fb_devices[0].resource = mxcfb_resources;
		mxc_register_device(&mxc_fb_devices[0], &fb_data[0]);

		/* DI1 -> DC channel: */
		mxc_register_device(&mxc_fb_devices[1], &fb_data[1]);
	}

	/*
	 * DI0/1 DP-FG channel:
	 */
	mxc_register_device(&mxc_fb_devices[2], NULL);

	return 0;
}
device_initcall(mxc_init_fb);

//-------------------------------
// I2C Device Setup
// ------------------------------

// I2C Bitbang

#include <linux/i2c-gpio.h>

// Bitbanged I2C is used as a master for the MCU
static struct i2c_gpio_platform_data i2c_mcu_bitbang_data = {
	.sda_pin = SCU2_MEZZ_MCU_I2C_SDA,
	.scl_pin = SCU2_MEZZ_MCU_I2C_SCL,
	.udelay = 50,
	.timeout = 100,
	.sda_is_open_drain = 0,
	.scl_is_open_drain = 0,
	.scl_is_output_only = 1,
};

static struct platform_device i2c_mcu_bitbang_device = {
        .name = "i2c-gpio",
        .id = 3,
        .dev = {
                .platform_data = &i2c_mcu_bitbang_data,
        },
};


// 3 I2C2 Devices On Main Board
// EEPROM: 		AT24C04
// Temp Sensor:	LM75

typedef enum {
	I2C2_BOARDINFOIDX_LM75_MAIN = 0,
	I2C2_BOARDINFOIDX_AT24C04_MAIN,
} I2C2_devices_E;

// AT24C04 Data structure
struct at24_platform_data at24c04_data = {
	.byte_len  	= 512,
    .page_size 	= 16,
    .flags		= 0,
    .setup		= populateUnitInfo,
};



static struct i2c_board_info mxc_i2c2_board_info[] __initdata = {
	{
	    .type = "lm75",
	    .addr = 0x48,
	},	
	{
	    .type = "24c04",
	    .addr = 0x50,
	    .platform_data = &at24c04_data,
	},
};


// ----------------------
// SPI Device Setup
// ----------------------

static struct mtd_partition mxc_spi_nor_partitions[] = {
    {
     .name = "NOR-config",
     .offset = 0,
     .size = 0x000000400,},
    {
     .name = "NOR-bootloader",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x0000FFC00,},
    {
     .name = "NOR-kernel",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x00300000,},
    {
     .name = "NOR-rootfs",
     .offset = MTDPART_OFS_APPEND,
     .size = MTDPART_SIZ_FULL,},
};

static struct mtd_partition mxc_dataflash_partitions[] = {
    {
     .name = "NOR-config",
     .offset = 0,
     .size = 0x000000400,},
    {
     .name = "NOR-bootloader",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x0000FFC00,},
    {
     .name = "NOR-kernel",
     .offset = MTDPART_OFS_APPEND,
     .size = 0x00300000,},
    {
     .name = "NOR-rootfs",
     .offset = MTDPART_OFS_APPEND,
     .size = MTDPART_SIZ_FULL,},
};


static struct flash_platform_data mxc_spi_flash_data[] = {
    {
     .name = "mxc_spi_nor",
     .parts = mxc_spi_nor_partitions,
     .nr_parts = ARRAY_SIZE(mxc_spi_nor_partitions),
     .type = "sst25vf016b",},
    {
     .name = "mxc_dataflash",
     .parts = mxc_dataflash_partitions,
     .nr_parts = ARRAY_SIZE(mxc_dataflash_partitions),
     .type = "at45db642d",},
};

static struct spi_board_info mxc_dataflash_device[] __initdata = {
    {
     .modalias = "mxc_dataflash",
     .max_speed_hz = 25000000,  /* max spi clock (SCK) speed in HZ */
     .bus_num = 1,
     .chip_select = 1,
     .platform_data = &mxc_spi_flash_data[1],},
};


#ifdef CONFIG_HOLT_ARINC

static intrio_assignments_t hi3593tx_1_intrio  = {
    // transmit interrupts
    SCU2_MEZZ_A429_TEMPTY1,
    SCU2_MEZZ_A429_TFULL1,
};

static intrio_assignments_t hi3593r1_1_intrio  = {
    // receive interrupts
    SCU2_MEZ_A429_1_RINT,
    SCU2_MEZZ_AC429_1_FLAG,
    SCU2_MEZZ_A429_1_MB1,
    SCU2_MEZZ_A429_1_MB2,
    SCU2_MEZZ_A429_1_MB3,
    0,
};

static intrio_assignments_t hi3593r2_1_intrio  = {
    // receive interrupts
    SCU2_MEZZ_A429_2_RINT,
    SCU2_MEZZ_AC429_2_FLAG,
    SCU2_MEZZ_A429_2_MB1,
    SCU2_MEZZ_A429_2_MB2,
    SCU2_MEZZ_A429_2_MB3,
    0,
};

static intrio_assignments_t hi3593tx_2_intrio  = {
    // transmit interrupts
    SCU2_MEZZ_A429_TEMPTY2,
    SCU2_MEZZ_A429_TFULL2,
};

static intrio_assignments_t hi3593r1_2_intrio  = {
    // receive interrupts
    SCU2_MEZZ_A429_3_RINT,
    SCU2_MEZZ_A429_3_FLAG,
    SCU2_MEZZ_A429_3_MB1,
    0,
    0,
    0,
};

static intrio_assignments_t hi3593r2_2_intrio  = {
    // receive interrupts
    SCU2_MEZZ_A429_4_RINT,
    SCU2_MEZZ_A429_4_FLAG,
    SCU2_MEZZ_A429_4_MB1,
    0,
    0,
    0,
};

/* Structure for configuing three arinc controllers */
static struct spi_board_info arinc_board_info[] = {
    // HOLT ARINC-429 Chip #1
    {
        .modalias = "hi3593tx-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 0,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593tx_1_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS0,
    },
    {
        .modalias = "hi3593r1-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 0,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593r1_1_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS0,
    },
    {
        .modalias = "hi3593r2-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 0,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593r2_1_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS0,
    },

    // HOLT ARINC-429 Chip #2
    {
        .modalias = "hi3593tx-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 1,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593tx_2_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS1,
    },
    {
        .modalias = "hi3593r1-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 1,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593r1_2_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS1,
    },
    {
        .modalias = "hi3593r2-arinc",
        .max_speed_hz = 4000000, /* SPI Speed.  max spi clock (SCK) speed in HZ */
        .bus_num = 2,           /* McSPI Bus Number */
        .chip_select = 1,       /* ChipSelect for McSPI */
        .mode = 0, // SPI_CS_HIGH,  /* SPI Mode */
        .platform_data = (void *)hi3593r2_2_intrio,
        .controller_data = (void *)SCU2_MEZZ_A429_SS1,
    },
    {
    },
};


struct arinc_spi_platform_info arinc_platform_info = {
    .cspi_info = &mxcspi2_device,
    .chip_info = arinc_board_info,
};

struct platform_device arinc_spi_device = {
    .name = "holt_arinc",
    .dev = {
        .platform_data = &arinc_platform_info,
    },
};

#endif


// --------------------------
// SDHC Device Setup
// --------------------------

static int sdhc_write_protect(struct device *dev)
{
	unsigned short rc = 0;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is never write protected.)
		rc = 0;
		break;

	case 1:
		// SD-2 (UNUSED)
		rc = 0;
		break;

	case 2:
		// SD-3 (UNUSED)
		rc = 0;
		break;
	case 3:
		// SD-4 (uSD Card Slot)
		// No Write Protect Pin
		rc = 0;
		break;
	}
	return rc;
}

static unsigned int sdhc_get_card_det_status(struct device *dev)
{
	int ret;

	ret = 1;
	switch(to_platform_device(dev)->id)
	{
	case 0:
		// SD-1 (On-board eMMC.  It is always present.)
		ret = 0;
		break;

	case 1:
		// SD-2 (UNUSED)
		break;

	case 2:
		// SD-3 (UNUSED)
		break;
	case 3:
		// SD-4 (uSD Card Slot)
		ret = gpio_get_value(SCU2_ESB_SD4_CD);
		break;
	}
	return ret;
}

static struct mxc_mmc_platform_data mmc1_data = {
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
	    MMC_VDD_31_32,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 52000000,
	.card_inserted_state = 1,
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
	.power_mmc = NULL,
};

// SCU uses microd SD so card inserted state is always 1, inserted state
static struct mxc_mmc_platform_data mmc4_data = {
	// %%jws%% use same as mmc1 IMS_DEBUG: try 3.3 
	.ocr_mask = MMC_VDD_27_28 | MMC_VDD_28_29 | MMC_VDD_29_30 |
		MMC_VDD_31_32,
//	.ocr_mask = MMC_VDD_32_33,
	.caps = MMC_CAP_4_BIT_DATA,
	.min_clk = 150000,
	.max_clk = 50000000,
	.card_inserted_state = 1, 
	.status = sdhc_get_card_det_status,
	.wp_status = sdhc_write_protect,
	.clock_mmc = "esdhc_clk",
};



// -----------------------------
// SWITCH
// -----------------------------
#include <linux/mdio-gpio.h>

static struct mdio_gpio_platform_data  bitbang_data = {
		.mdc = SCU2_MEZZ_SW_MDC,
        .mdio = SCU2_MEZZ_SW_MDIO,
};

static struct platform_device mdio_device = {
        .name = "mdio-gpio",
        .id = 1,
        .dev = {
                .platform_data = &bitbang_data,
        },
};

#ifdef CONFIG_NET_DSA
#include <net/dsa.h>

static struct dsa_chip_data marvell_switch_chip_data = {
         .mii_bus = &mdio_device.dev,
        .port_names[0]	= "port4",
        .port_names[1]	= "port5",
        .port_names[2]	= "port6",
        .port_names[3]	= "port7",
        .port_names[4]	= "cpu",
        .port_names[5]	= "mezz2esb",
        .port_names[6]  = 0 // Not Used
};

static struct dsa_platform_data switch_data = {
        .netdev = &mxc_fec_device.dev,
        .nr_chips = 1,
        .chip = &marvell_switch_chip_data,
        .hw_reset_gpio = SCU2_MEZZ_MEZZ_SWITCH_RESET,
        .hw_reset_polarity = RESET_ACTIVE_HIGH,
};

static struct resource dsa_irq = {
	.start = gpio_to_irq(SCU2_MEZZ_MEZZ_SW_INT_B),
	.end = gpio_to_irq(SCU2_MEZZ_MEZZ_SW_INT_B),
	.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_LOWLEVEL,
};

static struct platform_device dsa_device = {
        .name = "dsa",
        .dev = {
                .platform_data = &switch_data,
        },
	.num_resources = 1,
	.resource = &dsa_irq,
};
#endif

static void setup_ethernet_switch(void)
{
	platform_device_register(&mdio_device);

#ifdef CONFIG_NET_DSA
	platform_device_register(&dsa_device);
#endif
#ifdef CONFIG_MARVELL_M88E6161
	platform_device_register(&marvell_switch);
#endif
}



// ---------------------------
// System Type
// ---------------------------
struct gpio systemType_gpio[] = {
	{ .gpio = SCU2_MEZZ_SYSTEM_TYPE_3, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_3", },
	{ .gpio = SCU2_MEZZ_SYSTEM_TYPE_2, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_2", },
	{ .gpio = SCU2_MEZZ_SYSTEM_TYPE_1, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_1", },
	{ .gpio = SCU2_MEZZ_SYSTEM_TYPE_0, .flags = GPIOF_DIR_IN, .label = "SYSTEM_TYPE_0", },
};

// these definitions should be moved somewhere board specific that makes them
// available to anyone who might call IMSSystemType
#define SYSTEM_TYPE_MASK	0xF
#define	 RDU_REV_B	(0b0000 & SYSTEM_TYPE_MASK)
#define	 RDU_REV_C	(0b1101 & SYSTEM_TYPE_MASK)
#define SCU_MEZZ	(0b0010 & SYSTEM_TYPE_MASK)
#define NIU_REV  	(0b0011 & SYSTEM_TYPE_MASK)
#define SCU2_ESB   (0b0001 & SYSTEM_TYPE_MASK)
#define SCU2_MEZZ  (0b0100 & SYSTEM_TYPE_MASK)

static char *systemType_toStr( int type )
{
	switch ( type ) {
	case RDU_REV_B:     return "RDU Rev B";
	case RDU_REV_C:     return "RDU Rev C";
	case SCU_MEZZ:      return "SCU Mezz";
	case NIU_REV:       return "NIU";
	case SCU2_ESB:      return  "SCU2 ESB";
    case SCU2_MEZZ:     return  "SCU2 MEZZ";
    default:           return "Unknown";
	}
}

int IMSSystemType( void )
{
	static int type = -1;
	if ( type == -1 ) {
		type =	(gpio_get_value( SCU2_MEZZ_SYSTEM_TYPE_3 ) << 3 ) |
			(gpio_get_value( SCU2_MEZZ_SYSTEM_TYPE_2 ) << 2 ) |
			(gpio_get_value( SCU2_MEZZ_SYSTEM_TYPE_1 ) << 1 ) |
			(gpio_get_value( SCU2_MEZZ_SYSTEM_TYPE_0 ) << 0 );

		// NOTE: masking off the top bits because of varying configurations in populated resistors.
		type &= SYSTEM_TYPE_MASK;

		printk(KERN_INFO"%s: detected board type %s(%02x)\n", __func__, systemType_toStr( type ), type);
	}
	return type;
}
EXPORT_SYMBOL(IMSSystemType);

static int boardType_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	int type = IMSSystemType();
	len += sprintf(buf+len,"%s(%02x)\n", systemType_toStr( type ), type);
	*eof = 1;
	return len;
}



/*!
 * Board specific fixup function. It is called by \b setup_arch() in
 * setup.c file very early on during kernel starts. It allows the user to
 * statically fill in the proper values for the passed-in parameters. None of
 * the parameters is used currently.
 *
 * @param  desc         pointer to \b struct \b machine_desc
 * @param  tags         pointer to \b struct \b tag
 * @param  cmdline      pointer to the command line
 * @param  mi           pointer to \b struct \b meminfo
 */
static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
	char *str;
	struct tag *t;
	struct tag *mem_tag = 0;
	int total_mem = SZ_512M;
	int left_mem = 0;
	int gpu_mem = SZ_64M;
	int fb_mem = SZ_32M;

	mxc_set_cpu_type(MXC_CPU_MX51);

	get_cpu_wp = mx51_babbage_get_cpu_wp;
	set_num_cpu_wp = mx51_babbage_set_num_cpu_wp;
	get_dvfs_core_wp = mx51_babbage_get_dvfs_core_table;
	num_cpu_wp = ARRAY_SIZE(cpu_wp_auto);

	for_each_tag(mem_tag, tags) {
		if (mem_tag->hdr.tag == ATAG_MEM) {
			total_mem = mem_tag->u.mem.size;
			left_mem = total_mem - gpu_mem - fb_mem;
			break;
		}
	}

	for_each_tag(t, tags) {
		if (t->hdr.tag == ATAG_CMDLINE) {
			str = t->u.cmdline.cmdline;
			str = strstr(str, "mem=");
			if (str != NULL) {
				str += 4;
				left_mem = memparse(str, &str);
				if (left_mem == 0 || left_mem > total_mem)
					left_mem = total_mem - gpu_mem - fb_mem;
			}

			str = t->u.cmdline.cmdline;
			str = strstr(str, "gpu_memory=");
			if (str != NULL) {
				str += 11;
				gpu_mem = memparse(str, &str);
			}

			break;
		}
	}

	if (mem_tag) {
		fb_mem = total_mem - left_mem - gpu_mem;
		if (fb_mem < 0) {
			gpu_mem = total_mem - left_mem;
			fb_mem = 0;
		}
		mem_tag->u.mem.size = left_mem;

		/*reserve memory for gpu*/
		gpu_device.resource[5].start =
				mem_tag->u.mem.start + left_mem;
		gpu_device.resource[5].end =
				gpu_device.resource[5].start + gpu_mem - 1;
#if defined(CONFIG_FB_MXC_SYNC_PANEL) || \
	defined(CONFIG_FB_MXC_SYNC_PANEL_MODULE)
		if (fb_mem) {
			mxcfb_resources[0].start =
				gpu_device.resource[5].end + 1;
			mxcfb_resources[0].end =
				mxcfb_resources[0].start + fb_mem - 1;
		} else {
			mxcfb_resources[0].start = 0;
			mxcfb_resources[0].end = 0;
		}
#endif
	}
}

#define PWGT1SPIEN (1<<15)
#define PWGT2SPIEN (1<<16)
#define USEROFFSPI (1<<3)

static void mxc_power_off(void)
{
	/* We can do power down one of two ways:
	   Set the power gating
	   Set USEROFFSPI */

	/* Set the power gate bits to power down */
	pmic_write_reg(REG_POWER_MISC, (PWGT1SPIEN|PWGT2SPIEN),
		(PWGT1SPIEN|PWGT2SPIEN));
}

#if 0
void mxc_verify_gpio(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_gpio_pads, ARRAY_SIZE(mx51babbage_gpio_pads));
}

void mxc_verify_pins(void)
{
	// %%jws%% verify the pad values before setting them
	// outside callers need the pin list from here, so the call to verify
	// has to come thru mx51_babbage to get that
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
}
#endif

static void __init mx51_babbage_io_init(void)
{
	// %%jws%% verify the pad values before setting them
    #ifdef IMS_LOG_PINS
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
    #endif

	mxc_iomux_v3_setup_multiple_pads(mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));

    // -----------------
    // GPIO 1
    // -----------------

    // Free pins we are going to work on
    gpio_free(SCU2_MEZZ_PWM_Out);
    gpio_free(SCU2_MEZZ_HOST_INTERRUPT_B);
    gpio_free(SCU2_MEZZ_WDOG_B_GPIO1_4);
    gpio_free(SCU2_MEZZ_MEZZ_SW_INT_B);
    gpio_free(SCU2_MEZZ_INT_FROM_PMIC);
    gpio_free(SCU2_MEZZ_MEZZ_SWITCH_RESET);
    gpio_free(SCU2_MEZZ_DISCRETE_IN1_B);
    gpio_free(SCU2_MEZZ_DISCRETE_IN2_B);
    gpio_free(SCU2_MEZZ_DISCRETE_IN3_B);
    gpio_free(SCU2_MEZZ_DISCRETE_IN4_B);
    gpio_free(SCU2_MEZZ_DISCRETE_IN5_B);
    gpio_free(SCU2_MEZZ_DISCRETE_IN6_B);
    gpio_free(SCU2_MEZZ_SCU_DISABLE_B);
    gpio_free(SCU2_MEZZ_SCU_ACTIVE);
    gpio_free(SCU2_MEZZ_PRIMARY_SCU_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT1_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT2_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT3_B);

    // Schmatic Name:   PWM_Out
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_GPIO_1_2__GPIO1_2
    gpio_request(SCU2_MEZZ_PWM_Out, "gpio1_2");
    gpio_direction_output(SCU2_MEZZ_PWM_Out, 0);
    gpio_free(SCU2_MEZZ_PWM_Out);

    // Schmatic Name:   HOST_INTERRUPT_B
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_GPIO_1_3__GPIO1_3
    gpio_request(SCU2_MEZZ_HOST_INTERRUPT_B, "gpio1_3");
    gpio_direction_input(SCU2_MEZZ_HOST_INTERRUPT_B);
    gpio_free(SCU2_MEZZ_HOST_INTERRUPT_B);

    // Schmatic Name:   WDOG_B(GPIO1_4)
    // Direction:       output
    // Initial state:   1 (pin is active low)
    // MX51 Pin Name:   MX51_PAD_GPIO1_4__GPIO1_4
    gpio_request(SCU2_MEZZ_WDOG_B_GPIO1_4, "gpio1_4");
    gpio_direction_output(SCU2_MEZZ_WDOG_B_GPIO1_4, 1);
    gpio_free(SCU2_MEZZ_WDOG_B_GPIO1_4);

    // Schmatic Name:   Mezz_SW_INTn
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_GPIO_1_7__GPIO_1_7
    gpio_request(SCU2_MEZZ_MEZZ_SW_INT_B, "gpio1_7");
    gpio_direction_input(SCU2_MEZZ_MEZZ_SW_INT_B);
    gpio_free(SCU2_MEZZ_MEZZ_SW_INT_B);

    // Schmatic Name:   INT_FROM_PMIC(GPIO1_8)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_GPIO_1_8__GPIO1_8
    gpio_request(SCU2_MEZZ_INT_FROM_PMIC, "gpio1_8");
    gpio_direction_input(SCU2_MEZZ_INT_FROM_PMIC);
    gpio_free(SCU2_MEZZ_INT_FROM_PMIC);

    // Schmatic Name:   Mezz_Switch_Reset
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_GPIO_1_9__GPIO_1_9
    gpio_request(SCU2_MEZZ_MEZZ_SWITCH_RESET, "gpio1_9");
    gpio_direction_output(SCU2_MEZZ_MEZZ_SWITCH_RESET, 0);
    gpio_free(SCU2_MEZZ_MEZZ_SWITCH_RESET);

    // Schmatic Name:   DiscreteIN1c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA0__GPIO_1_11
    gpio_request(SCU2_MEZZ_DISCRETE_IN1_B, "gpio1_11");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN1_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN1_B, "discrete_in_z_1");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN1_B, 0);

    // Schmatic Name:   DiscreteIN2c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA1__GPIO_1_12
    gpio_request(SCU2_MEZZ_DISCRETE_IN2_B, "gpio1_12");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN2_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN2_B, "discrete_in_z_2");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN2_B, 0);

    // Schmatic Name:   DiscreteIN3c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA2__GPIO_1_13
    gpio_request(SCU2_MEZZ_DISCRETE_IN3_B, "gpio1_13");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN3_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN3_B, "discrete_in_z_3");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN3_B, 0);

    // Schmatic Name:   DiscreteIN4c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA3__GPIO_1_14
    gpio_request(SCU2_MEZZ_DISCRETE_IN4_B, "gpio1_14");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN4_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN4_B, "discrete_in_z_4");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN4_B, 0);

    // Schmatic Name:   DiscreteIN5c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA4__GPIO_1_15
    gpio_request(SCU2_MEZZ_DISCRETE_IN5_B, "gpio1_15");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN5_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN5_B, "discrete_in_z_5");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN5_B, 0);

    // Schmatic Name:   DiscreteIN6c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA5__GPIO_1_16
    gpio_request(SCU2_MEZZ_DISCRETE_IN6_B, "gpio1_16");
    gpio_direction_input(SCU2_MEZZ_DISCRETE_IN6_B);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_IN6_B, "discrete_in_z_6");  
    gpio_export(SCU2_MEZZ_DISCRETE_IN6_B, 0);

    // Schmatic Name:   SCU_Disable#_c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_DATA6__GPIO_1_17
    gpio_request(SCU2_MEZZ_SCU_DISABLE_B, "gpio1_17");
    gpio_direction_input(SCU2_MEZZ_SCU_DISABLE_B);

    mxc_gpio_set_name(SCU2_MEZZ_SCU_DISABLE_B, "scu_disable_b");
    gpio_export(SCU2_MEZZ_SCU_DISABLE_B, 0);

    // Schmatic Name:   SCU_Active
	// Direction:       output
	// MX51 Pin Name:   MX51_PAD_USBH1_DATA7__GPIO_1_18
	gpio_request(SCU2_MEZZ_SCU_ACTIVE, "gpio1_18");
	gpio_direction_output(SCU2_MEZZ_SCU_ACTIVE, 0);

	mxc_gpio_set_name(SCU2_MEZZ_SCU_ACTIVE, "scu_active_out");
	gpio_export(SCU2_MEZZ_SCU_ACTIVE, 0);

    // Schmatic Name:   Primary_SCU#_c
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_USBH1_CLK__GPIO_1_25
    gpio_request(SCU2_MEZZ_PRIMARY_SCU_B, "gpio1_25");
    gpio_direction_input(SCU2_MEZZ_PRIMARY_SCU_B);

    mxc_gpio_set_name(SCU2_MEZZ_PRIMARY_SCU_B, "primary_scu_b");  
    gpio_export(SCU2_MEZZ_PRIMARY_SCU_B, 0);

    // Schmatic Name:   DiscreteOUT1#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_USBH1_DIR__GPIO_1_26
    gpio_request(SCU2_MEZZ_DISCRETE_OUT1_B, "gpio1_26");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT1_B, 0);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_OUT1_B, "discrete_out_z_1");  
    gpio_export(SCU2_MEZZ_DISCRETE_OUT1_B, 0);

    // Schmatic Name:   DiscreteOUT2#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_USBH1_STP__GPIO_1_27
    gpio_request(SCU2_MEZZ_DISCRETE_OUT2_B, "gpio1_27");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT2_B, 0);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_OUT2_B, "discrete_out_z_2");  
    gpio_export(SCU2_MEZZ_DISCRETE_OUT2_B, 0);

    // Schmatic Name:   DiscreteOUT3#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_USBH1_NXT__GPIO_1_28
    gpio_request(SCU2_MEZZ_DISCRETE_OUT3_B, "gpio1_28");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT3_B, 0);

    mxc_gpio_set_name(SCU2_MEZZ_DISCRETE_OUT3_B, "discrete_out_z_3");  
    gpio_export(SCU2_MEZZ_DISCRETE_OUT3_B, 0);
    

    // -----------------
    // GPIO 2
    // -----------------

    // Free pins we are going to work on
    gpio_free(SCU2_MEZZ_A429_1_MB2);
    gpio_free(SCU2_MEZZ_A429_1_MB3);
    gpio_free(SCU2_MEZZ_A429_2_MB2);
    gpio_free(SCU2_MEZZ_A429_2_MB3);
    gpio_free(SCU2_MEZZ_A429_3_MB2);
    gpio_free(SCU2_MEZZ_A429_3_MB3);
    gpio_free(SCU2_MEZZ_DIAG_LED_GPIO);
    gpio_free(SCU2_MEZZ_RST_ENET_B);
    gpio_free(SCU2_MEZZ_IRQ_B);
    gpio_free(SCU2_1V8_DIG1);

    // Schmatic Name:   A429-1_MB2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D16__GPIO_2_0
    gpio_request(SCU2_MEZZ_A429_1_MB2, "gpio2_0");
    gpio_direction_input(SCU2_MEZZ_A429_1_MB2);
    gpio_free(SCU2_MEZZ_A429_1_MB2);

    // Schmatic Name:   A429-1_MB3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D17__GPIO_2_1
    gpio_request(SCU2_MEZZ_A429_1_MB3, "gpio2_1");
    gpio_direction_input(SCU2_MEZZ_A429_1_MB3);
    gpio_free(SCU2_MEZZ_A429_1_MB3);

    // Schmatic Name:   A429-2_MB2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D18__GPIO_2_2
    gpio_request(SCU2_MEZZ_A429_2_MB2, "gpio2_2");
    gpio_direction_input(SCU2_MEZZ_A429_2_MB2);
    gpio_free(SCU2_MEZZ_A429_2_MB2);

    // Schmatic Name:   A429-2_MB3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D19__GPIO_2_3
    gpio_request(SCU2_MEZZ_A429_2_MB3, "gpio2_3");
    gpio_direction_input(SCU2_MEZZ_A429_2_MB3);
    gpio_free(SCU2_MEZZ_A429_2_MB3);

    // Schmatic Name:   A429-3_MB2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D20__GPIO_2_4
    gpio_request(SCU2_MEZZ_A429_3_MB2, "gpio2_4");
    gpio_direction_input(SCU2_MEZZ_A429_3_MB2);
    gpio_free(SCU2_MEZZ_A429_3_MB2);

    // Schmatic Name:   A429-3_MB3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_D21__GPIO_2_5
    gpio_request(SCU2_MEZZ_A429_3_MB3, "gpio2_5");
    gpio_direction_input(SCU2_MEZZ_A429_3_MB3);
    gpio_free(SCU2_MEZZ_A429_3_MB3);

    // Schmatic Name:   DIAG_LED_GPIO(GPIO2_11)
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_EIM_A17__GPIO_2_11
    gpio_request(SCU2_MEZZ_DIAG_LED_GPIO, "gpio1_11");
    gpio_direction_output(SCU2_MEZZ_DIAG_LED_GPIO, 1);
    gpio_free(SCU2_MEZZ_DIAG_LED_GPIO);

    // Schmatic Name:   RST_ENET_B(EIM_A20_GPIO2_14)
    // Direction:       output, active low reset
    // MX51 Pin Name:   MX51_PAD_EIM_A20__GPIO_2_14
    gpio_request(SCU2_MEZZ_RST_ENET_B, "gpio2_14");
    gpio_direction_output(SCU2_MEZZ_RST_ENET_B, 1);
    gpio_free(SCU2_MEZZ_RST_ENET_B);

    // Schmatic Name:   ENET_IRQ_B(EIM_A21_GPIO2_15)
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_EIM_A21__GPIO_2_15
    gpio_request(SCU2_MEZZ_IRQ_B, "gpio1_17");
    gpio_direction_input(SCU2_MEZZ_IRQ_B);
    gpio_free(SCU2_MEZZ_IRQ_B);

    // -----------------
    // GPIO 3
    // -----------------

    // Free pins we are going to work on
    gpio_free(SCU2_MEZ_A429_1_RINT);
    gpio_free(SCU2_MEZZ_A429_2_RINT);
    gpio_free(SCU2_MEZZ_A429_3_RINT);
    gpio_free(SCU2_MEZZ_A429_4_RINT);
    gpio_free(SCU2_MEZZ_AC429_1_FLAG);
    gpio_free(SCU2_MEZZ_AC429_2_FLAG);
    gpio_free(SCU2_MEZZ_A429_MR1);
    gpio_free(SCU2_MEZZ_EMMC2_RESET);
    gpio_free(SCU2_MEZZ_A429_3_FLAG);
    gpio_free(SCU2_MEZZ_A429_4_FLAG);
    gpio_free(SCU2_MEZZ_A429_1_MB1);
    gpio_free(SCU2_MEZZ_A429_SS0);
    gpio_free(SCU2_MEZZ_A429_M2);
    gpio_free(SCU2_MEZZ_A429_SS1);
    gpio_free(SCU2_MEZZ_A429_4_MB1);
    gpio_free(SCU2_MEZZ_A429_TEMPTY2);
    gpio_free(SCU2_MEZZ_A429_TFULL2);


    // Schmatic Name:   A429-1_RINT
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_WE_B__GPIO_3_3
    gpio_request(SCU2_MEZ_A429_1_RINT, "gpio3_3");
    gpio_direction_input(SCU2_MEZ_A429_1_RINT);
    gpio_free(SCU2_MEZ_A429_1_RINT);

    // Schmatic Name:   A429-2_RINT
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_RE_B__GPIO_3_4
    gpio_request(SCU2_MEZZ_A429_2_RINT, "gpio3_4");
    gpio_direction_input(SCU2_MEZZ_A429_2_RINT);
    gpio_free(SCU2_MEZZ_A429_2_RINT);

    // Schmatic Name:   A429-3_RINT
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_ALE__GPIO_3_5
    gpio_request(SCU2_MEZZ_A429_3_RINT, "gpio3_5");
    gpio_direction_input(SCU2_MEZZ_A429_3_RINT);
    gpio_free(SCU2_MEZZ_A429_3_RINT);

    // Schmatic Name:   A429-4_RINT
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_CLE__GPIO_3_6
    gpio_request(SCU2_MEZZ_A429_4_RINT, "gpio3_6");
    gpio_direction_input(SCU2_MEZZ_A429_4_RINT);
    gpio_free(SCU2_MEZZ_A429_4_RINT);

    // Schmatic Name:   A429-1_FLAG
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_WP_B__GPIO_3_7
    gpio_request(SCU2_MEZZ_AC429_1_FLAG, "gpio3_7");
    gpio_direction_input(SCU2_MEZZ_AC429_1_FLAG);
    gpio_free(SCU2_MEZZ_AC429_1_FLAG);

    // Schmatic Name:   A429-2_FLAG
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_RB0__GPIO_3_8
    gpio_request(SCU2_MEZZ_AC429_2_FLAG, "gpio3_8");
    gpio_direction_input(SCU2_MEZZ_AC429_2_FLAG);
    gpio_free(SCU2_MEZZ_AC429_2_FLAG);

    // Schmatic Name:   A429_MR1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_GPIO_NAND__GPIO_3_12
    gpio_request(SCU2_MEZZ_A429_MR1, "gpio3_12");
    gpio_direction_output(SCU2_MEZZ_A429_MR1, 0);
    gpio_free(SCU2_MEZZ_A429_MR1);

    // Schmatic Name:   MMC2_Reset
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_CSI1_D9__GPIO_3_13
    gpio_request(SCU2_MEZZ_EMMC2_RESET, "gpio3_13");
    gpio_direction_output(SCU2_MEZZ_EMMC2_RESET, 1);
    usleep_range(10000, 20000); // Hold the part in reset for 10 ms
    gpio_direction_output(SCU2_MEZZ_EMMC2_RESET, 0);
    
    mxc_gpio_set_name(SCU2_MEZZ_EMMC2_RESET, "mmc2_reset");  
    gpio_export(SCU2_MEZZ_EMMC2_RESET, 0);

    // Schmatic Name:   A429-3_FLAG
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_CS0__GPIO_3_16
    gpio_request(SCU2_MEZZ_A429_3_FLAG, "gpio3_16");
    gpio_direction_input(SCU2_MEZZ_A429_3_FLAG);
    gpio_free(SCU2_MEZZ_A429_3_FLAG);

    // Schmatic Name:   A429-4_FLAG
    // Direction:       output (active high reset)
    // MX51 Pin Name:   MX51_PAD_NANDF_CS1__GPIO_3_17
    gpio_request(SCU2_MEZZ_A429_4_FLAG, "gpio3_17");
    gpio_direction_output(SCU2_MEZZ_A429_4_FLAG, 0);
    gpio_free(SCU2_MEZZ_A429_4_FLAG);

    // Schmatic Name:   A429-1_MB1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_CS7__GPIO_3_23
    gpio_request(SCU2_MEZZ_A429_1_MB1, "gpio3_23");
    gpio_direction_input(SCU2_MEZZ_A429_1_MB1);
    gpio_free(SCU2_MEZZ_A429_1_MB1);

    // Schmatic Name:   A429_SS0
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_NANDF_RDY_INT__GPIO_3_24
    gpio_request(SCU2_MEZZ_A429_SS0, "gpio3_24");
    gpio_direction_output(SCU2_MEZZ_A429_SS0, 1);
    gpio_free(SCU2_MEZZ_A429_SS0);

    // Schmatic Name:   A429_M2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D13__GPIO_3_27
    gpio_request(SCU2_MEZZ_A429_M2, "gpio3_27");
    gpio_direction_output(SCU2_MEZZ_A429_M2, 0);
    gpio_free(SCU2_MEZZ_A429_M2);

    // Schmatic Name:   A429_SS1
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_NANDF_D12__GPIO_3_28
    gpio_request(SCU2_MEZZ_A429_SS1, "gpio3_28");
    gpio_direction_output(SCU2_MEZZ_A429_SS1, 1);
    gpio_free(SCU2_MEZZ_A429_SS1);

    // Schmatic Name:   A429-4_MB1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D11__GPIO_3_29
    gpio_request(SCU2_MEZZ_A429_4_MB1, "gpio3_29");
    gpio_direction_input(SCU2_MEZZ_A429_4_MB1);
    gpio_free(SCU2_MEZZ_A429_4_MB1);

    // Schmatic Name:   A429_TEMPTY2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D10__GPIO_3_30
    gpio_request(SCU2_MEZZ_A429_TEMPTY2, "gpio3_30");
    gpio_direction_input(SCU2_MEZZ_A429_TEMPTY2);
    gpio_free(SCU2_MEZZ_A429_TEMPTY2);

    // Schmatic Name:   A429_TFULL2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D9__GPIO_3_31
    gpio_request(SCU2_MEZZ_A429_TFULL2, "gpio3_30");
    gpio_direction_input(SCU2_MEZZ_A429_TFULL2);
    gpio_free(SCU2_MEZZ_A429_TFULL2);


    // -----------------
    // GPIO 4
    // -----------------

    // free pins we are going to work on
    gpio_free(SCU2_MEZZ_A429_TEMPTY1);
    gpio_free(SCU2_MEZZ_A429_TFULL1);
    gpio_free(SCU2_MEZZ_A429_3_MB1);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_0);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_1);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_2);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_3);
    gpio_free(SCU2_MEZZ_A429_2_MB1);
    gpio_free(SCU2_ESB_SD4_CD);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT4_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT5_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT6_B);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT7_B);
    gpio_free(SCU2_MEZZ_SW_RESET_REQUEST);

    // Schmatic Name:   A429_TEMPTY1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D8__GPIO_4_0
    gpio_request(SCU2_MEZZ_A429_TEMPTY1, "gpio4_0");
    gpio_direction_input(SCU2_MEZZ_A429_TEMPTY1);
    gpio_free(SCU2_MEZZ_A429_TEMPTY1);

    // Schmatic Name:   A429-3_MB1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D6__GPIO_4_2
    gpio_request(SCU2_MEZZ_A429_TFULL1, "gpio4_2");
    gpio_direction_input(SCU2_MEZZ_A429_TFULL1);
    gpio_free(SCU2_MEZZ_A429_TFULL1);

    // Schmatic Name:   A429-3_MB1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D7__GPIO_4_1
    gpio_request(SCU2_MEZZ_A429_3_MB1, "gpio4_1");
    gpio_direction_input(SCU2_MEZZ_A429_3_MB1);
    gpio_free(SCU2_MEZZ_A429_3_MB1);

    // Schmatic Name:   SYSTEM_TYPE_0
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D5__GPIO_4_3
    gpio_request(SCU2_MEZZ_SYSTEM_TYPE_0, "gpio4_3");
    gpio_direction_input(SCU2_MEZZ_SYSTEM_TYPE_0);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_0);

    // Schmatic Name:   SYSTEM_TYPE_1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D4__GPIO_4_4
    gpio_request(SCU2_MEZZ_SYSTEM_TYPE_1, "gpio4_4");
    gpio_direction_input(SCU2_MEZZ_SYSTEM_TYPE_1);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_1);

    // Schmatic Name:   SYSTEM_TYPE_2
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D3__GPIO_4_5
    gpio_request(SCU2_MEZZ_SYSTEM_TYPE_2, "gpio4_5");
    gpio_direction_input(SCU2_MEZZ_SYSTEM_TYPE_2);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_2);

    // Schmatic Name:   SYSTEM_TYPE_3
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D2__GPIO_4_6
    gpio_request(SCU2_MEZZ_SYSTEM_TYPE_3, "gpio4_6");
    gpio_direction_input(SCU2_MEZZ_SYSTEM_TYPE_3);
    gpio_free(SCU2_MEZZ_SYSTEM_TYPE_3);

    // Schmatic Name:   A429-2_MB1
    // Direction:       input
    // MX51 Pin Name:   MX51_PAD_NANDF_D1__GPIO_4_7
    gpio_request(SCU2_MEZZ_A429_2_MB1, "gpio4_7");
    gpio_direction_input(SCU2_MEZZ_A429_2_MB1);
    gpio_free(SCU2_MEZZ_A429_2_MB1);

    // Schmatic Name:   DiscreteOUT4#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_TXD__GPIO_4_18
    gpio_request(SCU2_MEZZ_DISCRETE_OUT4_B, "gpio4_18");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT4_B, 0);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT4_B);

    // Schmatic Name:   DiscreteOUT5#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_RXD__GPIO_4_19
    gpio_request(SCU2_MEZZ_DISCRETE_OUT5_B, "gpio4_19");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT5_B, 0);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT5_B);


    // Schmatic Name:   DiscreteOUT6#
    // Direction:       output
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_CK__GPIO4_20
    gpio_request(SCU2_MEZZ_DISCRETE_OUT6_B, "gpio4_20");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT6_B, 0);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT6_B);

    // Schmatic Name:   DiscreteOUT7#
    // Direction:       output
    // Initial State:   1
    // MX51 Pin Name:   MX51_PAD_AUD3_BB_FS__GPIO4_21
    gpio_request(SCU2_MEZZ_DISCRETE_OUT7_B, "gpio4_21");
    gpio_direction_output(SCU2_MEZZ_DISCRETE_OUT7_B, 0);
    gpio_free(SCU2_MEZZ_DISCRETE_OUT7_B);

    // Schmatic Name:   SWITCH_RESET_REQUEST
    // Direction:       output
    // Initial State:   1 (switch reset is active low)
    // MX51 Pin Name:   MX51_PAD_CSPI1_RDY__GPIO_4_26
    gpio_request(SCU2_MEZZ_SW_RESET_REQUEST, "gpio4_26");
    gpio_direction_output(SCU2_MEZZ_SW_RESET_REQUEST, 1);
    gpio_free(SCU2_MEZZ_SW_RESET_REQUEST);

#ifdef IMS_LOG
	// %%jws%% verify them again after the setup
	mxc_iomux_v3_verify_pads( mx51babbage_pads, ARRAY_SIZE(mx51babbage_pads));
#endif
}

// Proc Entries
struct proc_dir_entry *rave_proc_dir;

static int _reset_reason;
static int resetReason_proc( char *buf, char **start, off_t offset_param, int count, int *eof, void *data_param )
{
	int len = 0;
	switch (_reset_reason) {
	case 0x09:
		len += sprintf(buf+len, "Reset reason: User\n");
		break;
	case 0x01:
		len += sprintf(buf+len, "Reset reason: Power-on\n");
		break;
	case 0x10:
	case 0x11:
		len += sprintf(buf+len, "Reset reason: WDOG\n");
		break;
	default:
		len += sprintf(buf+len, "Reset reason unknown: 0x%x\n", _reset_reason);
		break;
	}

	*eof = 1;
	return len;
}

// Populate the /proc/rave folder with the values stored in the nameplate eeprom
void populateUnitInfo(struct memory_accessor *mem_accessor, void *context) {
	scu2_esb_mezz_namepate(mem_accessor, rave_proc_dir);
}


/*!
 * Board specific initialization.
 */
static void __init mxc_board_init(void)
{
	void    *memptr;

    mxc_ipu_data.di_clk[0] = clk_get(NULL, "ipu_di0_clk");
	mxc_ipu_data.di_clk[1] = clk_get(NULL, "ipu_di1_clk");
	mxc_ipu_data.csi_clk[0] = clk_get(NULL, "csi_mclk1");
	mxc_ipu_data.csi_clk[1] = clk_get(NULL, "csi_mclk2");

	/* SD card detect irqs */
 
	// NIU has no SD card detect GPIOs.  SD1 is a fixed part.  SD4 is a
	// micro footprint SD card.  Card present is assumed, there is no hardware sense
	// for card present.
	// write protect is not implemented for micro sd cards in hardware either so
	// no gpio for that either.

	mxc_cpu_common_init();
	mx51_babbage_io_init();

	mxc_register_device(&mxc_dma_device, NULL);
	mxc_register_device(&mxc_wdt_device, NULL);

	// eCSPI and CSPI Ports
	mxc_register_device(&mxcspi1_device, &mxcspi1_data);
    #ifndef CONFIG_HOLT_ARINC
	    mxc_register_device(&mxcspi2_device, &mxcspi2_data);
    #else
	    platform_device_register( &arinc_spi_device );
    #endif

	mxc_register_device(&mxci2c_devices[1], &mxci2c_data);
//	mxc_register_device(&mxc_w1_master_device, &mxc_w1_data);
	mxc_register_device(&mxc_ipu_device, &mxc_ipu_data);
	mxc_register_device(&mxc_tve_device, &tve_data);
	mxc_register_device(&mxcvpu_device, &mxc_vpu_data);
	mxc_register_device(&gpu_device, NULL);
	mxc_register_device(&mxcscc_device, NULL);
	mxc_register_device(&mx51_lpmode_device, NULL);
	mxc_register_device(&busfreq_device, &bus_freq_data);
	mxc_register_device(&sdram_autogating_device, NULL);
	mxc_register_device(&mxc_dvfs_core_device, &dvfs_core_data);
	mxc_register_device(&mxc_dvfs_per_device, &dvfs_per_data);
	mxc_register_device(&mxc_iim_device, &iim_data);
	mxc_register_device(&mxc_pwm1_device, NULL);

	// SDHC Devices
	mxc_register_device(&mxcsdhc1_device, &mmc1_data);
	mxc_register_device(&mxcsdhc4_device, &mmc4_data);

	mxc_register_device(&mxc_ssi1_device, NULL);
	mxc_register_device(&mxc_ssi2_device, NULL);
	mxc_register_device(&mxc_ssi3_device, NULL);

	// FEC
	mxc_register_device(&mxc_fec_device, NULL);

    // V4L2
	mxc_register_device(&mxc_v4l2_device, NULL);
	mxc_register_device(&mxc_v4l2out_device, NULL);

	// PMIC
	mx51_babbage_init_mc13892();

	// I2C Devices
	//platform_device_register(&i2c_mcu_bitbang_device);

	i2c_register_board_info(1, mxc_i2c2_board_info,
		ARRAY_SIZE(mxc_i2c2_board_info));

	// SPI Devices
	spi_register_board_info(mxc_dataflash_device,
				ARRAY_SIZE(mxc_dataflash_device));

    #ifdef CONFIG_HOLT_ARINC
        spi_register_board_info(arinc_board_info, ARRAY_SIZE(arinc_board_info));
    #endif


	pm_power_off = mxc_power_off;

	// USB
	mx5_usb_dr_init();	// OTG Port

	// Ethernet Drivers
	setup_ethernet_switch();

	memptr = ioremap( MX51_SRC_BASE_ADDR, 0x1000 );
	_reset_reason = readl(memptr + 0x8);
	iounmap( memptr );

	{
		rave_proc_dir = proc_mkdir( "rave", NULL );
		create_proc_read_entry( "reset_reason",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				resetReason_proc,
				NULL );		/* client data */

		create_proc_read_entry( "board_type",
				0,		/* default mode */
				rave_proc_dir,	/* parent dir */
				boardType_proc,
				NULL );		/* client data */
	}
                                          
}

static void __init mx51_babbage_timer_init(void)
{
	struct clk *uart_clk;

	/* Change the CPU voltages for TO2*/
	if (mx51_revision() == IMX_CHIP_REVISION_2_0) {
		cpu_wp_auto[0].cpu_voltage = 1175000;
		cpu_wp_auto[1].cpu_voltage = 1100000;
		cpu_wp_auto[2].cpu_voltage = 1000000;
	}

	mx51_clocks_init(32768, 24000000, 22579200, 24576000);

	uart_clk = clk_get_sys("mxcintuart.0", NULL);
	early_console_setup(UART1_BASE_ADDR, uart_clk);
}

static struct sys_timer mxc_timer = {
	.init	= mx51_babbage_timer_init,
};

/*
 * The following uses standard kernel macros define in arch.h in order to
 * initialize __mach_desc_MX51_BABBAGE data structure.
 */
/* *INDENT-OFF* */
MACHINE_START(MX51_BABBAGE, "Freescale MX51 Babbage Board")
	/* Maintainer: Freescale Semiconductor, Inc. */
	.phys_io	= AIPS1_BASE_ADDR,
	.io_pg_offst	= ((AIPS1_BASE_ADDR_VIRT) >> 18) & 0xfffc,
	.fixup = fixup_mxc_board,
	.map_io = mx5_map_io,
	.init_irq = mx5_init_irq,
	.init_machine = mxc_board_init,
	.timer = &mxc_timer,
MACHINE_END

