#ifndef __MARVELL6183_H
#define __MARVELL6183_H


/** multi-chip mode register definitions *************************************/
#define SMIDA_SMICOMMAND_REG	0x00
#define SSR_SMIBUSY		BIT(15)
#define SSR_SMIMODE_22		BIT(12)
#define SSR_SMIMODE_45		0
#define SSR_SMIOP(x)		(((x)&MASK(1,0))<<10)
#define SSR_SMIOP_WRADDR	0
#define SSR_SMIOP_WRDATA	1
#define SSR_SMIOP_RDDATA	2
#define SSR_SMIOP_RDDATA_INC	3
#define SSR_DEVADDR(x)		(((x)&MASK(4,0))<<5)
#define SSR_REGADDR(x)		(((x)&MASK(4,0))<<0)

#define SMIDA_SMIDATA_REG	0x01



/** register definitions *****************************************************/
#define PHY_REG_OFFSET                  0x00 
#define SWITCHPORT_REG_OFFSET           0x10

// switch registers, ref 4.4
// SMI Device Addresses for Switch Registers, ref 4.4
typedef enum SMI_DeviceAddress_t {
	SMIDA_PHY0_REG = PHY_REG_OFFSET,
	SMIDA_PHY1_REG,
	SMIDA_PHY2_REG,
	SMIDA_PHY3_REG,
	SMIDA_PHY4_REG,
	SMIDA_PHY5_REG,
	
	SMIDA_PHYC_REG = 0xc,
	SMIDA_PHYD_REG,

	SMIDA_SwitchPort0_REG = SWITCHPORT_REG_OFFSET,
	SMIDA_SwitchPort1_REG,
	SMIDA_SwitchPort2_REG,
	SMIDA_SwitchPort3_REG,
	SMIDA_SwitchPort4_REG,
	SMIDA_SwitchPort5_REG,

	SMIDA_SwitchGlobal_REG = 0x1B,
	SMIDA_SwitchGlobal2_REG = 0x1C,
} SMI_DeviceAddress_t;

#define SMIDA_LASTPHY_REG	SMIDA_PHY4_REG // ignore unsued phy port
#define SMIDA_LASTPORT_REG	SMIDA_SwitchPort5_REG

#define NUM_PHYS        ((SMIDA_LASTPHY_REG - SMIDA_PHY0_REG) + 1)
#define NUM_SWITCHPORTS ((SMIDA_LASTPORT_REG - SMIDA_SwitchPort0_REG) + 1)

typedef enum SMI_Register_t {
//typedef enum SMI_IEEEPhy_Register_t {
	SIR_PHYControl_REG = 0,
	SIR_PHYStatus_REG,
	SIR_PHYIdentifier0_REG,
	SIR_PHYIdentifier1_REG,
	SIR_AuteNegAd_REG,
	SIR_LinkPartnerAbility_REG,
	SIR_AutoNegExpansion_REG,
	SIR_NextPage_REG,
	SIR_LinkPartnerNextPage_REG,
	SIR_MasterSlaveControl_REG,
	SIR_MasterSlaveStatus_REG,

	SIR_Reserved_B_REG,
	SIR_Reserved_C_REG,
	SIR_Reserved_D_REG,
	SIR_Reserved_E_REG,

	SIR_ExtendedStatus_REG,
	SIR_PHYSpecific1_REG,
	SIR_PHYStatus1_REG,
	SIR_PHYInterruptEnable_REG,
	SIR_PHYStatus2_REG,
	SIR_PHYControl3_REG,
	SIR_ReceiveErrorCounter_REG,
	SIR_PageAddress_REG,
	SIR_InterruptStatus_REG,

	SIR_Vendor_18_REG,
	SIR_Vendor_19_REG,
	SIR_Vendor_1A_REG,
	SIR_Vendor_1B_REG,

	SIR_PHYControl2_REG,

	SIR_Vendor_1D_REG,
	SIR_Vendor_1E_REG,
	SIR_Vendor_1F_REG,
//} SMI_IEEEPhy_Register_t;

// Switch Port Registers (dev 0x10:0x19), ref 4.4
//typedef enum SMI_Port_Register_t {
	SPR_PortStatus_REG = 0,
	SPR_PCS_Control_REG,		// 0x01
	SPR_JammingControl_REG,		// 0x02
	SPR_SwitchIdentifier_REG,	// 0x03
	SPR_PortControl_REG,		// 0x04
	SPR_PortControl_1_REG,		// 0x05
	SPR_PortBasedVLANMap_REG,	// 0x06
	SPR_DefaultVLAN_Pri_REG,	// 0x07
	SPR_PortControl_2_REG,		// 0x08
	SPR_EgressRateControl_REG,	// 0x09
	SPR_EgressRateControl_2_REG,	// 0x0A
	SPR_PORT_ASSOCIATION_VECTOR_REG,// 0x0B
	SPR_PortATUControl_REG,		// 0x0C
	SPR_PriorityOverride_REG,	// 0x0D

	SPR_RESERVED_E_REG,		// 0x0E

	SPR_PortEType_REG,		// 0x0F
	SPR_InDiscardLoFrameCounter_REG,// 0x10    // RO
	SPR_InDiscardHiFrameCounter_REG,// 0x11    // RO
	SPR_InFilteredFrameCounter_REG,	// 0x12
	SPR_OutFilteredFrameCounter_REG,// 0x13
	SPR_RESERVED_14_REG,		// 0x14
	SPR_RESERVED_15_REG,		// 0x15
	SPR_RESERVED_16_REG,		// 0x16
	SPR_RESERVED_17_REG,		// 0x17

	SPR_TAG_REMAP30_REG,		// 0x18
	SPR_TAG_REMAP74_REG,		// 0x19

	SPR_RESERVED_1A_REG,		// 0x1A

	SPR_QueueCounters_REG,		// 0x1B

	SPR_RESERVED_1C_REG,		// 0x1C
	SPR_RESERVED_1D_REG,		// 0x1D
	SPR_RESERVED_1E_REG,		// 0x1E
	SPR_RESERVED_1F_REG,		// 0x1F
//} SMI_Port_Register_t;

//Switch Global Registers (dev 0x1B), ref 4.4
//typedef enum SMI_Global_Register_t {
	SGR_GlobalStatus_REG = 0x00,
	SGR_MACAddrB01_REG,		// 0x01
	SGR_MACAddrB23_REG,		// 0x02
	SGR_MACAddrB45_REG,		// 0x03
	SGR_GlobalControl_REG,		// 0x04
	SGR_VTUOperation_REG,	       // 0x05
	SGR_VTUVID_REG,		       // 0x06
	SGR_VTU_DATA_PORT30_REG,	// 0x07
	SGR_VTU_DATA_PORT74_REG,	// 0x08
	SGR_VTU_DATA_PORT98_REG,	// 0x09
	SGR_ATUControl_REG,		// 0x0a
	SGR_ATUOperation_REG,		// 0x0b
	SGR_ATUData_REG,		// 0x0c
	SGR_ATUMACAddrB01_REG,		// 0x0d
	SGR_ATUMACAddrB23_REG,		// 0x0e
	SGR_ATUMacAddrB45_REG,		// 0x0f

	SGR_IPPRI0Mapping_REG,		// 0x10
	SGR_IPPRI1Mapping_REG,		// 0x11
	SGR_IPPRI2Mapping_REG,		// 0x12
	SGR_IPPRI3Mapping_REG,		// 0x13
	SGR_IPPRI4Mapping_REG,		// 0x14
	SGR_IPPRI5Mapping_REG,		// 0x15
	SGR_IPPRI6Mapping_REG,		// 0x16
	SGR_IPPRI7Mapping_REG,		// 0x17
	SGR_IEEEPRIMapping_REG,		// 0x18

	SGR_RESERVED_19_REG,		// 0x19
	SGR_RESERVED_1A_REG,		// 0x1A
	SGR_RESERVED_1B_REG,		// 0x1B

	SGR_GlobalControl2_REG,		// 0x1C
	SGR_StatsOperation_REG,		// 0x1D
	SGR_StatsDataBytes32_REG,	// 0x1E
	SGR_StatsDataBytes10_REG,	// 0x1F
//} SMI_Global_Register_t;
} SMI_Register_t;

// SPR Port Status Register (0x00), ref 4.4, table 44
#define SPSR_PAUSEENABLED		BIT(15) // RO
#define SPSR_MyPause			BIT(14) // RO
#define SPSR_HALF_DUP			BIT(13) // RO
#define SPSR_PHY_DETECT			BIT(12) // RWR
#define SPSR_Link			BIT(11) // RO
#define SPSR_Duplex			BIT(10)  // RO
#define SPSR_SPEED			(BIT(9)|BIT(8))  // RO
#define SPSR_RESERVED_7			BIT(7)  // RO
#define SPSR_HI_ERR_RATE		BIT(6)  // RO
#define SPSR_TX_PAUSED			BIT(5)  // RO
#define SPSR_FLOW_CNTRL			BIT(4)  // RO
#define SPSR_C_MODE(x)			((x)&(BIT(2)|BIT(1)|BIT(0))) // RO
#define SPSR_C_DUPLIX			BIT(3)  // only valid for port 9
#define SPSR_C_MODE_FD_GMII		0x0000
#define SPSR_C_MODE_FD_MII		(BIT(0))  // RO
#define SPSR_C_MODE_FD_MII_100		(BIT(1))  // RO
#define SPSR_C_MODE_FD_MII_10		(BIT(1)|BIT(0)) // RO
#define SPSR_C_MODE_FD_Internal		(BIT(2))        // RO
#define SPSR_C_MODE_FD_1000		(BIT(2)|BIT(0)) // RO
#define SPSR_C_MODE_MGMII		(BIT(2)|BIT(1)) // RO
#define SPSR_C_MODE_PortDisabled	(BIT(2)|BIT(1)|BIT(0))  // RO

// SPR PCS Control Register (0x01), ref 4.4, table 46
#define SPCS_CommaDet			BIT(15) // RO
#define SPCS_SyncOK			BIT(14) // RO
#define SPCS_SyncFail			BIT(13) // RO
#define SPCS_AnBypassed			BIT(12) // RO
#define SPCS_AnBypass			BIT(11) // RWS
#define SPCS_PCSAnEn			BIT(10) // RWR/RWS
#define SPCS_RestartPCSAn		BIT(9)  // SC
#define SPCS_PCSAnDone			BIT(8)  // RO
#define SPCS_Reserved7			BIT(7)  // RES
#define SPCS_Reserved6			BIT(6)  // RES
#define SPCS_LinkValue			BIT(5)  // RWR
#define SPCS_ForcedLink			BIT(4)  // RWR
#define SPCS_DpxValue			BIT(3)  // RWR
#define SPCS_ForcedDpx			BIT(2)  // RWR
#define SPCS_ForceSpd			(BIT(1)|BIT(0))  // RWS to 0x3
#define SPCS_NormDet			(BIT(1)|BIT(0))  // RWS to 0x3
#define SPCS_Mbps1000			BIT(1)  // RWS to 0x3
#define SPCS_Mbps100			BIT(0)  // RWS to 0x3
#define SPCS_Mbps10			(0x00)  // RWS to 0x3

// SPR Switch Identifier Register (0x03), ref 4.4, table 47
#define SSIR_DeviceID			0xFFF0  // RO
#define SSIR_RevID			0x000F  // RO

// SPR Port Control Register (0x04), ref 4.4, table 48
#define SPCR_ForceFlowControl		BIT(15) // RWR
#define SPCR_DropOnLock			BIT(14) // RWR

#define SPCR_EgressMode_MASK		MASK(13,12)
#define SPCR_EgressMode_Add		(0x0003 << 12) // RWR
#define SPCR_EgressMode_Tagged		(0x0002 << 12) // RWR
#define SPCR_EgressMode_Untagged	(0x0001 << 12) // RWR
#define SPCR_EgressMode_Normal		(0x0000 << 12)

#define SPCR_Header			BIT(11) // RWR Ingress header
#define SPCR_IgmpMldSnooping		BIT(10) // RWR

#define SPCR_DoubleTag			BIT(9)  // RWR Ingress double tagging
#define SPCR_InterswitchPort		BIT(8)  // RWR Enable Marvell tags on ingress/egress

#define SPCR_VLANTunnel			BIT(7)  // RWR
#define SPCR_TagIfBoth			BIT(6)  // RWS
#define SPCR_UseIP			BIT(5)  // RWS
#define SPCR_UseTag			BIT(4)  // RWS
#define SPCR_PROTECTED_PORT		BIT(3)  // RWR
#define SPCR_FORWARD_UNKN		BIT(2)  // RWS

#define SPCR_PortState_MASK		MASK(1,0)   // RW
#define SPCR_PortState(x)		((x)&SPCR_PortState_MASK)   // RW
#define SPCR_PortState_Forwarding	((3)<<0)
#define SPCR_PortState_Learning		((2)<<0)
#define SPCR_PortState_Blocking		((1)<<0)
#define SPCR_PortState_Disabled		((0)<<0)

#define SPCR_TaggingMask		(SPCR_EgressMode_MASK|SPCR_Header|SPCR_DoubleTag|SPCR_InterswitchPort)



// SPR Port Based VLAN Map (0x06), ref 4.4, table 49
#define SPVM_DBNum_MASK			MASK(15,12) // RWR
#define SPVM_LEARN_DISABLE		BIT(11)  // RWR
#define SPVM_IGNORE_FCS			BIT(10)  // RWR
#define SPVM_VLANTable_MASK		MASK(9,0) // RW
#define SPVM_PortVec(x)			(BIT(x) & SPVM_VLANTable_MASK)

// SPR Default VLAN and Priority (0x07), ref 4.4, table 50
#define SDVP_DefaultPri			(BIT(15)|BIT(14)) // RWR
#define SDVP_TagPriLSB			BIT(13)
#define SDVP_ForceDefaultVID		BIT(12) // RWR
#define SDVP_DefaultVID			MASK(11,0)  // RWS to 0x001

// SPCR2 Port Control 2 (0x08), ref 4.4, table 51
// SRC Rate Control (0x09), ref 4.4, table 52
// SRC2 Rate Control 2 (0x10), ref 4.4, table 53
// SPAV Port Association Vector (0x11), ref 4.4, table 54

// SGR Global Status (0x00), ref 4.5
#define SGSR_PPU_STATE(x)		((x)&(BIT(15)|BIT(14))) // RO
#define SGSR_PPUSTATE_DISABLEDATRESET	(0<<14)
#define SGSR_PPUSTATE_ACTIVE		(1<<14)
#define SGSR_PPUSTATE_DISABLED		(2<<14)
#define SGSR_PPUSTATE_POLLING		(3<<14)

#define SGSR_SWMode(x)			((x)&MASK(13,12)) // RO
#define SGSR_SWMode_CPUAttached		(0 << 12)
#define SGSR_SWMode_StandAlone		(2 << 12)
#define SGSR_SWMode_EEPROMAtttached	(3 << 12)

#define SGSR_INIT_READY             BIT(11) // RO
#define SGSR_RESERVED_10            BIT(10) // RO
#define SGSR_RESERVED_9             BIT(9) // RO
#define SGSR_RESERVED_8             BIT(8) // RO
#define SGSR_RESERVED_7             BIT(7) // RO
#define SGSR_STATS_DONE             BIT(6) // ROC
#define SGSR_VTU_PROBLEM            BIT(5) // RO
#define SGSR_VTU_DONE               BIT(4) // ROC
#define SGSR_ATU_PROBLEM            BIT(3) // RO
#define SGSR_ATUDone                BIT(2) // ROC
#define SGSR_RESERVED_1             BIT(1) // RO
#define SGSR_EEInt                  BIT(0)

// SGR MAC Address Bytes 0 & 1 (0x01), ref 4.5
#define SMAB_DiffAddr			BIT(8)  // RWR

// SGR MAC Address Bytes 2 & 3 (0x02), ref 4.5
// SGR MAC Address Bytes 4 & 5 (0x03), ref 4.5
#define SMAB_MACByte_lo			0xff00
#define SMAB_MACByte_hi			0x00ff

// SGR Global Control Register (0x04), ref 4.5
#define SGCR_SW_RESET			BIT(15) // SC
#define SGCR_PPU_ENABLE			BIT(14)
#define SGCR_DISCARD_EXCESSIVE		BIT(13) // RES

#define SGCR_ReLoad			BIT(9)  // SC
#define SGCR_AVBIntEn			BIT(8)  // RWR
#define SGCR_DevOmtEn			BIT(7)
#define SGCR_StatsDoneIntEn		BIT(6)
#define SGCR_VTUProbIntEn		BIT(5)
#define SGCR_VTUDoneIntEn		BIT(4)
#define SGCR_ATUProbIntEn		BIT(3)
#define SGCR_ATUDoneIntEn		BIT(2)  // RWR
#define SGCR_EEIntEn			BIT(0)  // RWS


// SGR VTU VID Regsiter (0x06), ref ...
#define SVVR_Valid			BIT(12)
#define SVVR_VID			MASK(11,0)

// SGR ATU Control Register (0x0a), ref 4.5
#define SACR_AgeTime			MASK(11,4)	// RWS 0x14
#define SACR_Learn2All			BIT(3)
#define SACR_RESERVED_2to0		MASK(2,0)

// SGR ATU Operation Register (0x0b), ref 4.5
#define SAOR_ATUBusy			BIT(15) // SC
#define SAOR_ATUOp_GetClearViolation	((7)<<12)
#define SAOR_ATUOp_FlushUnlockedDBn	((6)<<12)
#define SAOR_ATUOp_FlushAllDBn		((5)<<12)
#define SAOR_ATUOp_GetNext		((4)<<12)
#define SAOR_ATUOp_LoadEntry		((3)<<12)
#define SAOR_ATUOp_FlushUnlocked	((2)<<12)
#define SAOR_ATUOp_FlushAll		((1)<<12)
#define SAOR_ATUOp_None			((0)<<12)
#define SAOR_MACPRI_MASK		MASK(10,8)
#define SAOR_EntryPri_Set(x)		(((x)&MASK(2,0))<<8)	// 6185 only
#define SAOR_EntryPri_Get(x)		(((x)>>8)&MASK(2,0))	// 6185 only
#define SAOR_AgeOutViolation		BIT(7)
#define SAOR_MemberViolation		BIT(6)
#define SAOR_MissViolation		BIT(5)
#define SAOR_ATUFullViolation		BIT(4)

// SGR ATU Data Register (0x0c), ref 4.5
#define SADR_Trunk			BIT(15)
#define SADR_PortVec_MASK		MASK(11,4)		// 
#define SADR_PortVec_Set(x)		(((x)&MASK(7,0))<<4)	// set port vector MASK
#define SADR_PortVec_Get(x)		(((x)>>4)&MASK(7,0))	// get port vector MASK
#define SADR_PortVec_BIT(x)		BIT((x)+4)		// set port vector bit
#define SADR_EntryState_Set(x)		((x)&MASK(3,0))		// return entry state
#define SADR_EntryState_Get(x)		((x)&MASK(3,0))		// return entry state

	#define SADR_EntryState_Management		0xe
	#define SADR_EntryState_UnicastStatic		0x000f
	#define SADR_EntryState_MulticastStatic		0x0005
	#define SADR_EntryState_MulticastRateLimit	0x0007
	#define SADR_EntryState_MulticastManagement	0x000e

// SGR ATU MAC Address Bytes 0 & 1 (0x0d), ref 4.2.3.7
// SGR ATU MAC Address Bytes 2 & 3 (0x0e), ref 4.2.3.7
// SGR ATU MAC Address Bytes 4 & 5 (0x0f), ref 4.2.3.7
#define SAMB_ATUByte_lo			0xff00  // RWR
#define SAMB_ATUByte_hi			0x00ff  // RWR

// SGR IP Priority 0 Mapping (0x10), ref 4.5, table 75
// SGR IP Priority 1 Mapping (0x11)
// SGR IP Priority 2 Mapping (0x12)
// SGR IP Priority 3 Mapping (0x13)
// SGR IP Priority 4 Mapping (0x14)
// SGR IP Priority 5 Mapping (0x15)
// SGR IP Priority 6 Mapping (0x16)
// SGR IP Priority 7 Mapping (0x17)
// SGR IEEE Priority Mapping (0x18)

// Global Control 2, (0x1c)

// Stats Operation Register (0x1d)
#define SOR_StatsBusy					BIT(15)
#define SOR_StatsOp_Mask				MASK(14,12)
	#define SOR_StatsOp_NoOp			(0x0<<12)
	#define SOR_StatsOp_ClrAllPortAll		(0x1<<12)
	#define SOR_StatsOp_ClrAllPort			(0x2<<12)
	#define SOR_StatsOp_ReadCounter			(0x4<<12)
	#define SOR_StatsOp_CapAllCntPort		(0x5<<12)
#define SOR_HistMode_Mask				(BIT(11) | BIT(10))
	#define SOR_HM_CountRxdOnly			(0x1<<10)
	#define SOR_HM_CountTxdOnly			(0x2<<10)
	#define SOR_HM_CountALL				(0x3<<10)
#define SOR_StatsPort_Mask				MASK(8,5)
#define SOR_StatsPtr_Mask				MASK(5,0)


// Stats Counter Reg Bytes 3 & 2 (0x1e)
// Stats Counter Reg Bytes 1 & 0 (0x1f)

// SMI Device Addresses for PHY Registers, ref 
#define SMIDA_PHYPort0_REG              0x00
#define SMIDA_PHYPort1_REG              0x01
#define SMIDA_PHYPort2_REG              0x02
#define SMIDA_PHYPort3_REG              0x03
#define SMIDA_PHYPort4_REG              0x04
#define SMIDA_PHYPort5_REG              0x05
#define SMIDA_PHYPort6_REG              0x06
#define SMIDA_PHYPort7_REG              0x07
#define SMIDA_PHYPort8_REG              0x08
#define SMIDA_PHYPort9_REG              0x09

// PHY Registers, ref 4., these are originally define in 802.3 Section 22.2.4
#define PHY_Control_REG                 0x00
#define PHY_Status_REG                  0x01
#define PHY_IdentHi_REG                 0x02
#define PHY_IdentLo_REG                 0x03
#define PHY_AnegAd_REG                  0x04
#define PHY_LinkPartnerAbility_REG      0x05
#define PHY_AnegExpansion_REG           0x06
#define PHY_NxtPageTransmit_REG         0x07
#define PHY_LinkPartnerNxtPage_REG      0x08
#define PHY_SpecificControl_REG         0x10
#define PHY_SpecificStatus_REG          0x11
#define PHY_InterruptEnable_REG         0x12
#define PHY_InterruptStatus_REG         0x13
#define PHY_InterruptPortSummary_REG    0x14
#define PHY_ReceiveErrorCounter_REG     0x15
#define PHY_LEDParallelSelect_REG       0x16
#define PHY_LEDControl_REG              0x18


// PHY Control Register (0x00), ref 
#define PCR_SWReset                 BIT(15) // RWR, SC
#define PCR_Loopback                BIT(14) // RWR
#define PCR_Speed                   BIT(13)
#define PCR_AnegEn                  BIT(12) // RWS
#define PCR_PwrDwn                  BIT(11) // RWR
#define PCR_Isolate                 BIT(10) // RO
#define PCR_RestartAneg             BIT(9)  // RWR, SC
#define PCR_Duplex                  BIT(8)  // RWS
#define PCR_ColTest                 BIT(7)  // RO
#define PCR_SpeedMSB                BIT(6)  // RO

// PHY Status Register (0x01), ref , RO
#define PSR_CAN_100T4               BIT(15)
#define PSR_CAN_100FDX              BIT(14)
#define PSR_CAN_100HDX              BIT(13)
#define PSR_CAN_10FDX               BIT(12)
#define PSR_CAN_10HDX               BIT(11)
#define PSR_CAN_100T2FDX            BIT(10)
#define PSR_CAN_100T2HDX            BIT(9)
#define PSR_ExtdStatus              BIT(8)
#define PSR_MFPreSup                BIT(6)
#define PSR_AnegDone                BIT(5)
#define PSR_RemoteFault             BIT(4)
#define PSR_AnegAble                BIT(3)
#define PSR_Lin                     BIT(2)
#define PSR_JabberDet               BIT(1)
#define PSR_ExtdReg                 BIT(0)

// PHY Identifier Msb Register (0x02), ref 4.3, RO
// PHY Identifier Lsb Register (0x03), ref 4.3, RO
// PHY Auto-Negotiation Advertisement Register (0x04), ref 4.3
#define PAAR_AnegAdNxtPage          BIT(15) // RWR
#define PAAR_AnegAdReFault          BIT(13) // RWR
#define PAAR_AnegAdPause            BIT(10) // RWR
#define PAAR_AnegAd100T4            BIT(9)  // RO
#define PAAR_AnegAd100FDX           BIT(8)  // RWS
#define PAAR_AnegAd100HDX           BIT(7)  // RWS
#define PAAR_AnegAd10FDX            BIT(6)  // RWS
#define PAAR_AnegAd10HDX            BIT(5)  // RWS
#define PAAR_AnegAdSelector         0x000f  // RO

// PHY Link Partner Ability Register, Base Page (0x05), ref 4.3.2.6, RO
#define PLPAR_LPNxtPage             BIT(15)
#define PLPAR_LPAck                 BIT(14)
#define PLPAR_LPRemoteFault         BIT(13)
#define PLPAR_LPTechAble            0x1fe0
#define PLPAR_LPSelector            0x001f

// PHY Link Partner Ability Regsiter, Next Page (0x05), ref 4.3.2.7, RO
#define PLPAR_LPMessage             BIT(13)
#define PLPAR_LPAck2                BIT(12)
#define PLPAR_LPToggle              BIT(11)
#define PLPAR_LPData                0x07ff

// PHY Auto-Negotiation Expansion Register (0x06), ref 4.3.2.8, RO
#define PAER_ParFaultDet            BIT(4)
#define PAER_LPNxtPgAble            BIT(3)
#define PAER_LocalNxtPgAble         BIT(2)
#define PAER_RxNewPage              BIT(1)
#define PAER_LPAnegAble             BIT(0)

// PHY Next Page Transmit Register (0x07), ref 4.3.2.9
#define PNPTR_TxNxtPage             BIT(15) // RWR
#define PNPTR_TxMessage             BIT(13) // RWS
#define PNPTR_TxAck2                BIT(12) // RWR
#define PNPTR_TxToggle              BIT(11) // RO
#define PNPTR_TxData                0x07ff  // RWS to 0x001

// PHY Link Partner Next Page Rgister (0x08), ref 4.3.2.10, RO
#define PLPNPR_RxNxtPage            BIT(15)
#define PLPNPR_RxAck                BIT(14)
#define PLPNPR_RxMessage            BIT(13)
#define PLPNPR_RxAck2               BIT(12)
#define PLPNPR_RxToggle             BIT(11)
#define PLPNPR_RxData               0x07ff

// PHY Specific Control Register (0x10), ref 4.3.2.12
#define PSCR_DisNLPCheck            BIT(13) // RWR
#define PSCR_Reg8NxtPg              BIT(12) // RWR
#define PSCR_DisNLPGen              BIT(11) // RWR
#define PSCR_ForceLink              BIT(10) // RWR
#define PSCR_DisScrambler           BIT(9)  // RWR
#define PSCR_DisFEFI                BIT(8)  // RWR
#define PSCR_ExtdDistance           BIT(7)  // RWR
#define PSCR_TPSelect               BIT(6)  // RWR
#define PSCR_AutoMDI                (BIT(5)|BIT(4))   // RWR to 0x2
#define PSCR_ForceMDI				BIT(4)
#define PSCR_ForceMDIX				0
#define PSCR_RxFIFODepth            (BIT(3)|BIT(2))   // RWR
#define PSCR_PHYAutoPol             BIT(1)  // RWR
#define PSCR_DisJabber              BIT(0)

// Marvell 11xx externel phy defs for Reg (0x10)
#define PHY_11xx_AutoMDI                (BIT(6)|BIT(5))   // RWR to 0x2
#define PHY_11xx_ForceMDI				0
#define PHY_11xx_ForceMDIX				BIT(5)


// PHY Specific Status Register (0x11), ref 4.3.2.13, RO
#define PSSR_PHYSpeed               BIT(14)
#define PSSR_PHYDuplex              BIT(13)
#define PSSR_PHYRxPage              BIT(12)
#define PSSR_PHYLink                BIT(10)
#define PSSR_PHYMDI                 BIT(6)
#define PSSR_PHYPolarity            BIT(1)
#define PSSR_Jabber                 BIT(0)

// PHY Interrupt Enable Register (0x12), ref 4.3.2.14
#define PIER_SpeedIntEn             BIT(14) // RWR
#define PIER_DuplexIntEn            BIT(13) // RWR
#define PIER_RxPageIntEn            BIT(12) // RWR
#define PIER_AnegDoneIntEn          BIT(11) // RWR
#define PIER_LinkIntEn              BIT(10) // RWR
#define PIER_SymErrIntEn            BIT(9)  // RWR
#define PIER_FalseCarrierIntEn      BIT(8)  // RWR
#define PIER_FIFOIntEn              BIT(7)  // RWR
#define PIER_MDIMDIXIntEn           BIT(6)  // RWR
#define PIER_PolarityEn             BIT(1)  // RWR
#define PIER_JabberIntEn            BIT(0)  // RWR

// PHY Interrupt Status Register (0x13), ref 4.3.2.15
#define PISR_SpeedInt               BIT(14)
#define PISR_DuplexInt              BIT(13)
#define PISR_RxPageInt              BIT(12)
#define PISR_AnegDoneInt            BIT(11)
#define PISR_LinkInt                BIT(10)
#define PISR_SymErrInt              BIT(9)
#define PISR_FalseCarrierInt        BIT(8)
#define PISR_FIFOInt                BIT(7)
#define PISR_MDIMDIXInt             BIT(6)
#define PISR_Polarity               BIT(1)
#define PISR_JabberInt              BIT(0)

// PHY Interrupt Port Summary Register (0x14), ref 4.3.2.16, RO
#define PIPSR_Port7IntActive        BIT(7)
#define PIPSR_Port6IntActive        BIT(6)
#define PIPSR_Port5IntActive        BIT(5)
#define PIPSR_Port4IntActive        BIT(4)
#define PIPSR_Port3IntActive        BIT(3)
#define PIPSR_Port2IntActive        BIT(2)
#define PIPSR_Port1IntActive        BIT(1)
#define PIPSR_Port0IntActive        BIT(0)

// PHY Receive Error Counter Register (0x15), ref 4.3.2.17, RO
// PHY LED Parallel Select Register (0x16), ref 4.3.2.18
#define PLPSR_LED2_COLX             0
#define PLPSR_LED2_ERROR            BIT(8)
#define PLPSR_LED2_DUPLEX           BIT(9)
#define PLPSR_LED2_DUPLEX_COLX      (BIT(9)|BIT(8))
#define PLPSR_LED2_SPEED            BIT(10)
#define PLPSR_LED2_LINK             (BIT(10)|BIT(8))
#define PLPSR_LED2_TX               (BIT(10)|BIT(9))
#define PLPSR_LED2_RX               (BIT(10)|BIT(9)|BIT(8))
#define PLPSR_LED2_ACT              BIT(11)
#define PLPSR_LED2_LINK_RX          (BIT(11)|BIT(8))
#define PLPSR_LED2_LINK_ACT         (BIT(11)|BIT(9))
#define PLPSR_LED2_FORCE            (BIT(11)|BIT(10)|BIT(9)|BIT(8))
#define PLPSR_LED1_COLX             0
#define PLPSR_LED1_ERROR            BIT(4)
#define PLPSR_LED1_DUPLEX           BIT(5)
#define PLPSR_LED1_DUPLEX_COLX      (BIT(5)|BIT(4))
#define PLPSR_LED1_SPEED            BIT(6)
#define PLPSR_LED1_LINK             (BIT(6)|BIT(4))
#define PLPSR_LED1_TX               (BIT(6)|BIT(5))
#define PLPSR_LED1_RX               (BIT(6)|BIT(5)|BIT(4))
#define PLPSR_LED1_ACT              BIT(7)
#define PLPSR_LED1_LINK_RX          (BIT(7)|BIT(4))
#define PLPSR_LED1_LINK_ACT         (BIT(7)|BIT(5))
#define PLPSR_LED1_FORCE            (BIT(7)|BIT(6)|BIT(5)|BIT(4))
#define PLPSR_LED0_COLX             0
#define PLPSR_LED0_ERROR            BIT(0)
#define PLPSR_LED0_DUPLEX           BIT(1)
#define PLPSR_LED0_DUPLEX_COLX      (BIT(1)|BIT(0))
#define PLPSR_LED0_SPEED            BIT(2)
#define PLPSR_LED0_LINK             (BIT(2)|BIT(0))
#define PLPSR_LED0_TX               (BIT(2)|BIT(1))
#define PLPSR_LED0_RX               (BIT(2)|BIT(1)|BIT(0))
#define PLPSR_LED0_ACT              BIT(3)
#define PLPSR_LED0_LINK_RX          (BIT(3)|BIT(0))
#define PLPSR_LED0_LINK_ACT         (BIT(3)|BIT(1))
#define PLPSR_LED0_FORCE            (BIT(3)|BIT(2)|BIT(1)|BIT(0))

// PHY LED Control Register (0x18), ref 4.3.2.20
#define PLCR_PulseStretch           0x7000
#define PLCR_BlinkRate              0x0e00

typedef struct MarvellIngressTag_t {
	unsigned int
	Operation:2,
	Trg_Tagged:1,
	Trg_Dev:5,
	Trg_Port:5,
	Reserved18:1,
	Prio:2,

	PRI:3,
	Reserved12:1,
	VID:12;
} MarvellIngressTag_t;

#define OPERATION_FORWARD           0x3 // Ingress Forward
#define OPERATION_INGRESSFROMCPU    0x1 // Ingress "From_CPU"
#define OPERATION_EGRESSTOCPU       0x0

#define TARGET_UNTAGGED     0   // egress untagged
#define TARGET_TAGGED       1   // egress tagged

typedef struct MarvellEgressTag_t {
	unsigned int
	Operation:2,
	Src_Tagged:1,
	Src_Dev:5,
	Src_Port:5,
	Code3_1:3,

	PRI:3,
	Code0:1,
	VID:12;
} MarvellEgressTag_t;


typedef union MarvellTag_t {
	MarvellIngressTag_t ingress;
	MarvellEgressTag_t egress;
	int u32;
	short u16[2];
} MarvellTag_t;

typedef struct Ieee8023ac_t {
	unsigned int
	proto:16, // should always be 0x8100
	pri:3,
	cfi:1,	// should always be 0
	vid:12;
} Ieee8023ac_t;

/*************************************************/
enum {
	STATS_INGOODOCTETSLO = 0,
	STATS_INGOODOCTETSHI,
	STATS_INBADOCTETS,
	STATS_OUTFCSERR,
	STATS_INUNICAST,
	STATS_DEFERRED,
	STATS_INBROADCASTS,
	STATS_INMULTICASTS,
	STATS_64OCTETS,
	STATS_65to127OCTETS,
	STATS_128to255OCTETS,
	STATS_256to511OCTETS,
	STATS_512to1023OCTETS,
	STATS_1024toMAXOCTETS,
	STATS_OUTOCTETSLO,
	STATS_OUTOCTETSHI,
	STATS_OUTUNICAST,
	STATS_EXCESSIVE,
	STATS_OUTMULTICASTS,
	STATS_OUTBROADCASTS,
	STATS_SINGLE,
	STATS_OUTPAUSE,
	STATS_INPAUSE,
	STATS_MULTIPLE,
	STATS_INUNDERSIZE,
	STATS_INFRAGMENTS,
	STATS_INOVERSIZE,
	STATS_INJABBER,
	STATS_INRXERR,
	STATS_INFCSERR,
	STATS_COLLISIONS,
	STATS_LATE,
};

typedef union atuEntry_t {
	struct {
		unsigned short SAOR;
		unsigned short SADR;
		unsigned char esa[6];
	} data;
	struct {
		unsigned short ATUOperation;
		unsigned short ATUData;
		unsigned short ATUMACAddrB01;
		unsigned short ATUMACAddrB23;
		unsigned short ATUMACAddrB45;
	} regs;
	unsigned short raw[4];
} atuEntry_t;

typedef union marvell_PortCounters_t {
    struct {
        unsigned int GoodOctetes_Rec_low;       // 0
		unsigned int GoodOctetes_Rec_high;      // 1
		unsigned int BadOctets_Rec;             // 2
		unsigned int OutFCSErr;                 // 3
		unsigned int InUniCast;                 // 4 
		unsigned int Deffered;                  // 5
		unsigned int BroadcastFramesRec;        // 6
		unsigned int MulticastFramesRec;        // 7
		unsigned int Frames64Octets;            // 8
		unsigned int Frames_65_to_127_Octets;   // 9
		unsigned int Frames_128_to_255_Octets;  // A
		unsigned int Frames_256_to_511_Octets;  // B
		unsigned int Frames_512_to_1023_Octets; // C
		unsigned int Frames_1024_to_Max_Octets; // D
		unsigned int OutOctetsLo;               // E
		unsigned int OutOctetsHigh;             // F
		unsigned int OutUnicast;              	// 10
		unsigned int Excessive;             	// 11
		unsigned int OutMuiltcast;             	// 12
		unsigned int OutBroadcast;           	// 13
		unsigned int Single;       		       	// 14
		unsigned int OutPause;          	   	// 15
		unsigned int InPause;           		// 16
		unsigned int Multiple;           		// 17
		unsigned int InUndersize;           	// 18
		unsigned int InFragments;            	// 19
		unsigned int InOversize;              	// 1A
		unsigned int InJabber;         			// 1B
		unsigned int Reserved;              	// 1C
		unsigned int InFCSErr;          		// 1D
		unsigned int Collisions;      			// 1E
		unsigned int LateColls;                 // 1F
		} counters;
		unsigned int raw[32];	    			
} marvell_PortCounters_t;

#endif

