#ifndef __HI3593_H
#define __HI3593_H

#ifndef BIT
#define BIT(x)	(1<<(x))
#endif

// cmd format: b7 r/w, b6:2 source/destination, b1:b0 don't care
#define HI3593_CMD_S_MRESET	    0x4	// no field
#define HI3593_CMD_S_FIFORST	0x44	// no field

#define HI3593_CMD_S_INTR	    0x34	// 8b field
#define HI3593_CMD_G_INTR	    0xD0	// 8b field
#define HI3593_CMD_S_ACLKDIV	0x38	// 8b field
#define HI3593_CMD_G_ACLKDIV	0xD4	// 8b field

// transmit commands
#define HI3593_CMD_S_TXCTRL	    0x08	// 8b field
#define HI3593_CMD_G_TXCTRL	    0x84	// 8b field
#define HI3593_CMD_S_TXFIFO	    0x0C	// 32b field
#define HI3593_CMD_S_TXSTART	0x40	// no field
#define HI3593_CMD_G_TXSTATUS	0x80	// 8b field

// receive channel 1 commands
#define HI3593_CMD_S_RX1CTRL	0x10	// 8b field
#define HI3593_CMD_S_RX1LABEL	0x14	// 256b field
#define HI3593_CMD_S_RX1PRI	    0x18	// 24b field
#define HI3593_CMD_S_RX1AO	    0x48	// no field (All Ones)

#define HI3593_CMD_G_RX1STATUS	0x90	// 8b field
#define HI3593_CMD_G_RX1CTRL	0x94	// 8b field
#define HI3593_CMD_G_RX1LABEL	0x98	// 256b field
#define HI3593_CMD_G_RX1PRI	    0x9C	// 24b field
#define HI3593_CMD_G_RX1FIFO	0xA0	// 32b field
#define HI3593_CMD_G_RX1PRI1	0xA4	// 24b field
#define HI3593_CMD_G_RX1PRI2	0xA8	// 24b field
#define HI3593_CMD_G_RX1PRI3	0xAC	// 24b field

// receive channel 2 commands
#define HI3593_CMD_S_RX2CTRL	0x24	// 8b field
#define HI3593_CMD_S_RX2LABEL	0x28	// 256b field
#define HI3593_CMD_S_RX2PRI	    0x2C	// 24b field
#define HI3593_CMD_S_RX2AO	    0x4C	// no field

#define HI3593_CMD_G_RX2STATUS	0xB0	// 8b field
#define HI3593_CMD_G_RX2CTRL	0xB4	// 8b field
#define HI3593_CMD_G_RX2LABEL	0xB8	// 256b field
#define HI3593_CMD_G_RX2PRI	    0xBC	// 24b field
#define HI3593_CMD_G_RX2FIFO	0xC0	// 24b field
#define HI3593_CMD_G_RX2PRI1	0xC4	// 24b field
#define HI3593_CMD_G_RX2PRI2	0xC8	// 24b field
#define HI3593_CMD_G_RX2PRI3	0xCC	// 24b field


// control register, accessed with commands n4, n5
#define RC7_RFLIP		BIT(7)	// reverse receive bit order of first 8
#define RC6_SD9			BIT(6)	// arinc b9 match-bit
#define RC5_SD10		BIT(5)	// arinc b10 match-bit
#define RC4_SDON		BIT(4)	// match arinch b9&10
#define RC3_PARITY		BIT(3)	// parity check
#define RC2_LABREC		BIT(2)	// enable filtering by label
#define RC1_PLON		BIT(1)	// priority-label register enable
#define RC0_RATE		BIT(0)	// 0 selects high-speed rate. 1 selects /8 rate

#define TC7_HIZ			BIT(7)	// place TX in High-Z
#define TC6_TFLIP		BIT(6)	// reverse xmit order of first 8
#define TC5_TMODE		BIT(5)	// if set, hold data until "start xmt" command
#define TC4_SELFTEST   BIT(4)	// loop back to both rx'ers
#define TC3_ODDEVEN		BIT(3)	// set check parity
#define TC2_TPARITY		BIT(2)	// tranmit parity
#define TC0_RATE		BIT(1)	// set 0 for xmit high-rate

#define HI3593_RX_INIT	( 0 )
#define HI3593_TX_INIT	( 0 )

// receive status register
#define RS5_PL3			BIT(5)	// priority label 3 rcv'd
#define RS4_PL2			BIT(4)	// priority label 2 rcv'd
#define RS3_PL1			BIT(3)	// priority label 1 rcv'd
#define RS2_FFFULL		BIT(2)	// FIFO full
#define RS1_FFHALF		BIT(1)	// FIFO half full
#define RS0_FFEMPTY		BIT(0)	//

// transmit status register
#define TS2_TFFULL		BIT(2)	//
#define TS1_TFHALF		BIT(1)
#define TS0_TFEMPTY		BIT(0)

// ACLK divisor
#define ACLK_DIV(x)		(((x)&0xf)<<1)

// interrupt control register
#define FI_R2INTR_MB3		((3)<<6) // interrupt on mail box 3
#define FI_R2INTR_MB2		((2)<<6)
#define FI_R2INTR_MB1		((1)<<6)
#define FI_R2INTR_RCV		((0)<<6) // interrupt on any
#define FI_R2FLAG_FNE		((3)<<4) // interrupt on rcv fifo not empty
#define FI_R2FLAG_FHF		((2)<<4) // interrupt on rcv fifo half full
#define FI_R2FLAG_FF		((1)<<4) // interrupt on rcv fifo full
#define FI_R2FLAG_FE		((0)<<4) // interrupt on fifo empty
#define FI_R1INTR_MB3		((3)<<2) // interrupt on mail box 3
#define FI_R1INTR_MB2		((2)<<2)
#define FI_R1INTR_MB1		((1)<<2)
#define FI_R1INTR_RCV		((0)<<2) // interrupt on any
#define FI_R1FLAG_FNE		((3)<<0) // interrupt on rcv fifo not empty
#define FI_R1FLAG_FHF		((2)<<0) // interrupt on rcv fifo half full
#define FI_R1FLAG_FF		((1)<<0) // interrupt on rcv fifo full
#define FI_R1FLAG_FE		((0)<<0) // interrupt on fifo empty

// NOTE: the order of this enum matches the order of the commands below.
// this is intended to be an idx of the command descriptor array.
// structures are defined in holt_arinc.c
// the first 4 fields should always be the same from chip to chip.
enum {
	// general control
	HI3593_CMD_S_MRESET_IDX = 0,
	HI3593_CMD_S_FIFORST_IDX,
	HI3593_CMD_S_INTR_IDX,
	HI3593_CMD_G_INTR_IDX,
	HI3593_CMD_S_ACLKDIV_IDX,
	HI3593_CMD_G_ACLKDIV_IDX,

	// transmit commands
	HI3593_CMD_S_TXCTRL_IDX,
	HI3593_CMD_G_TXCTRL_IDX,
	HI3593_CMD_S_TXFIFO_IDX,
	HI3593_CMD_S_TXSTART_IDX,
	HI3593_CMD_G_TXSTATUS_IDX,

	// receive channel 1 commands
	HI3593_CMD_S_RX1CTRL_IDX,
	HI3593_CMD_S_RX1PRI_IDX,
	HI3593_CMD_S_RX1LABEL_IDX,
	HI3593_CMD_S_RX1AO_IDX,
	HI3593_CMD_G_RX1STATUS_IDX,
	HI3593_CMD_G_RX1CTRL_IDX,
	HI3593_CMD_G_RX1LABEL_IDX,
	HI3593_CMD_G_RX1PRI_IDX,
	HI3593_CMD_G_RX1FIFO_IDX,
	HI3593_CMD_G_RX1PRI1_IDX,
	HI3593_CMD_G_RX1PRI2_IDX,
	HI3593_CMD_G_RX1PRI3_IDX,

	// receive channel 2 commands
	HI3593_CMD_S_RX2CTRL_IDX,
	HI3593_CMD_S_RX2PRI_IDX,
	HI3593_CMD_S_RX2LABEL_IDX,
	HI3593_CMD_S_RX2AO_IDX,
	HI3593_CMD_G_RX2STATUS_IDX,
	HI3593_CMD_G_RX2CTRL_IDX,
	HI3593_CMD_G_RX2LABEL_IDX,
	HI3593_CMD_G_RX2PRI_IDX,
	HI3593_CMD_G_RX2FIFO_IDX,
	HI3593_CMD_G_RX2PRI1_IDX,
	HI3593_CMD_G_RX2PRI2_IDX,
	HI3593_CMD_G_RX2PRI3_IDX,
};

struct holt_arinc_cmd_desc hi3593_commands[] = {
	// general control
    {
         .name = "Master Reset",
         .cmd_id = HI3593_CMD_S_MRESET,
    },
	{
		.name = "Fifo Reset",
		.cmd_id = HI3593_CMD_S_FIFORST,
	},
	{
		.name = "Set Flag/Interrupt Assignment",
		.cmd_id = HI3593_CMD_S_INTR,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get Flag/Interrupt Assignment",
		.cmd_id = HI3593_CMD_G_INTR,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Set ACLK Divider",
		.cmd_id = HI3593_CMD_S_ACLKDIV,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get ACLK Divider",
		.cmd_id = HI3593_CMD_G_ACLKDIV,
		.field_sz = 1,
		.word_sz = 8,
	},

	// transmit commands
	{
		.name = "Get TX Control",
		.cmd_id = HI3593_CMD_S_TXCTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Set TX Control",
		.cmd_id = HI3593_CMD_G_TXCTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Load Transmit FIFO",
		.cmd_id = HI3593_CMD_S_TXFIFO,
		.field_sz = 1,
		.word_sz = 32,
	},
	{
		.name = "Start Transmit", // used only when tx is not autostart
		.cmd_id = HI3593_CMD_S_TXSTART,
	},
	{
		.name = "Get TX Status",
		.cmd_id = HI3593_CMD_G_TXSTATUS,
		.field_sz = 1,
		.word_sz = 8,
	},

	// receive channel 1 commands
	{
		.name = "Set R1 Control",
		.cmd_id = HI3593_CMD_S_RX1CTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Set R1 Priority Labels",
		.cmd_id = HI3593_CMD_S_RX1PRI,
		.field_sz = 3,
		.word_sz = 8,
	},
	{
		.name = "Set R1 Label Mask",
		.cmd_id = HI3593_CMD_S_RX1LABEL,
		.field_sz = 32,
		.word_sz = 8,
	},
	{
		.name = "Reset R1 Label Mask", // sets the label mask to all 1's (receive all)
		.cmd_id = HI3593_CMD_S_RX1AO,
	},
	{
		.name = "Get R1 Status",
		.cmd_id = HI3593_CMD_G_RX1STATUS,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get R1 Control",
		.cmd_id = HI3593_CMD_G_RX1CTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get R1 Label Mask",
		.cmd_id = HI3593_CMD_G_RX1LABEL,
		.field_sz = 32,
		.word_sz = 8,
	},
	{
		.name = "Read R1 Priority Labels",
		.cmd_id = HI3593_CMD_G_RX1PRI,
		.field_sz = 3,
		.word_sz = 8,
	},
	{
		.name = "Read R1 FIFO",
		.cmd_id = HI3593_CMD_G_RX1FIFO,
		.field_sz = 32,
		.word_sz = 32,
	},
	{
		.name = "Read R1 Priority FIFO 1",
		.cmd_id = HI3593_CMD_G_RX1PRI1,
		.field_sz = 1,
		.word_sz = 24, // no label
	},
	{
		.name = "Read R1 Priority FIFO 2",
		.cmd_id = HI3593_CMD_G_RX1PRI2,
		.field_sz = 1,
		.word_sz = 24, // no label
	},
	{
		.name = "Read R1 Priority FIFO 3",
		.cmd_id = HI3593_CMD_G_RX1PRI3,
		.field_sz = 1,
		.word_sz = 24, // no label
	},

	// receive channel 2 commands
	{
		.name = "Set R2 Control",
		.cmd_id = HI3593_CMD_S_RX2CTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Set R2 Priority Labels",
		.cmd_id = HI3593_CMD_S_RX2PRI,
		.field_sz = 3,
		.word_sz = 8,
	},
	{
		.name = "Set R2 Label Mask",
		.cmd_id = HI3593_CMD_S_RX2LABEL,
		.field_sz = 32,
		.word_sz = 8,
	},
	{
		.name = "Reset R2 Label Mask", // sets the label mask to all 1's (receive all)
		.cmd_id = HI3593_CMD_S_RX2AO,
	},
	{
		.name = "Get R2 Status",
		.cmd_id = HI3593_CMD_G_RX2STATUS,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get R2 Control",
		.cmd_id = HI3593_CMD_G_RX2CTRL,
		.field_sz = 1,
		.word_sz = 8,
	},
	{
		.name = "Get R2 Label Mask",
		.cmd_id = HI3593_CMD_G_RX2LABEL,
		.field_sz = 32,
		.word_sz = 8,
	},
	{
		.name = "Read R2 Priority Labels",
		.cmd_id = HI3593_CMD_G_RX2PRI,
		.field_sz = 3,
		.word_sz = 8,
	},
	{
		.name = "Read R2 FIFO",
		.cmd_id = HI3593_CMD_G_RX2FIFO,
		.field_sz = 32,
		.word_sz = 32,
	},
	{
		.name = "Read R2 Priority FIFO 1",
		.cmd_id = HI3593_CMD_G_RX2PRI1,
		.field_sz = 1,
		.word_sz = 24, // no label
	},
	{
		.name = "Read R2 Priority FIFO 2",
		.cmd_id = HI3593_CMD_G_RX2PRI2,
		.field_sz = 1,
		.word_sz = 24, // no label
	},
	{
		.name = "Read R2 Priority FIFO 3",
		.cmd_id = HI3593_CMD_G_RX2PRI3,
		.field_sz = 1,
		.word_sz = 24, // no label
	},
};

static struct chip_features hi3593tx_features = {
	.name = "hi3593tx",
	.cr_init = HI3593_TX_INIT,
	.baseminor_tx = 0,
	.num_txers = 1,
	.use_fifo_tx = true,
	.baseminor_rx = 0,
	.num_rxers = 0,
	.cmd_list = hi3593_commands,
	.ioctl_cmdIdx = {
		HI3593_CMD_S_MRESET_IDX,
		HI3593_CMD_G_TXSTATUS_IDX,
		HI3593_CMD_G_TXCTRL_IDX,
		HI3593_CMD_S_TXCTRL_IDX,
		-1,
		-1,
	}
};

static struct chip_features hi3593r1_features = {
	.name = "hi3593r1",
	.cr_init = HI3593_RX_INIT,
	.baseminor_tx = 0,
	.num_txers = 0,
	.use_fifo_rx = true,
	.baseminor_rx = 1,
	.num_rxers = 1,
	.cmd_list = hi3593_commands,
	.ioctl_cmdIdx = {
		HI3593_CMD_S_MRESET_IDX,
		HI3593_CMD_G_RX1STATUS_IDX,
		HI3593_CMD_G_RX1CTRL_IDX,
		HI3593_CMD_S_RX1CTRL_IDX,
		HI3593_CMD_G_RX1LABEL_IDX,
		HI3593_CMD_S_RX1LABEL_IDX,
	}
};

static struct chip_features hi3593r2_features = {
	.name = "hi3593r2",
	.cr_init = HI3593_RX_INIT,
	.baseminor_tx = 0,
	.num_txers = 0,
	.use_fifo_rx = true,
	.baseminor_rx = 2,
	.num_rxers = 1,
	.cmd_list = hi3593_commands,
	.ioctl_cmdIdx = {
		HI3593_CMD_S_MRESET_IDX,
		HI3593_CMD_G_RX2STATUS_IDX,
		HI3593_CMD_G_RX2CTRL_IDX,
		HI3593_CMD_S_RX2CTRL_IDX,
		HI3593_CMD_G_RX2LABEL_IDX,
		HI3593_CMD_S_RX2LABEL_IDX,
	}
};

#endif
