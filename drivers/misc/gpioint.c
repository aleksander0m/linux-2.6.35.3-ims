//*****************************************************************************
// $Id: GStreamerPlayer.cpp 16511 2011-03-16 19:32:24Z jws $
//
// Disclosure:
// Copyright(C) 2011 Systems and Software Enterprises, Inc. (dba IMS)
// ALL RIGHTS RESERVED.
// The contents of this medium may not be reproduced in whole or part without
// the written consent of IMS, except as authorized by section 117 of the U.S.
// Copyright law.
//*****************************************************************************

/*
 * gpio int driver
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/miscdevice.h>

#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/fcntl.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/console.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/jiffies.h>
#include <linux/interrupt.h>

#include <mach/hardware.h>
#include <mach/gpio.h>
#include <asm/irq.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/mach/irq.h>
#include <linux/proc_fs.h>

//#define DRIVERVERSION "1.0"
//0 means dynmaic assignment
// assign 239 so that nodes can be prebuilt
#define GPIO_MAJOR 0 
//#define GPIO_MAJOR 239
static int gpio_major = GPIO_MAJOR ;

#define GPIOMASK        0x001f
#define INTMASK         0x00e0
#define RISEEDGE        0x0080
#define FALLEDGE        0x0000
#define BOTH            0x0020
#define LEVELHIGH       0x0040
#define LEVELLOW        0x0060

int gpiomap[32] = {
        0x00,   /* 0*/
        53,     /* 1 evk power switch */
        0x00,   /* 2*/
        0x00,   /* 3*/
        0x00,   /* 4*/
        0x00,   /* 5*/
        0x00,   /* 6*/
        0x00,   /* 7*/
        0x00,   /* 8*/
        0x00,   /* 9*/
        0x00,   /* 10*/
        11,     /* 11 Discrete in 1*/
        12,     /* 12 Discrete in 2*/
        13,     /* 13 Discrete in 3*/
        14,     /* 14 Discrete in 4*/
        15,     /* 15 Discrete in 5*/
        16,     /* 16 Discrete in 6*/
        17,     /* 17 SCU_Disable# */
        0x00,   /* 18*/
        0x00,   /* 19*/
        0x00,   /* 20*/
        0x00,   /* 21*/
        0x00,   /* 22*/
        0x00,   /* 23*/
        0x00,   /* 24*/
        25,     /* gpio 25 Primary_SCU# */
        0x00,   /* 26*/
        0x00,   /* 27*/
        0x00,   /* 28*/
        0x5d,   /* gpio 29 on SCU 93 gpio3_29 Arinc 429 Flag3 */
        0x5e,   /* gpio 30 on SCU 94 gpio3_30 Arinc 429 Flag2 */
        0x5f    /* gpio 31 on SCU 95 gpio3_30 Arinc 429 Flag1 */
};

//%%ims%% debug
//#define DEBUG


static char const driverName[] = {
        "gpioint"
};

struct gpio_data {
	unsigned		gpionum ;
        wait_queue_head_t 	wait_queue ; // Used for blocking read, poll
	unsigned char		take ;
	unsigned char		add ;
	unsigned char		data[256];
};
	
static int total_ints = 0 ;


static ssize_t gpio_read(struct file * file, char __user * buf,
		       size_t count, loff_t *ppos)
{

	struct gpio_data *data = (struct gpio_data *)file->private_data ;

	#ifdef DEBUG
        printk(KERN_ERR "read: file %x flag %x", (unsigned int)file, file->f_flags );
	#endif

	if( data ){
		ssize_t rval = 0 ;
		do {
			while( (0 < count) && (data->add != data->take) ){
				unsigned char left = (data->add-data->take);
				if( left > count ){
					left = count ;
				}

				if( copy_to_user(buf,data->data+data->take,1) )
					return -EFAULT ;
				#ifdef DEBUG
                                printk(KERN_ERR "read: copy take %d rval %d", data->take, rval);
				#endif

				data->take++ ;
				count-- ;
				buf++ ;
				rval++ ;
			}
			if( (0 == rval) && (0==(file->f_flags & O_NONBLOCK)) ){
				#ifdef DEBUG
                                printk(KERN_ERR "read: wait flags: %x", file->f_flags);
				#endif
                                if( wait_event_interruptible(data->wait_queue, data->add != data->take) ){
					break;
				}
			} // wait for first
			else
				break;
		} while( 1 );

		return (0 == rval) ? -EINTR : rval ;
	}
	else
		return -EFAULT ;
}

static unsigned int gpio_poll(struct file *filp, poll_table *wait)
{
	struct gpio_data *data = (struct gpio_data *)filp->private_data ;

	#ifdef DEBUG
        printk(KERN_ERR "poll: file %x", (unsigned int)filp);
	#endif

	if( data ){
		poll_wait(filp, &data->wait_queue, wait);

		#ifdef DEBUG
                printk(KERN_ERR "poll: poll wait add %d take %d", data->add, data->take );
		#endif

		return (data->add != data->take) ? POLLIN | POLLRDNORM : 0 ;
	}
	else
		return -EINVAL ;
}

static void read_gpio_value (struct gpio_data *data) {
	data->data[data->add++] = gpio_get_value(data->gpionum);

	#ifdef DEBUG
        printk(KERN_ERR "sample: wakeup add %d", data->add);
	#endif

	wake_up(&data->wait_queue);
}

static irqreturn_t int_handler( int irq, void *param)
{
        ++total_ints ;

	#ifdef DEBUG
        printk( KERN_ERR "int irq %d param %x", irq, (unsigned int)param);
	#endif

	if( param && 1 ){
		struct file *file = (struct file *)param ;
                struct gpio_data *data = (struct gpio_data *)file->private_data ;
                read_gpio_value (data);
	}

	#ifdef DEBUG
        printk (KERN_ERR "%d ints", total_ints);
	#endif

	return IRQ_HANDLED ;
}

static int gpio_open(struct inode * inode, struct file * file)
{
        unsigned int gpio ;
        unsigned int edge_direction;

	struct gpio_data *data = kmalloc(sizeof(struct gpio_data),GFP_KERNEL);
        file->private_data = data ;
	memset(file->private_data,0,sizeof(*data));

	init_waitqueue_head(&data->wait_queue);

	#ifdef DEBUG
        printk( KERN_INFO "%s kernel mem %x", __FUNCTION__, (unsigned int) data);
	#endif

        /* select the gpio from map                                           */
        /* we cant expose all 128 due to limitations in the number of bits in */
        /* MINOR which only allows 8 bits total.  we use high 3 bits for the  */
        /* selection of the direction of the interrupt                        */

        gpio = gpiomap[(MINOR (file->f_dentry->d_inode->i_rdev) ) & GPIOMASK];
        edge_direction = (MINOR (file->f_dentry->d_inode->i_rdev) ) & INTMASK;

	#ifdef DEBUG
        printk( KERN_ERR "minor %x dir %d", gpio, edge_direction );
	#endif

	if (0 != gpio) {
                // switch to do gpio selects from edge_direction
                printk( KERN_ERR "minor %x dir %d", gpio, edge_direction );
                switch(edge_direction) {
                case FALLEDGE:
                        set_irq_type(gpio_to_irq(gpio), IRQ_TYPE_EDGE_FALLING);
                        break;
                case RISEEDGE:
                        set_irq_type(gpio_to_irq(gpio), IRQ_TYPE_EDGE_RISING);
                        break;
                case BOTH:
                        set_irq_type(gpio_to_irq(gpio), IRQ_TYPE_EDGE_BOTH);
                        break;
                case LEVELHIGH:
                        set_irq_type(gpio_to_irq(gpio), IRQ_TYPE_LEVEL_HIGH);
                        break;
                case LEVELLOW:
                        set_irq_type(gpio_to_irq(gpio), IRQ_TYPE_LEVEL_LOW);
                        break;
                }

		if( 0 == request_irq(gpio_to_irq(gpio), int_handler, IRQF_DISABLED, driverName, file) ){
			data->gpionum = gpio ;
			read_gpio_value (data);
		}
		else {
			//#ifdef DEBUG
			printk( KERN_ERR "Error grabbing interrupt for GPIO %u\n", gpio );
			return ( -EBUSY);
			//#endif
		}
	}
	return 0 ;
}

static int gpio_release(struct inode * inode, struct file * file)
{
        if(file->private_data){
		struct gpio_data *data = (struct gpio_data *)file->private_data ;

		//#ifdef DEBUG
		printk (KERN_INFO "%s: releasing %u, irq %d\n", __FUNCTION__, data->gpionum, gpio_to_irq(data->gpionum));
		//#endif

		free_irq(gpio_to_irq(data->gpionum), file);
		#ifdef DEBUG
                printk( KERN_INFO "%s free kmem %x", __FUNCTION__, (unsigned int) file->private_data);  // look for double release
		#endif
		kfree(file->private_data);
                file->private_data = 0 ;
	}
	return 0 ;
}

// no ioctl or write, but return fault or do nothing

/* header file here for now
#define GPIO_IOC_MAGIC 's' 
   */
static int gpio_ioctl(struct inode *inode, struct file *file,
		    unsigned int cmd, unsigned long arg)
{

	return -EFAULT ;
}

static ssize_t gpio_write(struct file * file, const char __user * buf,
		        size_t count, loff_t *ppos)
{
	#ifdef DEBUG
	printk( KERN_ERR "%s\n", __func__ );
	#endif
	return count ;
}


static const struct file_operations gpio_fops = {
	.read		= gpio_read,
	.owner		= THIS_MODULE,
	.write		= gpio_write,
	.ioctl		= gpio_ioctl,
	.open		= gpio_open,
	.release	= gpio_release,
	.poll		= gpio_poll,
};

#if 0
#define INTDEVBLOCK(intno, intname, name)  \
static struct miscdevice intname##both = {\
    32+intno,  \
    name "both",\
    &gpio_fops\
};\
static struct miscdevice intname##fall = {\
    intno,   \
    name "fall",\
    &gpio_fops\
};\
static struct miscdevice intname##high = {\
    64+intno,  \
    name "high",\
    &gpio_fops\
};\
static struct miscdevice intname##low = {\
    96+intno,  \
    name "low",\
    &gpio_fops\
};\
static struct miscdevice intname##rise = {\
    128+intno,\
     name "rise",\
    &gpio_fops\
};

/* define blocks of devices for each gpio's interrupts */
INTDEVBLOCK(1,  gpio1,  "gpio1");            // actually gpio 53, but mapped in table.
INTDEVBLOCK(11, gpio11, "gpio11");
INTDEVBLOCK(12, gpio12, "gpio12");
INTDEVBLOCK(13, gpio13, "gpio13");
INTDEVBLOCK(14, gpio14, "gpio14");
INTDEVBLOCK(15, gpio15, "gpio15");
INTDEVBLOCK(16, gpio16, "gpio16");
INTDEVBLOCK(17, gpio17, "gpio17");
INTDEVBLOCK(25, gpio25, "gpio25");
INTDEVBLOCK(29, gpio29, "gpio29");
INTDEVBLOCK(30, gpio30, "gpio30");
INTDEVBLOCK(31, gpio31, "gpio31");
#endif



static struct class *gpiointclass;

static int gpiointlist[] = {
    1,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    25,
    29,
    30,
    31
};

#define FALLMINOR 0   // minor = 0
#define BOTHMINOR 32  // minor = 1
#define HIGHMINOR 64  // minor = 2
#define LOWMINOR  96  // minor = 3
#define RISEMINOR 128 // minor = 4

static void gpio_cleanup_module (void)
{
    int i;
    char devname[12];

    // destroy devices
    for (i=0; i < ARRAY_SIZE( gpiointlist ); i++) {

        printk(KERN_INFO "register /dev for int %d", gpiointlist[i]);

        sprintf( devname, "gpio%dboth", gpiointlist[i]);
        device_destroy( gpiointclass, MKDEV(gpio_major, BOTHMINOR + gpiointlist[i] ) );
        sprintf( devname, "gpio%dfall", gpiointlist[i]);
        device_destroy( gpiointclass, MKDEV(gpio_major, FALLMINOR + gpiointlist[i] ) );
        sprintf( devname, "gpio%dhigh", gpiointlist[i]);
        device_destroy( gpiointclass, MKDEV(gpio_major, HIGHMINOR + gpiointlist[i] ) );
        sprintf( devname, "gpio%dlow", gpiointlist[i]);
        device_destroy( gpiointclass, MKDEV(gpio_major, LOWMINOR + gpiointlist[i] ) );
        sprintf( devname, "gpio%drise", gpiointlist[i]);
        device_destroy( gpiointclass, MKDEV(gpio_major, RISEMINOR + gpiointlist[i] ) );
    }

    // and class
    class_unregister(gpiointclass);
    class_destroy(gpiointclass);

    // finally unregister us
	unregister_chrdev(gpio_major,driverName);
}

static int __init gpio_init_module (void)
{
	int result ;
    //int ret;
    char devname[12];
    int i;

	result = register_chrdev(gpio_major,driverName,&gpio_fops);
	if (result<0) {
		printk (KERN_WARNING __FILE__": Couldn't register device %d.\n", gpio_major);
		return result;
	}

	if (gpio_major==0) 
		gpio_major = result; //dynamic assignment

    // create class
    gpiointclass = class_create(THIS_MODULE, "gpioclass");

    // check for error creating class
	if (IS_ERR(gpiointclass)) {
		unregister_chrdev(gpio_major, driverName );
		return PTR_ERR(gpiointclass);
	}

	printk (KERN_INFO "GPIO Int major %d", gpio_major);

    for (i=0; i < ARRAY_SIZE( gpiointlist ); i++) {

        printk(KERN_INFO "register dev entry for gpio %d", gpiointlist[i]);

        sprintf( devname, "gpio%dboth", gpiointlist[i]);
        device_create( gpiointclass, NULL, MKDEV(gpio_major, BOTHMINOR + gpiointlist[i] ), NULL, devname );
        sprintf( devname, "gpio%dfall", gpiointlist[i]);
        device_create( gpiointclass, NULL, MKDEV(gpio_major, FALLMINOR + gpiointlist[i] ), NULL, devname );
        sprintf( devname, "gpio%dhigh", gpiointlist[i]);
        device_create( gpiointclass, NULL, MKDEV(gpio_major, HIGHMINOR + gpiointlist[i] ), NULL, devname );
        sprintf( devname, "gpio%dlow", gpiointlist[i]);
        device_create( gpiointclass, NULL, MKDEV(gpio_major, LOWMINOR + gpiointlist[i] ), NULL, devname );
        sprintf( devname, "gpio%drise", gpiointlist[i]);
        device_create( gpiointclass, NULL, MKDEV(gpio_major, RISEMINOR + gpiointlist[i] ), NULL, devname );
    }



#if 0
    ret = misc_register(&gpio1both);
    ret = misc_register(&gpio1fall);
    ret = misc_register(&gpio1high);
    ret = misc_register(&gpio1low);
    ret = misc_register(&gpio1rise);

	printk (KERN_INFO "GPIO Int int 1");
    ret = misc_register(&gpio11both);
    ret = misc_register(&gpio11fall);
    ret = misc_register(&gpio11high);
    ret = misc_register(&gpio11low);
    ret = misc_register(&gpio11rise);

	printk (KERN_INFO "GPIO Int int 11");
    ret = misc_register(&gpio12both);
    ret = misc_register(&gpio12fall);
    ret = misc_register(&gpio12high);
    ret = misc_register(&gpio12low);
    ret = misc_register(&gpio12rise);

	printk (KERN_INFO "GPIO Int int 12");
    ret = misc_register(&gpio13both);
    ret = misc_register(&gpio13fall);
    ret = misc_register(&gpio13high);
    ret = misc_register(&gpio13low);
    ret = misc_register(&gpio13rise);

	printk (KERN_INFO "GPIO Int int 13");
    ret = misc_register(&gpio14both);
    ret = misc_register(&gpio14fall);
    ret = misc_register(&gpio14high);
    ret = misc_register(&gpio14low);
    ret = misc_register(&gpio14rise);

	printk (KERN_INFO "GPIO Int int 14");
    ret = misc_register(&gpio15both);
    ret = misc_register(&gpio15fall);
    ret = misc_register(&gpio15high);
    ret = misc_register(&gpio15low);
    ret = misc_register(&gpio15rise);

	printk (KERN_INFO "GPIO Int int 15");
    ret = misc_register(&gpio16both);
    ret = misc_register(&gpio16fall);
    ret = misc_register(&gpio16high);
    ret = misc_register(&gpio16low);
    ret = misc_register(&gpio16rise);

	printk (KERN_INFO "GPIO Int int 16");
    ret = misc_register(&gpio17both);
    ret = misc_register(&gpio17fall);
    ret = misc_register(&gpio17high);
    ret = misc_register(&gpio17low);
    ret = misc_register(&gpio17rise);

	printk (KERN_INFO "GPIO Int int 17");
    ret = misc_register(&gpio25both);
    ret = misc_register(&gpio25fall);
    ret = misc_register(&gpio25high);
    ret = misc_register(&gpio25low);
    ret = misc_register(&gpio25rise);

	printk (KERN_INFO "GPIO Int int 25");
    ret = misc_register(&gpio29both);
    ret = misc_register(&gpio29fall);
    ret = misc_register(&gpio29high);
    ret = misc_register(&gpio29low);
    ret = misc_register(&gpio29rise);

	printk (KERN_INFO "GPIO Int int 29");
    ret = misc_register(&gpio30both);
    ret = misc_register(&gpio30fall);
    ret = misc_register(&gpio30high);
    ret = misc_register(&gpio30low);
    ret = misc_register(&gpio30rise);

	printk (KERN_INFO "GPIO Int int 30");
    ret = misc_register(&gpio31both);
    ret = misc_register(&gpio31fall);
    ret = misc_register(&gpio31high);
    ret = misc_register(&gpio31low);
    ret = misc_register(&gpio31rise);

	printk (KERN_INFO "GPIO Int int 31");
#endif
	return 0 ;
}


#ifndef MODULE
static int __init gpio_setup (char *str)
{

	return 1;
}
#endif
__setup("gpioint=", gpio_setup);
module_init(gpio_init_module);
module_exit(gpio_cleanup_module);

MODULE_ALIAS_CHARDEV_MAJOR(LP_MAJOR);
MODULE_LICENSE("GPL");
