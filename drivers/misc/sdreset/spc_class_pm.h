/*
 * power_management.h - Device power management definition
 */

#ifndef POWER_MANAGEMENT_H
#define POWER_MANAGEMENT_H

#include <linux/device.h>

/* Support for device power management */
int device_suspend(struct device *dev);
int device_resume(struct device *dev);
int device_prepare_suspend(struct device *dev);


#endif
