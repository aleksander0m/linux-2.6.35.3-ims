/*
 * rdu_sdcard_blockdev.c - Block device interface implementation
 */

/*
 * This specific submodule was heavily inspired by the following pieces of Linux
 * source:
 * > md.c (raid): The RCU model is heavily related
 * > dm.c (device mapper)
 * > loop.c (loopback block device)
 */
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/vmalloc.h>
#include <linux/slab.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/time.h>

#include <scsi/scsi_device.h>
#include <scsi/scsi_host.h>
#include "../../scsi/sd.h"

#include "sdreset_driver.h"
#include "spc_class_mgr.h"
#include "rdu_sdcard_blockdev.h"
#include "rdu_sdcard_ops.h"


/* The prefix for safe device nodes */
static const char *devnode_prefix = "safesd-";

/* Major number for the block device */
static int major_num;

/* Class interface for monitoring device events*/
static struct class_interface class_intf;

/* List of physical devices to be scanned */
struct scan_dev {
	/* Device to be scanned */
	struct device *dev;

	/* Device list management */
	struct list_head list;
};
static LIST_HEAD(devices_to_scan);
static DEFINE_SPINLOCK(devices_to_scan_lock);

/* List of virtual drives to be attached */
struct stacked_blockdev {
	/* Generic disk for the virtual drive */
	struct gendisk *gd;

	/* Request queue for the virtual drive */
	struct request_queue *q;

	/* Wait queue while device is not attached */
	wait_queue_head_t wq_suspended_dead;

	/* Wait queue while device is attached */
	wait_queue_head_t wq_activeio;

	/* Work structure for drive release support */
	struct work_struct del_work;

	/* SCSI logical unit number for drive identification*/
	u32 lun;

	/* Disk capacity for drive identification*/
	sector_t capacity;

	/* Name of the USB interface device for drive identification*/
	char pusb_intf_name[128];

	/* Drive active flag indicating that drive is busy */
	atomic_t active_io;

	/* Drive internal reference count */
	atomic_t refcount;

	/* Drive attached flag indicating that drive is bound to a device */
	atomic_t suspended;

	/* Drive alive flag indicating that drive is operational */
	atomic_t alive;

	/* Drive bound physical device, which may change over reset events */
	struct block_device *mt_target_bdev;

	/* Time stamp of last disconnection from physical device */
	struct timespec mt_disc_time;

	/* Parent of the virtual drive */
	struct device *parent;

	/* Drive write-protect flag indicating that drive is read-only */
	int write_protect;

	/* Device list management */
	struct list_head list;
};
static LIST_HEAD(stacked_blockdevs);
static DEFINE_SPINLOCK(stacked_blockdevs_lock);

/* List of physical devices to be notified */
struct notify_dev {
	/* Device to be scanned */
	struct device *dev;

	/* Device list management */
	struct list_head list;
};
static LIST_HEAD(devices_to_notify);
static DEFINE_SPINLOCK(devices_to_notify_lock);

/* Work structure for device scan operations */
static void scan_work_cb(struct work_struct *w);
static DECLARE_DELAYED_WORK(scan_work, scan_work_cb);

/* Work structure for device notify operations */
static void notify_work_cb(struct work_struct *w);
static DECLARE_DELAYED_WORK(notify_work, notify_work_cb);

/*
 * Copy/paste from scsi/sd.c, but combined two functions and removed locking
 * calls.
 */
static struct scsi_disk *const scsi_disk_get_from_dev(
	struct device *const actual_dev)
{
	/*
	 * Fetch driver_data, which is known to be the scsi_disk for this
	 * device. Reference sd.c's sd_probe_async()
	 */
	struct scsi_disk *sdkp = dev_get_drvdata(actual_dev);
	if (!sdkp)
		return sdkp; /* This function can return NULL */

	if (!sdkp->device)
		return ERR_PTR(-ENOPKG);

	if (!sdkp->device->host)
		return ERR_PTR(-ETIME);

	/* Reftick the module */
	if (!try_module_get(sdkp->device->host->hostt->module))
		return ERR_PTR(-ENOLINK);

	if (sdkp->device->sdev_state == SDEV_DEL) {
		module_put(sdkp->device->host->hostt->module);
		return ERR_PTR(-ENXIO);
	}

	if (!get_device(&sdkp->device->sdev_gendev)) {
		module_put(sdkp->device->host->hostt->module);
		return ERR_PTR(-ENXIO);
	}

	get_device(&sdkp->dev);

	return sdkp;
}

static void scsi_disk_put(struct scsi_disk *sdkp)
{
	put_device(&sdkp->dev);
	scsi_device_put(sdkp->device);
	module_put(sdkp->device->host->hostt->module);
}

/* Have to guarantee that release and put of
 * the mt_target_bdev happens in non-atomic context */
static void sbd_delayed_release(struct work_struct *ws)
{
	struct stacked_blockdev *dev =
		container_of(ws, struct stacked_blockdev, del_work);

	if (dev->mt_target_bdev) {
		bd_release(dev->mt_target_bdev);
		blkdev_put(dev->mt_target_bdev, FMODE_READ|FMODE_WRITE);
		dev->mt_target_bdev = NULL;
	}

	kfree(dev);
}

/**
 * Decrement the reference count of a stacked_blockdev.
 * Must be called for every call to sbd_get.
 *
 * When the reference count hits 0, the stacked_blockdev is released.
 *
 * ASSUMPTIONS (MAJOR):
 *	stacked_blockdevs_lock is *NOT* held.
 *	You will deadlock if you hold stacked_blockdevs_lock and call
 *	 into this code.
 */
static void __sbd_put(struct stacked_blockdev *dev, const char *f, int l)
{
	if (!dev)
		return;

	if (!atomic_dec_and_lock(&dev->refcount, &stacked_blockdevs_lock)) {
		/* If the device's refcount goes under 0, it's a bug, but a
		 * workable one. Only the thread which sets the refcount to
		 * exactly 0 executes the release code. */
		smp_mb__after_atomic_dec();
		if (WARN_ON(atomic_read(&dev->refcount) < 0)) {
			printk(KERN_ERR "Device has negative refcount:\n"
				"\tName: %s\n"
				"\tRefcount: %d\n",
				dev->gd->disk_name,
				atomic_read(&dev->refcount));
		}

		return;
	}
	smp_mb__after_atomic_dec();

	/* Refcount is definitely 0 */

	list_del(&dev->list);
	spin_unlock(&stacked_blockdevs_lock);

	if (dev->gd) {
		dev->gd->private_data = NULL;
		put_disk(dev->gd);
		dev->gd = NULL;
	}

	if (dev->parent) {
		put_device(dev->parent);
		dev->parent = NULL;
	}

	if (dev->q) {
		blk_cleanup_queue(dev->q);
		dev->q = NULL;
	}

	/*
	 * sd_release() (scsi/sd.c) can sleep.
	 * Anything involving block devices has to be done in guaranteed
	 * non-atomic context.
	 */
	INIT_WORK(&dev->del_work, sbd_delayed_release);
	schedule_work(&dev->del_work);
}
#define sbd_put(stbdev_ptr) __sbd_put(stbdev_ptr, NULL, 0)

/**
 * Increment the reference count of a stacked_blockdev.
 * Must be called before every call to sbd_put.
 *
 * ASSUMPTIONS:
 *	None besides obvious sanity checking.
 */
static struct stacked_blockdev *__sbd_get(struct stacked_blockdev *dev,
	const char *f, int l)
{
	if (!dev)
		return NULL;

	smp_mb();
	WARN_ON(!atomic_read(&dev->refcount));
	atomic_inc(&dev->refcount);
	smp_mb();
	return dev;
}
#define sbd_get(stbdev_ptr) __sbd_get(stbdev_ptr, NULL, 0)

/**
 * Called via block_device_operations every time the gendisk/block_device
 * is opened, whether by userspace or kernel. It most importantly increments
 * the refcount.
 *
 * ASSUMPTIONS:
 *	bdev is claimed by our gendisk, private_data is set.
 */
static int sbd_bdops_open(struct block_device *bdev, fmode_t mode)
{
	struct stacked_blockdev *dev = sbd_get(bdev->bd_disk->private_data);

	rcu_read_lock();
	if (!atomic_read(&dev->alive)) {
		printk(KERN_DEBUG "%s: %s failed, not alive\n",
			__func__, (char *) &bdev->bd_disk->disk_name);
		rcu_read_unlock();
		sbd_put(dev);
		return -ENXIO;
	}
	rcu_read_unlock();

	if (dev->write_protect && (mode & FMODE_WRITE)) {
		sbd_put(dev);
		return -EROFS;
	}

	return 0;
}

/**
 * Called via block_device_operations every time the gendisk/block_device
 * is closed, whether by userspace or kernel. It most importantly decrements
 * the refcount.
 *
 * ASSUMPTIONS:
 *	disk->private_data is set
 */
static int sbd_bdops_release(struct gendisk *disk, fmode_t mode)
{
	struct stacked_blockdev *dev = disk->private_data;
	if (!dev)
		return 0;
	sbd_put(dev);
	return 0;
}

static const struct block_device_operations sbd_ops = {
	.owner		= THIS_MODULE,
	.open		= sbd_bdops_open,
	.release	= sbd_bdops_release,
};

/**
 * Detach a stacked_blockdev.
 * Label the device as suspended so that new data does not arrive
 *	on the underlying device's queue.
 *
 * ASSUMPTIONS:
 *	None
 */
static void sbd_suspend(struct stacked_blockdev *dev)
{
	int already_suspended;

	smp_mb();
	already_suspended = atomic_xchg(&dev->suspended, 1);
	smp_mb();

	if (already_suspended)
		return;

	getnstimeofday(&dev->mt_disc_time); /* Needed for cleanup */

	synchronize_rcu();

	/* No need to wake_up(), nobody is sleeping */
}

/*
 * Remove a stacked_blockdev, equivalent to a bus' "disconnect" phase.
 * Assumes that the stacked_blockdevs_lock is not held.
 */
static void sbd_fail(struct stacked_blockdev *dev)
{
	int was_alive;

	smp_mb();
	was_alive = atomic_xchg(&dev->alive, 0);
	smp_mb();

	if (!was_alive)
		return;

	synchronize_rcu();
	wake_up_all(&dev->wq_suspended_dead);
	wait_event(dev->wq_activeio, atomic_read(&dev->active_io) == 0);

	kobject_uevent(&disk_to_dev(dev->gd)->kobj, KOBJ_OFFLINE);

	put_device(dev->gd->driverfs_dev);

	dev_dbg(dev->gd->driverfs_dev, "calling del_gendisk()\n");
	del_gendisk(dev->gd);

	sbd_put(dev);
}

/**
 * Mark a stacked_blockdev as no longer suspended, allowing data
 * to flow to underlying device queues.
 *
 * ASSUMPTIONS (MAJOR):
 *	Underlying device (mt_target_bdev) is attached.
 */
static void sbd_resume(struct stacked_blockdev *dev)
{
	int was_suspended;

	smp_mb();
	was_suspended = atomic_xchg(&dev->suspended, 0);
	smp_mb();

	if (was_suspended)
		wake_up_all(&dev->wq_suspended_dead);
}


/**
 * The block layer calls this for accounting and (de)prioritization.
 */
static int sbd_congested(void *data, int bits)
{
	struct stacked_blockdev *dev = data;
	return atomic_read(&dev->suspended) || !atomic_read(&dev->alive);
}

/**
 * The poweroff operation is blocked till this returns.
 * Data structures are not yet destroyed or modified.
 * The kernel has no idea that the power rail is about to drop.
 */
int sdcard_passthrough_blockdev_pre_powerdown(struct device *ldm_cdev)
{
	struct list_head *pos, *tmp;
	struct stacked_blockdev *dev = NULL;

	spin_lock(&stacked_blockdevs_lock);
	list_for_each_safe(pos, tmp, &stacked_blockdevs) {
		struct stacked_blockdev *cur =
			container_of(pos, struct stacked_blockdev, list);

		if (cur->parent == ldm_cdev) {
			dev = sbd_get(cur);
			break; /* Nominal loop exit */
		}
	}
	spin_unlock(&stacked_blockdevs_lock);

	if (dev) {
		dev_dbg(dev->gd->driverfs_dev, "Suspending operation\n");
		sbd_suspend(dev);
		sbd_put(dev);
	}

	return 0;
}

/*
 * Called by the block layer to pass data to our "device."  Blocks data flow if
 * device is suspended.  Reacts accordingly if device fails.
 */
static int sbd_make_request(struct request_queue *queue, struct bio *bio)
{
	struct stacked_blockdev *dev = queue->queuedata;
	struct request_queue *tq;
	int rv;

	if (dev == NULL) {
		bio_io_error(bio);
		return 0;
	}

	rcu_read_lock();
	if (atomic_read(&dev->suspended) && atomic_read(&dev->alive)) {
		DEFINE_WAIT(__wait);

		for (;;) {
			prepare_to_wait(&dev->wq_suspended_dead, &__wait,
				TASK_UNINTERRUPTIBLE);
			smp_mb();
			if (!atomic_read(&dev->suspended) ||
				!atomic_read(&dev->alive))
				break;

			rcu_read_unlock();
			schedule();
			rcu_read_lock();
		}
		finish_wait(&dev->wq_suspended_dead, &__wait);
	}

	if (!atomic_read(&dev->alive) /* is dead */) {
		rcu_read_unlock();
		bio_io_error(bio);
		return 0;
	}

	atomic_inc(&dev->active_io);
	smp_mb__after_atomic_inc();
	rcu_read_unlock();

	bio->bi_bdev = dev->mt_target_bdev;
	tq = bdev_get_queue(dev->mt_target_bdev);
	rv = tq->make_request_fn(tq, bio);

	/* Decrement the inflight count; wake up waiters if 0 */
	if (atomic_dec_and_test(&dev->active_io))
		wake_up_all(&dev->wq_activeio);

	return rv;
}

/*
 * Helper: make a passthrough request queue.
 */
static struct request_queue *make_request_queue(void *data)
{
	struct request_queue *q;

	q = blk_alloc_queue(GFP_KERNEL);
	if (IS_ERR_OR_NULL(q))
		goto out;

	blk_queue_make_request(q, sbd_make_request);
	q->queuedata = data;

	/* Must be set null explicitly, or risk a SCSI deadlock */
	q->unplug_fn = NULL;

	q->merge_bvec_fn = NULL;
	q->backing_dev_info.congested_fn = sbd_congested;
	q->backing_dev_info.congested_data = data;

out:
	if (!q)
		q = ERR_PTR(-ENOMEM);
	return q;
}

/*
 * Helper: make a gendisk
 */
static struct gendisk *make_disk(void *data, struct gendisk *old_disk,
	struct request_queue *q)
{
	struct gendisk *dsk;
	char namebuf[DISK_NAME_LEN];

	dsk = alloc_disk(old_disk->minors);
	if (IS_ERR_OR_NULL(dsk))
		goto out;

	dsk->queue = q;
	dsk->major = major_num;
	dsk->first_minor = old_disk->first_minor;
	dsk->fops = &sbd_ops;
	dsk->private_data = data;
	dsk->flags |= GENHD_FL_EXT_DEVT;
	set_capacity(dsk, get_capacity(old_disk));

	snprintf((char *)&namebuf, sizeof(namebuf), "%s%s",
		devnode_prefix, old_disk->disk_name);
	strncpy(dsk->disk_name, (char *)&namebuf, sizeof(namebuf));

out:
	if (!dsk)
		dsk = ERR_PTR(-ENOMEM);
	return dsk;
}

/*
 * Return a pointer to a stacked blockdev.
 * It is either pre-existing or newly allocated.
 * Returns -ERRNO on error.
 */
static noinline struct stacked_blockdev *find_or_allocate(char *usb_name,
	unsigned int lun, sector_t capacity, struct device *ldm_cdev,
	int *is_new_dev)
{
	struct stacked_blockdev *dev = NULL, *cursor;
	struct device *old;

	spin_lock(&stacked_blockdevs_lock);
	list_for_each_entry(cursor, &stacked_blockdevs, list) {
		/* Match SCSI LUN */
		if (lun != cursor->lun)
			continue;

		/* Match SCSI disk capacity */
		if (capacity != cursor->capacity)
			continue;

		/* Match USB interface name */
		if (usb_name && strcmp(cursor->pusb_intf_name, usb_name))
			continue;

		rcu_read_lock();
		smp_mb();
		if (!atomic_read(&cursor->alive)) {
			rcu_read_unlock();
			continue;
		}
		rcu_read_unlock();

		/* Device found, return it */
		dev = cursor;
		goto out;
	}

	/* Allocate new device */
	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		dev = ERR_PTR(-ENOMEM);
		goto out_bad_dev;
	}

	/* New devices are dead until
	 *  [1] attached, and
	 *  [2] add_disk() called */
	atomic_set(&dev->alive, 0);
	atomic_set(&dev->suspended, 0);

	dev->capacity = capacity;
	dev->lun = lun;
	strncpy((char *)&dev->pusb_intf_name, usb_name,
		sizeof(dev->pusb_intf_name));

	atomic_inc(&dev->refcount); /* Set an initial refcount of 1 */
	smp_mb__after_atomic_inc();

	list_add_tail(&dev->list, &stacked_blockdevs);

	if (is_new_dev)
		*is_new_dev = 1;

out:
	/* Anything on the list must have ->parent set for cleanup to work.
	 * Protect against freeing the parent during the unlikely case of
	 * reattaching to the same parent by holding a reference across
	 * assignments. */
	old = dev->parent;
	dev->parent = get_device(ldm_cdev);
	smp_mb();

	spin_unlock(&stacked_blockdevs_lock);
	put_device(old);
out_bad_dev:
	return dev;
}

/**
 * Set default values for a stacked_blockdev
 */
static int sbd_finish_create(struct stacked_blockdev *new_stbdev,
	struct gendisk *old_disk)
{
	struct gendisk *dsk;
	struct request_queue *q;
	int rv = 0;

	init_waitqueue_head(&new_stbdev->wq_suspended_dead);
	init_waitqueue_head(&new_stbdev->wq_activeio);

	q = make_request_queue((void *) new_stbdev);
	if (IS_ERR(q)) {
		rv = PTR_ERR(q);
		goto out;
	}

	dsk = make_disk((void *) new_stbdev, old_disk, q);
	if (IS_ERR(dsk)) {
		rv = PTR_ERR(dsk);
		blk_cleanup_queue(q);
		goto out;
	}

	new_stbdev->gd = dsk;
	new_stbdev->q = q;

out:
	return rv;
}

 /*
  * Attach a stacked blockdev, either initially or for reattachment.
  */
static int sbd_do_attach(struct stacked_blockdev *dev, struct scsi_disk *sdkp)
{
	fmode_t disk_mode;
	struct block_device *newb;
	int rv = 1;

	dev->write_protect = sdkp->write_prot;
	set_disk_ro(dev->gd, dev->write_protect);

	disk_mode = FMODE_READ;
	if (!dev->write_protect)
		disk_mode |= FMODE_WRITE;

	newb = open_by_devnum(disk_devt(sdkp->disk), disk_mode);
	if (IS_ERR(newb)) {
		rv = PTR_ERR(newb);
		goto out; /* Is a match, but error */
	}

	rv = bd_claim(newb, dev);
	if (rv) {
		blkdev_put(newb, disk_mode);
		goto out; /* Is a match, but error */
	}

	/* Release old block device (for reattaches) */
	if (dev->mt_target_bdev) {
		bd_release(dev->mt_target_bdev);
		blkdev_put(dev->mt_target_bdev, disk_mode);
	}
	/* Set new block device */
	dev->mt_target_bdev = newb;

	sbd_resume(dev);

out:
	return rv;
}

/**
 * Ascend the device tree to find a relevant usb_name that can be used for
 * string/name comparisons across power-rail "kicks"
 */
#define ANN(x, y, g0t0) ({ if (WARN_ON(!y)) goto g0t0; x = y; })
static inline int get_pusb_intf_name(struct device *ldm_cdev,
	char *dst, size_t len)
{
	struct device *pusb_intf;

	/* SCSI LUN device, 1:0:0:0 */
	ANN(pusb_intf, ldm_cdev->parent, out);

	/* SCSI Target device, target1:0:0 */
	ANN(pusb_intf, pusb_intf->parent, out);

	/* SCSI host device, host1 */
	ANN(pusb_intf, pusb_intf->parent, out);

	/* Finally at the pusb_intf, 3-1.1:1.0 */
	ANN(pusb_intf, pusb_intf->parent, out);
	snprintf(dst, len, "%s", dev_name(pusb_intf));

	return 0;
out:
	return -ENODATA;

#undef ANN
}

/**
 * Given a newly-added device (ldm_cdev) and its associated scsi_disk (sdkp),
 * determine if the device is a reattachment or new attachment.
 */
static int attach_or_reattach(struct device *ldm_cdev, struct scsi_disk *sdkp)
{
	struct stacked_blockdev *stbdev;
	char usb_name[128] = {'\0'};
	int rv = -ENODATA;
	int is_new_dev = 0;

	dev_dbg(ldm_cdev, "Entry for disk %s\n", (char *) &sdkp->disk->disk_name);

	rv = get_pusb_intf_name(ldm_cdev, (char *)&usb_name, sizeof(usb_name));
	if (rv)
		goto out;

	stbdev = find_or_allocate((char *)usb_name,
						   sdkp->device->lun,
						   get_capacity(sdkp->disk),
						   ldm_cdev,
						   &is_new_dev);
	if (IS_ERR(stbdev)) {
		rv = PTR_ERR(stbdev);
		goto out;
	}

	rcu_read_lock();
	smp_rmb();
	BUG_ON(is_new_dev && atomic_read(&stbdev->alive));
	BUG_ON(is_new_dev && atomic_read(&stbdev->suspended));
	rcu_read_unlock();

	if (is_new_dev) {

		rv = sbd_finish_create(stbdev, sdkp->disk);
		if (rv) {
			sbd_fail(stbdev); /* initial reference */
			goto out;
		}
	}

	rv = sbd_do_attach(stbdev, sdkp);

	if (rv == -ENOMEDIUM) {
		/* A removable media is not inserted. */
			rv = 0;
	} else if (!rv) {
		/* Nominal/success from sbd_do_attach */

		if (is_new_dev) {

			/* Need to hold a reference. When the parent de-attaches
			 * upon kick, it will get freed otherwise. */
			stbdev->gd->driverfs_dev = get_device(ldm_cdev->parent);

			atomic_set(&stbdev->alive, 1);

			add_disk(stbdev->gd);
		}
	} else {
		/* error */
		printk(KERN_ERR "ERROR: cannot attach to disk %s. "
			"Ensure it is not mounted.\n",
			(char *) &sdkp->disk->disk_name);
		sbd_fail(stbdev); /* initial reference */
		rv = -EBUSY;
	}

out:
	return rv;
}

/* Notify user space that a virtual drive is ready */
static void notify_work_cb(struct work_struct *w)
{
	struct notify_dev *nd_pos;
	struct notify_dev *nd_tmp;
	char sdreset_event[] = "SDRESETBIND=1";
	char *sdreset_envp[] = {sdreset_event, NULL};

	/* Send a notification for each drive on the list */
	spin_lock(&devices_to_notify_lock);

	list_for_each_entry_safe(nd_pos, nd_tmp, &devices_to_notify, list) {
		/* Notify that the device state has changed */
		spin_unlock(&devices_to_notify_lock);

		kobject_uevent_env(&nd_pos->dev->kobj, KOBJ_CHANGE, sdreset_envp);

		spin_lock(&devices_to_notify_lock);

		/* Remove the notify entry for the device from the notify list */
		list_del(&nd_pos->list);

		/* Release the reference to the device from the notify list */
		spin_unlock(&devices_to_notify_lock);

		put_device(nd_pos->dev);

		spin_lock(&devices_to_notify_lock);

		/* Free the entry for the device from the notify list */
		vfree(nd_pos);
	}

	spin_unlock(&devices_to_notify_lock);
}

static void scan_work_cb(struct work_struct *w)
{
	struct scan_dev *sd_pos;
	struct scan_dev *sd_tmp;
	struct scsi_disk *sdkp;
	struct notify_dev *notify_work_entry;

	/* Lock the device list for access */
	spin_lock(&devices_to_scan_lock);

	/* Process each the device in the scan list */
	list_for_each_entry_safe(sd_pos, sd_tmp, &devices_to_scan, list) {
		/* Find and obtain a reference to the SCSI drive for this device */
		sdkp = scsi_disk_get_from_dev(sd_pos->dev->parent);
		if (!IS_ERR_OR_NULL(sdkp)) {
			/* Unlock the device list from access */
			spin_unlock(&devices_to_scan_lock);

			dev_info(sd_pos->dev, "attach device %s\n", sdkp->disk->disk_name);

			/* Attach the device to the SCSI drive */
			if (attach_or_reattach(sd_pos->dev, sdkp)) {
				dev_err(sd_pos->dev, "error, attach physical drive fail\n");
				scsi_disk_put(sdkp);
				spin_lock(&devices_to_scan_lock);
				continue;
			}

			/* Release the reference to the SCSI drive for this device */
			scsi_disk_put(sdkp);

			/* Lock the device list for access */
			spin_lock(&devices_to_scan_lock);

			/* Allocate a notify entry for the device */
			notify_work_entry = vmalloc(sizeof(*notify_work_entry));
			if (!notify_work_entry) {
				list_del(&sd_pos->list);
				put_device(sd_pos->dev);
				vfree(sd_pos);
				return;
			}

			/* Initialize the notify entry for the device */
			notify_work_entry->dev = get_device(sd_pos->dev->parent);

			/* Add the notify entry for the device to the notify list */
			spin_lock(&devices_to_notify_lock);

			list_add_tail(&notify_work_entry->list, &devices_to_notify);

			spin_unlock(&devices_to_notify_lock);

			/* Schedule a device notify operation */
			schedule_delayed_work(&notify_work, msecs_to_jiffies(5000));

			/* Remove the scan entry for the device from the scan list */
			list_del(&sd_pos->list);

			/* Release the reference to the device from the scan list */
			spin_unlock(&devices_to_scan_lock);

			put_device(sd_pos->dev);

			spin_lock(&devices_to_scan_lock);

			/* Free the entry for the device from the scan list */
			vfree(sd_pos);
		}
	}

	/* Unlock the device list from access */
	spin_unlock(&devices_to_scan_lock);

	/* Schedule another scan if there are additional drives for processing */
	if (!list_empty(&devices_to_scan))
		schedule_delayed_work(&scan_work, msecs_to_jiffies(100));
}

/* Handle devices added to the class */
static int blockdev_add_dev(struct device *dev, struct class_interface *ci)
{
	struct scan_dev *scan_work_entry;

	/* Verify that the device is of the correct type */
	device_lock(dev);
	if (dev->type != &sp_dev_type_sdcard.ldm_type) {
		device_unlock(dev);
		return -ENODATA;
	}
	device_unlock(dev);

	/* Allocate a scan entry for the device */
	scan_work_entry = vmalloc(sizeof(*scan_work_entry));
	if (!scan_work_entry) {
		dev_err(dev, "unable to allocate drive scan entry\n");
		return -ENOMEM;
	}

	/* Initialize the scan entry for the device */
	scan_work_entry->dev = get_device(dev);

	/* Lock the device list for access */
	spin_lock(&devices_to_scan_lock);

	/* Add the scan entry for the device to the scan list */
	list_add_tail(&scan_work_entry->list, &devices_to_scan);

	/* Unlock the device list from access */
	spin_unlock(&devices_to_scan_lock);

	dev_info(dev, "scheduled for binding to a physical device\n");

	/* Schedule a device scan operation */
	schedule_delayed_work(&scan_work, 0);

	/* Report that the operation was successful */
	return 0;
}

/* Handle devices removed from the class */
static void blockdev_remove_dev(struct device *dev, struct class_interface *ci)
{
	struct stacked_blockdev *sbd_pos;
	struct stacked_blockdev *sbd_tmp;

	/* Verify that the device is of the correct type */
	device_lock(dev);
	if (dev->type != &sp_dev_type_sdcard.ldm_type) {
		device_unlock(dev);
		return;
	}
	device_unlock(dev);

	/* Lock the device list for access */
	spin_lock(&stacked_blockdevs_lock);

	/* Find the SCSI disk attached to this device */
	list_for_each_entry_safe(sbd_pos, sbd_tmp, &stacked_blockdevs, list)
		if (sbd_pos->parent == dev) {
			/* Unlock the device list from access */
			spin_unlock(&stacked_blockdevs_lock);

			/* Obtain a reference to the device */
			sbd_get(sbd_pos);

			/* Block access across all processors */
			rcu_read_lock();

			/* Block across all processors */
			smp_mb();

			if (!atomic_read(&sbd_pos->suspended)) {
				dev_info(dev, "got removing suspended parent %s\n",
					sbd_pos->gd->disk_name);

				/* Restore access across all processors */
				rcu_read_unlock();

				/* Remove the drive from the device list */
				sbd_fail(sbd_pos);
			} else
				/* Restore access across all processors */
				rcu_read_unlock();

			/* Release the reference to the device */
			sbd_put(sbd_pos);

			/* Lock the device list for access */
			spin_lock(&stacked_blockdevs_lock);

			/* Stop processing existing drives */
			break;
		}

	/* Unlock the device list from access */
	spin_unlock(&stacked_blockdevs_lock);
}

/* Submodule initialization */
int rdu_sdcard_blockdev_init(struct class *cl)
{
	/* Allocate the major device ID */
	major_num = register_blkdev(major_num, devnode_prefix);
	if (major_num < 0) {
		printk(KERN_ERR "unable to allocate major device number\n");
		return major_num;
	}

	/* Initialize the class interface */
	class_intf.add_dev = blockdev_add_dev;
	class_intf.remove_dev = blockdev_remove_dev;
	class_intf.class = cl;

	/* Register the class interface */
	return class_interface_register(&class_intf);
}

/* Submodule exit */
void rdu_sdcard_blockdev_exit(void)
{
	struct scan_dev *sd_pos;
	struct scan_dev *sd_tmp;
	struct stacked_blockdev *sbd_pos;
	struct stacked_blockdev *sbd_tmp;

	/* Cancel all pending work tasks */
	cancel_delayed_work_sync(&scan_work);

	/* Lock the device list for access */
	spin_lock(&devices_to_scan_lock);

	/* Remove all unregistered devices */
	list_for_each_entry_safe(sd_pos, sd_tmp, &devices_to_scan, list) {
		list_del(&sd_pos->list);
		put_device(sd_pos->dev);
		vfree(sd_pos);
	}

	/* Unlock the device list from access */
	spin_unlock(&devices_to_scan_lock);

	/* Lock the device list for access */
	spin_lock(&stacked_blockdevs_lock);

	/* Remove all active devices */
	list_for_each_entry_safe(sbd_pos, sbd_tmp, &stacked_blockdevs, list)
		sbd_fail(sbd_pos);

	/* Unlock the device list from access */
	spin_unlock(&stacked_blockdevs_lock);

	/* Unregister the class interface */
	class_interface_unregister(&class_intf);

	/* Release the major device ID */
	unregister_blkdev(major_num, devnode_prefix);
}
