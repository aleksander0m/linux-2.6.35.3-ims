/*
 * spc_class_mgr.h - Switchable power manager definition
 */

#ifndef SPC_CLASS_MGR_H
#define SPC_CLASS_MGR_H

#include <linux/device.h>

/*###########################################################################
 * Public API: Create a Switchable Power Class (SP) instance.
 * No devices are added. Default attributes are set.
 *##########################################################################*/
struct class *create_sp_class(const char *const name, void (*reset)(void));

/*###########################################################################
 * Public API: Remove an LDM ClassDev
 *##########################################################################*/
int sp_dev_unregister(struct device *dev, struct class *cl);

/*###########################################################################
 * Public API: Remove an LDM ClassDev
 *##########################################################################*/
void sp_class_unregister(struct class *cl);

/*###########################################################################
 * Public API: Safe all devs in a given class
 *##########################################################################*/
int sp_class_pre_poweroff_all_devs(struct class *cl);

/*###########################################################################
 * Public API: Resume all devs in a given class
 *##########################################################################*/
int sp_class_post_poweron_all_devs(struct class *cl);

struct sp_dev_type;
struct gendisk;
struct scs;

/**
 * sp_class: Switchable Power Class
 *
 * A set (represented via an LDM class) of devices which together can be
 * switched on and off. If "device1," "device2," and "device3" are all
 * controlled by "switch1," they belong in an sp_class together.
 *
 * This driver does not add pre-existing devices to this class. Rather, this
 * driver creates psuedo/class devices (see below) and adds those devices
 * to this class/set.  For each "struct device" "dev" in this set, the
 * actual device of relevance is "dev->parent"
 */
struct sp_class {
	struct class class;
	void (*safe_power_reset) (void);
	void (*async_safe_power_reset) (struct class *cl);

	struct mutex sp_mutex;
	int in_reset;
	int in_power_op;
	struct list_head reset_unregs;
	int reset_occurred;
};
#define to_sp_class(c) container_of(c, struct sp_class, class)

/*
 * For late binding attributes to an sp_dev.
 */
struct dev_attributes_list_entry {
	struct list_head list;
	struct device_attribute attr;
};

/**
 * sp_dev_type: Switchable Power Device Type.
 *
 * Every sp_dev device (above) has a type. There can be many "sp_devs"
 * sharing the same "sp_dev_type." For example, an sp_dev might correspond
 * with "usb-gadget-foo" and another with "usb-gadget-bar," but they'll
 * both have typpe "usb-gadget."
 *
 * If any function pointer is null, it will be safely ignored. Function
 * pointers accept an argument of "ldm_cdev," which is a
 * "Linux Device Model (LDM) Class Device." Use "to_sp_dev" to
 * get a "struct sp_dev" pointer.
 */
struct sp_dev_type {
	/* Operations that should be carried out before power is removed. */
	int (*pre_poweroff) (struct device *const ldm_spdev);

	/* Operations that should be carried out after power is restored. */
	int (*post_poweron) (struct device *const ldm_spdev);

	/* Plug this type into the linux device model. */
	struct device_type ldm_type;
};
#define to_sp_dev_type(d) container_of(d, struct sp_dev_type, ldm_type)


#endif
