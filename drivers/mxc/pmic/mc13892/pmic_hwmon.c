/*
 * Driver for the adc on Freescale Semiconductor MC13783 and MC13892 PMICs.
 *
 * Copyright 2004-2007 Freescale Semiconductor, Inc. All Rights Reserved.
 * Copyright (C) 2009 Sascha Hauer, Pengutronix
 * Heavily modified by Jean Delvare to fit in the Freescale driver set.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <linux/platform_device.h>
#include <linux/hwmon-sysfs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/hwmon.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/err.h>
#include <asm/mach-types.h>
#include <linux/pmic_adc.h>
#include <linux/pmic_status.h>

#define DRIVER_NAME	"pmic_hwmon"

/* platform device id driver data */
#define MC13783_ADC_16CHANS	1
#define MC13783_ADC_BPDIV2	2

/* device flags */
#define MC13XXX_USE_TOUCHSCREEN	(1 << 0)

static unsigned long driver_data = MC13783_ADC_BPDIV2;
static unsigned long flags;
struct device *hwmon_dev;

static ssize_t mc13783_adc_show_name(struct device *dev, struct device_attribute
			      *devattr, char *buf)
{
	return sprintf(buf, "mc13892\n");
}

static int mc13783_adc_read(struct device *dev,
		struct device_attribute *devattr, unsigned int *val)
{
	struct sensor_device_attribute *attr = to_sensor_dev_attr(devattr);
	t_channel channel = attr->index;
	unsigned short result[8];

	CHECK_ERROR(pmic_adc_convert(channel, result));
	*val = result[0];

	return 0;
}

static ssize_t mc13783_adc_read_bp(struct device *dev,
		struct device_attribute *devattr, char *buf)
{
	unsigned val;
	int ret = mc13783_adc_read(dev, devattr, &val);

	if (ret)
		return ret;

	if (driver_data & MC13783_ADC_BPDIV2)
		val = DIV_ROUND_CLOSEST(val * 9, 2);
	else
		/*
		 * BP (channel 2) reports with offset 2.4V to the actual value
		 * to fit the input range of the ADC.  unit = 2.25mV = 9/4 mV.
		 */
		val = DIV_ROUND_CLOSEST(val * 9, 4) + 2400;

	return sprintf(buf, "%u\n", val);
}

static ssize_t mc13783_adc_read_gp(struct device *dev,
		struct device_attribute *devattr, char *buf)
{
	unsigned val;
	int ret = mc13783_adc_read(dev, devattr, &val);

	if (ret)
		return ret;

	/*
	 * input range is [0, 2.3V], val has 10 bits, so each bit
	 * is worth 9/4 mV.
	 */
	val = DIV_ROUND_CLOSEST(val * 9, 4);

	return sprintf(buf, "%u\n", val);
}

static ssize_t mc13783_adc_read_temp(struct device *dev,
				     struct device_attribute *devattr, char *buf)
{
	unsigned val;
	int ret = mc13783_adc_read(dev, devattr, &val);

	if (ret)
		return ret;

	/*
	 * Return value is raw temperature.
	 * Convert per MC13892 datasheet.
	 */
	return sprintf(buf, "%d\n", DIV_ROUND_CLOSEST(-2635920 + val * 4244, 10));
}

static ssize_t show_in2_label(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "vcc_bp\n");
}

static ssize_t show_in7_label(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "2v775_boot\n");
}

static ssize_t show_temp_label(struct device *dev,
			  struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "pmic_die_temp\n");
}

static DEVICE_ATTR(name, S_IRUGO, mc13783_adc_show_name, NULL);
static SENSOR_DEVICE_ATTR(in2_input, S_IRUGO, mc13783_adc_read_bp, NULL, APPLICATION_SUPPLY);
static SENSOR_DEVICE_ATTR(in2_label, S_IRUGO, show_in2_label, NULL, 0);
static SENSOR_DEVICE_ATTR(in5_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD5);
static SENSOR_DEVICE_ATTR(in6_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD6);
static SENSOR_DEVICE_ATTR(in7_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD7);
static SENSOR_DEVICE_ATTR(in7_label, S_IRUGO, show_in7_label, NULL, 0);
static SENSOR_DEVICE_ATTR(in8_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD8);
static SENSOR_DEVICE_ATTR(in9_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD9);
static SENSOR_DEVICE_ATTR(in10_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD10);
static SENSOR_DEVICE_ATTR(in11_input, S_IRUGO, mc13783_adc_read_gp, NULL, GEN_PURPOSE_AD11);
static SENSOR_DEVICE_ATTR(in12_input, S_IRUGO, mc13783_adc_read_gp, NULL, TS_X_POS1);
static SENSOR_DEVICE_ATTR(in13_input, S_IRUGO, mc13783_adc_read_gp, NULL, TS_X_POS2);
static SENSOR_DEVICE_ATTR(in14_input, S_IRUGO, mc13783_adc_read_gp, NULL, TS_Y_POS1);
static SENSOR_DEVICE_ATTR(in15_input, S_IRUGO, mc13783_adc_read_gp, NULL, TS_Y_POS2);
static SENSOR_DEVICE_ATTR(temp1_input, S_IRUGO, mc13783_adc_read_temp, NULL, DIE_TEMP);
static SENSOR_DEVICE_ATTR(temp1_label, S_IRUGO, show_temp_label, NULL, 0);

static struct attribute *mc13783_attr_base[] = {
	&dev_attr_name.attr,
	&sensor_dev_attr_in2_input.dev_attr.attr,
	&sensor_dev_attr_in2_label.dev_attr.attr,
	&sensor_dev_attr_in5_input.dev_attr.attr,
	&sensor_dev_attr_in6_input.dev_attr.attr,
	&sensor_dev_attr_in7_input.dev_attr.attr,
	&sensor_dev_attr_in7_label.dev_attr.attr,
	&sensor_dev_attr_temp1_input.dev_attr.attr,
	&sensor_dev_attr_temp1_label.dev_attr.attr,
	NULL
};

static const struct attribute_group mc13783_group_base = {
	.attrs = mc13783_attr_base,
};

/* these are only used if MC13783_ADC_16CHANS is provided in driver data */
static struct attribute *mc13783_attr_16chans[] = {
	&sensor_dev_attr_in8_input.dev_attr.attr,
	&sensor_dev_attr_in9_input.dev_attr.attr,
	&sensor_dev_attr_in10_input.dev_attr.attr,
	&sensor_dev_attr_in11_input.dev_attr.attr,
	NULL
};

static const struct attribute_group mc13783_group_16chans = {
	.attrs = mc13783_attr_16chans,
};

/* last four channels may be occupied by the touchscreen */
static struct attribute *mc13783_attr_ts[] = {
	&sensor_dev_attr_in12_input.dev_attr.attr,
	&sensor_dev_attr_in13_input.dev_attr.attr,
	&sensor_dev_attr_in14_input.dev_attr.attr,
	&sensor_dev_attr_in15_input.dev_attr.attr,
	NULL
};

static const struct attribute_group mc13783_group_ts = {
	.attrs = mc13783_attr_ts,
};

static int mc13783_adc_use_touchscreen(void)
{
	return flags & MC13XXX_USE_TOUCHSCREEN;
}

static int __init mc13783_adc_init(void)
{
	int ret;

	hwmon_dev = hwmon_device_register(NULL);
	if (IS_ERR(hwmon_dev)) {
		ret = PTR_ERR(hwmon_dev);
		pr_err("hwmon_device_register failed with %d.\n", ret);
		goto out_err_register;
	}

	/* Register sysfs hooks */
	ret = sysfs_create_group(&hwmon_dev->kobj, &mc13783_group_base);
	if (ret)
		goto out_err_create_base;

	if (driver_data & MC13783_ADC_16CHANS) {
		ret = sysfs_create_group(&hwmon_dev->kobj,
				&mc13783_group_16chans);
		if (ret)
			goto out_err_create_16chans;
	}

	if (!mc13783_adc_use_touchscreen()) {
		ret = sysfs_create_group(&hwmon_dev->kobj, &mc13783_group_ts);
		if (ret)
			goto out_err_create_ts;
	}

	return 0;


out_err_create_ts:

	if (driver_data & MC13783_ADC_16CHANS)
		sysfs_remove_group(&hwmon_dev->kobj, &mc13783_group_16chans);
out_err_create_16chans:

	sysfs_remove_group(&hwmon_dev->kobj, &mc13783_group_base);
out_err_create_base:

	hwmon_device_unregister(hwmon_dev);
out_err_register:

	return ret;
}

static void __exit mc13783_adc_exit(void)
{
	if (!mc13783_adc_use_touchscreen())
		sysfs_remove_group(&hwmon_dev->kobj, &mc13783_group_ts);

	if (driver_data & MC13783_ADC_16CHANS)
		sysfs_remove_group(&hwmon_dev->kobj, &mc13783_group_16chans);

	sysfs_remove_group(&hwmon_dev->kobj, &mc13783_group_base);

	hwmon_device_unregister(hwmon_dev);
}

module_init(mc13783_adc_init);
module_exit(mc13783_adc_exit);

MODULE_DESCRIPTION("MC13783 ADC driver");
MODULE_AUTHOR("Luotao Fu <l.fu@pengutronix.de>");
MODULE_LICENSE("GPL");
