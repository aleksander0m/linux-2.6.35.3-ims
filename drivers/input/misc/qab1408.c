/*
 * Driver for Quick Access Buttons 1408
 *
 * Copyright (c) 2013 The IMS Company
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/input.h>
#include <linux/serio.h>
#include <linux/init.h>
#include <linux/delay.h>

#define DRIVER_DESC	"Quick Access Buttons 1408 driver"

MODULE_AUTHOR("The IMS Company");
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");

#define QAB1408_PACKET_LEN	4
#define QAB1408_FW_QUERY_LEN	46
#define QAB1408_BUFFER_SIZE	QAB1408_FW_QUERY_LEN
#define QAB1408_KEYMAP_SIZE	8

#define QAB1408_PARTNUM_OFFSET	2
#define QAB1408_PARTNUM_LEN	16

#define QAB1408_SERIAL_OFFSET	19
#define QAB1408_SERIAL_LEN	7

#define QAB1408_FW_REV_OFFSET	26
#define QAB1408_FW_REV_LEN	3

#define QAB1408_FW_VER_OFFSET	36
#define QAB1408_FW_VER_LEN	10

#define QAB1408_FW_VERSION_LEN	(QAB1408_FW_VER_LEN + QAB1408_FW_REV_LEN)

static const unsigned short qab1408_keymap[QAB1408_KEYMAP_SIZE] = {
	KEY_LIGHTS_TOGGLE,
	KEY_ATTENDANT_TOGGLE,
	KEY_RESERVED,
	KEY_RESERVED,
	KEY_VOLUMEDOWN,
	KEY_VOLUMEUP,
	KEY_PLAYPAUSE,
	KEY_RESERVED
};

/*
 * Per-touchscreen data.
 */

struct qab1408 {
	struct input_dev *input;
	struct serio *serio;
	struct timer_list timer;

	unsigned int idx;

	unsigned int response_len;
	unsigned char expected_response;
	unsigned char cmd_response[QAB1408_BUFFER_SIZE];
	struct completion cmd_done;

	unsigned short keymap[QAB1408_KEYMAP_SIZE];

	char part_number[QAB1408_PARTNUM_LEN];
	char serial_number[QAB1408_SERIAL_LEN];
	char firmware_version[QAB1408_FW_VERSION_LEN];
	char phys[32];
};

static void qab1408_handle_cmd_response(struct qab1408 *qab1408, unsigned char data)
{
	qab1408->cmd_response[qab1408->idx] = data;

	if (qab1408->cmd_response[0] == qab1408->expected_response &&
	    ++qab1408->idx >= qab1408->response_len) {
		qab1408->idx = 0;
		qab1408->expected_response = 0;
		complete(&qab1408->cmd_done);
	}
}

static void qab1408_report_buttons(struct qab1408 *qab1408, unsigned char data)
{
	struct input_dev *input = qab1408->input;
	int i;

	for (i = 0; i < 8; i++)
		input_report_key(input, qab1408->keymap[i], data & (1 << i));

	input_sync(input);
}

static irqreturn_t qab1408_interrupt(struct serio *serio,
				   unsigned char data, unsigned int flags)
{
	static const unsigned char lead_data[] = { 0x80, 0x00, 0x00 };
	struct qab1408 *qab1408 = serio_get_drvdata(serio);

	if (unlikely(qab1408->expected_response)) {
		qab1408_handle_cmd_response(qab1408, data);
	} else if (qab1408->idx == QAB1408_PACKET_LEN - 1) {
		qab1408_report_buttons(qab1408, data);
		qab1408->idx = 0;
		mod_timer(&qab1408->timer, jiffies + msecs_to_jiffies(50));
	} else if (data != lead_data[qab1408->idx]) {
		dev_dbg(&serio->dev,
			"lost sync at byte %d, expected %x, received %x\n",
			qab1408->idx, lead_data[qab1408->idx], data);
		qab1408->idx = 0;
	} else {
		qab1408->idx++;
	}

	return IRQ_HANDLED;
}

static void qab1408_keyup_timer(unsigned long data)
{
	struct qab1408 *qab1408 = (struct qab1408 *)data;

	serio_pause_rx(qab1408->serio);
	qab1408_report_buttons(qab1408, 0);
	serio_continue_rx(qab1408->serio);
}

struct qab1408_attribute {
	struct device_attribute dattr;
	size_t field_offset;
	int field_length;
};

static ssize_t qab1408_attribute_show(struct device *dev,
				      struct device_attribute *dattr,
				      char *buf)
{
	struct serio *serio = to_serio_port(dev);
	struct qab1408 *qab1408 = serio_get_drvdata(serio);
	struct qab1408_attribute *attr =
			container_of(dattr, struct qab1408_attribute, dattr);
	char *field = (char *)qab1408 + attr->field_offset;

	return scnprintf(buf, PAGE_SIZE, "%.*s\n", attr->field_length, field);
}


#define QAB1408_FW_ATTR(_field)						\
struct qab1408_attribute qab1408_attr_##_field = {			\
	.dattr = __ATTR(_field, S_IRUGO, qab1408_attribute_show, NULL),	\
	.field_offset = offsetof(struct qab1408, _field),		\
	.field_length = sizeof(((struct qab1408 *)NULL)->_field),	\
}

static QAB1408_FW_ATTR(part_number);
static QAB1408_FW_ATTR(serial_number);
static QAB1408_FW_ATTR(firmware_version);

static struct attribute *qab1408_attributes[] = {
	&qab1408_attr_part_number.dattr.attr,
	&qab1408_attr_serial_number.dattr.attr,
	&qab1408_attr_firmware_version.dattr.attr,
	NULL
};

static struct attribute_group qab1408_attr_group = {
	.attrs = qab1408_attributes,
};

static int qab1408_command(struct qab1408 *qab1408,
			   unsigned char *command, unsigned int cmd_len,
			   unsigned char expected_response,
			   unsigned int response_len)
{
	int i;
	int error;

	serio_pause_rx(qab1408->serio);
	init_completion(&qab1408->cmd_done);
	qab1408->expected_response = expected_response;
	qab1408->response_len = response_len;
	memset(qab1408->cmd_response, 0, sizeof(qab1408->cmd_response));
	serio_continue_rx(qab1408->serio);

	for (i = 0; i < cmd_len; i++) {
		error = serio_write(qab1408->serio, command[i]);
		if (error)
			return error;
	}

	wait_for_completion_timeout(&qab1408->cmd_done, HZ);
	if (qab1408->expected_response)
		return -EIO;

	return 0;
}

static int qab1408_query_firmware(struct qab1408 *qab1408)
{
	int error;

	error = qab1408_command(qab1408, "\x42", 1, 0xaf, 1);
	if (error) {
		dev_err(&qab1408->serio->dev,
			"Failed to request BITE, error: %d",
			error);
		return error;
	}

	error = qab1408_command(qab1408, "\x55\xd5", 2, 0x55, 1);
	if (error) {
		dev_err(&qab1408->serio->dev,
			"Failed to switch device to maintenance mode, error: %d",
			error);
		return error;
	}

	error = qab1408_command(qab1408, "\x26\x01", 2, 0x26, 46);
	if (error) {
		dev_err(&qab1408->serio->dev,
			"Failed to query firmware data, error: %d",
			error);
	}

#if 0
	memcpy(qab1408->cmd_response, "& QAB1408103001R00 S000004R00       1339301000", 46);
#endif

	memcpy(qab1408->part_number,
		&qab1408->cmd_response[QAB1408_PARTNUM_OFFSET],
		QAB1408_PARTNUM_LEN);
	memcpy(qab1408->serial_number,
		&qab1408->cmd_response[QAB1408_SERIAL_OFFSET],
		QAB1408_SERIAL_LEN);
	memcpy(qab1408->firmware_version,
		&qab1408->cmd_response[QAB1408_FW_VER_OFFSET],
		QAB1408_FW_VER_LEN);
	memcpy(&qab1408->firmware_version[QAB1408_FW_VER_LEN],
		&qab1408->cmd_response[QAB1408_FW_REV_OFFSET],
		QAB1408_FW_REV_LEN);

	dev_info(&qab1408->serio->dev,
		 "P/N: %.*s, S/N: %.*s, FW: %.*s\n",
		 QAB1408_PARTNUM_LEN, qab1408->part_number,
		 QAB1408_SERIAL_LEN, qab1408->serial_number,
		 QAB1408_FW_VERSION_LEN, qab1408->firmware_version);

	error = qab1408_command(qab1408, "\x52", 1, 0x52, 1);
	if (error) {
		dev_err(&qab1408->serio->dev,
			"Failed to switch device to operation mode, error: %d",
			error);
		return error;
	}

	return 0;
}

static int qab1408_connect(struct serio *serio, struct serio_driver *drv)
{
	struct qab1408 *qab1408;
	struct input_dev *input;
	int i;
	int error;

	qab1408 = kzalloc(sizeof(struct qab1408), GFP_KERNEL);
	input = input_allocate_device();
	if (!qab1408 || !input) {
		dev_err(&serio->dev, "Failed to open allocate memory\n");
		error = -ENOMEM;
		goto err_free_mem;
	}

	qab1408->serio = serio;
	qab1408->input = input;
	setup_timer(&qab1408->timer, qab1408_keyup_timer, (unsigned long) qab1408);
	init_completion(&qab1408->cmd_done);
	snprintf(qab1408->phys, sizeof(qab1408->phys),
		 "%s/input0", serio->phys);
	memcpy(qab1408->keymap, qab1408_keymap, sizeof(qab1408->keymap));

	serio_set_drvdata(serio, qab1408);
	error = serio_open(serio, drv);
	if (error) {
		dev_err(&serio->dev,
			"Failed to open serio port, error: %d\n", error);
		goto err_reset_drvdata;
	}

	error = qab1408_query_firmware(qab1408);
	if (error) {
		dev_err(&serio->dev,
			"Failed to open serio port, error: %d\n", error);
		goto err_close_port;
	}

	error = sysfs_create_group(&serio->dev.kobj, &qab1408_attr_group);
	if (error) {
		dev_err(&serio->dev,
			"Failed to create sysfs attribute group, error: %d\n",
			error);
		goto err_close_port;
	}

	input->name = "Quick Access Buttons QAB1408";
	input->phys = qab1408->phys;
	input->id.product = 0x1408;
	input->id.bustype = BUS_RS232;
	input->id.vendor = 0x00;
	input->id.version = 0x00;
	input->dev.parent = &serio->dev;

	input->keycode = qab1408->keymap;
	input->keycodemax = ARRAY_SIZE(qab1408->keymap);
	input->keycodesize = sizeof(qab1408->keymap[0]);

	__set_bit(EV_KEY, input->evbit);
	for (i = 0; i < QAB1408_KEYMAP_SIZE; i++)
		__set_bit(qab1408->keymap[i], input->keybit);

	input_set_drvdata(input, qab1408);

	error = input_register_device(input);
	if (error) {
		dev_err(&serio->dev,
			"Failed to register input device, error: %d\n",
			error);
		goto err_remove_sysfs;
	}

	return 0;

err_remove_sysfs:
	sysfs_remove_group(&serio->dev.kobj, &qab1408_attr_group);
err_close_port:
	serio_close(serio);
	del_timer_sync(&qab1408->timer);
err_reset_drvdata:
	serio_set_drvdata(serio, NULL);
err_free_mem:
	input_free_device(input);
	kfree(qab1408);
	return error;
}

static void qab1408_disconnect(struct serio *serio)
{
	struct qab1408 *qab1408 = serio_get_drvdata(serio);

	serio_close(serio);
	del_timer_sync(&qab1408->timer);

	input_unregister_device(qab1408->input);
	sysfs_remove_group(&serio->dev.kobj, &qab1408_attr_group);
	kfree(qab1408);

	serio_set_drvdata(serio, NULL);
}

static struct serio_device_id qab1408_serio_ids[] = {
	{
		.type	= SERIO_RS232,
		.proto	= SERIO_QAB1408,
		.id	= SERIO_ANY,
		.extra	= SERIO_ANY,
	},
	{ 0 }
};

MODULE_DEVICE_TABLE(serio, qab1408_serio_ids);

static struct serio_driver qab1408_drv = {
	.driver		= {
		.name	= "qab1408",
	},
	.description	= DRIVER_DESC,
	.id_table	= qab1408_serio_ids,
	.interrupt	= qab1408_interrupt,
	.connect	= qab1408_connect,
	.disconnect	= qab1408_disconnect,
};

static int __init qab1408_init(void)
{
	return serio_register_driver(&qab1408_drv);
}
module_init(qab1408_init);

static void __exit qab1408_exit(void)
{
	serio_unregister_driver(&qab1408_drv);
}
module_exit(qab1408_exit);
