/*
 * Atmel maXTouch Touchscreen driver
 *
 * Copyright (C) 2010 Samsung Electronics Co.Ltd
 * Author: Joonyoung Shim <jy0922.shim@samsung.com>
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */

//#define DEBUG
//#define NOFAIL
//#define TRACE

#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/firmware.h>
#include <linux/i2c.h>
#include <linux/i2c/atmel_mxt_ts.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/debugfs.h>
#include <asm/unaligned.h>

#include <linux/miscdevice.h>
#include <linux/input/atmel_mxt_ts.h>
#include <asm/uaccess.h>
#include <linux/version.h>

/* can't insert over this limit */
#define MAX_MXT_MESSAGE_QUEUE   16
#define MXT_MAX_BLOCK_WRITE	255
#define DEBUG_MSG_MAX		200

struct mxt_message {
	u8 reportid;
	u8 message[7];
	u8 checksum;
};

typedef struct mxt_message_element
{
   struct list_head     list;
   struct mxt_message   msg;
} mxt_message_element;

typedef struct mxt_message_queue
{
   struct list_head list;
   spinlock_t       lock;   // lock for irq and userspace access
   int              size;   // size counter
   int              active; // whether queueing is active. Turned on via the message attribute: cat 1 > .../messages
} mxt_message_queue;

struct mxt_finger {
	int status;
	int x;
	int y;
	int area;
	int pressure;
};

/* Each client has this additional data */
struct mxt_data {
	struct i2c_client *client;
	struct input_dev *input_dev;
	const struct mxt_platform_data *pdata;
	struct mxt_object *object_table;
	struct mxt_info info;
	struct mxt_finger finger[MXT_MAX_FINGER];
	unsigned int irq;
	unsigned int max_x;
	unsigned int max_y;
	mxt_message_queue* message_queue;
	bool debug_enabled;
	bool debug_v2_enabled;
	u8 *debug_msg_data;
	u16 debug_msg_count;
	struct bin_attribute debug_msg_attr;
	struct mutex debug_msg_lock;
	struct bin_attribute mem_access_attr;
	bool in_bootloader;
	u16 mem_size;
	u8 T5_msg_size;

	u16 t37_address;
	u16 diag_cmd_address;
	struct t37_debug *t37_buf;
	u16 *debug_buf;
	unsigned int t37_pages;
	unsigned int t37_nodes;

	struct dentry *debugfs_dir;
	struct dentry *refs_file;
	struct dentry *deltas_file;
	struct debugfs_blob_wrapper debug_blob;
};

static struct mxt_data *private_data;

#ifdef TRACE
#define trace	pr_debug
#else
#define trace(...)
#endif
#ifdef DEBUG
#ifndef BIT
#define BIT(x)	(1<<(x))
#endif
static char buf[64];
static inline char *cmdStat_toStr( u8 status )
{
	int len = 0;
	buf[0] = 0;
	if ( status & BIT(7)) len += sprintf( buf+len, "RESET " ); 
	if ( status & BIT(6)) len += sprintf( buf+len, "OFL " ); 
	if ( status & BIT(5)) len += sprintf( buf+len, "SIGERR " ); 
	if ( status & BIT(4)) len += sprintf( buf+len, "CAL " ); 
	if ( status & BIT(3)) len += sprintf( buf+len, "CFGERR " ); 
	if ( status & BIT(2)) len += sprintf( buf+len, "COMSERR" ); 
	return buf;
}

static inline char *proxRprt_toStr( u8 report )
{
	int len = 0;
	buf[0] = 0;
	if ( report & BIT(7)) len += sprintf( buf+len, "FXDDETECT " ); 
	if ( report & BIT(6)) len += sprintf( buf+len, "MVRISE " ); 
	if ( report & BIT(5)) len += sprintf( buf+len, "MVFALL" ); 
	return buf;
}

static char *touchStatus_toStr( u8 status )
{
	int len = 0;
	buf[0] = 0;
	if ( status & BIT(7)) len += sprintf( buf+len, "DETECT " ); 
	if ( status & BIT(6)) len += sprintf( buf+len, "PRESS " ); 
	if ( status & BIT(5)) len += sprintf( buf+len, "RELEASE " ); 
	if ( status & BIT(4)) len += sprintf( buf+len, "MOVE " ); 
	if ( status & BIT(3)) len += sprintf( buf+len, "VECTOR " ); 
	if ( status & BIT(2)) len += sprintf( buf+len, "AMP " ); 
	if ( status & BIT(1)) len += sprintf( buf+len, "SUPPRESS" ); 
	return buf;
}

static inline char *noisesup_toStr( u8 status )
{
	int len = 0;
	buf[0] = 0;
	if ( status & BIT(4)) len += sprintf( buf+len, "GCAFCHG " ); 
	if ( status & BIT(3)) len += sprintf( buf+len, "FHERR " ); 
	if ( status & BIT(2)) len += sprintf( buf+len, "GCAFERR " ); 
	if ( status & BIT(0)) len += sprintf( buf+len, "FHCHG" ); 
	return buf;
}

static inline char *oneTouchEvent_toStr( u8 event )
{
	switch (event & 0xf) {
	case 1: return "Press";
	case 2: return "Release";
	case 3: return "Tap";
	case 4: return "Double-tap";
	case 5: return "Flick";
	case 6: return "Drag";
	case 7: return "Short Press";
	case 8: return "Long Press";
	case 9: return "Repeat Press";
	case 10: return "Tap and Press";
	case 11: return "Throw";
	default: return "Reserved";
	}
}

static inline char* objectTypeName( u8 typeId )
{
	switch(typeId)
	{
	case 5: return "MXT_GEN_MESSAGE_T5";
	case 6: return "MXT_GEN_COMMAND_T6";
	case 7: return "MXT_GEN_POWER_T7";
	case 8: return "MXT_GEN_ACQUIRE_T8";
	case 9: return "MXT_TOUCH_MULTI_T9";
	case 15: return "MXT_TOUCH_KEYARRAY_T15";
	case 18: return "MXT_SPT_COMMSCONFIG_T18";
	case 19: return "MXT_SPT_GPIOPWM_T19";
	case 20: return "MXT_PROCI_GRIPFACE_T20";
	case 22: return "MXT_PROCG_NOISE_T22";
	case 23: return "MXT_TOUCH_PROXIMITY_T23";
	case 24: return "MXT_PROCI_ONETOUCH_T24";
	case 25: return "MXT_SPT_SELFTEST_T25";
	case 27: return "MXT_PROCI_TWOTOUCH_T27";
	case 28: return "MXT_SPT_CTECONFIG_T28";
	case 37: return "MXT_DEBUG_DIAGNOSTIC_T37";
	case 38: return "MXT_SPT_USERDATA_T38";
	case 40: return "MXT_PROCI_GRIP_T40";
	case 41: return "MXT_PROCI_PALM_T41";
	case 42: return "MXT_PROCI_TOUCHSUPPRESSION_T42";
	case 43: return "MXT_SPT_DIGITIZER_T43";
	case 44: return "MXT_SPT_MESSAGECOUNT_T44";

	case 46: return "MXT_SPT_CTECONFIG_T46";
	case 47: return "MXT_PROCI_STYLUS_T47";
	case 48: return "MXT_PROCG_NOISESUPPRESSION_T48";
//	case 48: return "MXT_SPT_NOISESUPPRESSION_T48";
	case 52: return "MXT_TOUCH_PROXKEY_T52";
	case 53: return "MXT_GEN_DATASOURCE_T53";
	case 63: return "MXT_PROCI_ACTIVE_STYLUS_T63";
	case 100: return "MXT_TOUCH_MULTITOUCHSCREEN_T100";
	default: return "Unknown objed?";
	}
}
#endif

static void mxt_dump_message(struct device *dev,
				  struct mxt_message *message)
{
#ifdef DEBUG
	// be smarter about message dump...
	// these report id's may change with a fw change...
	switch( message->reportid ) {
	case 0xff:
		//dev_dbg(dev, "EOL\n" );
		break;
	case 0x01:
		dev_dbg(dev, "Command Status:\t%s(%02x)\n",
			cmdStat_toStr( message->message[0]), message->message[0]);
		dev_dbg(dev, "  checksum:\t0x%x\n",
			(message->message[1] << 0)|(message->message[2]<<8)|(message->message[3]<<16));
		break;
	case 2 ... 11:
		dev_dbg(dev, "MultiTouch Report:\t\n" );
		break;
	case 13:
		dev_dbg(dev, "Key Array Report:\t\n" );
		break;
	case 14:
		dev_dbg(dev, "Grip Face Report:\t%s\n", message->message[0] & BIT(0) ? "FACESUP":"" );
		break;
	case 15:
		dev_dbg(dev, "Proc Noise Status:\t%s(%02x)\n", 
			noisesup_toStr(message->message[0]), message->message[0]);
		dev_dbg(dev, "  Acquisition Ave:\t0x%x\n", message->message[1]);
		dev_dbg(dev, "  Freq hop idx:\t0x%x\n", message->message[2]);
		break;
	case 16:
		dev_dbg(dev, "Proximity Report:\t%s\n", proxRprt_toStr( message->message[0]));
		dev_dbg(dev, "  prox delta:\t0x%x\n", (message->message[1]<<0)|(message->message[2]<<8));
		break;
	case 17 ... 20:
		dev_dbg(dev, "One Touch Report (%d):\t\n", message->reportid - 17 );
		dev_dbg(dev, "  event:\t%s\n", oneTouchEvent_toStr( message->message[0] ));
		dev_dbg(dev, "  X pos:\t%d\n", (message->message[1]<<4)|((message->message[3]>>4)&0xf));
		dev_dbg(dev, "  Y pos:\t%d\n", (message->message[2]<<4)|((message->message[3]>>0)&0xf));
		dev_dbg(dev, "  Direction:\t0x%x\n", message->message[4]);
		dev_dbg(dev, "  Distance:\t0x%x\n", message->message[5]);
		break;
	case 21:
		dev_dbg(dev, "SelfTest Report:\t\n" );
		break;
	case 22:
		dev_dbg(dev, "Two Touch Report:\t\n" );
		break;
	case 23:
		dev_dbg(dev, "CTE Config Report:\t\n" );
		break;
	}
#endif

	print_hex_dump(KERN_DEBUG, "MXT MSG:", DUMP_PREFIX_NONE, 16, 1,
		message, sizeof(struct mxt_message), false);
}

static void mxt_debug_msg_enable(struct mxt_data *data)
{
	struct device *dev = &data->client->dev;

	if (data->debug_v2_enabled)
		return;

	mutex_lock(&data->debug_msg_lock);

	data->debug_msg_data = kcalloc(DEBUG_MSG_MAX,
				       data->T5_msg_size, GFP_KERNEL);
	if (!data->debug_msg_data)
		return;

	data->debug_v2_enabled = true;
	mutex_unlock(&data->debug_msg_lock);

	dev_info(dev, "Enabled message output\n");
}

static void mxt_debug_msg_disable(struct mxt_data *data)
{
	struct device *dev = &data->client->dev;

	if (!data->debug_v2_enabled)
		return;

	dev_info(dev, "disabling message output\n");
	data->debug_v2_enabled = false;

	mutex_lock(&data->debug_msg_lock);
	kfree(data->debug_msg_data);
	data->debug_msg_data = NULL;
	data->debug_msg_count = 0;
	mutex_unlock(&data->debug_msg_lock);
	dev_info(dev, "Disabled message output\n");
}

static void mxt_debug_msg_add(struct mxt_data *data, u8 *msg)
{
	struct device *dev = &data->client->dev;

	mutex_lock(&data->debug_msg_lock);

	if (!data->debug_msg_data) {
		dev_err(dev, "No buffer!\n");
		return;
	}

	if (data->debug_msg_count < DEBUG_MSG_MAX) {
		memcpy(data->debug_msg_data +
		       data->debug_msg_count * sizeof(struct mxt_message), msg,
		       data->T5_msg_size);
		data->debug_msg_count++;
	} else {
		dev_dbg(dev, "Discarding %u messages\n", data->debug_msg_count);
		data->debug_msg_count = 0;
	}

	mutex_unlock(&data->debug_msg_lock);

	sysfs_notify(&data->client->dev.kobj, NULL, "debug_notify");
}

static ssize_t mxt_debug_msg_write(struct file *filp, struct kobject *kobj,
	struct bin_attribute *bin_attr, char *buf, loff_t off,
	size_t count)
{
	return -EIO;
}

static ssize_t mxt_debug_msg_read(struct file *filp, struct kobject *kobj,
	struct bin_attribute *bin_attr, char *buf, loff_t off, size_t bytes)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct mxt_data *data = dev_get_drvdata(dev);
	int count;
	size_t bytes_read;

	if (!data->debug_msg_data) {
		dev_err(dev, "No buffer!\n");
		return 0;
	}

	count = bytes / data->T5_msg_size;

	if (count > DEBUG_MSG_MAX)
		count = DEBUG_MSG_MAX;

	mutex_lock(&data->debug_msg_lock);

	if (count > data->debug_msg_count)
		count = data->debug_msg_count;

	bytes_read = count * data->T5_msg_size;

	memcpy(buf, data->debug_msg_data, bytes_read);
	data->debug_msg_count = 0;

	mutex_unlock(&data->debug_msg_lock);

	return bytes_read;
}

static int mxt_debug_msg_init(struct mxt_data *data)
{
	sysfs_bin_attr_init(&data->debug_msg_attr);
	data->debug_msg_attr.attr.name = "debug_msg";
	data->debug_msg_attr.attr.mode = 0666;
	data->debug_msg_attr.read = mxt_debug_msg_read;
	data->debug_msg_attr.write = mxt_debug_msg_write;
	data->debug_msg_attr.size = data->T5_msg_size * DEBUG_MSG_MAX;

	if (sysfs_create_bin_file(&data->client->dev.kobj,
				  &data->debug_msg_attr) < 0) {
		dev_err(&data->client->dev, "Failed to create %s\n",
			data->debug_msg_attr.attr.name);
		return -EINVAL;
	}

	return 0;
}

static void mxt_debug_msg_remove(struct mxt_data *data)
{
	if (data->debug_msg_attr.attr.name)
		sysfs_remove_bin_file(&data->client->dev.kobj,
				      &data->debug_msg_attr);
}

static int mxt_message_queue_is_empty ( mxt_message_queue *msg_q ) {

    int ret;

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return 1;
    }

    spin_lock ( & ( msg_q->lock ) );
    ret = list_empty ( & ( msg_q->list ) );
    spin_unlock ( & ( msg_q->lock ) );

    return ret;
}

static int mxt_message_queue_size ( mxt_message_queue *msg_q ) {

    int ret;

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return 0;
    }

    spin_lock ( & ( msg_q->lock ) );
    ret = msg_q->size;
    spin_unlock ( & ( msg_q->lock ) );

    return ret;
}

static int mxt_message_queue_active ( mxt_message_queue *msg_q ) {

    int ret;

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return 0;
    }

    spin_lock ( & ( msg_q->lock ) );
    ret = msg_q->active;
    spin_unlock ( & ( msg_q->lock ) );

    return ret;
}

static int mxt_message_queue_push ( mxt_message_queue* msg_q, struct mxt_message* msg ) {

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return 0;
    }

    /*
     * NOTE:    Pushing elements in the queue are passive actions.
     *          We are allowing messages to be dropped!
     */
    if( mxt_message_queue_active ( msg_q ) &&
        mxt_message_queue_size(msg_q) < MAX_MXT_MESSAGE_QUEUE )
    {
        struct mxt_message_element * element;

        element = (mxt_message_element *) kmalloc ( sizeof(mxt_message_element), GFP_ATOMIC );
        if ( !element ) {
            printk(KERN_DEBUG "mxt_message_queue_push failed\n");
            return 0;
        }

        memcpy ( &element->msg, msg, sizeof(struct mxt_message) );

        spin_lock ( & ( msg_q->lock ) );

        list_add_tail ( & ( element->list ), & ( msg_q->list ) );
        msg_q->size++;

        spin_unlock ( & ( msg_q->lock ) );

        return 1; // we've added something
    }

    return 0 ;
}

static int mxt_message_queue_pop ( mxt_message_queue* msg_q, struct mxt_message* msg ) {

    mxt_message_element * element;

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return 0;
    }

    if ( mxt_message_queue_is_empty ( msg_q ) ) {
        return 0;
    }

    spin_lock ( & ( msg_q->lock ) );

    element = list_first_entry(&(msg_q->list), mxt_message_element, list);

    if ( msg ) {
        memcpy ( msg, &element->msg, sizeof(struct mxt_message) );
    }

    list_del ( & ( element->list ) );
    kfree ( (void *) element );

    msg_q->size--;

    spin_unlock ( & ( msg_q->lock ) );

    return 1;
}

static void mxt_message_queue_clear ( mxt_message_queue *msg_q ) {

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return ;
    }

    // this is purposefully pop'ing the messages down to nothing before we free
    while ( mxt_message_queue_pop ( msg_q, 0 ) )
        ;
}

static void mxt_message_queue_set_active ( mxt_message_queue *msg_q, int active ) {

    trace("%s\n", __func__ );

    if( !msg_q )
    {
        printk(KERN_DEBUG"%s - No 'msg_q'!\n", __func__);
        return ;
    }

    spin_lock ( & ( msg_q->lock ) );
    msg_q->active = active;
    spin_unlock ( & ( msg_q->lock ) );

    // not active, clear elements
    if( !active )
    {
        mxt_message_queue_clear( msg_q );
    }
}

static struct mxt_message_queue * mxt_message_queue_create ( void ) {

    mxt_message_queue * msg_q;

    trace("%s\n", __func__ );

    msg_q = (mxt_message_queue*) kmalloc ( sizeof(mxt_message_queue), GFP_ATOMIC );
    if ( !msg_q ) {
        printk(KERN_DEBUG "mxt_message_queue_create failed\n");
        return 0;
    }

    INIT_LIST_HEAD ( & ( msg_q->list ) );
    spin_lock_init( &(msg_q->lock) );
    msg_q->size = 0;
    msg_q->active = 0;

    return msg_q;
}

static void mxt_message_queue_delete ( mxt_message_queue *msg_q ) {

    trace("%s\n", __func__ );

    if ( !msg_q ) {
        printk(KERN_DEBUG "mxt_message_queue_delete null pointer\n");
        return;
    }

    mxt_message_queue_clear ( msg_q );

    kfree ( (void *) msg_q );
}

static int mxt_check_bootloader(struct i2c_client *client,
				     unsigned int state)
{
	u8 val;

	trace("%s@%d\n", __func__, __LINE__ );

recheck:
	if (i2c_master_recv(client, &val, 1) != 1) {
		dev_err(&client->dev, "%s: i2c recv failed\n", __func__);
		return -EIO;
	}

	switch (state) {
	case MXT_WAITING_BOOTLOAD_CMD:
	case MXT_WAITING_FRAME_DATA:
		val &= ~MXT_BOOT_STATUS_MASK;
		break;
	case MXT_FRAME_CRC_PASS:
		if (val == MXT_FRAME_CRC_CHECK)
			goto recheck;
		break;
	default:
		return -EINVAL;
	}

	if (val != state) {
		dev_err(&client->dev, "Unvalid bootloader mode state\n");
		return -EINVAL;
	}

	return 0;
}

static int mxt_unlock_bootloader(struct i2c_client *client)
{
	u8 buf[2];

	trace("%s@%d\n", __func__, __LINE__ );

	buf[0] = MXT_UNLOCK_CMD_LSB;
	buf[1] = MXT_UNLOCK_CMD_MSB;

	if (i2c_master_send(client, buf, 2) != 2) {
		dev_err(&client->dev, "%s: i2c send failed\n", __func__);
		return -EIO;
	}

	return 0;
}

static int mxt_fw_write(struct i2c_client *client,
			     const u8 *data, unsigned int frame_size)
{
	trace("%s@%d\n", __func__, __LINE__ );

	if (i2c_master_send(client, data, frame_size) != frame_size) {
		dev_err(&client->dev, "%s: i2c send failed\n", __func__);
		return -EIO;
	}

	return 0;
}

static int __mxt_read_reg(struct i2c_client *client,
			       u16 reg, u16 len, void *val)
{
	struct i2c_msg xfer[2];
	u8 buf[2];

	trace("%s@%d: 0x%04X x %d\n", __func__, __LINE__, reg, len );

	buf[0] = reg & 0xff;
	buf[1] = (reg >> 8) & 0xff;

	/* Write register */
	xfer[0].addr = client->addr;
	xfer[0].flags = 0;
	xfer[0].len = 2;
	xfer[0].buf = buf;

	/* Read data */
	xfer[1].addr = client->addr;
	xfer[1].flags = I2C_M_RD;
	xfer[1].len = len;
	xfer[1].buf = val;

	if (i2c_transfer(client->adapter, xfer, 2) != 2) {
		dev_err(&client->dev, "%s: i2c transfer failed\n", __func__ );
		return -EIO;
	}

	return 0;
}

static int mxt_read_reg(struct i2c_client *client, u16 reg, u8 *val)
{
	return __mxt_read_reg(client, reg, 1, val);
}

static int __mxt_write_reg(struct i2c_client *client, u16 reg, u16 len, u8 *val)
{
	u8 buf[256];

	u8* bufPtr = buf;
	u16 bufLen = len + 2;

	*bufPtr++ = reg & 0xff;
	*bufPtr++ = (reg >> 8) & 0xff;
	memcpy( bufPtr, val, len);

	if (i2c_master_send(client, buf, bufLen ) != bufLen ) {
		dev_err(&client->dev, "%s: i2c send failed\n", __func__);
		return -EIO;
	}

	return 0;
}

static int mxt_write_reg(struct i2c_client *client, u16 reg, u8 val)
{
	trace("%s@%d: 0x%04X <- 0x%02x\n", __func__, __LINE__, reg, val );
	return __mxt_write_reg(client, reg, 1, &val);
}

static int mxt_read_object_table(struct i2c_client *client,
				      u16 reg, u8 *object_buf)
{
	return __mxt_read_reg(client, reg, MXT_OBJECT_SIZE,
				   object_buf);
}

static struct mxt_object *
mxt_get_object(struct mxt_data *data, u8 type)
{
	struct mxt_object *object;
	int i;

	trace("%s@%d: type %s\n", __func__, __LINE__, objectTypeName(type) );

	for (i = 0; i < data->info.object_num; i++) {
		object = data->object_table + i;
		if (object->type == type)
			return object;
	}

	dev_err(&data->client->dev, "Invalid object type\n");
	return NULL;
}

static int mxt_read_message(struct mxt_data *data,
				 struct mxt_message *message)
{
	struct mxt_object *object;
	u16 reg;

	trace("%s@%d\n", __func__, __LINE__ );

	object = mxt_get_object(data, MXT_GEN_MESSAGE_T5);
	if (!object)
		return -EINVAL;

	reg = object->start_address;
	return __mxt_read_reg(data->client, reg,
			sizeof(struct mxt_message), message);
}

static int mxt_read_object(struct mxt_data *data,
				u8 type, u8 offset, u8 *val)
{
	struct mxt_object *object;
	u16 reg;

	trace("%s@%d\n", __func__, __LINE__ );

	object = mxt_get_object(data, type);
	if (!object)
		return -EINVAL;

	reg = object->start_address;
	return __mxt_read_reg(data->client, reg + offset, 1, val);
}

static int mxt_write_object(struct mxt_data *data,
				 u8 type, u8 offset, u8 val)
{
	struct mxt_object *object;
	u16 reg;

	trace("%s@%d[%s]: 0x%02X <- 0x%02X\n", __func__, __LINE__, objectTypeName(type), offset, val );

	object = mxt_get_object(data, type);
	if (!object)
		return -EINVAL;

	reg = object->start_address;
	return mxt_write_reg(data->client, reg + offset, val);
}

static void mxt_input_report(struct mxt_data *data, int single_id)
{
	struct mxt_finger *finger = data->finger;
	struct input_dev *input_dev = data->input_dev;
	int status = finger[single_id].status;
#ifdef CONFIG_ATMEL_MULTITOUCH_SUPPORT
	int finger_num = 0;
	int id;
#endif
#ifdef DEBUG
	printk(KERN_DEBUG "%s(id %d): %4d %4d %s(%02x)\n", __func__, single_id, 
		finger[single_id].x, finger[single_id].y,
		touchStatus_toStr( finger[single_id].status ), finger[single_id].status );
#endif
#ifdef CONFIG_ATMEL_MULTITOUCH_SUPPORT
	for (id = 0; id < MXT_MAX_FINGER; id++) {
		if (!finger[id].status)
			continue;

		dev_dbg(dev, "  finger %d: %4d %4d %s(%02x)\n", id,
			finger[id].x, finger[id].y,
			touchStatus_toStr( finger[id].status ), finger[id].status );

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,38)
		input_mt_slot(input_dev, id);
		input_mt_report_slot_state(input_dev, MT_TOOL_FINGER,
				finger[id].status != MXT_RELEASE);
#endif
		if (finger[id].status != MXT_RELEASE) {
			finger_num++;

			input_report_abs(input_dev, ABS_MT_POSITION_X,
					finger[id].x);
			input_report_abs(input_dev, ABS_MT_POSITION_Y,
					finger[id].y);
			input_report_abs(input_dev, ABS_MT_TOUCH_MAJOR,
					finger[id].area);
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,38)
			input_mt_sync(input_dev);
#endif
		} else {
			finger[id].status = 0;
		}
	}
	input_report_key(input_dev, BTN_TOUCH, finger_num > 0);
#endif

	if (status != MXT_RELEASE) {
		input_report_abs(input_dev, ABS_X, finger[single_id].x);
		input_report_abs(input_dev, ABS_Y, finger[single_id].y);
		input_report_abs(input_dev, ABS_PRESSURE, finger[single_id].pressure);
	}

#ifndef CONFIG_ATMEL_MULTITOUCH_SUPPORT
	input_report_key(input_dev, BTN_TOUCH, status != MXT_RELEASE);
#endif

	input_sync(input_dev);
}

static void mxt_input_touchevent(struct mxt_data *data,
				      struct mxt_message *message, int id)
{
	struct mxt_finger *finger = data->finger;
	struct device *dev = &data->client->dev;
	u8 status = message->message[0];
	int x;
	int y;
	int area;
	int pressure;

	/* Check the touch is present on the screen */
	if (!(status & MXT_DETECT)) {
		if (status & MXT_RELEASE) {
			dev_dbg(dev, "[%d] released\n", id);

			finger[id].status = MXT_RELEASE;
			mxt_input_report(data, id);
		}
		return;
	}

	/* Check only AMP detection */
	if (!(status & (MXT_PRESS | MXT_MOVE)))
		return;

	x = (message->message[1] << 4) | ((message->message[3] >> 4) & 0xf);
	y = (message->message[2] << 4) | ((message->message[3] & 0xf));
	if (data->max_x < 1024)
		x = x >> 2;
	if (data->max_y < 1024)
		y = y >> 2;

	area = message->message[4];
	pressure = message->message[5];

	dev_dbg(dev, "[%d] %s x: %d, y: %d, area: %d\n", id,
		status & MXT_MOVE ? "moved" : "pressed",
		x, y, area);

	finger[id].status = status & MXT_MOVE ?
				MXT_MOVE : MXT_PRESS;
	finger[id].x = x;
	finger[id].y = y;
	finger[id].area = area;
	finger[id].pressure = pressure;

	mxt_input_report(data, id);
}

static irqreturn_t mxt_interrupt(int irq, void *dev_id)
{
	struct mxt_data *data = dev_id;
	struct mxt_message message;
	struct mxt_object *object;
	struct device *dev = &data->client->dev;
	int id;
	u8 reportid;
	u8 max_reportid;
	u8 min_reportid;

	trace("%s@%d\n", __func__, __LINE__ );

	do {
		bool dump = data->debug_enabled;
		if (mxt_read_message(data, &message)) {
			dev_err(dev, "Failed to read message\n");
			goto end;
		}

		if (message.reportid == MXT_RPTID_NOMSG)
			break;

		if (mxt_message_queue_push ( data->message_queue, &message ) )
        {
            dev_err(dev, "Added a message to the queue!\n");
            sysfs_notify(&dev->kobj, 0, "message_queue");
        }

		reportid = message.reportid;

		/* whether reportid is thing of MXT_TOUCH_MULTI_T9 */
		object = mxt_get_object(data, MXT_TOUCH_MULTI_T9);
		if (!object)
			goto end;

		max_reportid = object->max_reportid;
		min_reportid = max_reportid - object->num_report_ids + 1;
		id = reportid - min_reportid;

		if (reportid >= min_reportid && reportid <= max_reportid)
			mxt_input_touchevent(data, &message, id);
#ifdef DEBUG
		else
			dump = true;
#endif

		if (dump)
			mxt_dump_message(dev, &message);

		if (data->debug_v2_enabled)
			mxt_debug_msg_add(data, (u8 *)&message);

	} while (reportid != MXT_RPTID_NOMSG);

end:
	return IRQ_HANDLED;
}

static int mxt_check_reg_init(struct mxt_data *data)
{
	const struct mxt_platform_data *pdata = data->pdata;
	struct mxt_object *object;
	struct device *dev = &data->client->dev;
	int index = 0;
	int i, j, config_offset;

	trace("%s@%d\n", __func__, __LINE__ );

	if (!pdata->config) {
		dev_dbg(dev, "No cfg data defined, skipping reg init\n");
		return 0;
	}

	for (i = 0; i < data->info.object_num; i++) {
		object = data->object_table + i;

		if ((object->type == MXT_DEBUG_DIAGNOSTIC_T37)||(object->type == MXT_SPT_USERDATA_T38))
			continue;

		if (!mxt_object_writable(object->type))
			continue;

		for (j = 0;
		     j < (object->size + 1) * (object->instances + 1);
		     j++) {
			config_offset = index + j;
			if (config_offset > pdata->config_length) {
				dev_err(dev, "Not enough config data!\n");
				return -EINVAL;
			}
			mxt_write_object(data, object->type, j,
					 pdata->config[config_offset]);
		}
		index += (object->size + 1) * (object->instances + 1);
	}

	return 0;
}

static int mxt_make_highchg(struct mxt_data *data)
{
	struct device *dev = &data->client->dev;
	struct mxt_message message;
	int count = 10;
	int error;

	trace("%s@%d\n", __func__, __LINE__ );

	/* Read dummy message to make high CHG pin */
	do {
		error = mxt_read_message(data, &message);
		if (error)
			return error;
	} while (message.reportid != 0xff && --count);

	if (!count) {
		dev_err(dev, "CHG pin isn't cleared\n");
		return -EBUSY;
	}

	return 0;
}

static void mxt_handle_pdata(struct mxt_data *data)
{
	const struct mxt_platform_data *pdata = data->pdata;
	u8 voltage;

	trace("%s@%d\n", __func__, __LINE__ );

	/* Set touchscreen lines */
	mxt_write_object(data, MXT_TOUCH_MULTI_T9, MXT_TOUCH_XSIZE,
			pdata->x_line);
	mxt_write_object(data, MXT_TOUCH_MULTI_T9, MXT_TOUCH_YSIZE,
			pdata->y_line);

	/* Set touchscreen orient */
	mxt_write_object(data, MXT_TOUCH_MULTI_T9, MXT_TOUCH_ORIENT,
			pdata->orient);

	/* Set touchscreen burst length */
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_BLEN, pdata->blen);

	/* Set touchscreen threshold */
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_TCHTHR, pdata->threshold);

	/* Set touchscreen resolution */
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_XRANGE_LSB, (pdata->x_size - 1) & 0xff);
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_XRANGE_MSB, (pdata->x_size - 1) >> 8);
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_YRANGE_LSB, (pdata->y_size - 1) & 0xff);
	mxt_write_object(data, MXT_TOUCH_MULTI_T9,
			MXT_TOUCH_YRANGE_MSB, (pdata->y_size - 1) >> 8);

	/* Set touchscreen voltage */
	if (pdata->voltage) {
		if (pdata->voltage < MXT_VOLTAGE_DEFAULT) {
			voltage = (MXT_VOLTAGE_DEFAULT - pdata->voltage) /
				MXT_VOLTAGE_STEP;
			voltage = 0xff - voltage + 1;
		} else
			voltage = (pdata->voltage - MXT_VOLTAGE_DEFAULT) /
				MXT_VOLTAGE_STEP;

		mxt_write_object(data, MXT_SPT_CTECONFIG_T28,
				MXT_CTE_VOLTAGE, voltage);
	}
}

static int mxt_get_info(struct mxt_data *data)
{
	struct i2c_client *client = data->client;
	struct mxt_info *info = &data->info;
	int error;
	u8 val;

	trace("%s@%d\n", __func__, __LINE__ );

	error = mxt_read_reg(client, MXT_FAMILY_ID, &val);
	if (error)
		return error;
	info->family_id = val;

	error = mxt_read_reg(client, MXT_VARIANT_ID, &val);
	if (error)
		return error;
	info->variant_id = val;

	error = mxt_read_reg(client, MXT_VERSION, &val);
	if (error)
		return error;
	info->version = val;

	error = mxt_read_reg(client, MXT_BUILD, &val);
	if (error)
		return error;
	info->build = val;

	error = mxt_read_reg(client, MXT_OBJECT_NUM, &val);
	if (error)
		return error;
	info->object_num = val;

	return 0;
}

static int mxt_get_object_table(struct mxt_data *data)
{
	int error;
	int i;
	u16 reg;
	u16 end_address;
	u8 reportid = 0;
	u8 buf[MXT_OBJECT_SIZE];

	trace("%s@%d\n", __func__, __LINE__ );

	data->mem_size = 0;

	for (i = 0; i < data->info.object_num; i++) {
		struct mxt_object *object = data->object_table + i;

		reg = MXT_OBJECT_START + MXT_OBJECT_SIZE * i;
		error = mxt_read_object_table(data->client, reg, buf);
		if (error)
			return error;

		object->type = buf[0];
		object->start_address = (buf[2] << 8) | buf[1];
		object->size = buf[3];
		object->instances = buf[4];
		object->num_report_ids = buf[5];

		if (object->type == MXT_GEN_MESSAGE_T5)
			data->T5_msg_size = object->size + 1;

		if (object->num_report_ids) {
			reportid += object->num_report_ids *
					(object->instances + 1);
			object->max_reportid = reportid;
		}

		end_address = object->start_address
			+ (object->size + 1) * (object->instances + 1) - 1;

		if (end_address >= data->mem_size)
			data->mem_size = end_address + 1;

	}

	return 0;
}

static int mxt_initialize(struct mxt_data *data)
{
	struct i2c_client *client = data->client;
	struct mxt_info *info = &data->info;
	int error;
	u8 val;

	trace("%s@%d\n", __func__, __LINE__ );

	error = mxt_get_info(data);
	if (error)
		return error;

	data->object_table = kcalloc(info->object_num,
				     sizeof(struct mxt_object),
				     GFP_KERNEL);
	if (!data->object_table) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		return -ENOMEM;
	}

	/* Get object table information */
	error = mxt_get_object_table(data);
	if (error)
		return error;

	/* Check register init values */
	error = mxt_check_reg_init(data);
	if (error)
		return error;

	mxt_handle_pdata(data);

	/* Backup to memory */
	mxt_write_object(data, MXT_GEN_COMMAND_T6,
			MXT_COMMAND_BACKUPNV,
			MXT_BACKUP_VALUE);
	msleep(MXT_BACKUP_TIME);

	/* Soft reset */
	mxt_write_object(data, MXT_GEN_COMMAND_T6,
			MXT_COMMAND_RESET, 1);
	msleep(MXT_RESET_TIME);

	/* Update matrix size at info struct */
	error = mxt_read_reg(client, MXT_MATRIX_X_SIZE, &val);
	if (error)
		return error;
	info->matrix_xsize = val;

	error = mxt_read_reg(client, MXT_MATRIX_Y_SIZE, &val);
	if (error)
		return error;
	info->matrix_ysize = val;

	error = mxt_debug_msg_init(data);
	if (error)
		return error;

	dev_info(&client->dev,
			"Family ID: %d Variant ID: %d Version: %d Build: %d\n",
			info->family_id, info->variant_id, info->version,
			info->build);

	dev_info(&client->dev,
			"Matrix X Size: %d Matrix Y Size: %d Object Num: %d\n",
			info->matrix_xsize, info->matrix_ysize,
			info->object_num);

	return 0;
}

static void mxt_calc_resolution(struct mxt_data *data)
{
	unsigned int max_x = data->pdata->x_size - 1;
	unsigned int max_y = data->pdata->y_size - 1;

	trace("%s@%d\n", __func__, __LINE__ );

	if (data->pdata->orient & MXT_XY_SWITCH) {
		data->max_x = max_y;
		data->max_y = max_x;
	} else {
		data->max_x = max_x;
		data->max_y = max_y;
	}
}

/*
 * Attribute functions for mxt_messages
 *  uses sysfs_notify(...);
 */
static ssize_t mxt_message_queue_show(struct device *dev,
                    struct device_attribute *attr, char *buf)
{
    struct mxt_data *data = dev_get_drvdata(dev);
    struct mxt_message msg;
    ssize_t count = -EWOULDBLOCK;

    if( mxt_message_queue_pop ( data->message_queue, &msg ) )
    {
        count = sizeof(msg);
        memcpy( buf, &msg, count);
    }

    return count;
}

static ssize_t mxt_message_active_store(struct device *dev,
                    struct device_attribute *attr,
                    const char *buf, size_t count)
{
    struct mxt_data *data = dev_get_drvdata(dev);

    if( count )
    {
        printk(KERN_DEBUG"%s@%d\n", __func__, __LINE__ );
        if( buf[0] == 0x30 )
            mxt_message_queue_set_active( data->message_queue, 0);
        else if( buf[0] == 0x31 )
            mxt_message_queue_set_active( data->message_queue, 1);
    } else
    {
        printk(KERN_DEBUG"%s@%d\n", __func__, __LINE__ );
    }

    return count;
}

static ssize_t mxt_object_show(struct device *dev,
				    struct device_attribute *attr, char *buf)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	struct mxt_object *object;
	int count = 0;
	int i, j;
	int error;
	u8 val;

	trace("%s@%d\n", __func__, __LINE__ );

	for (i = 0; i < data->info.object_num; i++) {
		object = data->object_table + i;

		count += snprintf(buf + count, PAGE_SIZE - count,
				"Object[%d] (Type %d)\n",
				i + 1, object->type);
		if ( count >= PAGE_SIZE )
			return PAGE_SIZE - 1;

		if (!mxt_object_readable(object->type)) {
			count += snprintf(buf + count, PAGE_SIZE - count,
					"\n");
			if ( count >= PAGE_SIZE )
				return PAGE_SIZE - 1;
			continue;
		}

		for (j = 0; j < object->size + 1; j++) {
			error = mxt_read_object(data,
						object->type, j, &val);
			if (error)
				return error;

			count += snprintf(buf + count, PAGE_SIZE - count,
					"\t[%2d]: %02x (%d)\n", j, val, val);
			if ( count >= PAGE_SIZE )
				return PAGE_SIZE - 1;
		}

		count += snprintf(buf + count, PAGE_SIZE - count, "\n");
		if ( count >= PAGE_SIZE )
			return PAGE_SIZE - 1;
	}

	return count;
}

static int mxt_load_fw(struct device *dev, const char *fn)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	struct i2c_client *client = data->client;
	const struct firmware *fw = NULL;
	unsigned int frame_size;
	unsigned int pos = 0;
	int ret;
	data->in_bootloader = true;

	trace("%s@%d\n", __func__, __LINE__ );

	ret = request_firmware(&fw, fn, dev);
	if (ret) {
		dev_err(dev, "Unable to open firmware %s\n", fn);
		return ret;
	}

	/* Change to the bootloader mode */
	mxt_write_object(data, MXT_GEN_COMMAND_T6,
			MXT_COMMAND_RESET, MXT_BOOT_VALUE);
	msleep(MXT_RESET_TIME);

	/* Change to slave address of bootloader */
	if (client->addr == MXT_APP_LOW)
		client->addr = MXT_BOOT_LOW;
	else
		client->addr = MXT_BOOT_HIGH;

	ret = mxt_check_bootloader(client, MXT_WAITING_BOOTLOAD_CMD);
	if (ret)
		goto out;

	/* Unlock bootloader */
	mxt_unlock_bootloader(client);

	while (pos < fw->size) {
		ret = mxt_check_bootloader(client,
						MXT_WAITING_FRAME_DATA);
		if (ret)
			goto out;

		frame_size = ((*(fw->data + pos) << 8) | *(fw->data + pos + 1));

		/* We should add 2 at frame size as the the firmware data is not
		 * included the CRC bytes.
		 */
		frame_size += 2;

		/* Write one frame to device */
		mxt_fw_write(client, fw->data + pos, frame_size);

		ret = mxt_check_bootloader(client,
						MXT_FRAME_CRC_PASS);
		if (ret)
			goto out;

		pos += frame_size;

		dev_dbg(dev, "Updated %d bytes / %zd bytes\n", pos, fw->size);
	}

out:
	release_firmware(fw);

	/* Change to slave address of application */
	if (client->addr == MXT_BOOT_LOW)
		client->addr = MXT_APP_LOW;
	else
		client->addr = MXT_APP_HIGH;

	data->in_bootloader = false;

	return ret;
}

static ssize_t mxt_update_fw_store(struct device *dev,
					struct device_attribute *attr,
					const char *buf, size_t count)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	int error;

	trace("%s@%d\n", __func__, __LINE__ );

	mxt_debug_msg_remove(data);

	disable_irq(data->irq);

	error = mxt_load_fw(dev, MXT_FW_NAME);
	if (error) {
		dev_err(dev, "The firmware update failed(%d)\n", error);
		count = error;
	} else {
		dev_dbg(dev, "The firmware update succeeded\n");

		/* Wait for reset */
		msleep(MXT_FWRESET_TIME);

		kfree(data->object_table);
		data->object_table = NULL;

		mxt_initialize(data);
	}

	enable_irq(data->irq);

	error = mxt_make_highchg(data);
	if (error)
		return error;

	return count;
}

static ssize_t mxt_debug_notify_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "0\n");
}

static ssize_t mxt_debug_v2_enable_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	u8 i;
	ssize_t ret;

	i = simple_strtoul(buf, NULL, 0);
	if (i < 2) {
		if (i == 1)
			mxt_debug_msg_enable(data);
		else
			mxt_debug_msg_disable(data);

		ret = count;
	} else {
		dev_dbg(dev, "debug_enabled write error\n");
		ret = -EINVAL;
	}

	return ret;
}

static ssize_t mxt_debug_enable_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	char c;

	c = data->debug_enabled ? '1' : '0';
	return scnprintf(buf, PAGE_SIZE, "%c\n", c);
}

static ssize_t mxt_debug_enable_store(struct device *dev,
	struct device_attribute *attr, const char *buf,	size_t count)
{
	struct mxt_data *data = dev_get_drvdata(dev);
	u8 i;
	ssize_t ret;

	i = simple_strtoul(buf, NULL, 0);
	if (i < 2) {
		data->debug_enabled = (i == 1);

		dev_dbg(dev, "%s\n", i ? "debug enabled" : "debug disabled");
		ret = count;
	} else {
		dev_dbg(dev, "debug_enabled write error\n");
		ret = -EINVAL;
	}

	return ret;
}

/* Firmware Version is returned as Major.Minor.Build */
static ssize_t mxt_fw_version_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	struct mxt_data *data = dev_get_drvdata(dev);

	return scnprintf(buf, PAGE_SIZE, "%u.%u.%02X\n",
			 data->info.version >> 4, data->info.version & 0xf,
			 data->info.build);
}

/* Hardware Version is returned as FamilyID.VariantID */
static ssize_t mxt_hw_version_show(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	struct mxt_data *data = dev_get_drvdata(dev);

	return scnprintf(buf, PAGE_SIZE, "%u.%u\n",
			data->info.family_id, data->info.variant_id);
}

static DEVICE_ATTR(object, 0444, mxt_object_show, NULL);
static DEVICE_ATTR(update_fw, 0664, NULL, mxt_update_fw_store);
static DEVICE_ATTR(message_queue, 0777, mxt_message_queue_show, 0);
static DEVICE_ATTR(message_active, 0777, 0, mxt_message_active_store);
static DEVICE_ATTR(debug_enable, S_IWUSR | S_IRUSR, mxt_debug_enable_show,
	mxt_debug_enable_store);

static DEVICE_ATTR(debug_v2_enable, S_IWUSR | S_IRUSR, NULL,
		   mxt_debug_v2_enable_store);
static DEVICE_ATTR(debug_notify, S_IRUGO, mxt_debug_notify_show, NULL);
static DEVICE_ATTR(fw_version, S_IRUGO, mxt_fw_version_show, NULL);
static DEVICE_ATTR(hw_version, S_IRUGO, mxt_hw_version_show, NULL);

static struct attribute *mxt_attrs[] = {
	&dev_attr_debug_enable.attr,
	&dev_attr_object.attr,
	&dev_attr_update_fw.attr,
	&dev_attr_message_queue.attr,
    &dev_attr_message_active.attr,
	&dev_attr_debug_v2_enable.attr,
	&dev_attr_debug_notify.attr,
	&dev_attr_fw_version.attr,
	&dev_attr_hw_version.attr,
	NULL
};

static const struct attribute_group mxt_attr_group = {
	.attrs = mxt_attrs,
};

static u16 mxt_get_debug_value(struct mxt_data *data, unsigned int x,
				unsigned int y)
{
	struct mxt_info *info = &data->info;
	unsigned int ofs, page;
	unsigned int stripe;
	unsigned int stripe_width;

	trace("%s@%d\n", __func__, __LINE__);

	if (info->family_id == MXT_FAMILY_1386) {
		stripe_width = info->matrix_ysize / MXT1386_STRIPES;
		stripe = y / stripe_width;
		y = y % stripe_width;
	} else {
		stripe = 0;
		stripe_width = info->matrix_ysize;
	}

	ofs = (y + (x * stripe_width)) * sizeof(u16);
	page = ofs / MXT_DIAGNOSTIC_SIZE;
	ofs %= MXT_DIAGNOSTIC_SIZE;

	if (info->family_id == MXT_FAMILY_1386)
		page += stripe * MXT1386_PAGES_PER_STRIPE;

	return get_unaligned_le16(&data->t37_buf[page].data[ofs]);
}

static void mxt_convert_debug_pages(struct mxt_data *data)
{
	const struct mxt_platform_data *pd = data->pdata;
	unsigned int x = 0;
	unsigned int y = 0;
	unsigned int i, rx, ry;
	bool flip = (pd->orient & MXT_XY_SWITCH);

	trace("%s@%d\n", __func__, __LINE__);

	for (i = 0; i < data->t37_nodes; i++) {
		/* Handle orientation */
		rx = flip ? y : x;
		ry = flip ? x : y;
		rx = (pd->orient & MXT_X_INVERT) ? (pd->x_line - 1 - rx) : rx;
		ry = (pd->orient & MXT_Y_INVERT) ? (pd->y_line - 1 - ry) : ry;

		data->debug_buf[i] = mxt_get_debug_value(data, rx, ry);

		/* Next value */
		if (++x >= (flip ? pd->y_line : pd->x_line)) {
			x = 0;
			y++;
		}
	}
}

static int mxt_read_debug(struct mxt_data *data, u8 mode)
{
	int retries = 0;
	int page;
	int ret;
	u8 cmd = mode;
	struct t37_debug *p;

	trace("%s@%d\n", __func__, __LINE__);

	for (page = 0; page < data->t37_pages; page++) {
		p = data->t37_buf + page;

		ret = mxt_write_reg(data->client, data->diag_cmd_address,
				    cmd);
		if (ret)
			return ret;

		retries = 0;

		/* Poll until command is actioned */
		msleep(2);
wait_cmd:
		/* Read first two bytes only */
		ret = __mxt_read_reg(data->client, data->t37_address,
				     2, p);
		if (ret)
			return ret;

		if ((p->mode != mode) || (p->page != page)) {
			if (retries++ > 100)
				return -EINVAL;

			msleep(1);
			goto wait_cmd;
		}

		/* Read entire T37 page */
		ret = __mxt_read_reg(data->client, data->t37_address,
				sizeof(struct t37_debug), p);
		if (ret)
			return ret;

		dev_dbg(&data->client->dev, "%s page:%d retries:%d\n",
			__func__, page, retries);

		/* For remaining pages, write PAGEUP rather than mode */
		cmd = MXT_DIAGNOSTIC_PAGEUP;
	}

	mxt_convert_debug_pages(data);

	return 0;
}

static int mxt_open_refs(struct inode *inode, struct file *file)
{
	struct mxt_data *data = inode->i_private;

	trace("%s@%d\n", __func__, __LINE__);

	file->private_data = data;

	return mxt_read_debug(data, MXT_DIAGNOSTIC_REFS);
}

static ssize_t mxt_read_refs(struct file *file, char __user *ubuf,
			     size_t count, loff_t *offp)
{
	struct mxt_data *data = file->private_data;

	trace("%s@%d\n", __func__, __LINE__);

	return simple_read_from_buffer(ubuf, count, offp,
			data->debug_buf,
			data->t37_nodes * sizeof(u16));
}

static const struct file_operations atmel_mxt_refs_fops = {
	.open = mxt_open_refs,
	.read = mxt_read_refs,
};

static int mxt_open_deltas(struct inode *inode, struct file *file)
{
	struct mxt_data *data = inode->i_private;

	trace("%s@%d\n", __func__, __LINE__);

	file->private_data = data;

	return mxt_read_debug(data, MXT_DIAGNOSTIC_DELTAS);
}

static ssize_t mxt_read_deltas(struct file *file, char __user *ubuf,
			       size_t count, loff_t *offp)
{
	struct mxt_data *data = file->private_data;

	trace("%s@%d\n", __func__, __LINE__);

	return simple_read_from_buffer(ubuf, count, offp,
			data->debug_buf,
			data->t37_nodes * sizeof(u16));
}

static const struct file_operations atmel_mxt_deltas_fops = {
	.open = mxt_open_deltas,
	.read = mxt_read_deltas,
};

static void mxt_debugfs_init(struct mxt_data *data)
{
	const struct mxt_platform_data *pd = data->pdata;
	struct mxt_object *object;

	trace("%s@%d\n", __func__, __LINE__);

	object = mxt_get_object(data, MXT_GEN_COMMAND_T6);
	if (!object)
		return;

	data->diag_cmd_address = object->start_address + MXT_COMMAND_DIAGNOSTIC;

	object = mxt_get_object(data, MXT_DEBUG_DIAGNOSTIC_T37);
	if (!object)
		return;

	if ((object->size + 1) != sizeof(struct t37_debug)) {
		dev_warn(&data->client->dev, "Bad T37 size");
		return;
	}

	data->t37_address = object->start_address;

	data->debugfs_dir =
		debugfs_create_dir(dev_driver_string(&data->client->dev), NULL);
	if (!data->debugfs_dir)
		return;

	data->deltas_file = debugfs_create_file("deltas", S_IRUGO,
						data->debugfs_dir, data,
						&atmel_mxt_deltas_fops);
	if (!data->deltas_file)
		goto error;


	data->refs_file = debugfs_create_file("refs", S_IRUGO,
						data->debugfs_dir, data,
						&atmel_mxt_refs_fops);
	if (!data->refs_file)
		goto refs_error;

	/* Calculate size of file and allocate buffer */
	data->t37_nodes = pd->x_line * pd->y_line;
	data->deltas_file->d_inode->i_size = data->t37_nodes * sizeof(u16);
	data->refs_file->d_inode->i_size = data->t37_nodes * sizeof(u16);

	if (data->info.family_id == MXT_FAMILY_1386)
		data->t37_pages = MXT1386_STRIPES * MXT1386_PAGES_PER_STRIPE;
	else
		/* Calculate number of pages to retrieve */
		data->t37_pages = ((pd->x_line * data->info.matrix_ysize)
				   * sizeof(u16) / sizeof(data->t37_buf->data))
				   + 1;

	data->debug_buf = devm_kzalloc(&data->client->dev,
				     data->t37_nodes * sizeof(u16),
				     GFP_KERNEL);
	if (!data->debug_buf)
		goto alloc_error;

	data->t37_buf = devm_kzalloc(&data->client->dev,
				     sizeof(struct t37_debug) * data->t37_pages,
				     GFP_KERNEL);
	if (!data->t37_buf)
		goto alloc_error;

	return;

alloc_error:
	debugfs_remove(data->refs_file);
	data->refs_file = NULL;
refs_error:
	debugfs_remove(data->deltas_file);
	data->deltas_file = NULL;
error:
	debugfs_remove(data->debugfs_dir);
	data->debugfs_dir = NULL;
}

static void mxt_debugfs_remove(struct mxt_data *data)
{
	trace("%s@%d\n", __func__, __LINE__);

	debugfs_remove_recursive(data->debugfs_dir);
}

static void mxt_start(struct mxt_data *data)
{
	trace("%s@%d\n", __func__, __LINE__ );

	/* Touch enable */
	mxt_write_object(data,
			MXT_TOUCH_MULTI_T9, MXT_TOUCH_CTRL, 0x83);
}

static void mxt_stop(struct mxt_data *data)
{
	trace("%s@%d\n", __func__, __LINE__ );

	/* Touch disable */
	mxt_write_object(data,
			MXT_TOUCH_MULTI_T9, MXT_TOUCH_CTRL, 0);
}

static int mxt_input_open(struct input_dev *dev)
{
	struct mxt_data *data = input_get_drvdata(dev);

	trace("%s@%d\n", __func__, __LINE__ );

	mxt_start(data);

	return 0;
}

static void mxt_input_close(struct input_dev *dev)
{
	struct mxt_data *data = input_get_drvdata(dev);

	trace("%s@%d\n", __func__, __LINE__ );

	mxt_stop(data);
}

static int mxt_check_mem_access_params(struct mxt_data *data, loff_t off,
	size_t *count)
{
	if (data->in_bootloader)
		return -EINVAL;

	if (off >= data->mem_size)
		return -EIO;

	if (off + *count > data->mem_size)
		*count = data->mem_size - off;

	if (*count > MXT_MAX_BLOCK_WRITE)
		*count = MXT_MAX_BLOCK_WRITE;

	return 0;
}

static ssize_t mxt_mem_access_read(struct file *filp, struct kobject *kobj,
	struct bin_attribute *bin_attr, char *buf, loff_t off, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct mxt_data *data = dev_get_drvdata(dev);
	int ret = 0;

	ret = mxt_check_mem_access_params(data, off, &count);
	if (ret < 0)
		return ret;

	if (count > 0)
		ret = __mxt_read_reg(data->client, off, count, buf);

	return ret == 0 ? count : ret;
}

static ssize_t mxt_mem_access_write(struct file *filp, struct kobject *kobj,
	struct bin_attribute *bin_attr, char *buf, loff_t off, size_t count)
{
	struct device *dev = container_of(kobj, struct device, kobj);
	struct mxt_data *data = dev_get_drvdata(dev);
	int ret = 0;

	ret = mxt_check_mem_access_params(data, off, &count);
	if (ret < 0)
		return ret;

	if (count > 0)
		ret = __mxt_write_reg(data->client, off, count,	buf);

	return ret == 0 ? count : ret;
}

static int __devinit mxt_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	const struct mxt_platform_data *pdata = client->dev.platform_data;
	struct mxt_data *data;
	struct input_dev *input_dev;
	int error;

	trace("%s@%d\n", __func__, __LINE__ );

	if (!pdata)
		return -EINVAL;

	data = kzalloc(sizeof(struct mxt_data), GFP_KERNEL);
	input_dev = input_allocate_device();
	if (!data || !input_dev) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		error = -ENOMEM;
		goto err_free_mem;
	}

    data->message_queue = mxt_message_queue_create();
    if ( !data->message_queue )
        goto err_message_queue;

	input_dev->name = "Atmel maXTouch Touchscreen";
	input_dev->id.bustype = BUS_I2C;
	input_dev->dev.parent = &client->dev;
	input_dev->open = mxt_input_open;
	input_dev->close = mxt_input_close;

	data->client = client;
	data->input_dev = input_dev;
	data->pdata = pdata;
	data->irq = client->irq;

	mxt_calc_resolution(data);

	__set_bit(EV_ABS, input_dev->evbit);
	__set_bit(EV_KEY, input_dev->evbit);
	__set_bit(BTN_TOUCH, input_dev->keybit);

	/* For single touch */
	input_set_abs_params(input_dev, ABS_X,
			     0, data->max_x, 0, 0);
	input_set_abs_params(input_dev, ABS_Y,
			     0, data->max_y, 0, 0);
	input_set_abs_params(input_dev, ABS_PRESSURE,
			     0, 255, 0, 0);

	/* For multi touch */
#ifdef CONFIG_ATMEL_MULTITOUCH_SUPPORT
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,38)
	input_mt_init_slots(input_dev, MXT_MAX_FINGER);
#endif
	input_set_abs_params(input_dev, ABS_MT_TOUCH_MAJOR,
			     0, MXT_MAX_AREA, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_X,
			     0, data->max_x, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_Y,
			     0, data->max_y, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_PRESSURE,
			     0, 255, 0, 0);
#endif

	input_set_drvdata(input_dev, data);
	i2c_set_clientdata(client, data);

	error = mxt_initialize(data);
#ifndef NOFAIL
	if (error)
		goto err_free_object;
#endif

	error = request_threaded_irq(client->irq, NULL, mxt_interrupt,
			pdata->irqflags, client->dev.driver->name, data);
	if (error) {
		dev_err(&client->dev, "Failed to register interrupt\n");
		goto err_free_object;
	}

	error = mxt_make_highchg(data);
#ifndef NOFAIL		
	if (error)
		goto err_free_irq;
#endif

	error = input_register_device(input_dev);
	if (error)
		goto err_free_irq;

	mutex_init(&data->debug_msg_lock);

	error = sysfs_create_group(&client->dev.kobj, &mxt_attr_group);
	if (error)
		goto err_unregister_device;

	sysfs_bin_attr_init(&data->mem_access_attr);
	data->mem_access_attr.attr.name = "mem_access";
	data->mem_access_attr.attr.mode = S_IRUGO | S_IWUSR;
	data->mem_access_attr.read = mxt_mem_access_read;
	data->mem_access_attr.write = mxt_mem_access_write;
	data->mem_access_attr.size = data->mem_size;

	if (sysfs_create_bin_file(&client->dev.kobj,
				  &data->mem_access_attr) < 0) {
			dev_err(&client->dev, "Failed to create	%s\n",
				data->mem_access_attr.attr.name);
			goto err_remove_sysfs_group;
	}

	if ((data->info.family_id == MXT_FAMILY_224)
	    || (data->info.family_id == MXT_FAMILY_1386))
		mxt_debugfs_init(data);

	private_data = data;
	return 0;

err_remove_sysfs_group:
	sysfs_remove_group(&client->dev.kobj, &mxt_attr_group);
err_unregister_device:
	input_unregister_device(input_dev);
	input_dev = NULL;
err_free_irq:
	free_irq(client->irq, data);
err_free_object:
	kfree(data->object_table);
err_message_queue:
    mxt_message_queue_delete( data->message_queue );
err_free_mem:
	input_free_device(input_dev);
	kfree(data);
	return error;
}

static int __devexit mxt_remove(struct i2c_client *client)
{
	struct mxt_data *data = i2c_get_clientdata(client);

	trace("%s@%d\n", __func__, __LINE__ );

	mxt_debugfs_remove(data);

	if (data->mem_access_attr.attr.name)
		sysfs_remove_bin_file(&client->dev.kobj,
			&data->mem_access_attr);

	sysfs_remove_group(&client->dev.kobj, &mxt_attr_group);
	free_irq(data->irq, data);
	input_unregister_device(data->input_dev);
    mxt_message_queue_delete( data->message_queue );
	kfree(data->object_table);
	mxt_debug_msg_remove(data);
	kfree(data);

	return 0;
}

#ifdef CONFIG_PM
static int mxt_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mxt_data *data = i2c_get_clientdata(client);
	struct input_dev *input_dev = data->input_dev;

	trace("%s@%d\n", __func__, __LINE__ );

	mutex_lock(&input_dev->mutex);

	if (input_dev->users)
		mxt_stop(data);

	mutex_unlock(&input_dev->mutex);

	return 0;
}

static int mxt_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mxt_data *data = i2c_get_clientdata(client);
	struct input_dev *input_dev = data->input_dev;

	trace("%s@%d\n", __func__, __LINE__ );

	/* Soft reset */
	mxt_write_object(data, MXT_GEN_COMMAND_T6,
			MXT_COMMAND_RESET, 1);

	msleep(MXT_RESET_TIME);

	mutex_lock(&input_dev->mutex);

	if (input_dev->users)
		mxt_start(data);

	mutex_unlock(&input_dev->mutex);

	return 0;
}

static const struct dev_pm_ops mxt_pm_ops = {
	.suspend	= mxt_suspend,
	.resume		= mxt_resume,
};
#endif

static int mxt_ioctl(struct inode *inode, struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
	int retval = 0;
	struct mxt_data *data = private_data;

	trace("%s@%d\n", __func__, __LINE__ );

	if ( private_data == NULL )
		return -ENODEV;

	switch ( ioctl_num ) {
	case TSIOCGDEVINFO: {
		copy_to_user((mxt_info_t *)ioctl_param, &data->info, sizeof(mxt_info_t));
		break;
	}
	case TSIOCGOBJENTRY: { 	// get object entry
		tsObjectTableRequest_t *tor = (tsObjectTableRequest_t *)ioctl_param;
		struct mxt_object *object = data->object_table + tor->num;

		copy_to_user( &tor->entry, object, sizeof(mxt_object_t));
		break;
	}
	case TSIOCGOBJDATA: {	// get object data by instance
		tsObjectDataRequest_t *tdr = (tsObjectDataRequest_t *)ioctl_param;
		struct mxt_object *object = data->object_table + tdr->num + tdr->instance;

		if (!mxt_object_readable(object->type)) {
			printk(KERN_DEBUG"object type %d not readable\n",object->type);
			retval = -EINVAL;
			break;
		}

		/*
		 * we're reading the entire object in one pass, rather than byte walking the registers
		 * 	Byte reading introduces noise
		 */
		{
			u8 objectData[256];
			u8 objectSize;

			objectSize = object->size + 1;

			if( sizeof(objectData) > objectSize )
			{
				retval = __mxt_read_reg(data->client, object->start_address, objectSize, objectData);
				if (retval) {
					printk(KERN_DEBUG" TSIOCGOBJDATA::__mxt_read_reg failed with error %d\n",retval);
					break;
				}

				copy_to_user( tdr->data, objectData, objectSize );
			}
			else
			{
				printk(KERN_DEBUG"TSIOCGOBJDATA:: not enough storage space for object: %d - %d\n", sizeof(objectData), objectSize );
			}
		}
		break;
	}
	case TSIOCSOBJDATA: {	// set object data by instance
		tsObjectDataRequest_t *tdr = (tsObjectDataRequest_t *)ioctl_param;
		struct mxt_object *object = data->object_table + tdr->num + tdr->instance;

		if (!mxt_object_writable(object->type)) {
			printk(KERN_DEBUG"object type %d not writable\n",object->type);
			retval = -EINVAL;
			break;
		}

		{
			u8 objectData[256];
			u8 objectSize;

			objectSize = object->size + 1;
			if( sizeof(objectData) > objectSize )
			{
				copy_from_user( objectData, tdr->data, objectSize  );

				retval = __mxt_write_reg(data->client, object->start_address, objectSize, objectData);
				if (retval) {
					printk(KERN_DEBUG"mxt_write_object failed with error %d\n",retval);
					break;
				}
			}
		}
		break;
	}
	case TSIOCWRNVRAM:	// write NVRAM
	{
	    //sysfs_notify(&data->client->dev->kobj, 0, "message");

/*
	    mxt_message_queue * msg_q;

	    msg_q = mxt_message_queue_create();
	    if( msg_q )
	    {
	        int i ;

            printk(KERN_DEBUG"mxt_message_queue_is_empty: %d\n",mxt_message_queue_is_empty(msg_q));

            for( i = 1; i < 32; ++i )
            {
                struct mxt_message msg;
                memset(&msg, 0x00, sizeof(struct mxt_message ));
                msg.checksum = i;

                mxt_message_queue_push(msg_q, &msg);
            }

            printk(KERN_DEBUG"mxt_message_queue_size: %d\n",mxt_message_queue_size(msg_q));

            {
                struct mxt_message msg;
                if( mxt_message_queue_pop( msg_q, &msg) )
                {
                    printk(KERN_DEBUG"checksum: %d\n", msg.checksum );
                }
            }

            {
                struct mxt_message msg;
                if( mxt_message_queue_pop( msg_q, &msg) )
                {
                    printk(KERN_DEBUG"checksum: %d\n", msg.checksum );
                }
            }

            printk(KERN_DEBUG"mxt_message_queue_size: %d\n",mxt_message_queue_size(msg_q));

            mxt_message_queue_clear(msg_q);

            printk(KERN_DEBUG"mxt_message_queue_size: %d\n",mxt_message_queue_size(msg_q));

            mxt_message_queue_delete(msg_q);
	    }
*/
	}
	    break;
	default:
            printk(KERN_DEBUG"%s@%d: unsupported ioctl %04x\n", __func__, __LINE__, ioctl_num );
		break;
	}
	return retval;
}

static int mxt_release(struct inode *inode, struct file *file)
{
	// needed for close() call
	return 0;
}


static const struct file_operations mxt_fops = {
	.ioctl = mxt_ioctl,
//	.read = mxt_read,
//	.write = mxt_write,
	.open = nonseekable_open,
	.release = mxt_release,
};

static struct miscdevice mxt_miscdev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "touchscreenic",
	.fops = &mxt_fops,
};


static const struct i2c_device_id mxt_id[] = {
	{ "qt602240_ts", 0 },
	{ "atmel_mxt_ts", 0 },
	{ "mXT224", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, mxt_id);

static struct i2c_driver mxt_driver = {
	.driver = {
		.name	= "atmel_mxt_ts",
		.owner	= THIS_MODULE,
#ifdef CONFIG_PM
		.pm	= &mxt_pm_ops,
#endif
	},
	.probe		= mxt_probe,
	.remove		= __devexit_p(mxt_remove),
	.id_table	= mxt_id,
};

static int __init mxt_init(void)
{
	misc_register(&mxt_miscdev);
	return i2c_add_driver(&mxt_driver);
}

static void __exit mxt_exit(void)
{
	i2c_del_driver(&mxt_driver);
	misc_deregister(&mxt_miscdev);
}

module_init(mxt_init);
module_exit(mxt_exit);

/* Module information */
MODULE_AUTHOR("Joonyoung Shim <jy0922.shim@samsung.com>");
MODULE_DESCRIPTION("Atmel maXTouch Touchscreen driver");
MODULE_LICENSE("GPL");
