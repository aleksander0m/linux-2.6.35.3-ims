/*
 * net/dsa/mv88e6xxx.h - Marvell 88e6xxx switch chip support
 * Copyright (c) 2008 Marvell Semiconductor
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __MV88E6XXX_H
#define __MV88E6XXX_H

#define REG_PORT(p)		(0x10 + (p))
#define REG_GLOBAL		0x1b
#define REG_GLOBAL2		0x1c

#define MV88E6XXX_MAX_PORTS	11

struct mv88e6xxx_priv_state {
	/*
	 * This mutex protects EEPROM accesses against all other
	 * MDIO accesses, and when using multi-chip addressing, this
	 * mutex protects access to the indirect access registers.
	 */
	struct mutex	smi_mutex;
	struct task_struct *smi_mutex_owner;
	int		smi_mutex_depth;

#ifdef CONFIG_NET_DSA_MV88E6XXX_NEED_PPU
	/*
	 * Handles automatic disabling and re-enabling of the PHY
	 * polling unit.
	 */
	struct mutex		ppu_mutex;
	int			ppu_disabled;
	struct work_struct	ppu_work;
	struct timer_list	ppu_timer;
#endif

	/*
	 * This mutex serialises access to the GLOBAL2 PHY
	 * indirect access registers.
	 */
	struct mutex	phy_indirect_mutex;

	/*
	 * This mutex serialises access to the statistics unit.
	 * Hold this mutex over snapshot + dump sequences.
	 */
	struct mutex	stats_mutex;

	/*
	 * This mutex serialises access to the address translation
	 * unit.
	 */
	struct mutex	atu_mutex;

	/*
	 * Serialises access to PHY paged registers, and cache the
	 * current PHY page number.
	 */
	struct mutex	phy_page_mutex[MV88E6XXX_MAX_PORTS];
	int		phy_page[MV88E6XXX_MAX_PORTS];

	/*
	 * This mutex serialises access to the temperature sensor.

	 */
	struct mutex	temp_mutex;

	/*
	 * Enable MDIO access debugging.
	 */
	int		mdio_debug;

	/*
	 * STP graceful restart.
	 */
	int		map_forwarding[MV88E6XXX_MAX_PORTS];
	struct timer_list	map_forwarding_timeout;

	/*
	 * Hardware bridging state.
	 */
	spinlock_t	hw_bridge_state;
	struct work_struct	hw_bridge_work;
	void		*bridge[MV88E6XXX_MAX_PORTS];
	int		fid_dirty[MV88E6XXX_MAX_PORTS];
	int		fid[MV88E6XXX_MAX_PORTS];
	int		stp_state_dirty[MV88E6XXX_MAX_PORTS];
	int		stp_state[MV88E6XXX_MAX_PORTS];

	/*
	 * Hardware bridging state.
	 */
	struct work_struct	atu_flush_work;

	/*
	 * EEPROM-related state.  Accesses to the EEPROM
	 * interface are protected by the smi mutex.
	 */
	int		eeprom_length;

	/*
	 * This mutex serialises access to the AVB registers.
	 */
	struct mutex	avb_mutex;

	/*
	 * VCT testing state.
	 */
	struct mutex	vct_mutex;
	struct workqueue_struct	*vct_wq;
	struct work_struct	vct_work;
	int		vct_tests_scheduled;
	int		vct_value[MV88E6XXX_MAX_PORTS];
        
	/*
	 * For sysfs calls
	 */
	struct device *device;

	/*
	 * IRQ handling state.
	 */
	int		irq_in_use;
	int		irq_count;
};

struct mv88e6xxx_hw_stat {
	char string[ETH_GSTRING_LEN];
	int sizeof_stat;
	int reg;
};

void mv88e6xxx_smi_lock(struct dsa_switch *ds);
void mv88e6xxx_smi_unlock(struct dsa_switch *ds);
int __mv88e6xxx_reg_read(struct mii_bus *bus, int sw_addr, int addr, int reg);
int mv88e6xxx_reg_read(struct dsa_switch *ds, int addr, int reg);
int __mv88e6xxx_reg_write(struct mii_bus *bus, int sw_addr, int addr,
                          int reg, u16 val);
int mv88e6xxx_reg_write(struct dsa_switch *ds, int addr, int reg, u16 val);
int mv88e6xxx_config_prio(struct dsa_switch *ds);
int mv88e6xxx_set_addr_direct(struct dsa_switch *ds, u8 *addr);
int mv88e6xxx_set_addr_indirect(struct dsa_switch *ds, u8 *addr);
int mv88e6xxx_phy_read(struct dsa_switch *ds, int addr, int regnum);
int mv88e6xxx_phy_write(struct dsa_switch *ds, int addr, int regnum, u16 val);
void mv88e6xxx_ppu_state_init(struct dsa_switch *ds);
int mv88e6xxx_phy_read_ppu(struct dsa_switch *ds, int addr, int regnum);
int mv88e6xxx_phy_write_ppu(struct dsa_switch *ds, int addr,
			    int regnum, u16 val);
int mv88e6xxx_phy_read_indirect(struct dsa_switch *ds, int addr, int regnum);
int mv88e6xxx_phy_write_indirect(struct dsa_switch *ds, int addr,
				 int regnum, u16 val);
void mv88e6xxx_poll_link(struct dsa_switch *ds, int master_up);
int mv88e6xxx_get_settings(struct dsa_switch *ds, int port,
			   struct ethtool_cmd *cmd);
void mv88e6xxx_get_strings(struct dsa_switch *ds,
			   int nr_stats, struct mv88e6xxx_hw_stat *stats,
			   int port, uint8_t *data);
void mv88e6xxx_get_ethtool_stats(struct dsa_switch *ds,
				 int nr_stats, struct mv88e6xxx_hw_stat *stats,
				 int port, uint64_t *data);
int mv88e6xxx_reset_stats(struct dsa_switch *ds);

#define REG_READ(addr, reg)						\
	({								\
		int __ret;						\
									\
		__ret = mv88e6xxx_reg_read(ds, addr, reg);		\
		if (__ret < 0)						\
			return __ret;					\
		__ret;							\
	})

#define REG_WRITE(addr, reg, val)					\
	({								\
		int __ret;						\
									\
		__ret = mv88e6xxx_reg_write(ds, addr, reg, val);	\
		if (__ret < 0)						\
			return __ret;					\
	})



#endif
