#ifndef __BPDU_H
#define __BPDU_H

#include <linux/if_ether.h>

#define BPDU_PROTOCOL_ID          0
#define BPDU_PROTOCOL_VERSION     0
#define BPDU_CNTL                 0x03
#define BPDU_TYPE                 0xDE
#define BPDU_DSAP_SSAP            0x42

#define CONNECTIONINFOCMD_ID      0xA1
#define SETIPCMD_ID               0xA2
#define POLLCMD_ID	          0xA3

#define CONNECTIONINFOACK_ID      0x51
#define EXCHANGEINFO_ID           0x52
#define SETIPACK_ID               0x53
#define POLLACK_ID                0x54
#define MULTICONNECTIONINFOACK_ID 0x55

#define DOWNSTREAMPORTLEN 2

typedef struct bpdu_ethheader_TYPE {
	unsigned char  destAddress[ETH_ALEN] __attribute__ ((packed));
	unsigned char  srcAddress[ETH_ALEN]  __attribute__ ((packed));
	unsigned short length_proto          __attribute__ ((packed));
	unsigned char  DSAP                  __attribute__ ((packed));
	unsigned char  SSAP                  __attribute__ ((packed));
	unsigned char  cntl                  __attribute__ ((packed));
} bpdu_ethheader_TYPE;

typedef struct bpdu_header_TYPE {
	bpdu_ethheader_TYPE ethhdr __attribute__ ((packed));
        unsigned short protocol_id    __attribute__ ((packed));
        unsigned char  protocol_vid   __attribute__ ((packed));
        unsigned char  bpdu_type      __attribute__ ((packed));
        unsigned char  command_type   __attribute__ ((packed));
} bpdu_header_TYPE;

typedef struct bpdu_connectionInfoCmd_TYPE {
	bpdu_header_TYPE hdr               __attribute__ ((packed));
	unsigned char serverAddress[ETH_ALEN] __attribute__ ((packed));
	unsigned char info_only               __attribute__ ((packed));
} bpdu_connectionInfoCmd_TYPE;

typedef struct bpdu_connectionInfoAck_TYPE {
        bpdu_header_TYPE hdr                __attribute__ ((packed));
        unsigned char macAddress[ETH_ALEN]     __attribute__ ((packed));
        unsigned char prevMacAddress[ETH_ALEN] __attribute__ ((packed));
        unsigned char nextMacAddress[ETH_ALEN] __attribute__ ((packed));
	unsigned int  ipAddress                __attribute__ ((packed));
        unsigned int  gatewayAddress           __attribute__ ((packed));
        unsigned int  dnsAddress               __attribute__ ((packed));
        unsigned int  netMask                  __attribute__ ((packed));
        char          serialno[24]             __attribute__ ((packed));
        unsigned int  lruType                  __attribute__ ((packed)); 
        unsigned int  lruSubType               __attribute__ ((packed));
} bpdu_connectionInfoAck_TYPE;

typedef struct bpdu_multiPortConnectionInfoAck_TYPE {
        bpdu_header_TYPE hdr                __attribute__ ((packed));
        unsigned char macAddress[ETH_ALEN]     __attribute__ ((packed));
	unsigned int  ipAddress                __attribute__ ((packed));
        unsigned int  gatewayAddress           __attribute__ ((packed));
        unsigned int  dnsAddress               __attribute__ ((packed));
        unsigned int  netMask                  __attribute__ ((packed));
        char          serialno[24]             __attribute__ ((packed));
        unsigned int  lruType                  __attribute__ ((packed)); 
        unsigned int  lruSubType               __attribute__ ((packed));
        unsigned char downstreamPortLen        __attribute__ ((packed));
        unsigned char upstreamESA[ETH_ALEN]    __attribute__ ((packed));
        unsigned char downstreamESA[DOWNSTREAMPORTLEN][ETH_ALEN]   __attribute__ ((packed));
} bpdu_multiPortConnectionInfoAck_TYPE;

typedef struct bpdu_exchangeInfo_TYPE {
	bpdu_header_TYPE hdr            __attribute__ ((packed));
	unsigned char macAddress[ETH_ALEN] __attribute__ ((packed));
} bpdu_exchangeInfo_TYPE;

typedef struct bpdu_setIpCmd_TYPE {
	bpdu_header_TYPE hdr     __attribute__ ((packed));
	unsigned int ipAddress      __attribute__ ((packed));
	unsigned int gatewayAddress __attribute__ ((packed));
	unsigned int dnsAddress     __attribute__ ((packed));
	unsigned int netMask        __attribute__ ((packed));
} bpdu_setIpCmd_TYPE;

typedef struct bpdu_setIpAck_TYPE {
        bpdu_header_TYPE hdr __attribute__ ((packed));
        unsigned int ipAddress  __attribute__ ((packed));
} bpdu_setIpAck_TYPE;

typedef struct bpdu_pollAck_TYPE {
	bpdu_header_TYPE hdr             __attribute__ ((packed));
	unsigned char macAddress[ETH_ALEN]  __attribute__ ((packed));
	unsigned int  ipAddress             __attribute__ ((packed));
	unsigned int  gatewayAddress        __attribute__ ((packed));
	unsigned int  dnsAddress            __attribute__ ((packed));
	unsigned int  netMask               __attribute__ ((packed));
} bpdu_pollAck_TYPE;

typedef union {
	bpdu_header_TYPE bpdu_hdr                    __attribute__ ((packed));
	bpdu_connectionInfoCmd_TYPE  connectionInfoCmd  __attribute__ ((packed));
	bpdu_connectionInfoAck_TYPE  connectionInfoAck  __attribute__ ((packed));
	bpdu_multiPortConnectionInfoAck_TYPE mpConnectionInfoAck __attribute__ ((packed));
	bpdu_exchangeInfo_TYPE       exchangeInfo       __attribute__ ((packed));
	bpdu_setIpCmd_TYPE 	        setIpCmd           __attribute__ ((packed));
	bpdu_setIpAck_TYPE           setIpAck           __attribute__ ((packed));
        bpdu_header_TYPE 	        pollCmd            __attribute__ ((packed));
        bpdu_pollAck_TYPE            pollAck            __attribute__ ((packed));
} bpdu_TYPE;

#endif /* ifdef __BPDU_H */

