#ifndef __MARVELLSWITCH_PLATFORM_H
#define __MARVELLSWITCH_PLATFORM_H

struct mport {
	int port_mask;
	char *description;
};

struct mswitch {
	int port_count;
	int gpioint;
	struct mport *ports;
};

struct marvellswitch_platform_data {
	int switch_count;
	struct mswitch *switches;
};


#endif
